Known Errors:

Add/Edit Project Details
	Editing existing values creating unexpected errors for items that relate to the changed item (eg objective and activities, activity with inputs and outputs)
	Right click functionality not implemented
	
General Forms:
	Removed introduction dialogue, therefore no organization specific dialogues
	Export Database not working.
	Save database in new location not working
	
Reports
	Search Implementation not adequately tested
	More detailed reports removed.
	
	
Fixed

v0.7
Improve editing and saving capabilities for existing projects.
Improve find functionality across the application. Search now case insensitive.
