package test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.csp.models.Organization;
import org.junit.Test;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

public class DeletingModels {

	/**
	 * @uml.property  name="url"
	 */
	public String url = "";
	@Test
	public void deleteOrganization() {
		try {
			ConnectionSource  connectionSource = new JdbcConnectionSource(url);
			assertNotNull("deleted DAO for  Organization", DaoManager.createDao(connectionSource, Organization.class));
		} catch (Exception e) {
			fail("Exception thrown: "+ e.getMessage());
		}
	}

	@Test
	public void deleteActivity(){
		
	}
	
	@Test
	public void deleteBeneficiary(){
	}
	
	@Test
	public void deleteCohort(){
		
	}
	
	@Test
	public void deleteField(){
		
	}
	
	@Test
	public void deleteIndicator(){
		
	}
	
	@Test
	public void deleteMember(){
		
	}
	
	@Test
	public void deleteObjective(){
		
	}
	
	@Test
	public void deleteOutput(){
		
	}
	
	@Test
	public void deletePerson(){
		
	}
	
	@Test
	public void deleteProject(){
		
	}
	
	@Test
	public void deleteQuestion(){
		
	}
	
	@Test
	public void deleteReport(){
		
	}
	
	@Test
	public void deleteUser(){
		
	}
}
