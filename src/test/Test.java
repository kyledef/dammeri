package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.SocketException;
import java.sql.SQLException;

import javax.swing.JComboBox;
import javax.swing.JFrame;

import org.csp.controller.OrganizationController;
import org.csp.controller.SettingController;
import org.csp.models.Member;
import org.csp.models.Organization;
import org.csp.models.Person;
import org.metawidget.swing.SwingMetawidget;

public class Test {
	
	public static void testOptionList() throws SQLException{
		JFrame frame = new JFrame();
		
		Member m = new Member();
		Person p = new Person();
		Organization o = new Organization();
		o = OrganizationController.getInstance().get("Default Organization:Regional");
		m.setPerson(p);
		m.setOrganization(o);
		
		
		final SwingMetawidget mMetawidget = new SwingMetawidget();		
		mMetawidget.setConfig( "org/csp/views/metawidget.xml" );
		mMetawidget.setToInspect(m);

		@SuppressWarnings("unchecked")
		JComboBox <Organization>orgBox = (JComboBox<Organization>)mMetawidget.getComponent("organization");
		System.out.printf("%s has an id of %d\n", o, o.getId());
		for (int i = 0; i < orgBox.getItemCount(); i++)		
			if (orgBox.getItemAt(i) != null)			
				if (orgBox.getItemAt(i).equals(m.getOrganization()))
					orgBox.setSelectedIndex(i);	
		
		@SuppressWarnings("unchecked")
		final
		JComboBox <String> roleBox =  (JComboBox<String>)mMetawidget.getComponent("role");
		roleBox.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				if (roleBox.getSelectedItem().equals("OTHER")){
					roleBox.setEditable(true);
					roleBox.setSelectedIndex(0);
				}				
			}			
		});
		
		frame.add(mMetawidget);		
		frame.pack();
		frame.setVisible(true);
	}
	
	public static void testExport(){
		
	}
	
	public static void testNetwork() throws SocketException{
		String className = Test.class.toString();
//		double seconds = 232;
		SettingController sc = SettingController.getInstance();
		sc.loadSettings();
//		sc.sendToServer(String.format("open,%s,%f", className, seconds), SettingController.SVR_PERF_MSG);
		sc.sendToServer("Testing Error Message,"+className, SettingController.SVR_DBG_MSG);
	}
	
	public static void testPullNetwork(){
		SettingController sc = SettingController.getInstance();
//		ServerCommunication scom = new ServerCommunication();
//		scom.getServerConnection(SettingController.SVR_VER_MSG);
		
		System.out.println(sc.getLatestVersion());
		System.out.println(sc.isNeedUpdate());
	}
	
	public static void main(String [] args) throws Exception{
//		testOptionList();
//		testNetwork();
		testPullNetwork();
	}
}
