package test;

import java.util.Collection;
import java.util.Iterator;

import org.csp.controller.CohortController;
import org.csp.controller.InputController;
import org.csp.controller.MemberController;
import org.csp.controller.ObjectiveController;
import org.csp.controller.OrganizationController;
import org.csp.controller.PersonController;
import org.csp.controller.ProjectController;
import org.csp.controller.QuestionActivityController;
import org.csp.controller.QuestionOutcomeController;
import org.csp.controller.QuestionOutputController;
import org.csp.controller.SettingController;
import org.csp.controller.UserController;
import org.csp.models.Cohort;
import org.csp.models.Input;
import org.csp.models.Member;
import org.csp.models.Objective;
import org.csp.models.Organization;
import org.csp.models.Person;
import org.csp.models.Project;
import org.csp.models.Question;
import org.csp.models.QuestionActivity;
import org.csp.models.QuestionOutcome;
import org.csp.models.QuestionOutput;
import org.csp.models.User;
import org.csp.models.UserLog;
import org.csp.utilities.VersionManager;
import org.kyledef.version.Updater;

import com.j256.ormlite.support.ConnectionSource;

public class TestController {

	public static void testMemberController() throws Exception{
		MemberController mc = MemberController.getInstance();
//		Collection <Member> list = mc.getAll();
//		System.out.println(list.size());
//		Member [] members = mc.getMembers();
//		System.out.println(members.length);
//		Organization o = OrganizationController.getInstance().read(1);
//		System.out.println(o);
//		members = mc.getMembers(o);
//		System.out.println(members.length);
		
//		Collection<Member> members = mc.get(new Member(o));
//		System.out.println(members.size());

		
//		Member m = new Member();
		Person p = new Person();
//		p.setFirstName("def");
		p.setLastName("per");
		Collection<Person> pcs = PersonController.getInstance().find(p);
		System.out.println("Persons "+pcs.size());
		Iterator<Person> ip = pcs.iterator();
		while (ip.hasNext()){
			Person tp = ip.next();
			Collection<Member> members = mc.find(new Member(tp));
			System.out.println(members.size());
//			Iterator <Member> mP = members.iterator();
//			while (mP.hasNext())
//				System.out.println(mP.next());
		}
		
//		Person p = new Person();
//		p.setFirstName("Ton");
//		Collection<Member> members = mc.find(new Member(p));
//		Iterator <Member> mP = members.iterator();
//		while (mP.hasNext())
//			System.out.println(mP.next());
		
//		System.out.println("Saving Person");
//		
//		Person p = new Person();
//		p.setFirstName("Test");
//		p.setLastName("Delete");
//		
//		PersonController.getInstance(SettingController.getInstance().getConnection()).save(p);
//		
//		Member m = new Member();
//		m.setPerson(p);
//		m.setOrganization(mc.read(1).getOrganization());
//		m.setRole("Member");
		
//		mc.save(m);
	}
	
	public static void testOrganizationController() throws Exception{
		OrganizationController oc = OrganizationController.getInstance();
		
		Organization or = new Organization();
		or.setName("Default");
		
		Collection<Organization> list = oc.find(or);
		System.out.println(list.size());
		
	}
	
	public static void testObjectiveController() throws Exception{
		ObjectiveController oc = ObjectiveController.getInstance();
		ConnectionSource conn  = oc.getConnection();
		Objective o = oc.read(10);
		
		ProjectController pc = ProjectController.getInstance(conn);
		Project p = pc.read(8);
		conn.close();
		o.setProject(p);
//		oc.update(o);
		ObjectiveController.getInstance().update(o);
	}
	
	public static void testSettingController(){
		SettingController sc = SettingController.getInstance();
//		sc.createDatabase();
//		System.out.println(sc.isDatabaseExist());
		sc.determineEnvironment();
	}
	
	public static void testPersonController(){
		SettingController sc = SettingController.getInstance();
		
		PersonController pc = PersonController.getInstance(sc.getConnection());
		Person p = new Person();
		p.setFirstName("default");
		p.setLastName("person");
		System.out.println(pc.isPersonExist(p));
	}
	
	public static void testCohortController() throws Exception{
		CohortController cController = CohortController.getInstance(SettingController.getInstance().getConnection());
		Cohort c = new Cohort(new Project("Simple", null));
		Collection<Cohort> results = cController.find(c);
		System.out.printf("Found %d records \n", results.size());
	}
	
	public static void testInputController() throws Exception{
		InputController inController = InputController.getInstance(SettingController.getInstance().getConnection());
		Cohort c = CohortController.getInstance().read(4);
		if (c != null){
			System.out.println(c.getProject().getName());
			Collection<Input> ins = inController.get(c);
			System.out.println("Number of Inputs "+ins.size());
			
			//Testing creating input
			Input i = new Input();
			i.setInput("Programatic input creation test");
			i.setType("Organization");
			i.setCohort(c);
			inController.save(i);
			System.out.println("id "+i.getId());
			
			
		}
		
//		Input i = new Input();
//		i.setId(8);
//		inController.delete(i);
	}
	
	public static void testQuestionController() throws Exception{
		CohortController cc = CohortController.getInstance();
//		QuestionController qc = QuestionController.getInstance();
		
		Cohort c = cc.read(1);
//		if (c != null){
//			Question q = new Question();
//			q.setCohort(c);
//			q.setQuestion("Can we make bug free applications");
//			qc.save(q);
//			System.out.println(q.getId());
//		}
		
//		Collection<Question> allq = qc.getAll();
		Collection<Question> allq = c.getQuestions();
		System.out.println(allq.size());
		
		Iterator<Question> i = allq.iterator();
		while (i.hasNext()){
			Question q = i.next();
			System.out.println(q.getRelation());
			
			if (q.getRelation() != null){
				if (q.getRelation().equals(Question.ACTIVITY)){			
					QuestionActivityController qac = QuestionActivityController.getInstance(SettingController.getInstance().getConnection());
					QuestionActivity qa = new QuestionActivity();
					qa.setQuestion(q);
					
					Collection<QuestionActivity> qas = qac.find(qa);
					System.out.println(qas.size());
				}
				
				if (q.getRelation().equals(Question.OUTCOME)){
					QuestionOutcomeController qoutc = QuestionOutcomeController.getInstance(SettingController.getInstance().getConnection());
					QuestionOutcome qout = new QuestionOutcome();
					qout.setQuestion(q);
					Collection<QuestionOutcome> qouts = qoutc.find(qout);
					System.out.println(qouts.size());
				}
				
				if (q.getRelation().equals(Question.OUTPUT)){
					QuestionOutputController qoc = QuestionOutputController.getInstance(SettingController.getInstance().getConnection());
					QuestionOutput qo = new QuestionOutput();
					qo.setQuestion(q);
					Collection<QuestionOutput> qos = qoc.find(qo);
					System.out.println(qos.size());
				}
			}			
			
		}
		
	}
	
	public static void testVersionManager(){
		SettingController sc = SettingController.getInstance();
		VersionManager vm = new VersionManager(sc.getBaseURL(), sc.appVersion);
		System.out.println(vm.upgrade());
	}
	
	public static void testUserController(){
		try{
			UserController uc = UserController.getInstance();
			User u = uc.read(1);
//			uc.registerUserEvent(u, UserController.LOGIN, "Testing the login session");
			
			Collection<UserLog> lists = uc.listUserEvents(u);
			System.out.println(lists);
			
		}catch(Exception e){e.printStackTrace();}
	}
	
	public static void testUpdate(){
		SettingController sc = SettingController.getInstance();
		if (sc.isNeedUpdate()){
			Updater updater = new Updater(sc.getUpdateLocation(), "dammeri", new Double(sc.getLatestVersion()).toString());
			updater.retrieveUpdate();
			
			updater = new Updater();
			if (updater.isUpdateExist()){
				System.out.println("Detected Update");
			}
		}
	}
	
	public static void main(String [] args) throws Exception{
		testMemberController();
//		testOrganizationController();
//		testObjectiveController();
//		testSettingController();
//		testPersonController();
//		testCohortController();
//		testVersionManager();
//		testInputController();
//		testQuestionController();
//		testUserController();
//		testUpdate();
	}
}
