package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.csp.controller.MemberController;
import org.csp.controller.OrganizationController;
import org.csp.controller.PersonController;
import org.csp.controller.SettingController;
import org.csp.models.Member;
import org.csp.models.Organization;
import org.csp.models.Person;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class JunitClassTest {

	@Test
	public void testMemberCreate() throws Exception{
//		try{
			MemberController mc = MemberController.getInstance(SettingController.getInstance().getConnection());
			assertNotNull("Created Member Controller",mc);
			
			Person p = new Person();
			p.setFirstName("Tester");
			p.setLastName("Person");
			p.setAddress("Trinidad and Tobago");
			p.setSex("MALE");
			assertNotNull("Created Instance of Person", p);
			
			OrganizationController oc = OrganizationController.getInstance(SettingController.getInstance().getConnection());
			assertNotNull("Retrieved Instance of the Organization Controller", oc);
			
			Organization o = oc.getAll().iterator().next();
			assertNotNull("Retrieved organization",o);
			
			PersonController pc = PersonController.getInstance(SettingController.getInstance().getConnection());
			assertNotNull("Retrieved PersonController",pc);
			
			pc.save(p);
			assertTrue("saved person", p.getId() > 0);
			
			
			//Saving Member Model
			Member m = new Member();
			m.setPerson(p);
			m.setOrganization(o);
			m.setRole("MEMBER");
			
			mc.save(m);
			assertTrue("saved model", m.getId() > 0);
			
			//Finding Member Model
			Collection<Person>ps = pc.find(p);
			assertTrue("retrieved models based on query", ps.size() == 1);
			
			
			//Deleting Member Model
	//		assertTrue("model deleted",mc.delete(m));
			
			Collection<Member>ms = mc.find(m);
			System.out.println(ms.size());
	//		assertFalse("member successfully deleted", ms.size() > 0);
			
			assertTrue("model deleted",pc.delete(p));
			ps = pc.find(p);
			assertFalse("person successfully deleted", ps.size() > 0);
//		}catch(Exception e){
//			e.printStackTrace();
//		}
	}
	
}
