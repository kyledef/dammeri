package test;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.csp.controller.BeneficiaryController;
import org.csp.controller.CohortController;
import org.csp.controller.ObjectiveController;
import org.csp.controller.PersonController;
import org.csp.controller.SettingController;
import org.csp.models.Activity;
import org.csp.models.Beneficiary;
import org.csp.models.Cohort;
import org.csp.models.Input;
import org.csp.models.Member;
import org.csp.models.Model;
import org.csp.models.Objective;
import org.csp.models.Organization;
import org.csp.models.Output;
import org.csp.models.Person;
import org.csp.models.Project;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.io.ICsvMapWriter;
import org.supercsv.prefs.CsvPreference;

import com.j256.ormlite.dao.ForeignCollection;

public class TestingExportcsv {
	
	public static void main(String[] args) throws Exception {
//		exportCohort();
//		exportPerson();
//		importPerson();
//		exportPersonExcel();
		exportCohortExcel();
	}
	
	public static void exportPerson(){
		PersonController pc = PersonController.getInstance(SettingController.getInstance().getConnection());
		pc.exportToCSV("./", "persons.csv", pc.getAll());
	}
	
	public static void exportPersonExcel() throws Exception{
		PersonController pc = PersonController.getInstance(SettingController.getInstance().getConnection());
		Person p = new Person("","","","","","","","");
		Map <String, Object> fieldMap = pc.extractField(p);
		
		
		Set<String> keys = fieldMap.keySet();
		String [] keyArr = new String[keys.size()];
		keyArr = keys.toArray(keyArr);
		
		Collection<Person> models = pc.getAll();
		
		XSSFWorkbook wb = new XSSFWorkbook();
		Sheet sheet = wb.createSheet("Person");
		sheet.setDisplayGridlines(true);
		
		Row headerRow = sheet.createRow(0);
		Cell cell;
		for(int i = 0; i < keys.size(); i ++){
			cell = headerRow.createCell(i);
			cell.setCellValue(keyArr[i]);
		}
		
		Row currRow = null;
		int rowCount = 1;
		Iterator <Person> iP = models.iterator();
		while (iP.hasNext()){
			Person pTemp = iP.next();
			currRow = sheet.createRow(rowCount++);
			for(int i = 0; i < keys.size(); i ++){
				cell = currRow.createCell(i);				
				Field f = pTemp.getClass().getDeclaredField(keyArr[i]);
				f.setAccessible(true);
				cell.setCellValue(f.get(pTemp).toString());
			}
		}
		
		String file = "person.xlsx";
		FileOutputStream out = new FileOutputStream(file);
        wb.write(out);
        out.close();
	}



	public static XSSFWorkbook createDetails(XSSFWorkbook wb, Cohort c) throws Exception{
		Sheet sheet;
		Row row;
		Cell cell;
		int rowNum;

		String [] cohortFields= {"id","project","startDate","endDate","funder", "totalBudget", "teamLead"};
		String [] projectFields = {"name","description","location"};

		sheet = wb.createSheet("Details");
		rowNum = 0;
		for(int i = 0; i < cohortFields.length; i++, rowNum++){
			row = sheet.createRow(rowNum);
			String fStr = cohortFields[i];				
			if (fStr.equals("project")){
				Project p = c.getProject();
				
				for(int j = 0; j < projectFields.length; j++){
					cell = row.createCell(0);
					cell.setCellValue(projectFields[j].toUpperCase());
					
					cell = row.createCell(1);
					Field f = p.getClass().getDeclaredField(projectFields[j]);
					f.setAccessible(true);
					if (f.get(p) != null)
						cell.setCellValue(f.get(p).toString());
					else 
						cell.setCellValue("");
					
					rowNum++;
					row = sheet.createRow(rowNum);
				}
			}else if (fStr.equals("funder")){
				Organization funder = c.getFunder();
				if (funder != null){
					cell = row.createCell(0);
					cell.setCellValue("FUNDER");
					cell = row.createCell(1);
					cell.setCellValue(funder.getName());
				}
			}else if (fStr.equals("teamLeader")){
				Member teamL = c.getTeamLead();
				if (teamL != null){
				
					cell = row.createCell(0);
					cell.setCellValue("teamLeader".toUpperCase());
					cell = row.createCell(1);
					cell.setCellValue(teamL.toString());
				}
			}else{
				cell = row.createCell(0);
				cell.setCellValue(fStr.toUpperCase());
				
				cell = row.createCell(1);
				Field f = c.getClass().getDeclaredField(fStr);
				f.setAccessible(true);
				if (f.get(c) != null)
					cell.setCellValue(f.get(c).toString());
				else 
					cell.setCellValue("");
			}
		}
			
		return wb;
	}
	
	public static XSSFWorkbook createBeneficiaries(XSSFWorkbook wb, Cohort c) throws Exception{
		Sheet sheet = wb.createSheet("Beneficiaries");
		Row row;
		Cell cell;
		int rowNum;

		Iterator<Beneficiary> iBen =  c.getBeneficiaries().iterator();
		
		row = sheet.createRow(0);
		int col = 0;

		Collection<String> beHead = BeneficiaryController.getInstance().extractField(new Beneficiary("","","")).keySet();
		Iterator<String> ib = beHead.iterator();
		
		while (ib.hasNext()){
			row.createCell(col++).setCellValue(ib.next());
		}
		
		rowNum = 1;
		while (iBen.hasNext()){
			Beneficiary b = iBen.next();
			
			row = sheet.createRow(rowNum++);
			col = 0;
			ib = beHead.iterator();
			while (ib.hasNext()){
				String fStr = ib.next();
				cell = row.createCell(col++);
				Field f = b.getClass().getDeclaredField(fStr);
				f.setAccessible(true);
				if (f.get(b) != null)
					cell.setCellValue(f.get(b).toString());
				else 
					cell.setCellValue("");
			}
		}

		return wb;
	}

	public static XSSFWorkbook createObjectives(XSSFWorkbook wb, Cohort c) throws Exception{
		Sheet sheet;
		Row row;
		Cell cell;
		int rowNum;

		sheet = wb.createSheet("Objectives");
			
		Iterator<Objective> iObj = c.getProject().getObjectives().iterator();
		row = sheet.createRow(0);
		int col = 0;

		Collection<String> ohead = ObjectiveController.getInstance().extractField(new Objective("")).keySet();
		Iterator<String> io = ohead.iterator();
		while (io.hasNext()){
			row.createCell(col++).setCellValue(io.next());
		}
		rowNum = 1;
		while(iObj.hasNext()){
			Objective o = iObj.next();
			row = sheet.createRow(rowNum++);
			col = 0;
			io = ohead.iterator();
			while (io.hasNext()){
				String fStr = io.next();
				cell = row.createCell(col++);
				Field f = o.getClass().getDeclaredField(fStr);
				f.setAccessible(true);
				if (f.get(o) != null)
					cell.setCellValue(f.get(o).toString());
				else 
					cell.setCellValue("");
			}
		}
		return wb;
	}

	public static XSSFWorkbook createActivities(XSSFWorkbook wb, Cohort c) throws Exception{
		Sheet sheet;
		Row row;
		int rowNum;

		sheet = wb.createSheet("Activities");

		row = sheet.createRow(0);
		row.createCell(0).setCellValue("ACTIVITY");
		row.createCell(1).setCellValue("INPUT");
		row.createCell(2).setCellValue("OUTPUT");

		rowNum = 1;
		int lastActRow = 1;
		int maxActRow = 1;
//		Iterator<String> aHead = ActivityController.getInstance().extractField(new Activity("","")).keySet().iterator();
		
		Iterator <Activity> iAct = c.getActivities().iterator();
		

		while (iAct.hasNext()){
			row = sheet.createRow(rowNum);
			lastActRow = rowNum;

			Activity a = iAct.next();
			row.createCell(0).setCellValue(a.getActivity());

			Iterator<Input> inputs = a.getInputs().iterator();
			while (inputs.hasNext()){
				Input input = inputs.next();
				row.createCell(1).setCellValue(input.getInput());
				row = sheet.createRow(++rowNum);
				if (rowNum > maxActRow)maxActRow = rowNum;
			}

			Iterator<Output> outputs = a.getOutputs().iterator();
			rowNum = lastActRow;
			row = sheet.createRow(rowNum);
			while (outputs.hasNext()){
				Output output = outputs.next();
				row.createCell(2).setCellValue(output.getOutput());
				row = sheet.createRow(++rowNum);
				if (rowNum > maxActRow)maxActRow = rowNum;
			}

			rowNum = maxActRow;
		}

		return wb;
	}

	public static XSSFWorkbook createOutcomes(XSSFWorkbook wb, Cohort c){
		
		
		return wb;
	}

	public static void exportCohortExcel() throws Exception{
		XSSFWorkbook wb = new XSSFWorkbook();
		
		CohortController cc = CohortController.getInstance(SettingController.getInstance().getConnection());
		Collection<Cohort> cols = cc.getAll();
		Iterator<Cohort> iCol = cols.iterator();
		while (iCol.hasNext()){
			Cohort c = iCol.next();
			wb = createDetails(wb, c);
			wb = createBeneficiaries(wb,c);
			wb = createObjectives(wb,c);
			wb = createActivities(wb,c);
			wb = createOutcomes(wb,c);
			
			String fileName = "cohorts-"+c.getId()+".xlsx";
			FileOutputStream out = new FileOutputStream(fileName);
			wb.write(out);
			out.close();
		}
	}
	
	public static void importPerson(){
		PersonController pc = PersonController.getInstance(SettingController.getInstance().getConnection());
		
		Person p = new Person();
		p.setAddress("");
		p.setAge("");
		p.setCity("");
		p.setEmail("");
		p.setFirstName("");
		p.setLastName("");
		p.setPhone("");
		p.setSex("");
		
		pc.importToCSV("./", "persons.csv", p);
	}
	
	public static void exportCohort(){
		CohortController cc = CohortController.getInstance(SettingController.getInstance().getConnection());
		Collection<Cohort> cols = cc.getAll();
		
		cc.exportToCSV("./", "cohorts.csv", cols);		
	}

	public static void exportCohortLong() throws IOException, IllegalArgumentException, IllegalAccessException{
		Cohort model;
		CohortController cc = CohortController.getInstance(SettingController.getInstance().getConnection());
		model = cc.getAll().iterator().next();
		Map<String, Object> cohortMap = new HashMap<String, Object>();
		
		
		Field [] aClassFields = model.getClass().getDeclaredFields();
		for(Field f : aClassFields){
			f.setAccessible(true);
			Object fObj = f.get(model);
			if (fObj != null && (fObj instanceof ForeignCollection) == false && !f.getName().equalsIgnoreCase("serialVersionUID")){				
				if (fObj instanceof Model){
					if (fObj instanceof Project){
						Project p = (Project)fObj;
						cohortMap.put("name", p.getName());
						cohortMap.put("description", p.getDescription());
					}else
						cohortMap.put(f.getName(), ((Model)fObj).getId());
				}else{
					cohortMap.put(f.getName(), fObj);
				}
			}else if (fObj instanceof ForeignCollection){
				
			}
		}
		if (cohortMap.size() > 0){
			Set<String> set = cohortMap.keySet();
			String [] keys = new String[set.size()];
			keys = set.toArray(keys);
			ICsvMapWriter mapWriter = null;
			try {
                mapWriter = new CsvMapWriter(new FileWriter("./writeWithCsvMapWriter.csv"),CsvPreference.STANDARD_PREFERENCE);
                mapWriter.writeHeader(keys);
                mapWriter.write(cohortMap, keys);
	        }
	        finally {
                if( mapWriter != null ) 
                        mapWriter.close();
	        }
		}
	}
}
