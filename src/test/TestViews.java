package test;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.sql.SQLException;
import java.util.Collection;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.csp.controller.ActivityController;
import org.csp.controller.CohortController;
import org.csp.controller.IndicatorController;
import org.csp.controller.ObjectiveController;
import org.csp.controller.SettingController;
import org.csp.controller.UserController;
import org.csp.models.Activity;
import org.csp.models.ActivityObjective;
import org.csp.models.Cohort;
import org.csp.models.Indicator;
import org.csp.models.Member;
import org.csp.models.Objective;
import org.csp.models.Organization;
import org.csp.models.Person;
import org.csp.models.User;
import org.csp.views.ActivityObjectiveView;
import org.csp.views.CohortAddView;
import org.csp.views.CohortManageView;
import org.csp.views.CohortTabbedView;
import org.csp.views.CohortView;
import org.csp.views.FieldManageView;
import org.csp.views.HomePanel;
import org.csp.views.IndicatorView;
import org.csp.views.InputView;
import org.csp.views.MemberManageView;
import org.csp.views.MemberView;
import org.csp.views.OrganizationManageView;
import org.csp.views.OrganizationView;
import org.csp.views.OutputView;
import org.csp.views.PersonView;
import org.csp.views.ProjectManageView;
import org.csp.views.ReportFunderViewBy;
import org.csp.views.ReportMemberViewBy;
import org.csp.views.ReportProjectViewByField;
import org.csp.views.ReportsManageView;
import org.csp.views.SettingManageView;
import org.csp.views.UserLoginView;
import org.csp.views.UserManagerView;
import org.csp.views.UserView;

public class TestViews {
	
	private static boolean readOnly = false;

	public static void testHome(JFrame frame) throws SQLException{
		UserController uc = UserController.getInstance(SettingController.getInstance().getConnection());
		User u = uc.read(1);
		uc.setLoggedInUser(u);
		new HomePanel(frame,  true, uc.isReadOnly());
	}
	
	public static void testUser(JFrame frame){
		new UserView(frame);
	}
	
	public static void testPerson(JFrame frame){
		new PersonView(frame);
	}
	public static void testMember(JFrame frame){
		new MemberView(frame);
	}
	public static void testOrganization(JFrame frame){
		new OrganizationView(frame);
	}
	public static void testCohort(JFrame frame){
		new CohortView(frame);
	}
	
	public static void testLogin(JFrame frame){
		new UserLoginView(frame,true, false);
	}
	
	public static void testProjectPanel(JFrame frame){
		new ProjectManageView(frame);
	}
	
	public static void testUserManager(JFrame frame){
		new UserManagerView(frame, true,readOnly);
	}
	
	public static void testMemberManager(JFrame frame){
		new MemberManageView(frame, true, readOnly);
	}
	
	public static void testMemberManagerWithMember(JFrame frame){
		MemberManageView mv = new MemberManageView(frame, true, readOnly);
		Member m = new Member();
		m.setOrganization(new Organization());
		m.setPerson(new Person());
		mv.setModel(m);
		
		//Remove the table from the default MemberManageView 
		BorderLayout layout = (BorderLayout) mv.getPanelRight().getLayout();
		JPanel p = (JPanel)layout.getLayoutComponent(BorderLayout.NORTH);
		layout = (BorderLayout)p.getLayout();
		p.remove(layout.getLayoutComponent(BorderLayout.SOUTH));
	}
	
	public static void testIndicatorManager(JFrame frame){
		//TODO Write code to test indicators
	}
	
	public static void testOrganizationManager(JFrame frame){
		new OrganizationManageView(frame, true, readOnly);
	}
	
	public static void testSettingManager(JFrame frame){
		SettingController sc = SettingController.getInstance();
		System.out.println(sc.getUpdateLocation()+sc.getLatestVersion());
		new SettingManageView(frame);
	}
	
	public static void testCohortManager(JFrame frame){
		new CohortManageView(frame, true, readOnly);
	}
	
	public static void testAddCohort(JFrame frame){
		new CohortAddView(frame);
	}
	public static void testReportView(JFrame frame){
		new ReportProjectViewByField(frame, ReportProjectViewByField.LOCATION_VIEW);
	}
	public static void testReportManager(JFrame frame){
		new ReportsManageView(frame);
	}
	public static void testReportFunderView(JFrame frame){
		new ReportFunderViewBy(frame);
	}
	public static void testReportMemberView(JFrame frame){
		new ReportMemberViewBy(frame, ReportMemberViewBy.PROJECT_VIEW);
	}
	
	public static void testIndicatorView(JFrame frame){
		new IndicatorView(frame, true, readOnly, null, true);
	}
	
	public static void testCohortViewWithInstance(JFrame frame){
		CohortController cc = CohortController.getInstance();
		Collection<Cohort> list = cc.getAll();
		System.out.println(list.size());
		if (list.size() > 0){
			Cohort c = list.iterator().next();
			new CohortAddView(frame, c, false);
		}
	}
	
	public static void testCohortTabbedView(JFrame frame){
		new CohortTabbedView(frame, false, readOnly);
		frame.setPreferredSize(new Dimension(400,600));
	}
	public static void testCohortTabbedViewWithInstance(JFrame frame) throws SQLException{
		CohortController cc = CohortController.getInstance();
		Cohort c = cc.read(1);
		new CohortTabbedView(frame, false, readOnly,c);
		frame.setPreferredSize(new Dimension(400,600));
	}
	
	public static void testInputView(JFrame frame){
		new InputView(frame, true, readOnly, null, true);
	}
	public static void testOutputView(JFrame frame){
		new OutputView(frame, true, readOnly, null, true);
	}
	
	public static void testFieldView(JFrame frame){
		new FieldManageView(frame, true, readOnly, null, true);
	}
	
	public static void testFieldViewWithIndicator(JFrame frame){
		Indicator i = IndicatorController.getInstance().getAll().iterator().next();
		FieldManageView fmv = new FieldManageView(frame, false, readOnly, null, true,true);
		System.out.println(i);
		fmv.setIndicator(i);
	}
	
	public static void testActivityObjectiveView(JFrame frame){
		ActivityObjective ao = new ActivityObjective();		
		Collection<Activity> acts = ActivityController.getInstance().getAll();
		Collection<Objective> objs = ObjectiveController.getInstance().getAll();
		if (acts.size() > 0 && objs.size() > 0){
			ao.setActivity(acts.iterator().next());
			Objective [] obs = new Objective[objs.size()];
			ao.setObjectives(objs.toArray(obs));
		}
		new ActivityObjectiveView(frame, false, readOnly, ao, true, true);
	}
	
	public static void main(String[] args) throws SQLException {
		long startTime = System.nanoTime();			
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		testHome(frame);
//		testUser(frame);
//		testPerson(frame);
//		testMember(frame);
//		testOrganization(frame);
//		testCohort(frame);
//		testLogin(frame);
//		testProjectPanel(frame);
//		testUserManager(frame);
//		testMemberManager(frame);
//		testMemberManagerWithMember(frame);
//		testOrganizationManager(frame);
//		testSettingManager(frame);
		testCohortManager(frame); 
//		testCohortFindView(frame);
//		testAddCohort(frame); 
//		testReportView(frame);
//		testReportManager(frame); //TODO NOT COMPLETED
//		testReportFunderView(frame); //TODO NOT COMPLETED
//		testReportMemberView(frame); //TODO NOT COMPLETED
//		testOutputView(frame);
//		testCohortViewWithInstance(frame);//Deprecated
//		testCohortTabbedView(frame); //TODO NOT COMPLETED 
//		testCohortTabbedViewWithInstance(frame);//TODO Current task: Improve the bugs relating to editing existing values
//		testIndicatorView(frame);
//		testInputView(frame);
//		testOutputView(frame);
//		testFieldView(frame);
//		testFieldViewWithIndicator(frame);
//		testActivityObjectiveView(frame);
		frame.pack();
		frame.setVisible(true);	
		double seconds = (double)((System.nanoTime() - startTime) / 1000000000.0);
		System.out.println("Elapsed time: "+seconds);
	}

}
