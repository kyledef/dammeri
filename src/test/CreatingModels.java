package test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.Collection;

import org.csp.controller.MemberController;
import org.csp.controller.OrganizationController;
import org.csp.controller.PersonController;
import org.csp.models.Activity;
import org.csp.models.Beneficiary;
import org.csp.models.Cohort;
import org.csp.models.Field;
import org.csp.models.Indicator;
import org.csp.models.Member;
import org.csp.models.Objective;
import org.csp.models.Organization;
import org.csp.models.Output;
import org.csp.models.Person;
import org.csp.models.Question;
import org.csp.models.Report;
//import org.junit.FixMethodOrder;
import org.junit.Test;
//import org.junit.runners.MethodSorters;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CreatingModels {

	public String url = "jdbc:sqlite:./data/test.db";	
	
//	@Test
//	public void test1_createUser(){
//		try {
//			String table = "user";
//			ConnectionSource  connectionSource = new JdbcConnectionSource(url);
//			Dao <User, String> d = DaoManager.createDao(connectionSource, User.class);
//			assertNotNull("Created DAO for  " + table, d );
//			User u = new User("testuser", AccessType.ADMIN, "defaultpassword") ;
//			Dao.CreateOrUpdateStatus  c = d.createOrUpdate(u);
//			assertEquals("Saved "+table+" to database", 1,c.getNumLinesChanged());
//		} catch (Exception e) {fail("Exception thrown: "+ e.getMessage());}
//	}
//	
//	@Test 
//	public void test2_createOrganization() {
//		try {
//			ConnectionSource  connectionSource = new JdbcConnectionSource(url);
//			Dao <Organization, String> d = DaoManager.createDao(connectionSource, Organization.class);
//			assertNotNull("Created DAO for  Organization", d );
//			Organization o = new Organization("TESTORG", "ADDRESS", "TT");
//			Dao.CreateOrUpdateStatus  c = d.createOrUpdate(o);
//			assertEquals("Saved organization to database", 1, c.getNumLinesChanged());
//		} catch (Exception e) {
//			fail("Exception thrown: "+ e.getMessage());
//		}
//	}
//	
//	@Test
//	public void test3_createPerson(){
//		try {
//			ConnectionSource  connectionSource = new JdbcConnectionSource(url);
//			Dao <Person, String> d = DaoManager.createDao(connectionSource, Person.class);
//			assertNotNull("Created DAO for  Organization", d );
//			Person p = new Person("test", "user", Sex.MALE, "30", "none", "nowhere");
//			Dao.CreateOrUpdateStatus  c = d.createOrUpdate(p);
//			assertEquals("Saved organization to database", 1, c.getNumLinesChanged());
//		} catch (Exception e) {
//			fail("Exception thrown: "+ e.getMessage());
//		}
//	}
//	
//	@Test
//	public void test4_createProject(){
//		try {
//			ConnectionSource  connectionSource = new JdbcConnectionSource(url);
//			Dao <Project, String> d = DaoManager.createDao(connectionSource, Project.class);
//			assertNotNull("Created DAO for  project", d );
//			Project p = new Project("test project", "used to test the implementation of ormlite");
//			Dao.CreateOrUpdateStatus  c = d.createOrUpdate(p);
//			assertEquals("Saved organization to database", 1, c.getNumLinesChanged());
//			
//		} catch (Exception e) {
//			fail("Exception thrown: "+ e.getMessage());
//		}
//	}
	
	@Test
	public void test5_createMember(){
		try {
			MemberController mc = MemberController.getInstance();
			Member model = new Member();
			
			Organization o = null;
			OrganizationController oc = OrganizationController.getInstance();
			Collection<Organization> orgs = oc.getAll();
			if (orgs.size() > 0){
				o = orgs.iterator().next();
			}
			
			Person p = null;
			PersonController pc = PersonController.getInstance();
			Collection<Person> ps = pc.getAll();			
			if (ps.size() > 0){
				p = ps.iterator().next();
			}
			
			System.out.println(p.getFirstName());
			System.out.println(o.getName());
			
			model.setOrganization(o);
			model.setPerson(p);
			model.setRole("ADMIN");
			
			Member result = mc.save(model);
			
			assertNotNull("Saved Member to database", result);			
		} catch (Exception e) {
			fail("Exception thrown: "+ e.getMessage());
		}
	}
	
//	@Test
	public void test6_createCohort(){
		try {
			String table = "cohort";
			ConnectionSource  connectionSource = new JdbcConnectionSource(url);
			Dao <Cohort, String> d = DaoManager.createDao(connectionSource, Cohort.class);
			assertNotNull("Created DAO for  " + table, d );
			
//			Cohort c = new Cohort(this.pj, "today", "tomorrow",100.00,this.m, 0);
			
//			assertEquals("Saved "+table+" to database", 1, d.create(c));

		} catch (Exception e) {
			fail("Exception thrown: "+ e.getMessage());
		}		
	}
		
//	@Test
	public void createBeneficiary(){
		try {
			String table = "beneficiary";
			ConnectionSource  connectionSource = new JdbcConnectionSource(url);
			Dao <Beneficiary, String> d = DaoManager.createDao(connectionSource, Beneficiary.class);
			assertNotNull("Created DAO for  " + table, d );
//			Beneficiary a = new Beneficiary("students",this.c, this.p, "Unknown affiliation of testing");
		
//			assertEquals("Saved "+table+" to database", 1, d.create(a));

		} catch (Exception e) {
			fail("Exception thrown: "+ e.getMessage());
		}
	}	
	
//	@Test
	public void createObjective(){
		try {
			String table = "objectives";
			ConnectionSource  connectionSource = new JdbcConnectionSource(url);
			Dao <Objective, String> d = DaoManager.createDao(connectionSource, Objective.class);
			assertNotNull("Created DAO for  " + table, d );
//			Objective obj = new Objective(this.pj, "To create a suitable objective", "expounds the works of the objective") ;
		
//			assertEquals("Saved "+table+" to database", 1, d.create(obj));

		} catch (Exception e) {
			fail("Exception thrown: "+ e.getMessage());
		}
	}
	
//	@Test
	public void createActivity(){
		try {
			String table = "activity";
			ConnectionSource  connectionSource = new JdbcConnectionSource(url);
			Dao <Activity, String> d = DaoManager.createDao(connectionSource, Activity.class);
			assertNotNull("Created DAO for  " + table, d );
//			Activity a = new Activity(this.c, "Starting the tests", "A simple activity for testing") ;
		
//			assertEquals("Saved "+table+" to database", 1, d.create(a));
//			this.a = a;
		} catch (Exception e) {
			fail("Exception thrown: "+ e.getMessage());
		}
	}

//	@Test
	public void createQuestion(){
		try {
			String table = "questions";
			ConnectionSource  connectionSource = new JdbcConnectionSource(url);
			Dao <Question, String> d = DaoManager.createDao(connectionSource, Question.class);
			assertNotNull("Created DAO for  " + table, d );
//			Question q = new Question( "does this test works", this.c, QType.QUALITATIVE);
		
//			assertEquals("Saved "+table+" to database", 1, d.create(q));
//			this.q = q;
		} catch (Exception e) {
			fail("Exception thrown: "+ e.getMessage());
		}
	}
	
//	@Test
	public void createIndicator(){
		try {
			String table = "indicator";
			ConnectionSource  connectionSource = new JdbcConnectionSource(url);
			Dao <Indicator, String> d = DaoManager.createDao(connectionSource, Indicator.class);
			assertNotNull("Created DAO for  " + table, d );
//			Indicator i = new Indicator(this.q, 0, "thinking about the future", this.m, FREQ.DAILY, 0, "unknown") ;
		
//			assertEquals("Saved "+table+" to database", 1, d.create(i));
//			this.i = i;
		} catch (Exception e) {
			fail("Exception thrown: "+ e.getMessage());
		}
	}
	
//	@Test
	public void createField(){
		try {
			String table = "field";
			ConnectionSource  connectionSource = new JdbcConnectionSource(url);
			Dao <Field, String> d = DaoManager.createDao(connectionSource, Field.class);
			assertNotNull("Created DAO for  " + table, d );
//			Field f = new Field(this.i, "going", "today") ;
		
//			assertEquals("Saved "+table+" to database", 1, d.create(f));
//			this.f = f;
		} catch (Exception e) {
			fail("Exception thrown: "+ e.getMessage());
		}
	}

//	@Test
	public void createOutput(){
		try {
			String table = "output";
			ConnectionSource  connectionSource = new JdbcConnectionSource(url);
			Dao <Output, String> d = DaoManager.createDao(connectionSource, Output.class);
			assertNotNull("Created DAO for  " + table, d );
//			Output o = new Output(this.a, "great", "work more complex relations")  ;
		
//			assertEquals("Saved "+table+" to database", 1, d.create(o));
//			this.out = o;
		} catch (Exception e) {
			fail("Exception thrown: "+ e.getMessage());
		}
	}	
	
//	@Test
	public void createReport(){
		try {
			String table = "report";
			ConnectionSource  connectionSource = new JdbcConnectionSource(url);
			Dao <Report, String> d = DaoManager.createDao(connectionSource, Report.class);
			assertNotNull("Created DAO for  " + table, d );
//			Report o = new Report(this.out, "report location",0 ,"today", this.p)  ;
		
//			assertEquals("Saved "+table+" to database", 1, d.create(o));
//			this.r = o;
		} catch (Exception e) {
			fail("Exception thrown: "+ e.getMessage());
		}
	}	

	
}
