
CREATE TABLE User (
	id INTEGER NOT NULL, 
	userName TEXT UNIQUE, 
	password TEXT,  
	accessLevel TEXT DEFAULT 'VIEWER',
	UNIQUE(userName),
	PRIMARY KEY (id)
);
INSERT INTO "User" VALUES(1,'admin','W6ph5Mm5Pz8GgiULbPgzG37mj9g=','ADMIN');

CREATE TABLE Organization
(
	id INTEGER NOT NULL, 
	name TEXT  NOT NULL,	
	type TEXT DEFAULT 'Local',
	address TEXT,
	UNIQUE(name, type),
	PRIMARY KEY (id)
);
INSERT INTO "Organization" VALUES(1,'Default Organization','Local','Trinidad and Tobago');

CREATE TABLE Person (
	id INTEGER NOT NULL, 
	firstName TEXT  NOT NULL, 
	lastName TEXT  NOT NULL, 
	sex TEXT, 
	age TEXT, 
	city TEXT, 
	address TEXT,
	phone TEXT,
	email TEXT,
	UNIQUE(firstName, lastName),
	PRIMARY KEY (id)
);
INSERT INTO "Person" VALUES(1,'DEFAULT','PERSON','MALE','25 - 30','Port Of Spain','Trinidad and Tobago', '345-6789','none@none.com');

CREATE TABLE Project
(
	id INTEGER NOT NULL, 
	name TEXT NOT NULL,
	description TEXT,
	location TEXT,
	PRIMARY KEY (id),
	UNIQUE(name, description)
);

CREATE TABLE Members
(
	id INTEGER NOT NULL, 
	organization INTEGER NOT NULL,
	person INTEGER NOT NULL,
	role TEXT,
	UNIQUE(person, organization),
	PRIMARY KEY (id),
	FOREIGN KEY (organization) REFERENCES Organization(id) ON DELETE CASCADE ON UPDATE CASCADE  ,	
	FOREIGN KEY (person) REFERENCES Person(id) ON DELETE CASCADE ON UPDATE CASCADE  
);

INSERT INTO "Members" VALUES(1,1,1,"MEMBER");

CREATE TABLE Cohort
(
	id INTEGER NOT NULL, 
	project INTEGER NOT NULL,
	startDate TEXT,
	endDate TEXT,
	totalBudget INTEGER,
	teamLead INTEGER,
	funder INTEGER,
	PRIMARY KEY (id),
	UNIQUE(project, startDate, endDate),
	FOREIGN KEY (teamLead)REFERENCES Members(id) ON DELETE CASCADE ON UPDATE CASCADE  ,
	FOREIGN KEY (funder)REFERENCES Organization(id) ON DELETE CASCADE ON UPDATE CASCADE   ,
	FOREIGN KEY (project)REFERENCES Project(id)  ON DELETE CASCADE ON UPDATE CASCADE  
);

CREATE TABLE Beneficiary
(
	id INTEGER NOT NULL, 
	name TEXT,
	sex TEXT,
	age TEXT,
	type TEXT,
	cohort INTEGER,
	PRIMARY KEY (id) ,
	FOREIGN KEY (cohort) REFERENCES Cohort(id) ON DELETE CASCADE ON UPDATE CASCADE 
);

CREATE TABLE IndividualBeneficiary
(
	id INTEGER NOT NULL, 
	name TEXT,
	cohort INTEGER,
	person INTEGER,
	Affiliation TEXT,
	UNIQUE(name, cohort),
	PRIMARY KEY (id) ,
	FOREIGN KEY (cohort) REFERENCES Cohort(id) ON DELETE CASCADE ON UPDATE CASCADE  ,
	FOREIGN KEY (person) REFERENCES Person(id) ON DELETE CASCADE ON UPDATE CASCADE  
);

CREATE TABLE Objectives (
    id INTEGER NOT NULL, 
    project INTEGER NOT NULL,
    title TEXT NOT NULL,
	number INTEGER,
	UNIQUE (project, title),
	PRIMARY KEY (id),
	FOREIGN KEY (project) REFERENCES Project(id) ON DELETE CASCADE ON UPDATE CASCADE 
);

CREATE TABLE Activity
(
	id INTEGER NOT NULL, 
	cohort INTEGER NOT NULL,
	title TEXT NOT NULL,
	description TEXT,
	objective INTEGER,
	UNIQUE (cohort,title, objective),
	PRIMARY KEY (id),
	FOREIGN KEY (objective) REFERENCES Objectives(id) ON DELETE CASCADE ON UPDATE CASCADE ,
	FOREIGN KEY (cohort) REFERENCES Cohort(id) ON DELETE CASCADE ON UPDATE CASCADE  
);

CREATE TABLE ActivityObjective(
	id INTEGER NOT NULL, 
	objective INTEGER NOT NULL,
	activity INTEGER NOT NULL,

	UNIQUE (objective, activity),
	PRIMARY KEY (id),
	FOREIGN KEY (objective) REFERENCES Objectives(id) ON DELETE CASCADE ON UPDATE CASCADE ,
	FOREIGN KEY (activity) REFERENCES Activity(id) ON DELETE CASCADE ON UPDATE CASCADE  
);

CREATE TABLE Outcome (
    id INTEGER NOT NULL, 
    cohort INTEGER NOT NULL,
    term TEXT,
    type TEXT,
    description TEXT NOT NULL,
	UNIQUE(cohort, description),
	PRIMARY KEY (id),
	FOREIGN KEY (cohort) REFERENCES Cohort(id) ON DELETE CASCADE ON UPDATE CASCADE 
);

CREATE TABLE Questions
(
	id INTEGER NOT NULL, 
	question TEXT NOT NULL,
	cohort INTEGER NOT NULL,
	type TEXT,
	relation TEXT,
	PRIMARY KEY (id),
	FOREIGN KEY (cohort) REFERENCES Cohort(id) ON DELETE CASCADE ON UPDATE CASCADE 
);

CREATE TABLE OutcomeQuestions
(
	id INTEGER NOT NULL,
	question INTEGER NOT NULL,
	outcome INTEGER NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (question) REFERENCES Questions(id) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (outcome) REFERENCES Outcome(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE ActivityQuestions
(
	id INTEGER NOT NULL,
	question INTEGER NOT NULL,
	activity INTEGER NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (question) REFERENCES Questions(id) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (activity) REFERENCES Activity(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE OutputQuestions
(
	id INTEGER NOT NULL,
	question INTEGER NOT NULL,
	output INTEGER NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (question) REFERENCES Questions(id) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (output) REFERENCES Output(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE Indicator (
    id INTEGER NOT NULL, 
    question INTEGER NOT NULL,
	indicator TEXT NOT NULL,
    type TEXT DEFAULT 'QUANTITATIVE',
    frequency TEXT DEFAULT 'MONTHLY', 
    source TEXT ,
	target TEXT,
	UNIQUE(question, indicator, type),
	PRIMARY KEY (id),
	FOREIGN KEY (question) REFERENCES Question(id) ON DELETE CASCADE ON UPDATE CASCADE 
);


CREATE TABLE Field
(
	id INTEGER NOT NULL, 
	indicator INTEGER NOT NULL,
	value TEXT,
	captureDate INTEGER,
	PRIMARY KEY (id),
	FOREIGN KEY (indicator) REFERENCES Indicator(id) ON DELETE CASCADE ON UPDATE CASCADE  
);

CREATE TABLE Input
(
	id INTEGER NOT NULL,
	activity INTEGER,
	type TEXT,
	input TEXT,
	cost INTEGER,
	cohort INTEGER,
	UNIQUE(input, activity, cohort),
	PRIMARY KEY (id),
	FOREIGN KEY (cohort) REFERENCES Cohort(id) ON DELETE CASCADE ON UPDATE CASCADE ,
	FOREIGN KEY (activity) REFERENCES Activity(id) ON DELETE CASCADE ON UPDATE CASCADE 
);


CREATE TABLE FollowUpTasks
(
	id INTEGER NOT NULL, 
	output INTEGER,
	date TEXT,
	responsiblePerson INTEGER,
	PRIMARY KEY (id),
	FOREIGN KEY (output) REFERENCES OutputOrganization(id) ON DELETE CASCADE ON UPDATE CASCADE  ,
	FOREIGN KEY (responsiblePerson)	REFERENCES Members(id) ON DELETE CASCADE ON UPDATE CASCADE  
);
CREATE TABLE InputBeneficiary
(
	id INTEGER NOT NULL, 
	activity INTEGER,
	person INTEGER,
	recruitmentStrategy TEXT,
	PRIMARY KEY (id),
	FOREIGN KEY (activity) REFERENCES Activity(id) ON DELETE CASCADE ON UPDATE CASCADE  ,
	FOREIGN KEY (activity) REFERENCES Activity(id) ON DELETE CASCADE ON UPDATE CASCADE  ,
	FOREIGN KEY (person) REFERENCES Person(id) ON DELETE CASCADE ON UPDATE CASCADE  
);
CREATE TABLE InputExternal
(
	id INTEGER NOT NULL, 
	description TEXT,
	activity INTEGER,
	funder INTEGER,
	duration INTEGER,
	contact INTEGER,
	PRIMARY KEY (id),
	FOREIGN KEY (activity)REFERENCES Activity(id) ON DELETE CASCADE ON UPDATE CASCADE  ,
	FOREIGN KEY (activity) REFERENCES Activity(id) ON DELETE CASCADE ON UPDATE CASCADE  ,
	FOREIGN KEY (funder) REFERENCES Organization(id) ON DELETE CASCADE ON UPDATE CASCADE  ,
	FOREIGN KEY (activity) REFERENCES Activity(id) ON DELETE CASCADE ON UPDATE CASCADE  ,
	FOREIGN KEY (funder) REFERENCES Organization(id) ON DELETE CASCADE ON UPDATE CASCADE  ,
	FOREIGN KEY (contact) REFERENCES Person(id) ON DELETE CASCADE ON UPDATE CASCADE  
		
);
CREATE TABLE InputOrganization
(
	id INTEGER NOT NULL, 
	activity INTEGER,
	resources TEXT,
	cost REAL,
	PRIMARY KEY (id),
	FOREIGN KEY (activity) REFERENCES Activity(id) ON DELETE CASCADE ON UPDATE CASCADE  
);
CREATE TABLE InputStaff
(
	id INTEGER NOT NULL, 
	activity INTEGER,
	person INTEGER,
	projectRole TEXT,
	salary REAL,
	PRIMARY KEY (id),
	FOREIGN KEY (activity) REFERENCES Activity(id) ON DELETE CASCADE ON UPDATE CASCADE  ,
	FOREIGN KEY (activity) REFERENCES Activity(id) ON DELETE CASCADE ON UPDATE CASCADE  ,
	FOREIGN KEY (person) REFERENCES Person(id) ON DELETE CASCADE ON UPDATE CASCADE  
);


CREATE TABLE OutputBeneficiary
(
	id INTEGER NOT NULL, 
	activity INTEGER,
	taskCompletion INTEGER,
	followUp TEXT,
	PRIMARY KEY (id),
	FOREIGN KEY (activity) REFERENCES Activity(id) ON DELETE CASCADE ON UPDATE CASCADE  
);
CREATE TABLE OutputExternal
(
	id INTEGER NOT NULL, 
	activity INTEGER,
	description TEXT,
	feedback TEXT,
	followUp TEXT,
	type TEXT,
	PRIMARY KEY (id),
	FOREIGN KEY (activity) REFERENCES Activity(id) ON DELETE CASCADE ON UPDATE CASCADE  
);
CREATE TABLE OutputOrganization
(
	id INTEGER NOT NULL, 
	activity INTEGER,
	PRIMARY KEY (id),
	FOREIGN KEY (activity) REFERENCES Activity(id) ON DELETE CASCADE ON UPDATE CASCADE  
);
CREATE TABLE OutputStaff
(
	id INTEGER NOT NULL, 
	activity INTEGER,
	sampleFormPath TEXT,
	linkToData TEXT,
	completedFormsDirectory TEXT,
	PRIMARY KEY (id),
	FOREIGN KEY (activity)	REFERENCES Activity(id) ON DELETE CASCADE ON UPDATE CASCADE  
);


CREATE TABLE Report
(
	id INTEGER NOT NULL, 
	output INTEGER,
	filePath TEXT,
	isComplete INTEGER,
	date TEXT,
	submittedBy INTEGER,
	PRIMARY KEY (id),
	FOREIGN KEY (output) REFERENCES OutputOrganization(id) ON DELETE CASCADE ON UPDATE CASCADE  
		
);



CREATE TABLE Setting (
    id INTEGER NOT NULL, 
    setting TEXT,
    value TEXT,
	UNIQUE(setting),
	PRIMARY KEY (id)
);

CREATE TABLE UserLogs(
	id INTEGER NOT NULL,
	user INTEGER NOT NULL,
	date TEXT,
	action TEXT,
	info TEXT,
	PRIMARY KEY (id),
	FOREIGN KEY (user) REFERENCES User(id)
);


