package org.csp.models;

import org.metawidget.inspector.annotation.UiHidden;
import org.metawidget.inspector.annotation.UiLarge;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Questions")
public class Question extends Model implements Comparable<Question>{

	private static final long serialVersionUID = 3614677213163396481L;
	
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField(canBeNull = false)
	private String question;
	@DatabaseField(foreign = true, columnName = "cohort", canBeNull = true, foreignAutoRefresh = true)
	private Cohort cohort;
	@DatabaseField
	private String type;
	@DatabaseField
	private String relation;
	
	@ForeignCollectionField
	private ForeignCollection<Indicator> indicators;
	
	public static String OUTCOME = "OUTCOME";
	public static String ACTIVITY = "ACTIVITY";
	public static String OUTPUT = "OUTPUT";
	
	
	public Question(String question, Cohort cohort, String type) {
		super();
		this.question = question;
		this.cohort = cohort;
		this.type = type;
	}

	public Question() { }

	@UiHidden
	public int getId() { return id;	}
	public void setId(int id) { this.id = id; } 

	@UiLarge
	public String getQuestion() { return question; }
	public void setQuestion(String question) { this.question = question; }
	
	@UiHidden
	public Cohort getCohort() { return cohort; }
	public void setCohort(Cohort cohort) { this.cohort = cohort; 	}
	
	public String getType() { return type; }
	public void setType(String type) { this.type = type; }
		
	public String getRelation() { return relation; }
	public void setRelation(String relation) { this.relation = relation; }

	public ForeignCollection<Indicator> getIndicators() { return indicators; }

	@Override
	public int compareTo(Question q) {
		if (q.id != 0 && this.id != 0)return this.id - q.id;
		return this.question.compareTo(q.question);
	}
	
	public boolean equals(Question q){ return (compareTo(q) == 0); }

	public String toString(){
		StringBuilder stb = new StringBuilder();
		if (this.question != null)stb.append(question);
		return stb.toString();
	}
}
