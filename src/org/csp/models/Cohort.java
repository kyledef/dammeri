package org.csp.models;

import java.util.Date;

import org.csp.controller.MemberController;
import org.csp.controller.OrganizationController;
import org.metawidget.inspector.annotation.UiAttribute;
import org.metawidget.inspector.annotation.UiHidden;
import org.metawidget.inspector.annotation.UiLabel;
import org.metawidget.inspector.annotation.UiSection;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Cohort")
public class Cohort extends Model implements Comparable<Cohort> {

	private static final long serialVersionUID = -6115422794530447769L;

	@DatabaseField(generatedId = true)
	private int id;

	@DatabaseField(foreign = true, columnName = "project", canBeNull = false, foreignAutoRefresh = true)
	private Project project;

	@DatabaseField(canBeNull = true)
	private Date startDate;

	@DatabaseField(canBeNull = true)
	private Date endDate;
	
	@DatabaseField (foreign = true, columnName = "funder", canBeNull = true, foreignAutoRefresh = true)
	private Organization funder;

	@DatabaseField(canBeNull = true, columnName="totalBudget")
	private double totalBudget;

	@DatabaseField(foreign = true, columnName = "teamLead", canBeNull = true)
	private Member teamLead;
	
	@ForeignCollectionField
	private ForeignCollection<Beneficiary> beneficiaries;
	
	@ForeignCollectionField
	private ForeignCollection<Activity> activities;
	
	@ForeignCollectionField
	private ForeignCollection<Question> questions;	
	
	@ForeignCollectionField
	private ForeignCollection<Outcome> outcomes;
	
	@ForeignCollectionField
	private ForeignCollection<Input> inputs;
	
	public Cohort() { super(); }
			
	public Cohort(Project project, Date startDate, Date endDate, double totalBudget, Member teamLead, Organization funder) {
		super();		
		this.project = project;
		this.startDate = startDate;
		this.endDate = endDate;
		this.totalBudget = totalBudget;
		this.teamLead = teamLead;
		this.funder = funder;
	}

	public Cohort(Project project) {
		super();
		this.project = project;
	}
	public Cohort(Project project, Organization organ){
		super();
		this.project = project;
		this.funder = organ;
	}

	@UiHidden
	public int getId() { return id; }
	public void setId(int id) { this.id = id; }

	@UiSection("Project")
	@UiLabel("")
	public Project getProject() { return project; }
	public void setProject(Project project) { this.project = project; }
	public Date getStartDate() { return startDate; }
	@UiSection("Dates")
	public void setStartDate(Date startDate) { this.startDate = startDate; }
	public Date getEndDate() { return endDate; }
	public void setEndDate(Date endDate) { this.endDate = endDate; }
	@UiSection("Finances")
	public double getTotalBudget() { return totalBudget; }
	public void setTotalBudget(double totalBudget) { this.totalBudget = totalBudget; }
	
	@UiAttribute( name = "lookup",  value = "${this.organizations}" )
	public Organization getFunder() { return funder; }
	public void setFunder(Organization funder) { this.funder = funder; }

	@UiSection( "Leader")
	@UiAttribute( name = "lookup",  value = "${this.members}" )
	public Member getTeamLead() { return teamLead; }
	public void setTeamLead(Member teamLead) { this.teamLead = teamLead; }
	
	@Override
	public int compareTo(Cohort co) {
		if (co.id != 0 && this.id != 0)return this.id - co.id;	
		return this.project.compareTo(co.project);
	}

	public ForeignCollection<Beneficiary> getBeneficiaries() {
		return beneficiaries;
	}

	public ForeignCollection<Activity> getActivities() {
		return activities;
	}

	public ForeignCollection<Question> getQuestions() {
		return questions;
	}
	
	public ForeignCollection<Outcome> getOutcomes() {
		return outcomes;
	}
	
	//TODO MOVE this getOrganizations to an independent class [suggested to the organization controller]
	//Need to change the dependency from the this in @UiAttribute( name = "lookup",  value = "${this.organizations}" )
	@UiHidden
	public Organization [] getOrganizations(){
		OrganizationController oc = OrganizationController.getInstance();
		return oc.getOrganizations();
	}
	
	

	public ForeignCollection<Input> getInputs() {
		return inputs;
	}

	@UiHidden
	public Member [] getMembers(){		
//		SettingController sc = SettingController.getInstance();		
//		Member [] results = MemberController.getInstance().getMembers(sc.getDefaultOrganization()); //TODO taking too long for default organization
		Member [] results = MemberController.getInstance().getMembers();
		return results;	
	}	
	
	public String toString(){
		StringBuilder stb = new StringBuilder();
		if (this.project != null)stb.append("Name: ").append(this.project.getName());
		if (this.startDate != null)stb.append(" Start Date: ").append(this.startDate);
		return stb.toString();
	}
}
