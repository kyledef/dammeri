package org.csp.models;

import org.metawidget.inspector.annotation.UiHidden;
import org.metawidget.inspector.annotation.UiLarge;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Input")
public class Input extends Model implements Comparable<Input>{
	private static final long serialVersionUID = 5578662005342902731L;
	
	@DatabaseField(generatedId = true, canBeNull=false, columnName="id" )
	private int id;
	
	@DatabaseField(canBeNull=false)
	private String input;
	
	@DatabaseField(foreign = true, columnName = "activity", canBeNull = true, foreignAutoRefresh = true)
	private Activity activity;
	@DatabaseField
	private String type;
	@DatabaseField
	private int cost;
	@DatabaseField(foreign = true, columnName = "cohort", canBeNull = true, foreignAutoRefresh = true)
	private Cohort cohort;
	
	private Activity [] activities;
			
	public Input() {
//		super();
	}
	public Input(String input, Activity activity, String type, int cost) {
		super();
		this.input = input;
		this.activity = activity;
		this.type = type;
		this.cost = cost;
	}
	@UiLarge
	public String getInput() { return input; }
	public void setInput(String description) { this.input = description;}
	
	
	@UiHidden
	public int getId() {return this.id;	 }	
	public void setId(int id) {this.id = id;}

	public Activity getActivity() {
		return activity;
	}
	public void setActivity(Activity activity) {
		this.activity = activity;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	@UiHidden
	public Activity [] getActivities(){
		if (this.activities == null)return new Activity[1];
		return activities;		
	}
	
	public void setActivities(Activity[] activities) {
		this.activities = activities;
	}
	@Override
	public int compareTo(Input o) {
		if (o.id != 0 && this.id != 0)return this.id = o.id;		
		return this.input.compareTo(o.input);
	}

	@UiHidden
	public Cohort getCohort() { return cohort; }
	public void setCohort(Cohort cohort) { this.cohort = cohort; }
	

}
