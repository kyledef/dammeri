package org.csp.models;

import org.metawidget.inspector.annotation.UiAttribute;
import org.metawidget.inspector.annotation.UiHidden;
import org.metawidget.inspector.annotation.UiLabel;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ActivityObjective")
public class ActivityObjective extends Model implements Comparable<ActivityObjective> {
	
	private static final long serialVersionUID = -8404596948394560789L;

	@DatabaseField(generatedId = true)
	int id;

	@DatabaseField(foreign = true, columnName = "activity",canBeNull = false, foreignAutoRefresh = true)
	Activity activity;
	@DatabaseField(foreign = true, columnName = "objective",canBeNull = false, foreignAutoRefresh = true)
	Objective objective;
	
	private Objective[] objectives = new Objective[2];
	
	@UiHidden
	public int getId() { return id; }
	public void setId(int id) { this.id = id; }

	@UiLabel("")
	public Activity getActivity() {
		return activity;
	}
	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	
	@UiAttribute( name = "lookup",  value = "${this.objectives}" )
	public Objective getObjective() {
		return objective;
	}
	public void setObjective(Objective objective) {
		this.objective = objective;
	}

	@UiHidden
	public Objective[] getObjectives() { return objectives; }
	public void setObjectives(Objective[] objectives) { this.objectives = objectives; }



	public ActivityObjective(){
		super();
	}
	
	public ActivityObjective(Activity activity, Objective objective) {
		super();
		this.activity = activity;
		this.objective = objective;
	}

	@Override
	public int compareTo(ActivityObjective a) {
		if (this.id != 0 && a.id != 0)return id - a.id;
		
		return -1;
	}

}
