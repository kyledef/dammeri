package org.csp.models;

import org.metawidget.inspector.annotation.UiHidden;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ActivityQuestions")
public class QuestionActivity extends Model implements Comparable<QuestionActivity>{
	private static final long serialVersionUID = 3897024585883745774L;
	
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField(foreign = true, columnName = "question", canBeNull = true, foreignAutoRefresh = true)
	Question question;
	
	@DatabaseField(foreign = true, columnName = "activity", canBeNull = true, foreignAutoRefresh = true)
	Activity activity;
	
	public QuestionActivity() {
		super();
	}
	
	public QuestionActivity(Question question, Activity activity) {
		super();
		this.question = question;
		this.activity = activity;
	}
	@UiHidden
	public int getId() { return id;	}
	public void setId(int id) { this.id = id; } 

	public Question getQuestion() {return question;}
	public void setQuestion(Question question) {this.question = question;}

	public Activity getActivity() {return activity;}
	public void setActivity(Activity activity) {this.activity = activity;}

	@Override
	public int compareTo(QuestionActivity o) {
		if (o.id != 0 && this.id != 0)return this.id - o.id;
		return -1;
	} 

}
