package org.csp.models;

import org.metawidget.inspector.annotation.UiHidden;
import org.metawidget.inspector.annotation.UiLarge;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "OutputExternal")
public class Output extends Model implements Comparable<Output>{
	
	private static final long serialVersionUID = -3311628239558671813L;
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField(foreign = true, columnName = "activity", canBeNull = true)
	private Activity activity;
	@DatabaseField(columnName ="description", canBeNull = true)
	private String output;
	@DatabaseField
	private String feedback;
	@DatabaseField
	private String followUp;
	@DatabaseField
	private String type; //TODO Identify how to improve the different versions of outputs
	
	@ForeignCollectionField
	private ForeignCollection<QuestionOutput> questionOutputs;

	private Activity [] activities;
	
	public Output() { super(); }

	public Output(Activity activity, String feedback, String followUp) {
		super();		
		this.activity = activity;
		this.feedback = feedback;
		this.followUp = followUp;
	}

	@UiHidden
	public int getId() { return id; }
	public void setId(int id) { this.id = id; }

	public Activity getActivity() { return activity; }
	public void setActivity(Activity activity) { this.activity = activity; }

	@UiHidden
	@UiLarge
	public String getFeedback() { return feedback; }
	public void setFeedback(String feedback) { this.feedback = feedback; 	}
		
	@UiHidden
	public String getFollowUp() { return followUp; }
	public void setFollowUp(String followUp) { this.followUp = followUp; }

	public String getType() { return type; }
	public void setType(String type) { this.type = type; }
		
	@UiLarge
	public String getOutput() { return output; }		
	public void setOutput(String output) { this.output = output; }
	
	@UiHidden
	public ForeignCollection<QuestionOutput> getQuestionOutputs() { return questionOutputs; }
	
	@UiHidden
	public Activity [] getActivities(){
		if (this.activities == null)return new Activity[1];
		return activities;		
	}
	
	public void setActivities(Activity[] activities) {
		this.activities = activities;
	}
	
	@Override
	public int compareTo(Output o) {
		if (this.id != 0 && o.id != 0)return this.id - o.id;
		return this.activity.compareTo(o.activity) + this.output.compareTo(o.output);
	}
	
	public String toString(){
		return this.output;
	}
}
