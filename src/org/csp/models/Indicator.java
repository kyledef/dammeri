package org.csp.models;

import org.metawidget.inspector.annotation.UiHidden;
import org.metawidget.inspector.annotation.UiLarge;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Indicator")
public class Indicator extends Model implements Comparable<Indicator> {
	private static final long serialVersionUID = 6784050127061297991L;

	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField
	private String indicator;
	@DatabaseField(foreign = true, columnName = "question", canBeNull = false)
	private Question question;
	@DatabaseField
	private String type;
	@DatabaseField
	private String frequency;
	@DatabaseField
	private String target;
	@DatabaseField(columnName="source", canBeNull=true)
	private String meansOfVerification;
	
	@ForeignCollectionField
	private ForeignCollection<Field> fields;
	
	private Question [] questions = new Question[1];
	
	public Indicator() { super();	}
	
	public Indicator(Question [] questions){
		super();
		this.questions = questions;
	}
	
	public Indicator(Question question){
		this.question = question;
	}
	
	public Indicator(String indicator, Question question, String type, String frequency, String target, String source) {
		super();
		this.indicator = indicator;
		this.question = question;
		this.type = type;
		this.frequency = frequency;
		this.target = target;
		this.meansOfVerification = source;
	}

	@UiHidden
	public int getId() { return id; }
	public void setId(int id) { this.id = id; }

	public Question getQuestion() { return question; }
	public void setQuestion(Question question) { this.question = question; }	

	@UiLarge
	public String getIndicator() { return indicator; }
	public void setIndicator(String indicator) { this.indicator = indicator; }

	public String getType() { return type; }
	public void setType(String type) { this.type = type; }
	
	public String getFrequency() { return frequency; }
	public void setFrequency(String frequency) { this.frequency = frequency; }

	@UiHidden
	public ForeignCollection<Field> getFields() { return fields;}
	public void setFields(ForeignCollection<Field> fields) { this.fields = fields; }
	
	public String getTarget() { return target;	}
	public void setTarget(String target) { this.target = target;	}
//	@UiLarge
	public String getMeansOfVerification() { return meansOfVerification;	}
	public void setMeansOfVerification(String source) { this.meansOfVerification = source;	}
		
	@UiHidden
	public Question [] getQuestions() {
		if (this.questions == null)return new Question[1];
		return questions;
	}
	public void setQuestions(Question [] questions) {
		this.questions = questions;
	}
	@Override
	public int compareTo(Indicator o) {
		if (this.id != 0 && o.id != 0)return this.id - o.id;
		return this.indicator.compareTo(o.indicator);
	}
	
	public String toString(){
		StringBuilder stb = new StringBuilder();
		if (this.id != 0)stb.append(id).append(":");
		if (this.indicator != null)stb.append(this.indicator);
		return stb.toString();
	}
}
