package org.csp.models;

import org.metawidget.inspector.annotation.UiHidden;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "FollowUpTasks")
public class FollowUpTask extends Model  implements Comparable<FollowUpTask>{

	private static final long serialVersionUID = 345826460901070307L;
	@DatabaseField(id = true, generatedId = true)
	private int id;	
	@DatabaseField(canBeNull = false)
	private String task;
	@DatabaseField(foreign = true, columnName = "indicator", canBeNull = true)
	private Output output;
	@DatabaseField(canBeNull = false)
	private String date;
	
	@DatabaseField(foreign = true, columnName = "indicator", canBeNull = true)
	private Person responsiblePerson;
	
	public FollowUpTask() {
		
	}

	public FollowUpTask(int id, String task, Output output, String date,
			Person responsiblePerson) {
		super();
		this.id = id;
		this.task = task;
		this.output = output;
		this.date = date;
		this.responsiblePerson = responsiblePerson;
	}
	
	public String getTask() { return task; }
	public void setTask(String task) { this.task = task;	}

	@UiHidden
	public int getId() { return id; }
	public void setId(int id) { this.id = id; }
	
	@UiHidden
	public Output getOutput() { return output; }	
	public void setOutput(Output output) { this.output = output; }
			
	public String getDate() { return date; }
	public void setDate(String date) { this.date = date; }
		
	@UiHidden 
	public Person getResponsiblePerson() { return responsiblePerson; }
	public void setResponsiblePerson(Person responsiblePerson) { this.responsiblePerson = responsiblePerson; }
		
	@Override
	public int compareTo(FollowUpTask o) {
		if (o.id != 0 && this.id != 0)return this.id - o.id;	
		return this.task.compareTo(o.task) + this.date.compareTo(o.date);
	}
}
