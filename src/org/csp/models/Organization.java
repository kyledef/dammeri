package org.csp.models;

import org.metawidget.inspector.annotation.UiHidden;
import org.metawidget.inspector.annotation.UiLarge;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Organization")
public class Organization extends Model implements Comparable<Organization>{
	private static final long serialVersionUID = 4452135468222424203L;
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField(canBeNull = false)
	private String name;
	@DatabaseField
	private String address;
	@DatabaseField
	private String type;
	@ForeignCollectionField(eager=true)
	private ForeignCollection<Member> members;
	@UiHidden
	public ForeignCollection<Member> getMembers() {
		return members;
	}

	public Organization(){
		
	}
	
	public Organization(String name, String address, String type) {
		super();
		this.name = name;
		this.address = address;
		this.type = type;
	}
	
	@UiHidden
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@UiLarge
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int compareTo(Organization o) {
		if (this.id != 0 && o.id != 0)return this.id - o.id;
		if (this.name != null && o.name != null)return this.name.compareTo(o.name);
		return -1;
	}
	public boolean equals(Organization o){
		return (compareTo(o) == 0);
	}
	
	public String toString(){
		StringBuilder stb = new StringBuilder();
//		stb.append(name);
		stb.append(name).append(":").append(type);
		return stb.toString();
	}
	
}
