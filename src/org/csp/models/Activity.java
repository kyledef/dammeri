package org.csp.models;

import org.csp.controller.ObjectiveController;
import org.csp.controller.SettingController;
import org.metawidget.inspector.annotation.UiAttribute;
import org.metawidget.inspector.annotation.UiHidden;
import org.metawidget.inspector.annotation.UiLarge;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Activity")
public class Activity extends Model  implements Comparable<Activity>{
	
	private static final long serialVersionUID = 6508199009961696567L;

	@DatabaseField(generatedId = true, canBeNull=false )
	private int id;

	@DatabaseField(foreign = true, columnName = "cohort", canBeNull = true, foreignAutoRefresh = true)
	private Cohort cohort;

	@DatabaseField(canBeNull = false, columnName="title")
	private String activity;

	@DatabaseField
	private String description;
	
	@DatabaseField(foreign = true, columnName = "objective", canBeNull = true, foreignAutoRefresh = true)
	private Objective objective;
	
	private Objective[] objectives = new Objective[2];
	
	//Outputs
	@ForeignCollectionField
	private ForeignCollection<Output> outputs;
	
	//Inputs
	@ForeignCollectionField
	private ForeignCollection<Input> inputs;
	
	@ForeignCollectionField
	private ForeignCollection<QuestionActivity> questionActivities;

	public Activity() { }

	public Activity(String activity, String description){
		super();
		this.activity = activity;
		this.description = description;
	}

	public Activity(Cohort cohort, String activity, String description) {
		super();
		
		this.cohort = cohort;
		this.activity = activity;
		this.description = description;
		this.objectives = ObjectiveController.getInstance(SettingController.getInstance().getConnection()).getObjectives();
	}

	@UiHidden
	public int getId() { return id; }
	public void setId(int id) {this.id = id; }
	
	@UiHidden
	public Cohort getCohort() { return cohort; }
	public void setCohort(Cohort cohort) {  this.cohort = cohort; }
	
	@UiLarge
	public String getActivity() { return activity; }
	public void setActivity(String activity) { this.activity = activity; }
		
	
	@UiHidden
	public String getDescription() { return description; }
	public void setDescription(String description) { this.description = description; }

	@UiHidden
	public ForeignCollection<Output> getOutputs() { return outputs; }
	@UiHidden
	public ForeignCollection<Input> getInputs() { return inputs; }
	@UiHidden
	public ForeignCollection<QuestionActivity> getQuestionActivities() { return questionActivities; }
	
	@UiHidden
	@UiAttribute( name = "lookup",  value = "${this.objectives}" )
	public Objective getObjective() { return objective; }
	public void setObjective(Objective objective) { this.objective = objective; }
	
	@UiHidden
	public Objective[] getObjectives() { return objectives; }
	public void setObjectives(Objective[] objectives) { this.objectives = objectives; }

	@Override
	public int compareTo(Activity a) {
		if (a.id != 0 && this.id != 0)return this.id - a.id;	
		return this.activity.compareTo(a.activity);
	}
	public boolean equals(Activity q){return (compareTo(q) == 0);}

	public String toString(){
		StringBuilder stb = new StringBuilder();
		if (this.activity != null)stb.append(this.activity);
		return stb.toString();
	}
}
