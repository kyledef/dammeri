package org.csp.models;

import java.io.Serializable;

import org.metawidget.inspector.annotation.UiHidden;
import org.metawidget.inspector.annotation.UiLarge;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Person")
public class Person extends Model implements Comparable<Person>, Serializable  {
	private static final long serialVersionUID = 1037766845822112665L;	
	
	@DatabaseField(generatedId = true)
	private int id;

	@DatabaseField(canBeNull = false)
	private String firstName;

	@DatabaseField(canBeNull = false)
	private String lastName;

	@DatabaseField(canBeNull = true)
	private String sex;

	@DatabaseField(canBeNull = true)
	private String age;

	@DatabaseField(canBeNull = true)
	private String city;

	@DatabaseField(canBeNull = true)
	private String address;
	
	@DatabaseField(canBeNull = true)
	private String email;
	
	@DatabaseField(canBeNull = true)
	private String phone;
	
	public Person(){
		
	}
	
	public Person(String firstName, String lastName, String sex,
			String age, String city, String address) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.sex = sex;
		this.age = age;
		this.city = city;
		this.address = address;
	}

	

	public Person(String firstName, String lastName, String sex, String age,
			String city, String address, String email, String phone) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.sex = sex;
		this.age = age;
		this.city = city;
		this.address = address;
		this.email = email;
		this.phone = phone;
	}

	@UiHidden
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	@UiHidden
	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	@UiHidden
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	@UiLarge
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public int compareTo(Person p) {
		if (p !=null )
			if (this.lastName != null && p.lastName != null && this.firstName != null && p.firstName != null)
				return this.lastName.compareToIgnoreCase(p.lastName) + this.firstName.compareToIgnoreCase(p.firstName);
		return -1;
	}
	
	public boolean equals(Person p){
		return compareTo(p) == 0;
	}
	
	public String toString(){
		StringBuilder stb = new StringBuilder();
		stb.append(this.firstName).append(" ").append(this.lastName);
		return stb.toString();
	}
	
}
