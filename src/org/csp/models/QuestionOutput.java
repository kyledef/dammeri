package org.csp.models;

import org.metawidget.inspector.annotation.UiHidden;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "OutputQuestions")
public class QuestionOutput extends Model implements Comparable<QuestionOutput>{
	private static final long serialVersionUID = 4669601307670726970L;
	
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField(foreign = true, columnName = "question", canBeNull = true, foreignAutoRefresh = true)
	Question question;
	
	@DatabaseField(foreign = true, columnName = "output", canBeNull = true, foreignAutoRefresh = true)
	Output output;
	
	
	
	public QuestionOutput() {
		super();
	}
	public QuestionOutput(Question question, Output output) {
		super();
		this.question = question;
		this.output = output;
	}
	@UiHidden
	public int getId() { return id;	}
	public void setId(int id) { this.id = id; }
	
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	public Output getOutput() {
		return output;
	}
	public void setOutput(Output output) {
		this.output = output;
	}
	@Override
	public int compareTo(QuestionOutput o) {
		if (o.id != 0 && this.id != 0)return this.id - o.id;
		return -1;
	} 
}
