package org.csp.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Setting")
public class Setting extends Model {
	private static final long serialVersionUID = -3297522946778026593L;

	@DatabaseField(generatedId = true, canBeNull=false )
	private int id;
	@DatabaseField(canBeNull = false)
	private String setting;
	@DatabaseField(canBeNull = false)
	private String value;
	
	@Override
	public int getId() {
		return id;
	}

	public String getSetting() {
		return setting;
	}

	public void setSetting(String setting) {
		this.setting = setting;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setId(int id) {
		this.id = id;
	}
	

}
