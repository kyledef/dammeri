package org.csp.models;

import java.io.Serializable;

import org.metawidget.inspector.annotation.UiHidden;
import org.metawidget.inspector.annotation.UiLarge;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Project")
public class Project extends Model implements Comparable<Project>, Serializable{
	private static final long serialVersionUID = -6095899560374234480L;

	@DatabaseField(generatedId = true)
	private int id;

	@DatabaseField(canBeNull = false)
	private String name;

	@DatabaseField
	private String description;
	
	//Objectives
	@ForeignCollectionField
	private ForeignCollection<Objective> objectives;
	
	@ForeignCollectionField
	private ForeignCollection<Cohort> cohorts;
	
	@DatabaseField
	private String location;
	
	public Project() {
		super();
	}

	public Project(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	@UiHidden
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@UiLarge
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int compareTo(Project p) {
		if (p.name != null && this.name != null)
			return this.name.compareTo(p.name);
		return -1;
	}

	public ForeignCollection<Objective> getObjectives() {
		return objectives;
	}

	public ForeignCollection<Cohort> getCohorts() {
		return cohorts;
	}
	
	public String toString(){
		StringBuilder stb = new StringBuilder();
		stb.append(this.name);
		return stb.toString();
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	
}
