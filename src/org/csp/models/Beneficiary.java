package org.csp.models;

import org.metawidget.inspector.annotation.UiHidden;
import org.metawidget.inspector.annotation.UiLarge;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Beneficiary")
public class Beneficiary extends Model  implements Comparable<Beneficiary> {
	private static final long serialVersionUID = 2306690114746018742L;

	@DatabaseField(generatedId = true)
	private int id;

	@DatabaseField(canBeNull = true, columnName="name")
	private String description;

	@DatabaseField(foreign = true, canBeNull = false, columnName = "cohort", foreignAutoRefresh = true)
	private Cohort cohort;
	
	//TODO Create database fields for the following attributes
	
	@DatabaseField(canBeNull = true, columnName = "age")
	private String ageGroup;
	
	@DatabaseField
	private String sex;
	
	@DatabaseField(canBeNull = true, columnName="type")
	private String beneficiaryType;
	
	public Beneficiary() { }
	public Beneficiary(String name, Cohort cohort) {
		super();
		this.description = name;
		this.cohort = cohort;
	}
	
	
	
	public Beneficiary(String ageGroup, String sex, String beneficiaryType) {
		super();
		this.ageGroup = ageGroup;
		this.sex = sex;
		this.beneficiaryType = beneficiaryType;
	}
	
	public Beneficiary(String description, Cohort cohort, String ageGroup,
			String sex, String beneficiaryType) {
		super();
		this.description = description;
		this.cohort = cohort;
		this.ageGroup = ageGroup;
		this.sex = sex;
		this.beneficiaryType = beneficiaryType;
	}
	@UiHidden
	public int getId() { return id; }
	public void setId(int id) { this.id = id; 	}
		
	@UiHidden
	public Cohort getCohort() { return cohort; }
	public void setCohort(Cohort cohort) { this.cohort = cohort; }
		
	@UiLarge
	@UiHidden
	public String getDescription() { return description; }
	public void setDescription(String name) { this.description = name; }
		
	public String getAgeGroup() { return ageGroup; }
	public void setAgeGroup(String ageGroup) { this.ageGroup = ageGroup; }
	
	public String getSex() { return sex; }
	public void setSex(String sex) { this.sex = sex; 	}
		

	public String getBeneficiaryType() {
		return beneficiaryType;
	}
	public void setBeneficiaryType(String beneficiaryType) {
		this.beneficiaryType = beneficiaryType;
	}
	@Override
	public int compareTo(Beneficiary o) {
		if (o.id != 0 && this.id != 0)
			return this.id - o.id;	
		int value = 0;
		if (this.sex != null && o.sex != null)
			value += sex.compareTo(o.sex);
		if (this.beneficiaryType != null && o.beneficiaryType != null)
			value += this.beneficiaryType.compareTo(o.beneficiaryType);
		if (this.description != null && o.description != null)
			value += this.description.compareTo(o.description);
		
		return value;
	}

	public String toString(){
		StringBuilder stb = new StringBuilder();
		stb.append(this.beneficiaryType);
		return stb.toString();
	}
	
	public boolean equals(Beneficiary b){
		System.out.println("Checked the beneficiaries equals method");
		return (compareTo(b) == 0);
	}
}
