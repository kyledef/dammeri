package org.csp.models;

import java.util.Date;

import org.metawidget.inspector.annotation.UiHidden;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Field")
public class Field extends Model  implements Comparable<Field>{

	private static final long serialVersionUID = -5889490533140266923L;

	@DatabaseField(generatedId = true)
	private int id;

	@DatabaseField(foreign = true, columnName = "indicator", canBeNull = false, foreignAutoRefresh = true)
	private Indicator indicator;

	@DatabaseField(canBeNull = false)
	private String value;

	@DatabaseField(canBeNull = false)
	private Date captureDate;
	
	public Field() { }
	public Field(Indicator indicator, String value, Date captureDate) {
		super();
		this.indicator = indicator;
		this.value = value;
		this.captureDate = captureDate;
	}

	@UiHidden
	public int getId() { return id; }
	public void setId(int id) { this.id = id; }
		
	@UiHidden
	public Indicator getIndicator() { return indicator; }
	public void setIndicator(Indicator indicator) { this.indicator = indicator; }

	public String getValue() { return value; }
	public void setValue(String value) { this.value = value; }
		
	public Date getCaptureDate() { return captureDate; }
	public void setCaptureDate(Date captureDate) { this.captureDate = captureDate; }
		
	@Override
	public int compareTo(Field o) {
		if (o.id != 0 && this.id != 0)return this.id - o.id;
		return this.indicator.compareTo(o.indicator) + this.value.compareTo(o.value);
	}
	
}
