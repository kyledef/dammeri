package org.csp.models;

import java.io.Serializable;

import org.csp.controller.MemberController;
import org.csp.controller.OrganizationController;
import org.csp.controller.SettingController;
import org.metawidget.inspector.annotation.UiAttribute;
import org.metawidget.inspector.annotation.UiHidden;
import org.metawidget.inspector.annotation.UiLabel;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Members")
public class Member extends Model  implements Comparable<Member>, Serializable{
	private static final long serialVersionUID = -3721432492473567489L;

	@DatabaseField(generatedId = true)
	private int id;

	@DatabaseField(foreign = true, columnName = "organization", foreignAutoRefresh = true)
	private Organization organization;

	@DatabaseField(foreign = true, columnName = "person", foreignAutoRefresh = true)
	private Person person;

	@DatabaseField(canBeNull = true)
	private String role;
		
	public Member(Organization organization, Person person, String role) {
		super();
		this.organization = organization;
		this.person = person;
		this.role = role;
	}
	
	public Member() { super(); }
	public Member(Person p){this.person = p;}
	public Member(Person p, Organization o){ this.person = p; this.organization = o; }
	public Member(Organization o) {this.organization = o;  }

	@UiHidden
	public int getId() { return id; }
	public void setId(int id) { this.id = id; }
	
	@UiAttribute( name = "lookup",  value = "${this.organizations}" )
	public Organization getOrganization() { return organization; }		
	public void setOrganization(Organization organization) { this.organization = organization; }
	
	@UiHidden
	public Organization [] getOrganizations(){
		OrganizationController oc = OrganizationController.getInstance(SettingController.getInstance().getConnection());
		return oc.getOrganizations();
	}
	
	@UiHidden
	public String [] getRoles(){
		MemberController mc = MemberController.getInstance(SettingController.getInstance().getConnection());
		if (this.organization.getId() > 0)mc.getRoles(organization);
		return mc.getRoles();//TODO get roles not functioning properly
	}
	
	@UiLabel( "" )
	public Person getPerson() { return person; }
	public void setPerson(Person person) { 	this.person = person; }
	
	public String getRole() { return role;	}
	public void setRole(String role) { this.role = role; }
	
	@Override
	public int compareTo(Member m) {
		int value = -1;
		if (m.id != 0 && this.id != 0)return this.id - m.id;		
		if (this.person != null && m.person != null){
			value = this.person.compareTo(m.person);
			if (this.organization != null && m.organization != null)
				value += this.organization.compareTo(m.organization);
		}
		return value;
	}
	
	public boolean equals(Member m){
		return (compareTo(m) == 0);
	}
	
	public String toString(){
		StringBuilder stb = new StringBuilder();
		if (this.person != null)stb.append(this.person.toString());
		if (this.organization != null)stb.append(":").append(this.organization.getName());		
		return stb.toString();
	}
}
