package org.csp.models;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

public abstract class Model implements  Serializable{
	private static final long serialVersionUID = 1316955304583418921L;
	public abstract int getId();
	private PropertyChangeSupport support = new PropertyChangeSupport(this);
	public void addPropertyChangeListener(PropertyChangeListener listener){
		this.support.addPropertyChangeListener(listener);
	}
	public void removePropertyChangeListener(PropertyChangeListener listener){
		this.support.removePropertyChangeListener(listener);
	}
}
