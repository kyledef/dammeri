package org.csp.models;

import org.metawidget.inspector.annotation.UiHidden;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Report")
public class Report extends Model implements Comparable<Report> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@DatabaseField( generatedId = true)
	private int id;
	@DatabaseField(foreign = true, columnName = "output", canBeNull = false)
	private Output output;

	@DatabaseField
	private String filePath;

	@DatabaseField
	private int isComplete;

	@DatabaseField(canBeNull = false)
	private String date;

	@DatabaseField(foreign = true, columnName = "submittedBy", canBeNull = false)
	private Person submittedBy;
	
	
	public Report() { }
	


	public Report(Output output, String filePath, int isComplete,
			String date, Person submittedBy) {
		super();
		
		this.output = output;
		this.filePath = filePath;
		this.isComplete = isComplete;
		this.date = date;
		this.submittedBy = submittedBy;
	}

	@UiHidden
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Output getOutput() {
		return output;
	}

	public void setOutput(Output output) {
		this.output = output;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public int getIsComplete() {
		return isComplete;
	}

	public void setIsComplete(int isComplete) {
		this.isComplete = isComplete;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Person getSubmittedBy() {
		return submittedBy;
	}

	public void setSubmittedBy(Person submittedBy) {
		this.submittedBy = submittedBy;
	}


	@Override
	public int compareTo(Report o) {
		
		return 0;
	}
	
	

}
