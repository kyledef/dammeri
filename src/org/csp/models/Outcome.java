package org.csp.models;

import org.metawidget.inspector.annotation.UiHidden;
import org.metawidget.inspector.annotation.UiLarge;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Outcome")
public class Outcome extends Model implements Comparable<Outcome>{

	private static final long serialVersionUID = 4995872291326659108L;
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField(foreign = true, columnName = "cohort", canBeNull = true, foreignAutoRefresh = true)
	private Cohort cohort;	
	@DatabaseField(columnName = "term")
	private String timeframe;
	@DatabaseField
	private String type;
	@DatabaseField(canBeNull = false,columnName = "description")
	private String outcome;
	
	@ForeignCollectionField
	private ForeignCollection<QuestionOutcome> questionOutcomes;
	
	public Outcome(){ }
	public Outcome(Cohort cohort, String term, String relation, String description) {
		super();
		this.cohort = cohort;
		this.timeframe = term;
		this.type = relation;
		this.outcome = description;
	}

	@UiHidden
	public Cohort getCohort() { return cohort; }	
	public void setCohort(Cohort cohort) { this.cohort = cohort; }
		
	public String getTimeframe() { return timeframe; }	
	public void setTimeframe(String term) { this.timeframe = term; }
			
	public String getType() { return type; }	
	public void setType(String relation) { this.type = relation; }
			
	@UiLarge
	public String getOutcome() { return outcome; }	
	public void setOutcome(String description) { this.outcome = description; }
		
	@UiHidden
	public int getId() { return id; }
	public void setId(int id) { this.id = id;	}

	@UiHidden
	public ForeignCollection<QuestionOutcome> getQuestionOutcomes() { return questionOutcomes; }
	
	@Override
	public int compareTo(Outcome a) {
		if (a.id != 0 && this.id != 0)return this.id - a.id;	
		if (this.outcome != null && a.outcome != null)return this.outcome.compareTo(a.outcome);
		return -1;
	}
	
	public String toString(){
		if (this.outcome != null)return this.outcome;
		else return null;
	}
}
