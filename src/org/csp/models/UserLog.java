package org.csp.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "UserLogs")
public class UserLog extends Model  implements Comparable<UserLog> {
	private static final long serialVersionUID = 7273382450642918881L;

	@DatabaseField(generatedId = true, canBeNull=false )
	private int id;
	@DatabaseField(foreign = true, columnName = "user", canBeNull = false, foreignAutoRefresh = true)
	private User user;
	@DatabaseField(canBeNull = false)
	private Date date;
	@DatabaseField(canBeNull = false)
	private String action;
	@DatabaseField(canBeNull = false)
	private String info;
	
	public UserLog() {
		super();
	}

	public UserLog(User user, Date date, String action, String info) {
		super();
		this.user = user;
		this.date = date;
		this.action = action;
		this.info = info;
	}

	@Override
	public int getId() {return id;}

	@Override
	public int compareTo(UserLog ul) {
		return this.id - ul.id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public void setId(int id) {
		this.id = id;
	}
}
