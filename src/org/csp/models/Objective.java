package org.csp.models;

import org.metawidget.inspector.annotation.UiHidden;
import org.metawidget.inspector.annotation.UiLarge;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Objectives")
public class Objective extends Model  implements Comparable<Objective>{
	private static final long serialVersionUID = -7859408612424111947L;

	@DatabaseField(generatedId = true)
	private int id;

	@DatabaseField(foreign = true, canBeNull = false, columnName = "project")
	private Project project;

	@DatabaseField(canBeNull = false,columnName = "title")
	private String objective;
	
	@DatabaseField
	private int number = 1;

	public Objective() { }

	public Objective(String title){
		this.objective = title;
	}

	public Objective(Project project, String title) {
		super();
		this.project = project;
		this.objective = title;
	}

	@UiHidden
	public int getId() { return id; }
	public void setId(int id) { this.id = id; }
		
	@UiHidden
	public Project getProject() { return project; 	}
	public void setProject(Project project) { this.project = project; }
	
	@UiLarge
	public String getObjective() { return objective; 	}
	public void setObjective(String title) { this.objective = title; }

	@UiHidden
	public int getNumber() { return number; }
	public void setNumber(int number) { this.number = number; }
	
	@Override
	public int compareTo(Objective o) {
		int value = 0;
		boolean used = false;
		if (o.id != 0 && this.id != 0)return this.id - o.id;
		if (this.objective != null && o.objective != null){
			value += this.objective.compareTo(o.objective);
			used = true;
		}
		if (this.project != null && o.project != null){
			value += this.project.compareTo(o.project);
			used = true;
		}
		System.out.println(value);
		
		if (used)
			return value;
		return -1;
	}
	
	public String toString(){
		StringBuilder stb = new StringBuilder();
		stb.append(this.objective);
		if (this.project != null && this.project.getName() != null)stb.append(":").append(this.project.getName());
		return stb.toString();
	}
	public boolean equals(Objective o){
		return (compareTo(o) == 0);
	}
}
