package org.csp.models;

import org.metawidget.inspector.annotation.UiAttribute;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "IndividualBeneficiary")
public class IndividualBeneficiary extends Model {

	private static final long serialVersionUID = -254811940148514896L;
	@DatabaseField(generatedId = true)
	private int id;	
	@DatabaseField(foreign = true, canBeNull = false, columnName = "person", foreignAutoRefresh = true)
	private Person person;
	@DatabaseField(foreign = true, canBeNull = false, columnName = "cohort", foreignAutoRefresh = true)
	private Cohort cohort;
	@DatabaseField
	private String affiliation;
	
	
	
	public IndividualBeneficiary(Person person, Cohort cohort, String affiliation) {
		super();
		this.person = person;
		this.cohort = cohort;
		this.affiliation = affiliation;
	}
	
	public IndividualBeneficiary(){ }


	@Override
	public int getId() {
		return id;
	}

	@UiAttribute( name = "lookup",  value = "${this.persons}" )
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}
	
	
	public Cohort getCohort() {
		return cohort;
	}

	public void setCohort(Cohort cohort) {
		this.cohort = cohort;
	}

	public String getAffiliation() {
		return affiliation;
	}

	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Person [] getPersons(){
		Person[] p = new Person[1]; //TODO implement lookup for person with person controller
		return p;
	}
	
}
