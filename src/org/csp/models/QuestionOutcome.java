package org.csp.models;

import org.metawidget.inspector.annotation.UiHidden;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "OutcomeQuestions")
public class QuestionOutcome extends Model implements Comparable<QuestionOutcome>{
	private static final long serialVersionUID = -5106699213024405237L;
	
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField(foreign = true, columnName = "question", canBeNull = true, foreignAutoRefresh = true)
	Question question;
	
	@DatabaseField(foreign = true, columnName = "outcome", canBeNull = true, foreignAutoRefresh = true)
	Outcome outcome;
	
	
	
	public QuestionOutcome() {
		super();
	}

	public QuestionOutcome(Question question, Outcome outcome) {
		super();
		this.question = question;
		this.outcome = outcome;
	}
	
	@UiHidden
	public int getId() { return id;	}
	public void setId(int id) { this.id = id; } 

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public Outcome getOutcome() {
		return outcome;
	}

	public void setOutcome(Outcome outcome) {
		this.outcome = outcome;
	}

	@Override
	public int compareTo(QuestionOutcome o) {
		if (o.id != 0 && this.id != 0)return this.id - o.id;
		return -1;
	} 
	
}
