package org.csp.models;

import org.metawidget.inspector.annotation.UiComesAfter;
import org.metawidget.inspector.annotation.UiHidden;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "User")
public class User extends Model implements Comparable<User> {
	
	private static final long serialVersionUID = 1L;
	@DatabaseField(generatedId = true)
	private int id;
	@DatabaseField(canBeNull = false)
	private String userName;
	@DatabaseField(canBeNull = false)
	private String accessLevel;
	@DatabaseField(canBeNull = false)
	private String password;	
	
	public User() {
	}


	public User(String userName, String accessLevel, String password) {
		super();
		this.userName = userName;
		this.accessLevel = accessLevel;
		this.password = password;
	}

	@UiHidden
	public int getId() { return id; }
	public void setId(int id) { this.id = id; }
		
	public String getUserName() { return userName; }
	public void setUserName(String userName) { this.userName = userName; }
	
	@UiComesAfter( "password" )
	public String getAccessLevel() { return this.accessLevel; }
	public void setAccessLevel(String accessLevel) { this.accessLevel = accessLevel; }

	@UiComesAfter( "userName" )
	public String getPassword() { return password; }
	public void setPassword(String password) { this.password = password; }

	@Override
	public int compareTo(User u) {
		if (u.id != 0 && this.id != 0)return this.id - u.id;		
		return this.userName.compareTo(u.userName);
	}
	
	public String toString(){
		StringBuilder stb = new StringBuilder();
		stb.append("Username: ").append(this.userName);
		//stb.append("Password: ").append(this.password);
		stb.append("Access Type: ").append(accessLevel);
		return stb.toString();
	}
	

}
