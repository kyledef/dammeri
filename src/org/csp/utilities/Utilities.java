package org.csp.utilities;

import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.commons.codec.binary.Base64;

public class Utilities {

	public static String DateToString(Date d){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return dateFormat.format(d);		
	}
	
	public static Date StringToDate(String d) throws ParseException{
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return dateFormat.parse(d);
	} 
	
	/**
	 * Function used to perform the encryption for all passwords in the system.
	 * The application uses a SHA1 encryption algorithm
	 * @param str The value to be encrypted
	 * @return The resulting hash after algorithm is applied
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	public static String encrypt(String str) throws UnsupportedEncodingException, NoSuchAlgorithmException{
		MessageDigest md = MessageDigest.getInstance("SHA");
		md.update(str.getBytes("UTF-8"));
		str = Base64.encodeBase64String(md.digest());
		return str;
	}
	/**
	 * Used to initialize the common parameters of the table. This helps ensure uniformity among the tables
	 * @param table The table which the rules will be applied
	 */
	public static void setTableDefaults(JTable table){
		setTableDefaults(table, null);
	}
	
	/**
	 * Used to initialize the common parameters of the table. This helps ensure uniformity among the tables
	 * @param table The table which the rules will be applied
	 * @param evts is an array of action events that can be performed on the system
	 */
	public static void setTableDefaults(JTable table, final ActionEvent [] evts){
		table.setAutoCreateColumnsFromModel( true );
		table.setRowHeight( 25 );
		table.setShowVerticalLines( false );
		table.setShowHorizontalLines(true);
		table.setCellSelectionEnabled( false );
		table.setRowSelectionAllowed( true );	
		table.setAutoCreateRowSorter(true);
		table.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		table.setDefaultRenderer(Class.class, new DefaultTableCellRenderer(){
			private static final long serialVersionUID = -2883561376070724744L;
			{ setHorizontalAlignment( SwingConstants.CENTER ); }
		});
		
		if (evts != null && evts.length > 0){		
			table.addMouseListener( new MouseAdapter() {
				@Override
				public void mouseClicked( MouseEvent event ) {
					//Ensure user double clicks table
					if (evts.length == 2){
						if ( event.getClickCount() == 2){}
							
					}
					
				}	
			});		
		}
		
	}
}
