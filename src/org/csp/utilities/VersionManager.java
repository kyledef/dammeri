package org.csp.utilities;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class VersionManager {

	private String baseUrl;
	private String currentVersion;
	
	public VersionManager(String base_url){
		this.baseUrl = base_url;
	}
	public VersionManager(String baseUrl, String currentVersion) {
		super();
		this.baseUrl = baseUrl;
		this.currentVersion = currentVersion;
	}
	
	public String getCurrentVersion() {
		return currentVersion;
	}
	public void setCurrentVersion(String currentVersion) {
		this.currentVersion = currentVersion;
	}

	public String getBaseUrl() {
		return baseUrl;
	}
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	
	public boolean upgrade(){
		StringBuilder urlBuilder = new StringBuilder();
		urlBuilder.append(baseUrl);
		urlBuilder.append("version");
		try{
			String agent = "Mozilla/4.0";
	        String content_type = "application/x-www-form-urlencoded";
	        
			URL url = new URL(urlBuilder.toString());
			System.out.println(urlBuilder);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoInput(true);
			conn.setRequestMethod("GET");
			conn.setRequestProperty( "User-Agent", agent );
			conn.setRequestProperty( "Content-Type", content_type );
			conn.setReadTimeout(10000);
			conn.connect();
            if (conn.getResponseCode() == 200){//If successful the send message to the server
				String line;
				StringBuilder sb = new StringBuilder();				
				BufferedReader rd  = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				while ((line = rd.readLine()) != null)
					sb.append(line);
				line = sb.toString();
				
				if (line != null && line.length() > 0){
					System.out.println("Received: "+ line);
					if (line.compareTo(this.currentVersion) > 0){//upgrade Needed
			
					}
				}
            }else{
            	System.out.println(conn.getResponseCode());
            	return false;
            }
		}catch( Exception ex ){
			System.out.println("Unable to access the internet. writing to file: "+ ex.getMessage());
		}
		return true;
	}
	
	
}
