package org.csp.controller;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.csp.models.Member;
import org.csp.models.Model;
import org.csp.models.Organization;
import org.csp.models.Person;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

public class MemberController extends Controller<Member> {
	
	String [] personCols = {"firstName", "lastName", "sex","age","city","address"};
	private static MemberController instance = null;
	
	private MemberController(){
		super();
	}

	private MemberController(ConnectionSource conn) {
		super(conn);
	}
	public static MemberController getInstance(){ return getInstance(null); }
	public static MemberController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new MemberController(conn);
			else instance = new MemberController();
			System.out.println("Creating new instance of the Member Controller Class");
		}
		return instance;
	}

	@Override
	protected void init() {
		try {
			if (connectionSource != null) modelDAO = DaoManager.createDao(connectionSource, Member.class);
		} catch (SQLException e) {
			System.out.println("Failed to initialize User Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}

	@Override
	public String validate(Member m) {
		String msg = "ok";
		if (m == null)msg = "Member is required";
		else if (m.getOrganization() == null)msg = "Organization Required";
		else if (m.getPerson() == null)msg = "Person must be defined";
		else if (m.getRole() == null )msg = "Must defined role";
		return msg;
	}
	
	public Member[] getMembers(Organization o) {
		
		Member [] results = new Member[0];
		Collection<Member> members = null;
		if (o != null && o.getId() > 0){
			Member m = new Member();
			m.setOrganization(o);			
			try {members =  read(m);}
			catch (Exception e) { e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }	
		}else{
			members = getAll();
		}
		if (members != null)results = members.toArray(results);
		
		return results;	
	}
	
	public Member [] getMembers(){
		return getMembers(null);
	}
	
	public Collection<Member> read(String field, String value) throws Exception{
		ArrayList<Member> list = new ArrayList<Member>();
		//Check if the values entered belong to the person class
		if (java.util.Arrays.asList(personCols).indexOf(field) != -1){
			PersonController pc = PersonController.getInstance(this.connectionSource);
			Collection<Person> pcol = pc.read(field, value);
			if (pcol.size() < 1)return list; //Return ths empty list
			
			Iterator <Person> pi = pcol.iterator();
			while(pi.hasNext()){
				Member m = new Member();
				m.setPerson(pi.next());
				Collection<Member> mCol = this.read(m);
				if (mCol.size() > 0)list.addAll(mCol);
			}
		}else
			return super.read(field, value);
		return list;
	}
	
	public Collection<Member> read(Member m) throws Exception{		
		ArrayList<Member> list = new ArrayList<Member>();				
		boolean searched = false;
		
		if (m.getPerson() != null){
			Iterator <Person>pi = PersonController.getInstance(this.connectionSource).read(m.getPerson()).iterator();
			while(pi.hasNext()){
				m.setPerson(pi.next());
				list.addAll(super.read(m));
				searched = true;
			}
			if (!searched)list.addAll(super.read(m));
		}else{
			list.addAll(super.read(m));
		}
		
		return list;
	}
	
	public boolean delete(Member model, boolean deletePerson) throws Exception{
		if (super.delete(model)){
			if (deletePerson)PersonController.getInstance(this.connectionSource).delete(model.getPerson());
			return true;
		}
		return false;
	}
	
	public boolean delete(Member model) throws Exception{
		return this.delete(model, true);
	}
	
	private Organization getOrganization(String name) throws Exception{
		OrganizationController oc = OrganizationController.getInstance(this.connectionSource);
		Collection<Organization> ocol = oc.read("name", name);
		if (ocol.size() > 1)System.out.println("WARNING: More  than one organization returned");
		if (ocol.size() < 1)return null;
		return ocol.iterator().next();
	}

	public Member get(String s) {
		try{
			if (s != null && s.length() > 0){				
				String [] parts = s.split(":");
				Organization o = null;
				if (parts.length > 1)o = getOrganization(parts[1]);				
				parts = parts[0].split(" ");
				
				Member m = new Member();
				Person p = new Person();
				
				if (parts.length == 1){
					p.setFirstName(parts[0]);
				}else{
					p.setFirstName(parts[0]);
					p.setLastName(parts[1]);				
				}
				PersonController pc = PersonController.getInstance(this.connectionSource);
				Collection<Person> ps = pc.get(p);
				if (ps.size() > 0){
					if (ps.size() > 1)System.out.println("WARNING: More than one person record returned");
					p = ps.iterator().next();
				}else{
					System.out.println("Unable to find: "+p);
					return null;
				}
				m.setPerson(p);
				if (o != null)m.setOrganization(o);
				Collection<Member> ms = this.get(m);
				if (ms.size() > 0){
					if (ms.size() > 1)System.out.println("Warning: More than one member record returned");
					return ms.iterator().next();
				}
			}else{
				if (s == null)System.out.println("Null passed");
				else System.out.println("Empty String passed: "+s);
			}
		}catch(Exception e){
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
		return null;
	}

	public String [] getRoles() {	return getRoles(null); }
	public String [] getRoles(Organization organization) {
		String [] roles = new String [0];
		try{
			ArrayList <String> resultRoles = new ArrayList<String>();
			QueryBuilder <Member, Integer> qBuilder = this.modelDAO.queryBuilder();
			if (organization != null)qBuilder.where().eq("organization", organization.getId());
			qBuilder.distinct().groupBy("role");
			
			List <Member>l = modelDAO.query(qBuilder.prepare());
			Iterator <Member> i = l.iterator();
			
			boolean foundOther = false;
			while (i.hasNext()){
				Member temp = i.next();
				if (temp.getRole().compareToIgnoreCase("OTHER") == 0)foundOther = true;
				resultRoles.add(temp.getRole());
			}
			
			if (!foundOther)resultRoles.add("OTHER");
			return resultRoles.toArray(roles);
		}catch(Exception e){	e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
		return roles;
	}
	
	public Collection<Member> find(Member m)throws Exception{	
		ArrayList<Member> list = new ArrayList<Member>();				
		boolean searched = false;
		Person p = m.getPerson();
		
		if (p != null && (p.getFirstName() != null || p.getLastName() != null || p.getSex() != null) ){
			Iterator <Person>pi = PersonController.getInstance(this.connectionSource).find(p).iterator();
			while(pi.hasNext()){
				m.setPerson(pi.next());
				list.addAll(super.find(m));
				searched = true;
			}
			if (!searched) list.addAll(super.find(m));
		}else
			list.addAll(super.find(m));
		return list;
	}
	
	public Map<String, Object> extractField(Member model) throws Exception{
		Map<String, Object> cohortMap = new HashMap<String, Object>();
		Field [] aClassFields = model.getClass().getDeclaredFields();
		for(Field f : aClassFields){
			f.setAccessible(true);
			Object fObj = f.get(model);
			if (fObj != null && (fObj instanceof ForeignCollection) == false && !f.getName().equalsIgnoreCase("serialVersionUID")){				
				if (fObj instanceof Model){
					if (fObj instanceof Person){
						Person p = (Person)fObj;
						cohortMap.put("first name", p.getFirstName());
						cohortMap.put("last name", p.getLastName());
						cohortMap.put("phone",p.getPhone() );
						cohortMap.put("email",p.getEmail());
						cohortMap.put("address",p.getAddress());
					}
					else if (fObj instanceof Organization){
						Organization o = (Organization)fObj;
						cohortMap.put("organization", o.getName());
					}
					else
						cohortMap.put(f.getName(), ((Model)fObj).getId());
				}else{
					cohortMap.put(f.getName(), fObj);
				}
			}else if (fObj instanceof ForeignCollection){
				
			}
		}
		return cohortMap;
	}

	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}
}
