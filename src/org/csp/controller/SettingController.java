package org.csp.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.csp.main.FileMsgWriter;
import org.csp.main.ServerCommunication;
import org.csp.models.Organization;
import org.csp.models.Setting;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

public class SettingController extends Controller<Setting>{

	private String DEFAULT_LOC = "./data/";
	private String SETTING_LOC = "./settings/";
	private String SETTING_FNAME = "config";
	private String UPDATE_FNAME = "update";
	private String DEFAULT_SVR_LOC = "http://kyledef.org/dammeri/index.php/";
	private String DEFAULT_UPDATE_LOC="http://kyledef.org/dammeri/bin/";
	private boolean DEFAULT_LOGIN = true;
	
	
	public static String DEFAULT_NAME = "DAMMERI";
	
	@SuppressWarnings("unused")
	private boolean user_changed = false;
	private boolean settingLoaded = true;
	private Properties prop;	
	private Properties updateProp;
	private boolean useLogin = DEFAULT_LOGIN;		
	private String db_path = DEFAULT_LOC;
	private String className = "org.sqlite.JDBC";
	
	private static SettingController instance = null;
	private SettingController(){ super(); }
	private SettingController(ConnectionSource conn){ super(conn); }
	public static SettingController getInstance(){return SettingController.getInstance(null); }
	public static SettingController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new SettingController(conn);
			else instance = new SettingController();
			System.out.println("Creating new instance of the Setting Controller Class");
		}
		return instance;
	}
	
	protected void createConnection(){
		if (prop != null){
			url = prop.getProperty("db_connector") +  prop.getProperty("db_path") + prop.getProperty("db_name");
			File dbFile = new File(prop.getProperty("db_path") + prop.getProperty("db_name"));
			if (!dbFile.exists()) createDatabase();				
		}
		super.createConnection();
	}
	
	@Override
	protected void init() {
		try {
			if (connectionSource != null) modelDAO = DaoManager.createDao(connectionSource, Setting.class);				
			else System.out.println("Warning: No Database connection Established");
		} catch (SQLException e) {
			System.out.println("Unable to create Setting controller: "+e.getMessage());
			//e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}
	
	//Reinitialize the controller. Useful for setting needed properties of the controller after instance created.
	//This arose because of the need to create the connection after the class creates the database
	public void reinitialize(){
		if (url == null || url.equals(""))url = dbConnector + dbPath;
		try {
			connectionSource = new JdbcConnectionSource(url);
			modelDAO = DaoManager.createDao(connectionSource, Setting.class);
		} catch (SQLException e) {			
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}

	@Override
	public String validate(Setting t) {
		String msg = "ok";
		if (t == null)msg = "Must specify a setting to be saved";
		if (t.getSetting() == null || t.getSetting().equals(""))msg = "Must specify a setting to be saved";
		return msg;
	}
	
	public boolean createSettingFolder(){
		File file = new File(SETTING_LOC);
		return file.mkdir();		
	}
	
	public void createDefaultsSettings(){
		try {
			File folder = new File(SETTING_LOC);
			if (!folder.exists())createSettingFolder();		
			
			prop = new Properties();
			prop.setProperty("db_path", DEFAULT_LOC);
			prop.setProperty("use_splash", "true");
			prop.setProperty("db_name", "dimmeri.db");
			prop.setProperty("db_connector", "jdbc:sqlite:");
			prop.setProperty("use_login", String.valueOf(DEFAULT_LOGIN));
			prop.setProperty("default_org", "1");
			prop.setProperty("use_telemetry", "true");
			prop.setProperty("transmit_telemetry", "true");
			prop.setProperty("server_location",DEFAULT_SVR_LOC);
			prop.setProperty("update_location", DEFAULT_UPDATE_LOC);
			prop.setProperty("machine_id", this.createMachineID());
			prop.setProperty("first_start_dialog", "true");
		
			prop.store(new FileOutputStream(SETTING_LOC+SETTING_FNAME), "Defaults");
			this.settingLoaded = true;
		} catch (IOException e) { System.out.println("Unable to load settings check file system privileges"); }
	}
	
	public void loadSettings(){
		try {	
			prop = new Properties();	
			prop.load(new FileInputStream(SETTING_LOC + SETTING_FNAME));
			this.settingLoaded = true;
			
			useLogin = Boolean.valueOf(prop.getProperty("use_login", String.valueOf(DEFAULT_LOGIN)));
			db_path = prop.getProperty("db_path", DEFAULT_LOC);			
		
		} catch (FileNotFoundException e) {		
			System.out.println("No settings found. Loading and saving defaults");
			createDefaultsSettings();//Didn't find the settings so we create the default
		} catch (IOException e) {
			System.out.println("Unable to load settings");
			System.exit(1);
		}		
	}
	
	public void saveSettings(){
		try {
			File folder = new File(SETTING_LOC);
			if (!folder.exists())createSettingFolder();	
			
			prop.store(new FileOutputStream(SETTING_LOC+SETTING_FNAME), "User Saved");
			
		} catch (IOException e) { System.out.println("Unable to load settings check file system privileges"); }
	}
	
	public boolean isDatabaseExist(){
		if (prop == null)this.loadSettings();
		File dbFolder = new File(prop.getProperty("db_path"));
		File dbFile = new File(prop.getProperty("db_path")+prop.getProperty("db_name"));
		return dbFolder.exists() && dbFile.exists();
	}
	
	
	public boolean createDatabase(){
		System.out.println("Creating Database");
		if (prop == null)this.loadSettings();
		
		File dbFolder = new File(prop.getProperty("db_path") );
		if (!dbFolder.exists())dbFolder.mkdir();		
		
		File dbFile = new File(prop.getProperty("db_path") + prop.getProperty("db_name"));
		if (dbFile.exists()){
			System.out.println("Database exist. Recreating");
			closeDatabase();
			
			if (!dbFile.delete()){ // Remove database if existing
				System.out.println("Unable to delete Database file");
				return false;
			}
		}
		
		try {
			Class.forName(className);
			Connection connection = DriverManager.getConnection(url);	
			Statement stmt = connection.createStatement();
			
			URL sqlURL = this.getClass().getClassLoader().getResource("res/db.sql");
			
			System.out.println(sqlURL.getPath());
			
			BufferedReader in = new BufferedReader(new InputStreamReader(sqlURL.openStream()));
			StringBuilder stbuilder = new StringBuilder();
			String line;
			while ((line = in.readLine()) != null)stbuilder.append(line);			
			stmt.executeUpdate(stbuilder.toString());
			
			stmt.close();
			connection.close();
			
			System.out.println("Database created successfully");
			return true;
		} catch (Exception e) {
			System.out.println("Unable to create database: ");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
		return false;
	}
	
	public boolean closeDatabase(){
		try{
			Class.forName(className);
			Connection connection = DriverManager.getConnection(url);	
			connection.close();
			if (this.connectionSource != null)this.connectionSource.close();
			
			return true;
		}catch(Exception e){
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
		return false;
	}
	
	public boolean recreateDatabase(){
		this.closeDatabase();		
		return this.createDatabase();
	}
	
	public boolean useSplash(){	return Boolean.valueOf(prop.getProperty("use_splash", "true")); }
	public void setSplash(boolean splash){ prop.setProperty("use_splash", String.valueOf(splash)); }
		
	public boolean isSettingLoaded() { return settingLoaded; }
	public void setSettingLoaded(boolean settingLoaded) { this.settingLoaded = settingLoaded; }
	
	public String getDBLocation(){ return this.db_path; }
	public void setDBLocation(String location){ this.db_path = location; }
	
	public String getBaseURL(){
		if (this.prop == null)this.loadSettings();
		return this.prop.getProperty("server_location",DEFAULT_SVR_LOC);
	}
	
	public boolean useFirstDialog(){
		if (this.prop == null)this.loadSettings();
		return Boolean.parseBoolean(this.prop.getProperty("first_start_dialog","true"));
	}
	
	public void setFirstDialog(boolean firstDialog){
		if (this.prop == null)this.loadSettings();
		this.prop.setProperty("first_start_dialog", Boolean.toString(firstDialog));
		this.saveSettings();
	}
	
	public Properties getProp() { if (prop == null) this.loadSettings(); return prop; }
	public void setProp(Properties prop) { this.prop = prop; }
	public boolean useLogin() {	 return useLogin; }	
	public void setLogin(boolean login){ useLogin = login; }
		
	public Organization getDefaultOrganization() {
		try {
			int id = Integer.valueOf(prop.getProperty("default_org", "1"));
			return OrganizationController.getInstance().read(id);
		} catch (SQLException e) { e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); return null; }
	}
	
	/**
	 * Separating the section because of future consideration of a separate class
	 * 
	 */
	double desiredVersion = 1.7;
	double actualVersion = 0;
	String operatingSystem = "";
	String architecture = "";
	
	public final String appVersion = "0.11";
	
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}

	public final static int SVR_ENVI_MSG 	= 1;		//Send Environmental Information
	public final static int SVR_ERR_MSG 	= 2;		//Send Error Messages
	public final static int SVR_DBG_MSG 	= 3;		//Send Debugging Messages
	public final static int SVR_INFO_MSG 	= 4;		//Send Information for General purposes
	public final static int SVR_PERF_MSG 	= 5;		//Send Performance data
	public final static int SVR_FEED_MSG 	= 6;		//Send Feedback Information
	public final static int SVR_VER_MSG		= 7;		//Retrieve Application Version
	
	
	public final static int FILE_DEBUG_MSG 	= 1;
	public final static int FILE_TRANS_MSG 	= 2;
	public final static int FILE_BCK_MSG 	= 3;
	
	public final static String FILE_TRANS_NAME = "settings/failed_transmission.txt";
	public final static String FILE_DEBUG_NAME = "settings/debug_messages.txt";
	public final static String FILE_BCK_NAME = "data/backup";
	
	public double getDesiredVersion() {
		return desiredVersion;
	}
	
	public double getLatestVersion(){
		try{
			ServerCommunication sc = new ServerCommunication();		
			URLConnection conn = sc.getServerConnection(SettingController.SVR_VER_MSG);
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String inputLine;
			inputLine = in.readLine();
			if (inputLine != null){
				return Double.parseDouble(inputLine);
			}
		}catch(Exception e){}
		return -1;
	}
	public double getAppVersion() {
		return Double.parseDouble(appVersion);
	}
	
	public String getOS() {
		return operatingSystem;
	}

	public boolean isNeedUpdate(){
		return this.getLatestVersion() > this.getAppVersion();
	}
	
	public String getUpdateLocation(){
		if (this.prop == null)this.loadSettings();
		return this.prop.getProperty("update_location", DEFAULT_UPDATE_LOC);
	}
	
	//Defining some additional features
	public void determineEnvironment(){
		String version = System.getProperty("java.version"),
				os = System.getProperty("os.name"),
				vmName = System.getProperty("java.vm.name");
		
		//Determine Operating System
		if (vmName != null && vmName.equalsIgnoreCase("dalvik")){
			operatingSystem = "android";
		}else if (os.equalsIgnoreCase("windows 8") || os.equalsIgnoreCase("windows 7")){
			operatingSystem = "windows";
		}else if (os.equalsIgnoreCase("linux")){
			operatingSystem = "linux";			
		}
		
		//Determine Architecture
		this.architecture = System.getProperty("os.arch");
		
		//Determine version
		if (version != null){
			String [] parts = version.split("_");
			if (parts.length > 1){
				version = parts[0];
			}
			
			parts = version.split("\\.");
			version = parts[0] + "." + parts[1];			
			actualVersion = Double.parseDouble(version);
			if (actualVersion < desiredVersion){
				System.out.println("Need to upgrade java");
			}
		}
		
		StringBuilder stb = new StringBuilder();
		stb.append(this.operatingSystem).append("\t")
			.append(this.architecture).append("\t")
			.append(this.actualVersion).append("\t")
			.append(this.getCurrentTimeStamp()).append("\t");
		
		this.sendToServer(stb.toString(), 1);
	}
	
	public void sendToServer(String message, int type){
		(new Thread(new ServerCommunication(message, type))).start();
	}
	
	public void getFromServer(String message, int type){
		(new Thread(new ServerCommunication(message, type))).start();
	}
	
	public void writeToFile(String message, int type){		
		(new Thread(new FileMsgWriter(message, type))).start();	
	}
	
	public void attemptRetransmission(){
		File file = new File(FILE_TRANS_NAME);
		if (file.exists()){
			//TODO Complete the attempt retransmission functionality
		}
	}
	
	public boolean setAppVersion2File(){
		try{
			updateProp = new Properties();
			updateProp.load(new FileInputStream(SETTING_LOC + UPDATE_FNAME));
			updateProp.setProperty("currentVersion", this.appVersion);
			this.saveSettings();
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
		return false;
	}
	
	public String getCurrentTimeStamp(){
		Date date= new Date();
		Timestamp ts = new Timestamp(date.getTime());
		return ts.toString();
	}
	
	public String getMachineID(){
		if (this.prop.getProperty("machine_id") != null)
			return this.prop.getProperty("machine_id");
		String id = this.createMachineID();
		this.prop.setProperty("machine_id", id);
		return id;
	}
	
	public String createMachineID(){
		UUID idOne = UUID.randomUUID();
		return idOne.toString();
	}
	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}
}
