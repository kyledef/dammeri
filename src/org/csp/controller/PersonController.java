package org.csp.controller;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.validator.EmailValidator;
import org.csp.models.Person;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

public class PersonController  extends Controller<Person>{
	
	private static PersonController instance = null;

	private PersonController() { super(); }
	private PersonController(ConnectionSource conn) { super(conn); }
		
	public static PersonController getInstance(){ return getInstance(null); }
	public static PersonController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new PersonController(conn);
			else instance = new PersonController();
			System.out.println("Creating new instance of the Person Controller Class");
		}
		return instance;
	}

	
	protected void init(){
		try {
			if (connectionSource != null) modelDAO = DaoManager.createDao(connectionSource, Person.class);
		} catch (SQLException e) {
			System.out.println("Failed to initialize User Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}
	
	public String validate(Person p){
		String message = "ok";		
		if (p == null)message = "Person is required";
		else if (p.getFirstName() == null || "".equals(p.getFirstName()))message ="First Name Required";
		else if (p.getLastName()  == null || "".equals(p.getLastName()))message  ="Last Name Required";
		else if (p.getSex() == null || "".equals(p.getSex()))message = "Sex is required";
		//Name validation for purpose of system functionality
		else if (p.getFirstName().split(" ").length > 1)message = "First Name cannot have space. Use '-' or Capitals to differentiate words";
		else if (p.getLastName().split(" ").length > 1)message 	= "Last Name cannot have space. Use '-' or Capitals to differentiate words";
		else if (p.getEmail() != null)
			if (!EmailValidator.getInstance().isValid(p.getEmail()))message = "Format for email not valid";
		
		return message;
	}
	
	public Collection<Person> get(String fullname){
		ArrayList<Person> persons = new ArrayList<Person>();
		try{
			Person p = new Person();
			String [] parts = fullname.split(" ");
			if (parts.length == 1){
				p.setFirstName(parts[0]);
			}else if (parts.length >= 2){
				p.setFirstName(parts[0]);
				p.setLastName(parts[1]);
			}
			persons.addAll(this.read(p));
			
		}
		catch(Exception e){
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
		return persons;
	}
	
	
	public boolean isPersonExist(Person p){
		QueryBuilder<Person, Integer> queryBuilder = modelDAO.queryBuilder();
		try {
			queryBuilder.where().like("firstName", p.getFirstName()).and().like("lastName", p.getLastName());
			List<Person> list = this.modelDAO.query(queryBuilder.prepare());
			if (list.size() > 0)return true;
		} catch (SQLException e) {
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
		
		return false;
	}
	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		
		Person p = new Person();
		try{
			Iterator <String> i = keys.iterator();
			while (i.hasNext()){
				String f = i.next();
				if (f.equalsIgnoreCase("id")){
					p.setId(Integer.parseInt(modelMap.get(f)));
				}else{
					Field field = p.getClass().getDeclaredField(f);
					field.setAccessible(true);
					field.set(p, modelMap.get(f));
				}
			}
			this.save(p);
			
			return true;
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
		return false;
	}
		
	
	
}
