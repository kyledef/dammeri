package org.csp.controller;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import org.csp.models.Objective;
import org.csp.models.Project;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

public class ObjectiveController extends Controller<Objective> {

	private static ObjectiveController instance = null;

	private ObjectiveController(){
		super();
	}	
	
	private ObjectiveController(ConnectionSource conn) {
		super(conn);
	}

	public static ObjectiveController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new ObjectiveController(conn);
			else instance = new ObjectiveController();
			System.out.println("Creating new instance of the Objective Class");
		}
		return instance;
	}
	public static ObjectiveController getInstance(){
		return getInstance(null);
	}

	@Override
	protected void init() {
		try {			
			modelDAO = DaoManager.createDao(connectionSource, Objective.class);
		} catch (SQLException e) {
			System.out.println("Failed to initialize User Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}

	@Override
	public String validate(Objective t) {
		String msg = "ok";	
		if(t == null)msg = "Objective is required";
		else if (t.getProject() == null)msg = "Objective must have a specified project";
		else if (t.getObjective() == null || t.getObjective() == "")msg = "Objective must be given a title";
		return msg;
	}

	
	
	public Objective get(String value){
		//TODO REVISIT acurracy of results returned
		try{
			String [] parts = value.split(":");
			if (parts.length > 1){
				Collection<Project> pcol = ProjectController.getInstance().read("name", parts[1]);
				if (pcol.size() > 0){
					Objective o = new Objective();
					o.setProject(pcol.iterator().next());
					o.setObjective(parts[0]);
					Collection<Objective> ocol = this.read(o);
					if (ocol.size() > 0)return ocol.iterator().next();
				}
			}else{
				Collection<Objective> res = this.read("title", parts[0]);
				if (res.size() > 0)
					return res.iterator().next();
			}
		}catch(Exception e){
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
		return null;
	}
	
	public Objective [] getObjectives(){
		Collection<Objective> col = this.getAll();
		Iterator<Objective> io = col.iterator();
		int i =0; 
		Objective [] objs = new Objective[col.size()];
		
		while (io.hasNext()){
			objs[i++] = io.next();
		}
		return objs;
	}

	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}
}
