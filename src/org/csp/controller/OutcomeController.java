package org.csp.controller;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import org.csp.models.Outcome;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

public class OutcomeController extends Controller<Outcome> {

	private static OutcomeController instance = null;

	private OutcomeController(){ super(); }
	private OutcomeController(ConnectionSource conn) { super(conn); }
	
	public static OutcomeController getInstance(){ return getInstance(null); }
	public static OutcomeController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new OutcomeController(conn);
			else instance = new OutcomeController();
			System.out.println("Creating new instance of the Outcome Controller Class");
		}
		return instance;
	}
	
	@Override
	protected void init() {
		try{
			this.modelDAO = DaoManager.createDao(connectionSource, Outcome.class);
		}catch (SQLException e){
			System.out.println("Failed to initialize User Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}

	@Override
	public String validate(Outcome t) {
		String msg = "ok";
		if ( t == null)msg = "must specify the object to be saved";
		return msg;
	}
	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}

}
