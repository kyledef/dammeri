package org.csp.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.csp.models.Cohort;
import org.csp.models.Input;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

public class InputController extends Controller<Input> {

	private static InputController instance = null;

	private InputController(){ super(); }
	private InputController(ConnectionSource conn) { super(conn); }
	
	public static InputController getInstance(){ return getInstance(null); }
	public static InputController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new InputController(conn);
			else instance = new InputController();
			System.out.println("Creating new instance of the Input Controller Class");
		}
		return instance;
	}
	
	@Override
	protected void init() {
		try{
			if (connectionSource != null) this.modelDAO = DaoManager.createDao(connectionSource, Input.class);
		}catch (SQLException e){
			System.out.println("Failed to initialize User Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}

	@Override
	public String validate(Input t) {
		String msg = "ok";
		if (t == null)msg = "No input received";
		else if (t.getInput() == null || t.getInput().equals(""))msg = "Must give description of the input";
		return msg;
	}
	
	public Collection<Input>get(Cohort c){
		Collection<Input> list = new ArrayList<Input>();
		Input i = new Input();
		i.setCohort(c);
		try {
			list =  this.modelDAO.queryForMatchingArgs(i);
			System.out.println(list.size());
		} catch (SQLException e) { e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
		return list;
	}
	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}

}
