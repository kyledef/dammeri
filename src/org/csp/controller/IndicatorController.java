package org.csp.controller;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import org.csp.models.Indicator;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

public class IndicatorController extends Controller<Indicator> {

	private static IndicatorController instance = null;

	private IndicatorController(){ super(); }
	private IndicatorController(ConnectionSource conn) { super(conn); }
	
	public static IndicatorController getInstance(){ return getInstance(null); }
	public static IndicatorController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new IndicatorController(conn);
			else instance = new IndicatorController();
			System.out.println("Creating new instance of the Indicator Controller Class");
		}
		return instance;
	}
	
	@Override
	protected void init() {
		try{
			if (connectionSource != null) this.modelDAO = DaoManager.createDao(connectionSource, Indicator.class);
		}catch (SQLException e){
			System.out.println("Failed to initialize User Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}

	@Override
	public String validate(Indicator t) {
		String msg = "ok";
		if (t == null)msg = "Must specify indicator";
		return msg;
	}
	
	public Indicator get(String s) {
		String [] args = s.split(":");
		try{
			if (args.length > 1){
				int id = Integer.parseInt(args[0]);
				System.out.println(id);
				return this.modelDAO.queryForId(id);
			}
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
		try{
			if (args.length == 1){
				QueryBuilder<Indicator, Integer> qb = this.modelDAO.queryBuilder();
				qb.where().eq("indicator", s);
				Collection<Indicator> ins = this.modelDAO.query(qb.prepare());
				if (ins.size() > 0)return ins.iterator().next();
				return null;
			}
		}catch(Exception e){ e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
		return null;
	}
	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}

}
