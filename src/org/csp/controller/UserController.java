package org.csp.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.csp.models.User;
import org.csp.models.UserLog;
import org.csp.utilities.Utilities;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

public class UserController extends Controller<User>{
	
	/* List of User Access Levels */
	public static String ADMIN 		= "ADMIN";
	public static String PRIVILEGED = "PRIVILEGED";
	public static String VIEWER 	= "VIEWER";
	
	/* List of Available User Actions */
	public static String LOGIN 	= "LOGIN";
	public static String LOGOUT = "LOGOUT";
	public static String CREATE = "CREATE";
	public static String UPDATE = "UPDATE";
	public static String DELETE = "DELETE";
	
	private User loggedInUser = null;
	private Dao <UserLog, Integer> userLogDao;
	
	private static UserController instance = null;
	
	private UserController() { super(); }
	private UserController(ConnectionSource conn) { super(conn); }
	public static UserController getInstance(){ return getInstance(null);}
	public static UserController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new UserController(conn);
			else instance = new UserController();
			System.out.println("Creating new instance of the UserController Class");
		}
		return instance;
	}
	
	protected void init(){
		try {
			modelDAO = DaoManager.createDao(connectionSource, User.class);
			userLogDao = DaoManager.createDao(connectionSource, UserLog.class);
		} catch (SQLException e) {
			System.out.println("Failed to initialize User Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}
	
	public String validate(User user){
		String message = "ok";
		if ( user == null ) 
			message = "User is required" ;		
		else if (user.getUserName() == null || "".equals(user.getUserName()))
			message =  "User must have a valid username";
		else if (user.getPassword() == null || "".equals(user.getPassword()))
			message = "User must have a valid password";
		else if (user.getUserName() != null && user.getUserName().length() < 5)		//Validate the username by length TODO ensure username is unique
			message = "Username must consist of at least 5 characters";
		else if (user.getPassword() != null && user.getPassword().length() < 8)		//Validate Password TODO Increase password strength
			message = "Password must consist of at least 8 characters";
		
		return message;
	}
	
	public boolean authenticate(User u){
		try {
			Collection<User> colu = this.read("userName", u.getUserName());
			String encrypted = Utilities.encrypt(u.getPassword());					//Apply encryption to user password
			u.setPassword(encrypted);
			
			if (colu.size() > 0){
				Iterator<User> i = colu.iterator();
				while (i.hasNext()){
					User us = i.next();
					if (us.getPassword().equals(u.getPassword())){
						this.loggedInUser = us;
						return true;
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Error occured while logging in: "+e.getMessage());
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
		return false;
	}
	
	public User getUser(User u){
		String username = u.getUserName();
		try{
			if (username != null || !("".equals(username))){
				Collection <User> uCol = this.read("userName", username);
				if (uCol.size() > 0)return uCol.iterator().next();
			}
		}catch (Exception e){
			System.out.println("Unable to read User");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
		return null;
	}
	
	public User getLoggedInUser() { return loggedInUser; }
	public void setLoggedInUser(User loggedInUser) { this.loggedInUser = loggedInUser; }
	
	public String getUserAccessLevel(){
		String type = null;
		if (this.loggedInUser != null)type = this.loggedInUser.getAccessLevel();
		if (type == null)type = VIEWER;
		return type;
	}	
	
	public boolean isReadOnly(User u){
		if (u == null) u = this.loggedInUser;
		if (u == null)return true;
		if (u.getAccessLevel().equals(PRIVILEGED) || u.getAccessLevel().equals(ADMIN))return false;
		return true;
	}
	
	public boolean isReadOnly(){
		return this.isReadOnly(null);
	}
	
	public boolean isAdmin(User u){
		if (u == null)u = this.loggedInUser;		
		if (u == null)return false;
		return u.getAccessLevel().equals(ADMIN);
	}
	
	public boolean isAdmin(){
		return isAdmin(null);
	}
	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean registerUserEvent(User u, String action, String info){
		try{
			UserLog ul = new UserLog(u, new Date(),action, info  );
			this.userLogDao.createIfNotExists(ul);
			return true;
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
		return false;
	}
	
	public boolean registerUserEvent(String action, String info){
		return this.registerUserEvent(this.loggedInUser, action, info);
	}
	
	public Collection<UserLog> listUserEvents(User u){
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -30);		
		return this.listUserEvents(u, cal.getTime(), new Date());
	}
	public Collection<UserLog> listUserEvents(User u, Date start, Date end){
		Collection<UserLog> list = new ArrayList<UserLog>();
		try{
			QueryBuilder<UserLog, Integer> qb = this.userLogDao.queryBuilder();
			qb.where().eq("user", u).and().between("date", start, end);
			list = this.userLogDao.query(qb.prepare());
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
		return list;
	}
}