package org.csp.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.csp.models.Project;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

public class ProjectController extends Controller<Project>{
	
	private static ProjectController instance = null;

	private ProjectController(){
		super();
	}
	
	
	private ProjectController(ConnectionSource conn) {
		super(conn);
	}
	public static ProjectController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new ProjectController(conn);
			else instance = new ProjectController();
			System.out.println("Creating new instance of the Project Controller Class");
		}
		return instance;
	}
	public static ProjectController getInstance(){
		return getInstance(null);
	}

	protected void init(){
		try {
//			connectionSource = new JdbcConnectionSource(url);
			if (connectionSource != null) modelDAO = DaoManager.createDao(connectionSource, Project.class);
		} catch (SQLException e) {
			System.out.println("Failed to initialize Project Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}
	
	public String validate(Project p){
		String message = "ok";
		if (p == null)message = "Project is required";
		else if (p.getName() == null || "".equals(p.getName()))message = "Name of project must be defined";
		return message;
	}	
	public Collection<Project> find(Project t) throws SQLException{
		Collection<Project> list = new ArrayList<Project>();
		QueryBuilder <Project, Integer> qb = this.modelDAO.queryBuilder();
		qb.where().like("name", "%"+t.getName()+ "%");
		list = this.modelDAO.query(qb.prepare());
		return list;
	}


	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}
}
