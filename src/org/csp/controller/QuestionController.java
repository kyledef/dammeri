package org.csp.controller;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import org.csp.models.Question;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

public class QuestionController extends Controller<Question>{

//	Dao<Indicator, Integer> indicatorDAO;	
	private static QuestionController instance = null;
	
	private QuestionController() {
		super();
	}

	private QuestionController(ConnectionSource conn) {
		super(conn);
	}
	
	public static QuestionController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new QuestionController(conn);
			else instance = new QuestionController();
			System.out.println("Creating new instance of the Question Controller Class");
		}
		return instance;
	}
	public static QuestionController getInstance(){
		return getInstance(null);
	}
	
	@Override
	protected void init() {
		try{
			if (connectionSource != null) {
				this.modelDAO = DaoManager.createDao(this.connectionSource, Question.class);
//				this.indicatorDAO = DaoManager.createDao(this.connectionSource, Indicator.class);
			}
		}catch(Exception e){
			System.out.println("Unable to initialise Question Controller: "+e.getMessage());
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}

	@Override
	public String validate(Question t) {
		String msg = "ok";
		if (t == null)msg = "Must specify a question";
		else if (t.getQuestion() == null || t.getQuestion().equals(""))msg = "must pass a questions";
		return msg;
	}
	
	public Question get(String s){
		Question q = null;
		QueryBuilder <Question, Integer>builder = this.modelDAO.queryBuilder();
		try {
			Collection <Question> col = this.modelDAO.query(builder.where().like("question", s).prepare());
			if (col.size() > 0){
				if (col.size() > 1)System.out.println("More than one result was returned in Question");
				return col.iterator().next();
			}
		} catch (SQLException e) {
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
		return q;		
	}

	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}

//	public Indicator addIndicator(Indicator i, Question q)throws Exception{
//		if (q.getId() == 0)q = this.modelDAO.createIfNotExists(q);		
//		i.setQuestion(q);		
//		return this.addIndicator(i);
//	}
//	
//	public Indicator addIndicator(Indicator i) throws Exception{
//		if (i.getQuestion() == null)return null;
//		if (i.getId() != 0)indicatorDAO.update(i);
//		else i = indicatorDAO.createIfNotExists(i);
//		return i;
//	}
//	
//	public Collection<Indicator> getIndicators(Question q) throws Exception{
//		if (q.getId() == 0)return null;
//		q = this.refreshModel(q);
//		return null;
//	}

	
}
