package org.csp.controller;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import org.csp.models.QuestionOutput;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

public class QuestionOutputController extends Controller<QuestionOutput>{

	private static QuestionOutputController instance = null;
	private QuestionOutputController(){ super(); }	
	private QuestionOutputController(ConnectionSource conn) { super(conn);	}
	public static QuestionOutputController getInstance(){ return getInstance(null); }
	public static QuestionOutputController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new QuestionOutputController(conn);
			else instance = new QuestionOutputController();
			System.out.println("Creating new instance of the QuestionOutput Controller Class");
		}
		return instance;
	}
	
	@Override
	protected void init() {
		try {
			if (connectionSource != null) modelDAO = DaoManager.createDao(connectionSource, QuestionOutput.class);
		} catch (SQLException e) {
			System.out.println("Failed to initialize QuestionOutput Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}

	@Override
	public String validate(QuestionOutput t) {
		String message = "ok";
		if (t.getQuestion() == null || t.getQuestion().getId() == 0)message="no question specified";
		if (t.getOutput() == null || t.getOutput().getId() == 0) message="no output spectified";
		return message;
	}
	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}

}
