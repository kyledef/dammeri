package org.csp.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.csp.models.Field;
import org.csp.models.Indicator;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

public class FieldController extends Controller<Field> {

	private static FieldController instance = null;
	
	private FieldController(){ super(); }
	private FieldController(ConnectionSource conn) { super(conn); }
	
	public static FieldController getInstance(){ return getInstance(null); }
	public static FieldController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new FieldController(conn);
			else instance = new FieldController();
			System.out.println("Creating new instance of the Indicator Controller Class");
		}
		return instance;
	}
	
	@Override
	protected void init() {
		try{
			if (connectionSource != null) this.modelDAO = DaoManager.createDao(connectionSource, Field.class);
		}catch (SQLException e){
			System.out.println("Failed to initialize User Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}

	}

	@Override
	public String validate(Field t) {
		String msg = "ok";
		if (t == null || t.getValue() == null)msg = "must specify the value to be saved";
		return msg;
	}
	
	public Collection<Field> get(Indicator i){
		Collection<Field> list = new ArrayList<Field>();
		try{
			QueryBuilder<Field, Integer> qb = this.modelDAO.queryBuilder();
			qb.where().eq("indicator", i);
			list = this.modelDAO.query(qb.prepare());
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
		return list;
	}
	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}

}
