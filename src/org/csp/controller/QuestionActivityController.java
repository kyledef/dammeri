package org.csp.controller;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import org.csp.models.QuestionActivity;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

public class QuestionActivityController extends Controller<QuestionActivity>{

	private static QuestionActivityController instance = null;
	private QuestionActivityController(){ super(); }	
	private QuestionActivityController(ConnectionSource conn) { super(conn);	}
	public static QuestionActivityController getInstance(){ return getInstance(null); }
	public static QuestionActivityController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new QuestionActivityController(conn);
			else instance = new QuestionActivityController();
			System.out.println("Creating new instance of the QuestionActivity Controller Class");
		}
		return instance;
	}
	
	@Override
	protected void init() {
		try {
			if (connectionSource != null) modelDAO = DaoManager.createDao(connectionSource, QuestionActivity.class);
		} catch (SQLException e) {
			System.out.println("Failed to initialize QuestionActivity Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}

	@Override
	public String validate(QuestionActivity t) {
		String message = "ok";
		if (t.getQuestion() == null || t.getQuestion().getId() == 0)message="no question specified";
		if (t.getActivity() == null || t.getActivity().getId() == 0) message="no activity spectified";
		return message;
	}
	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}

}
