package org.csp.controller;

import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.csp.models.Model;
import org.supercsv.io.CsvMapReader;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.io.ICsvMapReader;
import org.supercsv.io.ICsvMapWriter;
import org.supercsv.prefs.CsvPreference;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableInfo;

public abstract class Controller<T> {
	
	
	public static final int DEFAULT_LIMIT = 20;
	public static final int DEFAULT_OFFSET = 0;
	
	
	protected Dao<T, Integer> modelDAO = null;
	
	protected String dbPath = "./data/dimmeri.db";
	protected String dbConnector = "jdbc:sqlite:";
	protected String url = "";
	
	protected ConnectionSource  connectionSource;
	protected TableInfo<T, Integer> tInfo;
	
	protected boolean connectionExist = false;
	
	/**
	 * The no argument constructor will create a database connection for itself.
	 */
	protected Controller(){
		this(null);
	}

	/**
	 * 
	 * @param conn 
	 */
	protected Controller(ConnectionSource conn){		
		if (conn != null){
			this.connectionSource = conn;
			connectionExist = true;
		}
		else this.createConnection();			
		init();
	}
	
	protected void createConnection(){
		try {			
			if (url == null || url.equals(""))url = dbConnector + dbPath;
			if (this.connectionSource == null) {
				connectionSource = new JdbcConnectionSource(url);
				System.out.println("Using a unique connection");
			}
			else System.out.println("Connection already Created");			
			connectionExist = true;
		} catch (SQLException e) { System.out.println("error with connection: "+e.getMessage()); }
	}
	
	public ConnectionSource getConnection() { return connectionSource; }
	public void setConnection(ConnectionSource connectionSource) { this.connectionSource = connectionSource;connectionExist = true;	}
	
	public boolean isConnectionExist() {
		return connectionExist;
	}

	public void setConnectionExist(boolean connectionExist) {
		this.connectionExist = connectionExist;
	}

	public String getDbPath() {
		return dbPath;
	}

	public void setDbPath(String dbPath) {
		this.dbPath = dbPath;
	}

	public String getDbConnector() {
		return dbConnector;
	}

	public void setDbConnector(String dbConnector) {
		this.dbConnector = dbConnector;
	}

	/**
	 * 
	 */
	protected abstract void init();
	/**
	 * The validate function defines the business validation rules that must be in place for each specific Model
	 * before it can be saved to the database
	 * @param t The model to be assessed(validated)
	 * @return The related message based on the model. Will return ok if model passed validation test
	 */
	public abstract String validate(T t);
	/**
	 * Triggers the DAO save method. Provides an abstraction to the underlying database implementation
	 * @param model the model to be saved to the database
	 * @return Return the updated model with the id of the model now set.
	 * @throws Exception
	 */
	public T save(T model) throws Exception{
		String msg = validate(model);
		if (!msg.equals("ok"))throw new Exception(msg);
		Model m = (Model)model;
		if (m.getId() == 0)model = modelDAO.createIfNotExists(model);
		else modelDAO.update(model);
		return model;
	}
	/**
	 * Facilitates a bulk save operation for the model
	 * @param models The collection of models to be saved
	 * @return The collection of models that were successfully saved by the controller
	 */
	public Collection<T> save(Collection<T> models) throws Exception{
		ArrayList<T> savedModels = new ArrayList<T>();
		Iterator<T> i = models.iterator();
		while(i.hasNext()){
			T temp = this.save(i.next());
			if (temp != null && ((Model)temp).getId() != 0)savedModels.add(temp);
		}
		return savedModels;
	}
	
	/**
	 * Triggers the DAO update method.
	 * @param model
	 * @return The updated model with changed values from the database will be returned
	 * @throws Exception
	 */
	public T update(T model) throws Exception{
		Model tmp = (Model)model;
		if (tmp.getId() == 0) throw new Exception("Must have a valid ID. Use save if trying to create new user");
		String msg = validate(model);
		if (!msg.equals("ok"))throw new Exception(msg);
		modelDAO.update(model);
		return model;
	}
	
	/**
	 * Will attempt to perform a delete operation of the database for the identified model
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public boolean delete(T model) throws Exception{
		if (modelDAO.delete(model) == 1)return true;
		return false;
	}
	
	/**
	 * Will attempt to perform a query based on the values passed in to the model.
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Collection<T> read(T model) throws Exception{
		return modelDAO.queryForMatching(model);
	}
	/**
	 * 
	 * @param field
	 * @param value
	 * @return
	 * @throws Exception
	 */
	public Collection<T> read(String field, Object value) throws Exception{
		List <T> list;
		QueryBuilder<T, Integer>queryBuilder = modelDAO.queryBuilder();
		queryBuilder.where().eq(field, value);
		list = modelDAO.query(queryBuilder.prepare());
		return list;
	}
	/**
	 * 
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public T read(int id) throws SQLException{
		return this.modelDAO.queryForId(id);		
	}
	
	public Collection<T> findByKeyword(String field, Object value) throws Exception{
		List <T> list;
		QueryBuilder<T, Integer>queryBuilder = modelDAO.queryBuilder();
		queryBuilder.where().like(field, value);
		list = modelDAO.query(queryBuilder.prepare());
		return list;
	}
	
	/**
	 * Will return all of the models between the limit and the offset
	 * @param limit
	 * @param offset
	 * @return
	 */
	public Collection<T> getAll(long limit, long offset){
		List <T> list = new ArrayList<T>();
		QueryBuilder<T, Integer> queryBuilder = modelDAO.queryBuilder();
		try {
			queryBuilder.limit(limit).offset(offset);
			list = modelDAO.queryForAll();
		}catch (SQLException e) {System.out.println("Error getting all records: "+e.getMessage());}
		return list;
	}
	
	public Collection<T>  getAll() {
		return getAll(DEFAULT_LIMIT,DEFAULT_OFFSET);
	}
	
	/**
	 * Will reload the database values into the model. It finds the model based on the id of the model passed as a parameter
	 * @param model
	 * @return The model with updated information from database
	 * @throws Exception
	 */
	public T refreshModel(T model) throws Exception{
		Model m = (Model)model;
		if (m.getId() == 0)throw new Exception("Model must have a valid ID to be refreshed");
		return modelDAO.queryForId(m.getId());
	}
	
	/**
	 * Proivdes an alternative method for the read method
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Collection<T>  get(T model) throws Exception{
		return read(model);
	}
	/**
	 * Provides a case insensitive search using the values in the fields as opposed to the exact values
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Collection<T> find(T model)throws Exception{
		tInfo = ((BaseDaoImpl<T, Integer>) this.modelDAO).getTableInfo();
		Field [] aClassFields = model.getClass().getDeclaredFields();
		QueryBuilder<T, Integer> qb = this.modelDAO.queryBuilder();
		Where<T, Integer> where = qb.where();
		
		boolean seen = false;
		
		for(Field f : aClassFields){
			f.setAccessible(true);
			Object fObj = f.get(model);
			if (fObj != null && (fObj instanceof ForeignCollection) == false && !f.getName().equalsIgnoreCase("serialVersionUID")){
				if (f.getName().equalsIgnoreCase("id") ){
					if(!fObj.equals(new Integer(0))){
						T temp = this.modelDAO.queryForId(new Integer((Integer)fObj));
						Collection<T>list = new ArrayList<T>();
						list.add(temp);
						return list;
					}
				}else{
					if (fObj instanceof Model){
						if (((Model)fObj).getId() != 0){
							if (seen)where.and();
							where.eq(f.getName(), fObj);
							seen = true;
						}
					}else{				
						if (fObj instanceof String){
							if (seen)where.and();
							where.like(f.getName(), "%"+fObj+"%");
							seen = true;
						}
						else if (fObj instanceof Double && new Double((Double) fObj) < 0 ){}
						else{
							if (seen)where.and();
							where.eq(f.getName(), fObj);
							seen = true;
						}
					}
				}
			}
		}
		return modelDAO.query(qb.prepare());
	}
	
	public Map<String, Object> extractField(T model) throws Exception{
		Map<String, Object> fieldMap = new HashMap<String, Object>();
		
		Field [] aClassFields = model.getClass().getDeclaredFields();
		for(Field f : aClassFields){
			f.setAccessible(true);
			Object fObj = f.get(model);
			if (fObj != null && (fObj instanceof ForeignCollection) == false && !f.getName().equalsIgnoreCase("serialVersionUID")){				
				if (fObj instanceof Model){						
					fieldMap.put(f.getName(), ((Model)fObj).getId());
				}else{
					fieldMap.put(f.getName(), fObj);
				}
			}else if (fObj instanceof ForeignCollection){}//TODO produce subfiles ?
		}
		return fieldMap;
	}
	
	public boolean importToCSV(String directory, String fileName, T model) {
		try{
			Map <String, Object> fieldMap;
			ICsvMapReader mapReader = new CsvMapReader(new FileReader(directory+fileName), CsvPreference.STANDARD_PREFERENCE);
			final String [] header = mapReader.getHeader(true);
			
			//TODO Brute hack need revision
			if (model == null){
				Collection<T> mods = this.getAll();
				if (mods.size() > 0)model = mods.iterator().next();
				else {
					mapReader.close();
					return false;
				}
			}
			
			fieldMap = this.extractField(model);			
			Collection<String> keys = fieldMap.keySet();
			
			
			boolean proceed = true;
			
			for (int i = 0; i < header.length; i++){
				proceed = keys.contains(header[i]) && proceed;
			}
			
			Map<String, String> modelMap;
			
			if (proceed){
				while ( (modelMap = mapReader.read(header)) != null){
					if (!importMapString(modelMap, keys)){
						mapReader.close();
						return false;
					}
				}
			}
			
				
			mapReader.close();
			return true;
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
		
		return false;
	}
	
	protected abstract boolean importMapString(Map<String, String> modelMap,Collection<String> keys);

	public boolean exportToCSV(String directory, String fileName, Collection<T> models){
		try{
			T model = null;
			boolean writeHead = true;
			
			Map<String, Object> fieldMap;
			ICsvMapWriter mapWriter = new CsvMapWriter(new FileWriter(directory+fileName),CsvPreference.STANDARD_PREFERENCE);
			try {
				Iterator <T> iter = models.iterator();
				while (iter.hasNext()){
					model = iter.next();
					fieldMap = this.extractField(model);
					if (fieldMap.size() > 0){
						Set<String> set = fieldMap.keySet();
						String [] keys = new String[set.size()];
						keys = set.toArray(keys);	
						if (writeHead){
							mapWriter.writeHeader(keys);
							writeHead = false;
						}
		                mapWriter.write(fieldMap, keys);
					}
				}
			}finally {
                if( mapWriter != null ) 
                    mapWriter.close();
	        }
			return true;
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
		return false;
	}

	
	public boolean closeConnection(){
		if (this.connectionSource != null)
			try {
				connectionSource.close();
				return true;
			} catch (SQLException e) {
				e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
			}
		return false;
	}
	
	public Dao<T, Integer> getModelDAO() {
		return modelDAO;
	}
}
