package org.csp.controller;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import org.csp.models.Output;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

public class OutputController extends Controller <Output>{

	private static OutputController instance = null;

	private OutputController(){ super(); }
	private OutputController(ConnectionSource conn) { super(conn); }
	
	public static OutputController getInstance(){ return getInstance(null); }
	public static OutputController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new OutputController(conn);
			else instance = new OutputController();
			System.out.println("Creating new instance of the Output Controller Class");
		}
		return instance;
	}
	
	@Override
	protected void init() {
		try{
			if (connectionSource != null) this.modelDAO = DaoManager.createDao(connectionSource, Output.class);
		}catch (SQLException e){
			System.out.println("Failed to initialize User Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}

	@Override
	public String validate(Output t) {
		String msg = "ok";
		if (t == null)msg = "No output specified";
		else if (t.getOutput() == null || t.getOutput().equals(""))msg = "No output description specified";
		return msg;
	}
	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}

}
