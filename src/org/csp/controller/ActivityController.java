package org.csp.controller;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import org.csp.models.Activity;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

public class ActivityController extends Controller<Activity>{

	private static ActivityController instance = null;
	
	private ActivityController(){
		super();
	}
	
	private ActivityController(ConnectionSource conn) {
		super(conn);
	}

	@Override
	protected void init() {
		try {
			if (connectionSource != null) modelDAO = DaoManager.createDao(connectionSource, Activity.class);
		} catch (SQLException e) {
			System.out.println("Failed to initialize User Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}

	@Override
	public String validate(Activity t) {
		String msg = "ok";
		if(t == null)msg = "Must specify an activity to save";
		else if(t.getCohort() == null)msg = "Must be part of a specified project or cohort";
		else if(t.getActivity() == null || t.getActivity().equals(""))msg = "Must specify the name of the activity";
		return msg;
	}


	public static ActivityController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new ActivityController(conn);
			else instance = new ActivityController();
			System.out.println("Creating new instance of the Activity Controller Class");
		}
		return instance;
	}
	public static ActivityController getInstance(){
		return getInstance(null);
	}

	public Activity get(String s) {
//		Activity a = null;
		try{
			Collection<Activity> act = this.read("title", s);
			if (act.size() > 1)System.out.println("WARNING more than one activity returned");
			if (act.size() > 0)return act.iterator().next();
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); return null; }
		return null;
	}

	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}

}
