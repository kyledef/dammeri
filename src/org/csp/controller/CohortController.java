package org.csp.controller;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.csp.models.Cohort;
import org.csp.models.Model;
import org.csp.models.Organization;
import org.csp.models.Project;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

public class CohortController extends Controller<Cohort> {

	private static CohortController instance = null;
	
	protected String [] projectFields;
	protected String [] orgFields;

	private CohortController(){ super(); }	
	private CohortController(ConnectionSource conn) { super(conn);	}
	public static CohortController getInstance(){ return getInstance(null); }
	public static CohortController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new CohortController(conn);
			else instance = new CohortController();
			System.out.println("Creating new instance of the Cohort Controller Class");
		}
		return instance;
	}
	
	@Override
	protected void init() {
		try {
			if (connectionSource != null) modelDAO = DaoManager.createDao(connectionSource, Cohort.class);
			this.projectFields = new String[]{"name","description"};
			this.orgFields = new String []{"funder"};
		} catch (SQLException e) {
			System.out.println("Failed to initialize User Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}

	@Override
	public String validate(Cohort t) {
		String message = "ok";
		if (t == null)message = "Cohort is required";
		else if (t.getStartDate() != null && t.getEndDate() != null){
			if (t.getStartDate().after(t.getEndDate()))
				message = "Start date must be before the end date";
		}		
		else if (t.getProject() == null)message ="Cohort must be affiliated with a Project";
		return message;
	}
	
	public Collection<Cohort> read(Cohort model) throws Exception{
		ArrayList<Cohort> list = new ArrayList<Cohort>();
		Iterator <Project>pi = ProjectController.getInstance(this.connectionSource).read(model.getProject()).iterator();
		while (pi.hasNext()){
			model.setProject(pi.next());
			list.addAll(this.read(model));
		}
		return list;
	}
	
	public Collection<Cohort> getByDate(Date start, Date end) throws SQLException{
		List<Cohort> list = new ArrayList<Cohort>();
		QueryBuilder<Cohort, Integer> qBuilder = this.modelDAO.queryBuilder();
		qBuilder.where().le("endDate", end).and().ge("startDate", start);
		list = this.modelDAO.query(qBuilder.prepare());
		return list;
	}
	
	public Collection<Cohort> findByKeyword(String field, Object value) throws Exception {
		
		List<Cohort> list = new ArrayList<Cohort>();
		for(int i=0;i<this.projectFields.length;i++){
			if (projectFields[i].equals(field)){
				ProjectController pc = ProjectController.getInstance(this.connectionSource);
				Collection<Project> pCol = pc.findByKeyword(field, value);				
				Iterator<Project> ip = pCol.iterator();
				while(ip.hasNext()){
					Project p = ip.next();
					list.addAll(this.read("project", p.getId()));
				}
				return list;
			}
		}
		if (field.equals("funder")){
			OrganizationController oc = OrganizationController.getInstance(this.connectionSource);
			Collection<Organization> oCol = oc.findByKeyword("name", value);
			Iterator <Organization>oi = oCol.iterator();
			while(oi.hasNext())
				list.addAll(this.read("funder", oi.next().getId()));
			return list;
		}
		
		
//		QueryBuilder<Cohort, Integer> qBuilder = this.modelDAO.queryBuilder();
//		qBuilder.where().like(field, value);
		return list;
	}
	
	public boolean delete(Cohort model) throws Exception{
		Project p = model.getProject();
		boolean worked = ProjectController.getInstance(this.connectionSource).delete(p);
		if (modelDAO.delete(model) == 1 && worked)return true;
		return false;
	}

//	public Collection<Cohort> find(Cohort t){		
//		Collection<Cohort> list = new ArrayList<Cohort>();	
//		System.out.println("Running Find");
//		
//		QueryBuilder <Cohort, Integer>qb = this.modelDAO.queryBuilder();
//		try{
//			if (t.getProject().getName() != null && !t.getProject().getName().equals("")){
//				Iterator<Project> pIter = ProjectController.getInstance(this.connectionSource).find(t.getProject()).iterator();
//				while (pIter.hasNext())
//					qb.where().eq("project", pIter.next());
//			}
//			
//			if (t.getTeamLead() != null)
//				qb.where().eq("teamLead", t.getTeamLead());
//
//			if (t.getStartDate() != null)
//				qb.where().eq("startDate", t.getStartDate());
//			if (t.getEndDate() != null)
//				qb.where().eq("endDate", t.getEndDate());
//			
//			list = this.modelDAO.query(qb.prepare());
//			System.out.println(list.size());
//			
//		}catch(Exception e){ e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
//		
//		return list;
//	}
	
	public Collection<Cohort> find(Cohort t) throws Exception{	
		Collection<Cohort> list = new ArrayList<Cohort>();
		boolean searched = false;
		if (t != null){
			
			Project p = t.getProject();
			System.out.println(p);
			
			if (p != null && (p.getName() != null)){
				Iterator<Project>pi = ProjectController.getInstance(this.connectionSource).find(p).iterator();
				while(pi.hasNext()){
					t.setProject(pi.next());
					System.out.println(t.getProject());
					list.addAll(super.find(t));
					searched  = true;
				}
				if (!searched)list.addAll(super.find(t));
			}else
				list.addAll(super.find(t));
		}
		return list;
	}

	@Override
	public Map<String, Object> extractField(Cohort model) throws Exception{
		Map<String, Object> cohortMap = new HashMap<String, Object>();
		Field [] aClassFields = model.getClass().getDeclaredFields();
		for(Field f : aClassFields){
			f.setAccessible(true);
			Object fObj = f.get(model);
			if (fObj != null && (fObj instanceof ForeignCollection) == false && !f.getName().equalsIgnoreCase("serialVersionUID")){				
				if (fObj instanceof Model){
					if (fObj instanceof Project){
						Project p = (Project)fObj;
						cohortMap.put("name", p.getName());
						cohortMap.put("description", p.getDescription());
					}else
						cohortMap.put(f.getName(), ((Model)fObj).getId());
				}else{
					cohortMap.put(f.getName(), fObj);
				}
			}else if (fObj instanceof ForeignCollection){
				
			}
		}
		return cohortMap;
	}
	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}
}
