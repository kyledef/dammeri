package org.csp.controller;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import org.csp.models.FollowUpTask;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

public class FollowUpController extends Controller<FollowUpTask> {

	private static FollowUpController instance = null;
 
	private FollowUpController(){ super(); }	 
	private FollowUpController(ConnectionSource conn) { super(conn); }
		
	public static FollowUpController getInstance(){ return getInstance(null); }
	public static FollowUpController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new FollowUpController(conn);
			else instance = new FollowUpController();
			System.out.println("Creating new instance of the Followup Controller Class");
		}
		return instance;
	}
	
		
	
	
	@Override
	protected void init() {
		try {
			if (connectionSource != null) modelDAO = DaoManager.createDao(connectionSource, FollowUpTask.class);
		} catch (SQLException e) {
			System.out.println("Failed to initialize User Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}

	}
	@Override
	public String validate(FollowUpTask t) {
		String msg = "ok";		
		return msg;
	}
	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}

}
