package org.csp.controller;

public class TelemetryController {
	private static TelemetryController instance;
	
	protected TelemetryController(){
		init();
	}
	
	private void init(){
		
	}
	
	public static TelemetryController getInstance(){
		if (instance == null)instance = new TelemetryController();
		return instance;
	}
}
