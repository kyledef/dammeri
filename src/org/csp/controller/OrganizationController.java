package org.csp.controller;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.csp.models.Organization;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.support.ConnectionSource;

public class OrganizationController extends Controller<Organization> {

	private static OrganizationController instance = null;

	private OrganizationController(){
		super();
	}
	
	private OrganizationController(ConnectionSource conn) {
		super(conn);
	}
	public static OrganizationController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new OrganizationController(conn);
			else instance = new OrganizationController();
			System.out.println("Creating new instance of the OrganizationController Class");
		}
		return instance;
	}
	public static OrganizationController getInstance(){
		return getInstance(null);
	}

	@Override
	protected void init() {
		try{
			
			if (connectionSource != null) modelDAO = DaoManager.createDao(connectionSource, Organization.class);
		} catch (SQLException e) {
			System.out.println("Failed to initialize User Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}

	@Override
	public String validate(Organization o) {
		String message = "ok";		
		if (o == null)message = "Organization is required";
		else if(o.getName() == null || o.getName().equals(""))message = "Must specify a name for the orgqanization";
		else if(o.getType() == null || o.getType().equals(""))message = "Must specify the type of the organization"; 
		return message;
	}
	
	public Organization get(String strRep) throws SQLException{
		Organization o = null;	
		String [] parts = strRep.split(":");
		PreparedQuery<Organization> preparedQuery = this.modelDAO.queryBuilder().where()
			.eq("name", parts[0])
			.and()
			.eq("type", parts[1])
			.prepare();
		List<Organization> results = modelDAO.query(preparedQuery);
		if (results.size() > 0){
			if (results.size() > 1)System.out.println("WARNING: Organization get did not return a unique result");
			return results.iterator().next();
		}
		return o;
	}
	
	public Organization [] getOrganizations(){
		return getOrganizations(null);
	}
	
	public Organization [] getOrganizations(String type){
		Organization [] orgs = new Organization[0];
		try {
			Collection <Organization>oCol;		
			if (type != null)oCol = read("type", type);		
			else oCol = getAll();	
			return oCol.toArray(orgs);
		} catch (Exception e) { e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
		return orgs;
	}

	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		Organization model = new Organization();
		try{
			Iterator <String> i = keys.iterator();
			while (i.hasNext()){
				String f = i.next();
				if (f.equalsIgnoreCase("id")){
					model.setId(Integer.parseInt(modelMap.get(f)));
				}else{
					Field field = model.getClass().getDeclaredField(f);
					field.setAccessible(true);
					field.set(model, modelMap.get(f));
				}
			}
			
			this.save(model);
			return true;
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
		// TODO Auto-generated method stub
		return false;
	}

	
}
