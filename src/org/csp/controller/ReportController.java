package org.csp.controller;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import org.csp.models.Report;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

public class ReportController extends Controller <Report>{

	private static ReportController instance = null;

	private ReportController(){ super(); }
	private ReportController(ConnectionSource conn) { super(conn); }
	
	public static ReportController getInstance(){ return getInstance(null); }
	public static ReportController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new ReportController(conn);
			else instance = new ReportController();
			System.out.println("Creating new instance of the Report Controller Class");
		}
		return instance;
	}
	
	@Override
	protected void init() {
		try{
			if (connectionSource != null) this.modelDAO = DaoManager.createDao(connectionSource, Report.class);
		}catch (SQLException e){
			System.out.println("Failed to initialize User Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}

	@Override
	public String validate(Report t) {
		String msg = "ok";
		if (t == null)msg = "must specify report";
		return msg;
	}
	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}

}
