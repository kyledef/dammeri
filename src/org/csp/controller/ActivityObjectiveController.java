package org.csp.controller;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import org.csp.models.ActivityObjective;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

public class ActivityObjectiveController extends Controller<ActivityObjective> {

	private static ActivityObjectiveController instance = null;
	
	private ActivityObjectiveController() {
		super();
	}

	private ActivityObjectiveController(ConnectionSource conn) {
		super(conn);
	}

	@Override
	protected void init() {
		try {
			if (connectionSource != null) modelDAO = DaoManager.createDao(connectionSource, ActivityObjective.class);
		} catch (SQLException e) {
			System.out.println("Failed to initialize User Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}

	@Override
	public String validate(ActivityObjective ao) {
		String msg = "ok";
		if (ao.getActivity() == null)msg = "No Activity given";
		if (ao.getObjective() == null)msg = "No Objective given";
		return msg;
	}

	public static ActivityObjectiveController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new ActivityObjectiveController(conn);
			else instance = new ActivityObjectiveController();
		}
		return instance;
	}
	public static ActivityObjectiveController getInstance(){
		return getInstance(null);
	}

	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}
}
