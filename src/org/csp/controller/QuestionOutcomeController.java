package org.csp.controller;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import org.csp.models.QuestionOutcome;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

public class QuestionOutcomeController extends Controller<QuestionOutcome>{

	private static QuestionOutcomeController instance = null;
	private QuestionOutcomeController(){ super(); }	
	private QuestionOutcomeController(ConnectionSource conn) { super(conn);	}
	public static QuestionOutcomeController getInstance(){ return getInstance(null); }
	public static QuestionOutcomeController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new QuestionOutcomeController(conn);
			else instance = new QuestionOutcomeController();
			System.out.println("Creating new instance of the QuestionOutcome Controller Class");
		}
		return instance;
	}
	
	@Override
	protected void init() {
		try {
			if (connectionSource != null) modelDAO = DaoManager.createDao(connectionSource, QuestionOutcome.class);
		} catch (SQLException e) {
			System.out.println("Failed to initialize QuestionOutcome Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}

	@Override
	public String validate(QuestionOutcome t) {
		String message = "ok";
		if (t.getQuestion() == null || t.getQuestion().getId() == 0)message="no question specified";
		if (t.getOutcome()== null || t.getOutcome().getId() == 0) message="no outcome spectified";
		return message;
	}
	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}

}
