package org.csp.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.csp.models.Beneficiary;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

public class BeneficiaryController extends Controller<Beneficiary> {

	private static BeneficiaryController instance = null;
	
	private BeneficiaryController(){ super(); }	
	private BeneficiaryController(ConnectionSource conn) { super(conn); }
	public static BeneficiaryController getInstance(){ return getInstance(null); }
	public static BeneficiaryController getInstance(ConnectionSource conn){
		if (instance == null){
			if (conn != null)instance = new BeneficiaryController(conn);
			else instance = new BeneficiaryController();
			System.out.println("Creating new instance of the Beneficiary Controller Class");
		}
		return instance;
	}
	



	@Override
	protected void init() {
		try {
			if (connectionSource != null) modelDAO = DaoManager.createDao(connectionSource, Beneficiary.class);
		} catch (SQLException e) {
			System.out.println("Failed to initialize User Controller");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}		
	}

	@Override
	public String validate(Beneficiary t) {
		String msg = "ok";
		if (t == null)msg = "must specify beneficiary to be saved";
//		else if (t.getDescription() == null || t.getDescription().equals(""))msg = "Must specify description of the beneficiary";
//		else if (t.getCohort() == null)msg = "Beneficiary must be affiliated with a project";
		return msg;
	}
	public String [] getAgeGroup(){
		String [] ages = new String [0];
		ArrayList <String> resultAges = new ArrayList<String>();
		QueryBuilder <Beneficiary, Integer> qBuilder = this.modelDAO.queryBuilder();
		qBuilder.distinct().groupBy("age");
		List<Beneficiary> l;
		try {
			l = modelDAO.query(qBuilder.prepare());
			Iterator <Beneficiary> i = l.iterator();
			while (i.hasNext()){
				resultAges.add(i.next().getAgeGroup());
			}
		} catch (SQLException e) {
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
		return resultAges.toArray(ages);
		
	}
	@Override
	protected boolean importMapString(Map<String, String> modelMap,
			Collection<String> keys) {
		// TODO Auto-generated method stub
		return false;
	}
	
		
	
}
