package org.csp.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.Properties;

import org.csp.controller.SettingController;

public class ServerCommunication implements Runnable {
	SettingController sc;
	Properties prop;
	String message;
	String content_type;
	String agent;
	int type;
	
	public ServerCommunication(){
		sc = SettingController.getInstance();
		prop = sc.getProp();		
		content_type = "application/x-www-form-urlencoded";
		agent = "Mozilla/4.0";
	}
	
	public ServerCommunication(String message, int type) {
		this();
		this.message = message;
		this.type = type;
	}

	@Override
	public void run() {
		sendToServer(this.message, this.type);
	}
	
	public URLConnection getServerConnection(int type){
		StringBuilder urlBuilder = new StringBuilder();
		if (prop == null)sc.loadSettings();
		urlBuilder.append(prop.getProperty("server_location"));
		
		switch(type){
			case SettingController.SVR_VER_MSG:
				urlBuilder.append("version");
		}
		
		try{
			
			URL url = new URL(urlBuilder.toString());
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty( "User-Agent", agent );
			conn.setRequestProperty( "Content-Type", content_type );
			if (this.message != null && this.message.equals("") == false){
				String encodedData = message;
				conn.setRequestProperty( "Content-Length", Integer.toString(encodedData.length()) );
			}
			conn.setReadTimeout(10000);
			conn.connect();
			
			return conn;
		}catch(Exception e){
			
		}
		return null;
	}

	public void sendToServer(String message, int type){
		StringBuilder urlBuilder = new StringBuilder();
		if (prop == null)sc.loadSettings();
		urlBuilder.append(prop.getProperty("server_location"));
		switch(type){
			case SettingController.SVR_DBG_MSG:
				urlBuilder.append("debugging/");
				break;
			case SettingController.SVR_ENVI_MSG:
				urlBuilder.append("environment/");
				break;
			case SettingController.SVR_ERR_MSG:
				urlBuilder.append("error/");
				break;
			case SettingController.SVR_INFO_MSG:
				urlBuilder.append("info/");
				break;
			case SettingController.SVR_PERF_MSG:
				urlBuilder.append("performance/");
				break;
			case SettingController.SVR_FEED_MSG:
				urlBuilder.append("feedback/");
		}
	
		String id = sc.getMachineID();
		urlBuilder.append(id);
		String time = sc.getCurrentTimeStamp();
		message = time +","+message;
		System.out.printf("Attempting to send  %s  to :%s\n", message, urlBuilder.toString());
		try {
			try{
				this.message = message;
		        String encodedData = message;
				URL url = new URL(urlBuilder.toString());
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true);
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty( "User-Agent", agent );
				conn.setRequestProperty( "Content-Type", content_type );
				conn.setRequestProperty( "Content-Length", Integer.toString(encodedData.length()) );
				conn.setReadTimeout(10000);
				conn.connect();
				
				OutputStream os = conn.getOutputStream();
	            os.write( encodedData.getBytes() );
	            
	            if (conn.getResponseCode() == 200){
	            	//If successful the send message to the server
					String line;
					StringBuilder sb = new StringBuilder();				
					BufferedReader rd  = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					while ((line = rd.readLine()) != null){
						sb.append(line);
					}
	            }else{
	            	System.out.println(conn.getResponseMessage());
	            	throw new UnknownHostException("Error Code received from server: "+ conn.getResponseCode());
	            }
			}catch( UnknownHostException ex ){
				// Failed to transmit data then write information to a file			
				System.out.println("Unable to access the internet. writing to file: "+ ex.getMessage());
				sc.writeToFile(message, SettingController.FILE_TRANS_MSG);
				System.out.println("done");
			}
		} catch (IOException e) {e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
