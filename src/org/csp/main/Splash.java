package org.csp.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JWindow;

import org.csp.controller.SettingController;

public class Splash extends JWindow {
	private static final long serialVersionUID = -556155935190073317L;
	int width = 450;
    int height =310;
    
    private JProgressBar progressBar;
	private JTextArea taskOutput;
    
    public Splash(){
    	createUI();   	
    }
    
    public void createUI(){
    	JPanel mainContent = (JPanel)getContentPane();
    	mainContent.setBackground(Color.white);
        
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (screen.width-width)/2;
        int y = (screen.height-height)/2;
        setBounds(x,y,width,height);
        JLabel imglabel = null;
        
        try{
        	URL url = Thread.currentThread().getContextClassLoader().getResource( "res/simmeda_icon.png" );
        	imglabel = new JLabel(new ImageIcon( url ));
        }catch(Exception e){System.out.println("Unable to load label"); e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
        
        JLabel copyrt = new JLabel("DIMMERI: Making M&E Easier", JLabel.CENTER);
        copyrt.setFont(new Font("Sans-Serif", Font.BOLD, 24));
        
        JPanel content = new JPanel();
        content.setBackground(Color.white);
        mainContent.add(content, BorderLayout.CENTER);
        if (imglabel != null)content.add(imglabel);
        content.add(copyrt);
        
        //create Items for Interactive UI
        progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        
        taskOutput = new JTextArea(5, 20);
        
        JPanel progressPanel = new JPanel();
        mainContent.add(progressPanel, BorderLayout.SOUTH);
        progressPanel.setLayout(new BorderLayout(0, 0));
        progressPanel.add(progressBar, BorderLayout.NORTH);
        progressPanel.add(new JScrollPane(taskOutput), BorderLayout.SOUTH);
        
    }
    

    public void display(int duration){
    	this.setVisible(true);
    	try { Thread.sleep(duration); } catch (Exception e) {}
    	this.setVisible(false);
    	this.dispose();
    }
    
    public void display(boolean show){
    	if (show)
    		this.setVisible(show);
    	else{
    		this.setVisible(false);
    		this.dispose();
    	}
    }
    
    public static void main(String [] args){
    	Splash s = new Splash();
    	s.display(4000);
    }

	public JProgressBar getProgressBar() {
		return this.progressBar;
	}

	public JTextArea getTaskOutput() {
		return taskOutput;
	}
    
    
    
    
}
