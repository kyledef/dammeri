package org.csp.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.csp.controller.SettingController;

public class FileMsgWriter implements Runnable {

	String message;
	int type;
	
	public FileMsgWriter(){
		
	}
	
	public FileMsgWriter(String message, int type) {
		super();
		this.message = message;
		this.type = type;
	}

	@Override
	public void run() {
		writeToFile(message, type);
	}

	public void writeToFile(String message, int type){		
		String filename = "";
		if (type == SettingController.FILE_TRANS_MSG)filename = SettingController.FILE_TRANS_NAME;
		else if (type == SettingController.FILE_DEBUG_MSG)filename = SettingController.FILE_DEBUG_NAME;
		else if (type == SettingController.FILE_BCK_MSG)filename = SettingController.FILE_BCK_NAME;
		
		try {
			File file= new File(filename);
			if (!file.exists())
				file.createNewFile();
			
			FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(message+ "\r\n");
			bw.close();
		} catch (IOException e) {
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}			
	}
}
