package org.csp.main;

import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;

import org.csp.controller.ActivityController;
import org.csp.controller.ActivityObjectiveController;
import org.csp.controller.BeneficiaryController;
import org.csp.controller.CohortController;
import org.csp.controller.IndicatorController;
import org.csp.controller.InputController;
import org.csp.controller.MemberController;
import org.csp.controller.ObjectiveController;
import org.csp.controller.OrganizationController;
import org.csp.controller.OutcomeController;
import org.csp.controller.OutputController;
import org.csp.controller.PersonController;
import org.csp.controller.ProjectController;
import org.csp.controller.QuestionActivityController;
import org.csp.controller.QuestionController;
import org.csp.controller.QuestionOutcomeController;
import org.csp.controller.QuestionOutputController;
import org.csp.controller.SettingController;
import org.csp.controller.UserController;
import org.csp.views.MainFrame;
import org.csp.views.UserLoginView;

import com.j256.ormlite.support.ConnectionSource;

public class BootStrap implements Runnable {	
	ConnectionSource conn = null;
	Splash ps = null;
	JProgressBar progressBar = null;
	JTextArea taskOutput;
	SettingController sc;
	UserController uc;
	
	private void loadSettings(){
		sc = SettingController.getInstance();	
		sc.loadSettings();
		System.out.println("Loaded Settings");
		if (sc.useSplash() && ps != null)ps.display(true);
	}
	
	private void loadOptimizations(){
		
	}
	
	private void buildUIWidgets(){
		
	}
	
	private void testCreateDatabase(){
		if (!sc.isDatabaseExist())sc.createDatabase();
	}
	
	private void startControllers(){
		if (sc.isConnectionExist())conn = sc.getConnection();
		uc = UserController.getInstance(conn);		
		QuestionController.getInstance(conn);
		ActivityController.getInstance(conn);
		ObjectiveController.getInstance(conn);
		CohortController.getInstance(conn);
		ProjectController.getInstance(conn);
		BeneficiaryController.getInstance(conn);
		OutcomeController.getInstance(conn);
		IndicatorController.getInstance(conn);
		OrganizationController.getInstance(conn);
		MemberController.getInstance(conn);
		PersonController.getInstance(conn);
		InputController.getInstance(conn);
		OutputController.getInstance(conn);
		ActivityObjectiveController.getInstance(conn);
		QuestionOutcomeController.getInstance(conn);
		QuestionOutputController.getInstance(conn);
		QuestionActivityController.getInstance(conn);
	}

	protected void startUpdater(){
		try {
			String EXEC_ARGUMENT = new StringBuilder().
	    	      append( java.lang.System.getProperty( "java.home" ) ).
	    	      append( java.io.File.separator ).
	    	      append( "bin" ).
	    	      append( java.io.File.separator ).
	    	      append( "java" ).
	    	      append( " " ).
	    	      append("-jar").
	    	      append( " " ).
	    	      append( new java.io.File( "." ).getAbsolutePath() ).
	    	      append( java.io.File.separator ).
	    	      append( "updater.jar" ).
	    	      toString();
			
			System.out.println("Attempting to run updater with "+EXEC_ARGUMENT);
			Runtime.getRuntime().exec( EXEC_ARGUMENT );
		} catch (IOException e) {
			(new Thread(new FileMsgWriter("Update Failed: " + e.getMessage(), SettingController.FILE_DEBUG_MSG))).start();
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}
	@Override
	public void run() {
		
		(new Thread(new FileMsgWriter("In side the bootup process", SettingController.FILE_DEBUG_MSG))).start();
		
		try{
			ps = new Splash();
			progressBar = ps.getProgressBar();
			taskOutput = ps.getTaskOutput();	
			(new Thread(new FileMsgWriter("Created Progress Bar", SettingController.FILE_DEBUG_MSG))).start();
		
			loadSettings();
			setProgress(0);
			
			(new Thread(new FileMsgWriter("Load Settings", SettingController.FILE_DEBUG_MSG))).start();
			
			testCreateDatabase();
			setProgress(10);
			
			(new Thread(new FileMsgWriter("Complete Database Checks", SettingController.FILE_DEBUG_MSG))).start();
			
			startControllers();
			setProgress(20);
			
			(new Thread(new FileMsgWriter("Controllers Started", SettingController.FILE_DEBUG_MSG))).start();
			
			loadOptimizations();
			setProgress(30);
			
			(new Thread(new FileMsgWriter("Optimizations Loaded", SettingController.FILE_DEBUG_MSG))).start();
			
			//startUpdater();
			setProgress(40);
			
			(new Thread(new FileMsgWriter("Updater Loaded", SettingController.FILE_DEBUG_MSG))).start();
			
			buildUIWidgets();
			setProgress(100);
			
			startScreen();		
		}catch(Exception e){
			(new Thread(new FileMsgWriter(e.getMessage(), SettingController.FILE_DEBUG_MSG))).start();
		}
	}

	private void startScreen() {
		if (ps!= null)ps.display(false);
				
		JFrame frame = null;
		if (sc != null && sc.useLogin()){
			frame = new JFrame();
			new UserLoginView(frame, true, false);
			frame.pack();
		}else{
			System.out.println("Not using Login Screen");
			frame = new MainFrame();	
		}
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);		
		frame.setVisible( true );
	}
	
	public void setProgress(int progress){
		String item = "";
		switch(progress){
			case 0:
				item = "settings";
				break;
			case 25:
				item = "database";
				break;
			case 50:
				item = "optimmizations";
				break;
			case 75:
				item = "User Interface";
				break;
			case 100:
				item = "starting";
				break;
			default:
				item = "";
				break;
		}
		progressBar.setValue(progress);
		taskOutput.append(String.format("Loading %s: %d%% Completed \n",item, progress));
	}

	public static void main(String[] args) {
		(new Thread(new BootStrap())).start();
//		(new BootStrap()).run();
//		(new Thread(new FileMsgWriter("after the run was called", SettingController.FILE_DEBUG_MSG))).start();
	}

}
