package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.csp.controller.SettingController;
import org.csp.models.Member;
import org.csp.models.Organization;
import org.csp.models.Person;
import org.csp.utilities.ListTableModel;
import org.csp.utilities.Utilities;
import org.metawidget.inspector.InspectionResultConstants;
import org.metawidget.inspector.annotation.UiAction;
import org.metawidget.inspector.annotation.UiAttribute;



public class OrganizationManageView extends OrganizationView implements IRefreshable{

	ListTableModel<Member> mMemberTableModel;
	Collection<Member>members;
	
	Organization tempO;
	private JFrame memberFrame;
	
	public OrganizationManageView(Container container) {
		super(container);	
	}

	public OrganizationManageView(Container container, boolean useLeftPanel,boolean readOnly, Organization model, boolean useButtons) {
		super(container, useLeftPanel, readOnly, model, useButtons);
	}

	public OrganizationManageView(Container container, boolean useLeftPanel,boolean readOnly, Organization model) {
		super(container, useLeftPanel, readOnly, model);
	}

	public OrganizationManageView(Container container, boolean useLeftPanel,boolean readOnly) {
		super(container, useLeftPanel, readOnly);
	}

	public OrganizationManageView(Container container, boolean useLeftPanel) {
		super(container, useLeftPanel);
	}

	protected void configureModel(){
		super.configureModel();
		this.mTModel = new ListTableModel<Organization>(Organization.class, controller.getAll(), "Name", "Address", "Type" );
	}
	
	/**
	 * Develops the UI elements for adding and querying the organizations registerd in the system
	 */
	@Override
	protected void buildRightPanel(){
		super.buildRightPanel();
		JPanel panel = new JPanel(new BorderLayout());			
		panel.add(mMetawidget, BorderLayout.CENTER);		
		this.buildBtnReflection();
		this.displayResults = true;
		panel.add(this.createResultsSection() , BorderLayout.SOUTH);		
		panelRight.add(panel ,BorderLayout.NORTH);
		addTable();
	}
	
	/**
	 * this method adds the table of members for a given organization on the right of the panel
	 * it will:
	 * 		Create a table for members
	 * 		Create a button to add new members by launching a new UI for adding members
	 */
	protected void addTable(){
		members = new ArrayList<Member>();
		mMemberTableModel = new ListTableModel<Member>(Member.class,members,"Person", "Role" );
		
		
		JPanel panel = new JPanel(new BorderLayout());
		this.panelRight.getParent().add(panel,BorderLayout.EAST);
		
		JPanel btnPanel = new JPanel();
		panel.add(btnPanel, BorderLayout.NORTH);
		
		
		if (!this.readOnly){
			JButton addMember = new JButton("Add Member");
			btnPanel.add(addMember);
			addMember.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					addMemberView();
				}
			});
		}
		
		final JTable memberTable = new JTable(mMemberTableModel);
		panel.add(new JScrollPane( memberTable ), BorderLayout.CENTER);
		Utilities.setTableDefaults(memberTable);	
		memberTable.addMouseListener( new MouseAdapter() {
			@Override
			public void mouseClicked( MouseEvent event ) {
				//Ensure user double clicks table
				if ( event.getClickCount() != 2 )return; 
				@SuppressWarnings("unchecked")
				ListTableModel<Member> model = (ListTableModel<Member>) memberTable.getModel();	
				if (memberTable.getSelectedRow() >= 0){
					Member t = model.getValueAt( memberTable.getSelectedRow() );					
					handleMemberSelect(t);
				}					
			}
		});	
	}
	
	protected void handleMemberSelect(Member t) {
		addMemberView(t);		
	}
	
	protected void addMemberView(){
		addMemberView(null);
	}

	protected void addMemberView(Member m){
		Organization o = this.mMetawidget.getToInspect();
		this.tempO = o;		
		if (o.getId() == 0){
			JOptionPane.showMessageDialog(this.container, "Must select an organization before adding members");
			return;
		}
		
		if (m == null){
			//Assign the selected organization to a new member
			m = new Member();
			m.setOrganization(o);
			m.setPerson(new Person());
		}
		//check if frame existing before close first
		if (memberFrame != null)memberFrame.dispose();
		
		//Create a new Frame for loading the dialog for new member
		memberFrame = new JFrame();
		memberFrame.setTitle("Members of "+o.getName());
		MemberManageView mv = new MemberManageView(memberFrame, true, this.readOnly);
		mv.setModel(m);
		mv.setRefreshable(this);		
		memberFrame.pack();
		memberFrame.setLocationRelativeTo(container);
		memberFrame.setVisible(true);
	}
	
	@UiAction
	public void find(){
		Organization u = this.mMetawidget.getToInspect();
		this.mMetawidget.setToInspect(new Organization());
		try {
			Collection<Organization> us = this.controller.find(u);
			if (us.size() > 0){
				this.refreshTable(us);
			}else JOptionPane.showMessageDialog(this.container, "No Organization found");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this.container, "Unable to Organization user");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}
	
	@UiAction
	@UiAttribute( name = InspectionResultConstants.HIDDEN, value = "${this.readOnly}" )
	public void save(){
		Organization o = this.mMetawidget.getToInspect();
		//Confirm user wants to update an existing organization
		if (o.getId() != 0){
			int response;
			response = JOptionPane.showConfirmDialog(null, "Update the current Organization?" );
			if (response != JOptionPane.OK_OPTION)return;
		}
		this.mMetawidget.setToInspect(new Organization());		
		try {
			//TODO put dialog to confirm if saving or updating based of if id exists
			this.controller.save(o);
			if (o.getId() != 0)JOptionPane.showMessageDialog(this.container, "Saved Organization");
			else JOptionPane.showMessageDialog(this.container, "Unable to Save Organization");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this.container, "Unable to Save Organization");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}		
		this.refreshTable();
	}
	
	//Clears the screen
	@UiAction
	public void clear(){
		Organization o = new Organization();
		this.mMetawidget.setToInspect(o);
		reset();
	}
	
	@UiAction
	@UiAttribute( name = InspectionResultConstants.HIDDEN, value = "${this.readOnly}" )
	public void delete(){
		try {
			Organization o = this.mMetawidget.getToInspect();
			if (o.getId() > 0){			
				if (this.controller.delete(o))
					JOptionPane.showMessageDialog(null, "Organization deleted");
			}else {
				clear();
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Unable to delete organization");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}
	
	//TODO evaluate why is the reset needed
//	@UiAction
	@UiAttribute( name = InspectionResultConstants.HIDDEN, value = "${this.readOnly}" )
	public void reset(){
		this.mTModel.importCollection(this.controller.getAll());
	}
	
	@Override
	protected void handleModelSelect(Organization model) {		
		this.mMetawidget.setToInspect(model);
		this.members = model.getMembers();
		this.mMemberTableModel.importCollection(members);
	}

	@Override
	public boolean refresh() {	
		try {
//			Organization o = this.mMetawidget.getToInspect();
			Organization o = this.tempO;
			if (o != null && o.getId() != 0)o = this.controller.refreshModel(o);	
			this.mMemberTableModel.importCollection(o.getMembers());			
		} catch (Exception e) {
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
		
		return false;
	}
}
