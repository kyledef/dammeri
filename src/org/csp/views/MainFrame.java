package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.csp.controller.SettingController;
import org.csp.controller.UserController;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {

	private static int WIDTH = 640;
	private static int HEIGHT = 350;
	
	public MainFrame() throws HeadlessException {
		super("DAMMERI - Home");
		init();
	}
	
	protected void init(){
		setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		this.addWindowListener(new WindowAdapter(){
		    public void windowClosing(WindowEvent e){ performCloseOperations(); }
		});
		JPanel panel = new JPanel(new BorderLayout());
		add(panel);
		showFirstDialog();
		buildHomePanel(panel);
		UserController.getInstance().registerUserEvent(UserController.LOGIN, "Start Application");
	}
	
	public void buildHomePanel(Container container){
		this.setResizable(false);		
		new HomePanel(container,  true, UserController.getInstance().isReadOnly());
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize( WIDTH, HEIGHT );
		int x = (screen.width-WIDTH)/2;
        int y = (screen.height-HEIGHT)/2;
        this.setLocation(x,y);
	}
	
	public void showFirstDialog(){
		if (SettingController.getInstance().useFirstDialog())
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						(new FirstDialogView()).setVisible(true);
					} catch (Exception e) {
						e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
					}
				}
			});
	}
	
	public void performCloseOperations(){
		UserController.getInstance().registerUserEvent(UserController.LOGOUT, "Closed Application");
	}
}
