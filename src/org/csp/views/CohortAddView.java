package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.csp.controller.ActivityController;
import org.csp.controller.BeneficiaryController;
import org.csp.controller.CohortController;
import org.csp.controller.IndicatorController;
import org.csp.controller.ObjectiveController;
import org.csp.controller.OutcomeController;
import org.csp.controller.ProjectController;
import org.csp.controller.QuestionController;
import org.csp.controller.SettingController;
import org.csp.models.Activity;
import org.csp.models.Beneficiary;
import org.csp.models.Cohort;
import org.csp.models.Indicator;
import org.csp.models.Objective;
import org.csp.models.Organization;
import org.csp.models.Outcome;
import org.csp.models.Project;
import org.csp.models.Question;
import org.csp.utilities.ListTableModel;
import org.csp.utilities.Utilities;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.metawidget.swing.SwingMetawidget;
import org.metawidget.swing.widgetprocessor.binding.beansbinding.BeansBindingProcessor;
import org.metawidget.swing.widgetprocessor.binding.beansbinding.BeansBindingProcessorConfig;

import com.j256.ormlite.support.ConnectionSource;
import com.toedter.calendar.JDateChooser;

public class CohortAddView extends JPanel {
	private static final long serialVersionUID = 53946473006758447L;
	public static long startTime;
	
	JFrame container; // the container that will launch this dialog	
	JTabbedPane tabbedPane;
	JComponent detailsPanel;
	
	Cohort cohort;
	SwingMetawidget cohortWidget;
	ProjectController pController;
	CohortController cController;
	
	JComponent beneficiariesPanel;
	Beneficiary beneficiary;
	SwingMetawidget beneficiaryWidget;
	Collection<Beneficiary> beneficiaries;
	ListTableModel<Beneficiary> mBenTable;
	BeneficiaryController bController;
		
	JComponent objectivesPanel;
	Objective objective;
	SwingMetawidget objectiveWidget;	
	Collection<Objective>objectives;
	ListTableModel<Objective> mObjnTable;
	ObjectiveController oController;
	
	JComponent activitiesPanel;
	Activity activity;
	SwingMetawidget activityWidget;
	Collection<	Activity>activities;
	ListTableModel<Activity> mActTable;
	ActivityController aController; 
	
	JComponent mnePanel;
	Question question;
	SwingMetawidget questionWidget;
	Collection<Question>questions;
	ListTableModel<Question> mQuestTable;
	QuestionController qController;
	
	JComponent indicatorPanel;
	Indicator indicator;
	SwingMetawidget indicatorWidget;
	Collection<Indicator> indicators;
	ListTableModel<Indicator> mIndTable;
	IndicatorController iController;
	
	JComponent outcomePanel;	
	Outcome outcome;
	SwingMetawidget outcomeWidget;
	Collection<Outcome>outcomes;
	ListTableModel<Outcome> mOutTable;
	OutcomeController outController;
	
	IRefreshable dataset;	
	int objectiveNumber = 0;
	private int WIDTH = 600;
	private int HEIGHT = 450;
	
	protected int currentTab = 0;

	boolean isCohortSaved;
	boolean isBeneficiarySaved = false;
	boolean isObjectiveSaved = false;
	boolean isOutcomeSaved = false;
	boolean isActivitySaved = false;
	boolean isQuestionSaved = false;
	boolean isCohortLoaded = false;

	private ConnectionSource conn;
	
	final JTextField projectName = new JTextField();
	
	private boolean readOnly = true;

	public CohortAddView() {
		super(new BorderLayout());
		this.init();
	}
	
	public CohortAddView(JFrame frame) {
		this(frame, null);
	}
	
	public CohortAddView(JFrame frame, Cohort cohort){
		this(frame, cohort, false);
	}
	
	public CohortAddView(JFrame frame, Cohort cohort, boolean readOnly){
		super(new BorderLayout());
		this.readOnly = readOnly;
		this.container = frame;
		this.init();
		if (cohort != null)loadCohort(cohort);
		
		this.container.setResizable(false);
		this.container.add(this,BorderLayout.CENTER );
	}
	
	private void removeChanges(){
		try {
			if (this.cohort.getId() > 0)
				this.cController.delete(cohort);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Unable to removed Project");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}
	
	
	public void loadCohort(Cohort cohort){
		if (cohort.getId() != 0){
			try {
				//Refresh model to ensure we have all the required project, objectives, questions models in the object
				this.cohort = this.cController.refreshModel(cohort);
				this.cohortWidget.setToInspect(cohort);
				Project p = this.cohort.getProject();
				if (p.getId() > 0){
					this.objectives = p.getObjectives();	
					System.out.printf("Found %d objectives in %s \n", objectives.size(), cohort.getProject());
					this.refreshObjectives(); 
				}
				this.activities   = this.cohort.getActivities();
				this.refreshActivities();
				System.out.printf("Found %d activities in %s \n", activities.size(), cohort.getProject());
				
				this.beneficiaries= this.cohort.getBeneficiaries();
				this.refreshBeneficiaries();
				System.out.printf("Found %d beneficiaries in %s \n", beneficiaries.size(), cohort.getProject());
				
				this.questions    = this.cohort.getQuestions();
				System.out.printf("Found %d questions in %s \n", questions.size(), cohort.getProject());
				this.outcomes	  = this.cohort.getOutcomes();
				this.refreshOutcomes();
				System.out.printf("Found %d outcomes in %s \n", outcomes.size(), cohort.getProject());
				
			} catch (Exception e) {
				e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
			}
		}else{
			System.out.println("Entered a cohort without an id");
		}
	}
	
	private void init(){
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		this.initControllers();
		projectName.setEditable(false);
		projectName.setHorizontalAlignment(JTextField.CENTER);
		buildTabs();
		
		if (!readOnly){
			container.addWindowListener(new java.awt.event.WindowAdapter() {
			    @Override
			    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
			        if (JOptionPane.showConfirmDialog(container, 
			            "Do you want to save changes? ", "", JOptionPane.YES_NO_OPTION,
			            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
			            try { saveAll(); }						
						catch (Exception e) { JOptionPane.showMessageDialog(null, "Unable to save information: " + e); }
			        }
			        else removeChanges();		
			        
			        container.dispose();
			    }
			});
		}		
	}
	
	
	public IRefreshable getDataset() { return dataset; }
	public void setDataset(IRefreshable dataset) { this.dataset = dataset;	}
	
	public void initControllers(){		
		qController = QuestionController.getInstance();	
		conn = qController.getConnection();
		aController = ActivityController.getInstance(conn);
		oController = ObjectiveController.getInstance(conn);
		cController = CohortController.getInstance(conn);
		pController = ProjectController.getInstance(conn);
		bController = BeneficiaryController.getInstance(conn);
		outController= OutcomeController.getInstance(conn);
		iController = IndicatorController.getInstance(conn);
	}
	
	public void buildTabs(){	
		//Add Tabs
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		add(tabbedPane, BorderLayout.CENTER);
		add(this.projectName, BorderLayout.NORTH);
		
		this.buildDetailsPanel();
		this.buildBeneficiariesPanel();
		this.buildObjectiviesPanel();
		this.buildOutcomePanel();
		this.buildActivitiesPanel();
		this.buildMNEPanel();		
		this.buildIndicatorPanel();		
		tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		
		//Add buttons at the bottom of panel
		this.buildBottomBar();		
		//Add tree to the side of the panel
//		this.buildSideTBar(); // disable for future version
		//Start Tabs at the beginning
		this.tabbedPane.setSelectedIndex(0);
	}
	
	protected void buildSideTBar(){
		JPanel sideBarPanel = new JPanel();
		add(sideBarPanel, BorderLayout.WEST);
		
		JTree tree = new JTree();		
		sideBarPanel.add(new JScrollPane(tree));
	}
	
	protected void buildBottomBar(){
		JPanel btnPanel = new JPanel();
		add(btnPanel, BorderLayout.SOUTH);
		
		JButton btnPrevious = new JButton("Previous");
		btnPrevious.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				currentTab = tabbedPane.getSelectedIndex();
				if (currentTab > 0)
					tabbedPane.setSelectedIndex(--currentTab);
			}
		});
		btnPanel.add(btnPrevious);
		
		JButton btnNext = new JButton("Next");
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				currentTab = tabbedPane.getSelectedIndex();
				if (currentTab < tabbedPane.getTabCount()-1)
					tabbedPane.setSelectedIndex(++currentTab);
			}
		});
		btnPanel.add(btnNext);
		
		JButton btnSaveClose = new JButton("Save & Close");
		btnSaveClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				saveCloseAction();
			}
		});
		btnPanel.add(btnSaveClose);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int response = JOptionPane.showConfirmDialog(null, "Canceling will result in loss of all changes. Do you want to continue?", "Confirm Cancel", JOptionPane.OK_CANCEL_OPTION);
				if (response == JOptionPane.OK_OPTION){
					removeChanges();
					if(container != null)container.dispose();
					else System.exit(0);
				}					
			}
		});
		btnPanel.add(btnCancel);
	}

	private boolean saveCohort() throws Exception{
		Cohort tCohort = this.cohortWidget.getToInspect();	
		if (tCohort.getId() == 0 || tCohort.getProject().getId() == 0){
			isCohortSaved = false;
			Project tProject = tCohort.getProject();
			
			//Save Project First
			if (pController == null)pController = ProjectController.getInstance();
			tProject = pController.save(tProject);
						
			if (tProject.getId() != 0){ //Project Successfully saved
				System.out.println("Saved Project with Id: " + tProject.getId());
				tCohort.setProject(tProject);			
				
				//Pull the date Programmatically
				JDateChooser sDate = (JDateChooser)this.cohortWidget.getComponent("startDate");
				tCohort.setStartDate(sDate.getDate());
				
				JDateChooser eDate = (JDateChooser)this.cohortWidget.getComponent("startDate");
				tCohort.setEndDate(eDate.getDate());
				
				if (sDate.getDate() != null 
						&& eDate.getDate() != null 
							&& sDate.getDate().after(eDate.getDate())){
					JOptionPane.showMessageDialog(null, "Start of project cannot be after project ends.");
					isCohortSaved = false;
					return false;
				}
				
				//Save the Cohort Object
				if (cController == null)cController = CohortController.getInstance();
				tCohort = cController.save(tCohort);
				if (tCohort.getId() != 0) //Cohort Successfully saved
				{					
					isCohortSaved = true;
					this.cohort = tCohort;
					System.out.printf("Cohort Created with id: %d and project id %d \n",this.cohort.getId(), this.cohort.getProject().getId() );;
					//this.cohort = this.cController.refreshModel(tCohort);
					this.cohortWidget.setToInspect(this.cohort);
				}
			}
		}else isCohortSaved = true;
		
		return isCohortSaved;
	}
	
	private Objective saveObjective(Objective o) throws Exception{
		o.setProject(this.cohort.getProject());
		o = this.oController.save(o);
		if (o.getId() > 0)System.out.println("Saved : "+ o);
		return o;
	}
	
	private void saveObjectives() throws Exception{
		if (!this.isCohortSaved)this.saveCohort();
		Iterator<Objective> oi = this.objectives.iterator();
		if (this.cohort.getProject().getId() > 0){
			while(oi.hasNext()){
				Objective o = oi.next();
				saveObjective(o);
			}
		}
	}
	
	protected Beneficiary saveBeneficiary(Beneficiary b) throws Exception{
		b.setCohort(this.cohort);
		b = this.bController.save(b);
		if (b.getId() != 0)System.out.println("Saved : " + b);
		else System.out.println("Unable to save "+b);			
		return b;
	}
	
	private void saveBeneficiaries() throws Exception{
		if (!this.isCohortSaved)this.saveCohort();
		if (this.cohort.getId() > 0){
			Iterator<Beneficiary> bs = this.beneficiaries.iterator();
			while(bs.hasNext()){
				Beneficiary b = bs.next();
				if (b.getId() == 0){
					b = saveBeneficiary(b);
					if (b.getId() > 0)System.out.println("Saved Beneficiary: "+b);
				}
			}
		}		
	}
	
	private void saveActivities() throws Exception{
		if (!this.isCohortSaved)this.saveCohort();
		if (this.cohort.getId() > 0){
			Iterator<Activity> ia = this.activities.iterator();
			while(ia.hasNext())
				saveActivity(ia.next());
		}
	}
	
	protected Activity saveActivity(Activity a)throws Exception{
		if (!this.isCohortSaved)this.saveCohort();
		if (this.cohort.getId() > 0){
			a.setCohort(this.cohort);			
			this.aController.save(a);
			if (a.getId() > 0)System.out.println("Saved: "+ a);
			else System.out.println("Unable to save: "+a);			
		}
		return a;
	}
	
	private void saveQuestions() throws Exception{
		if (!this.isCohortSaved)this.saveCohort();		
		Iterator<Question> qs = this.questions.iterator();
		while(qs.hasNext())
			saveQuestion(qs.next());
	}
	
	protected Question saveQuestion(Question q) throws Exception{
		if (this.cohort.getId() == 0)this.saveCohort();
		q.setCohort(this.cohort);
		return this.qController.save(q);
	}
	
	protected Outcome saveOutcome(Outcome o) throws Exception {
		if (this.cohort.getId() == 0)this.saveCohort();
		o.setCohort(this.cohort);
		return this.outController.save(o);		
	}
	
	private void saveOutcomes() throws Exception{
		if (!this.isCohortSaved)this.saveCohort();		
		Iterator<Outcome> os = this.outcomes.iterator();
		while(os.hasNext()) saveOutcome(os.next());		
	}
	
	protected Indicator saveIndicator(Indicator i) throws Exception{
		if (i.getQuestion().getId() == 0 )throw new Exception("Indicator must be associated with a question");
		return i;
	}
	/**
	 * will save all the changes made via the form to the database
	 * Will save new record or update if record previously existed
	 */
	protected void saveCloseAction() {	
		try{
			saveAll();
			JOptionPane.showMessageDialog(this, "Project Saved");			
			this.container.dispose();
		}catch(Exception e){
			JOptionPane.showMessageDialog(this, "Could not save changes to the project: " + e.getMessage());
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
			System.out.println(e.getMessage());	
		}
	}
	
	public void saveAll() throws Exception{
		if (this.saveCohort()){
			this.saveObjectives();
			this.saveBeneficiaries();
			this.saveActivities();
			this.saveQuestions();
			this.saveOutcomes();
		}
	}
		
	protected JComponent makeTextPanel(String text) {
        JPanel panel = new JPanel(false);
        JLabel filler = new JLabel(text);
        filler.setHorizontalAlignment(JLabel.CENTER);
        panel.setLayout(new BorderLayout());
        panel.add(filler, BorderLayout.NORTH);
        return panel;
    }
	
	//Build a popup menu
	public JPopupMenu  buildPopUpMenuTemplate(){
		JPopupMenu popup = new JPopupMenu ();		
		return popup;
	}
	
	protected void buildDetailsPanel(){
		detailsPanel = this.makeTextPanel("Details");
		tabbedPane.addTab("Details", detailsPanel);
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
		
		if (cohort == null){
			cohort = new Cohort();
			cohort.setFunder(new Organization());			
			cohort.setProject(new Project());
		}
		cohortWidget = new SwingMetawidget();
		cohortWidget.setConfig( "org/csp/views/metawidget.xml" );
		cohortWidget.setToInspect(cohort);
		cohortWidget.setReadOnly(readOnly);
		cohortWidget.addWidgetProcessor( new BeansBindingProcessor(
				new BeansBindingProcessorConfig()
				.setUpdateStrategy( UpdateStrategy.READ_WRITE )) );		
		
		if (!readOnly){
			//Setup listener to record changes
			final JTextField nameF = ((SwingMetawidget)cohortWidget.getComponent("project")).getComponent("name");
			if (nameF != null){
				nameF.getDocument().addDocumentListener(new DocumentListener() {
					public void changedUpdate(DocumentEvent e) { updateLabel(e); }
					public void insertUpdate(DocumentEvent e)  { updateLabel(e); }
					public void removeUpdate(DocumentEvent e)  { updateLabel(e); }
					private void updateLabel(DocumentEvent e)  {
						EventQueue.invokeLater(new Runnable() {
		                    public void run() { projectName.setText(nameF.getText()); }	                    
		                });
					}
				});
			}
		}
		detailsPanel.add(cohortWidget, BorderLayout.CENTER);
	}
	
	protected void buildBeneficiariesPanel(){
		beneficiariesPanel = this.makeTextPanel("Beneficiaries");
		tabbedPane.addTab("Beneficiaries", beneficiariesPanel);
		tabbedPane.setMnemonicAt(1, KeyEvent.VK_2);
		
		if (this.beneficiary == null)beneficiary = new Beneficiary();
		if (this.beneficiaries == null)beneficiaries = new ArrayList<Beneficiary>();
		if (this.mBenTable == null)mBenTable = new ListTableModel<Beneficiary>(Beneficiary.class, this.beneficiaries, "Description", "Sex", "AgeGroup");
		
		this.beneficiaryWidget = new SwingMetawidget();
		beneficiaryWidget.setConfig( "org/csp/views/metawidget.xml" );
		beneficiaryWidget.setToInspect(beneficiary);
		beneficiaryWidget.setReadOnly(readOnly);
		beneficiaryWidget.addWidgetProcessor( new BeansBindingProcessor(
				new BeansBindingProcessorConfig()
				.setUpdateStrategy( UpdateStrategy.READ_WRITE )) );
		
		//Add checkbox for future implementation of single and group beneficiaries
		
		JCheckBox check = new JCheckBox("Individual Beneficiary");
		beneficiaryWidget.add(check);
		
		this.beneficiariesPanel.add(beneficiaryWidget, BorderLayout.NORTH);
		
		//Add Buttons
		JButton addBtn = new JButton("Add");
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Beneficiary b = beneficiaryWidget.getToInspect();
				try{
					beneficiaries.add(saveBeneficiary(b));					
					beneficiaryWidget.setToInspect(new Beneficiary());
					refreshBeneficiaries();
				}catch(Exception e){
					JOptionPane.showMessageDialog(null, "Unable to add Beneficiary: "+ e.getMessage());
				}
			}
		});
		
		JPanel btnPanel = new JPanel();
		btnPanel.add(addBtn);
		this.beneficiariesPanel.add(btnPanel, BorderLayout.CENTER);
		
		
		
		//Create table of added beneficiaries
		JTable table = new JTable(mBenTable);
		Utilities.setTableDefaults(table);
		JScrollPane scroll =  new JScrollPane( table ); 
		//Set Height of table
		scroll.setPreferredSize( new Dimension((int)  scroll.getPreferredSize().getWidth(), 200));
		this.beneficiariesPanel.add( scroll,BorderLayout.SOUTH);		
	}
	
	protected void buildObjectiviesPanel(){
		objectivesPanel = this.makeTextPanel("Objectives");
		tabbedPane.addTab("Objectives", objectivesPanel);
		tabbedPane.setMnemonicAt(2, KeyEvent.VK_3);
		
		if (this.objective == null)objective = new Objective();
		if (this.objectives == null)objectives = new ArrayList<Objective>();
		if (this.mObjnTable == null)this.mObjnTable = new ListTableModel<Objective>(Objective.class, this.objectives,"Number", "Objective");		
		
		this.objectiveWidget = new SwingMetawidget();
		objectiveWidget.setConfig( "org/csp/views/metawidget.xml" );
		objectiveWidget.setToInspect(objective);
		objectiveWidget.setReadOnly(readOnly);
		objectiveWidget.addWidgetProcessor( new BeansBindingProcessor(
				new BeansBindingProcessorConfig()
				.setUpdateStrategy( UpdateStrategy.READ_WRITE )) );
		this.objectivesPanel.add(objectiveWidget, BorderLayout.NORTH);
		
		
		//Create table of added objectives
		JTable table = new JTable(mObjnTable);
		Utilities.setTableDefaults(table);
		JScrollPane scroll =  new JScrollPane( table ); 
		//Set Height of table
		scroll.setPreferredSize( new Dimension((int)  scroll.getPreferredSize().getWidth(), 200));
		this.objectivesPanel.add(scroll,BorderLayout.SOUTH);
		
		
		//Add Buttons
		JButton addBtn = new JButton("Add");
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Objective o = objectiveWidget.getToInspect();
				try {
					if (cohort.getId() == 0)saveCohort();					
					o = saveObjective(o);
					o.setNumber(++objectiveNumber);
					objectives.add(o);					
					objectiveWidget.setToInspect(new Objective());
					refreshObjectives();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Unable to add Objective: "+ e.getMessage());
					e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
				}				
			}
		});
		
		JPanel btnPanel = new JPanel();
		btnPanel.add(addBtn);
		this.objectivesPanel.add(btnPanel, BorderLayout.CENTER);
	}
	
	protected void buildActivitiesPanel(){
		activitiesPanel = this.makeTextPanel("Activities");
		tabbedPane.addTab("Activities", activitiesPanel);
		tabbedPane.setMnemonicAt(3, KeyEvent.VK_4);
		
		if (this.activity == null){
			activity = new Activity();
			activity.setObjective(new Objective()); 
		}
		if (this.activities == null)activities = new ArrayList<Activity>();
		if (this.mActTable == null)mActTable = new ListTableModel<Activity>(Activity.class, this.activities, "Activity", "Objective", "Description");
		this.activityWidget = new SwingMetawidget();
		
		activityWidget.setConfig( "org/csp/views/metawidget.xml" );
		activityWidget.setToInspect(activity);
		activityWidget.setReadOnly(readOnly);
		activityWidget.addWidgetProcessor( new BeansBindingProcessor(
				new BeansBindingProcessorConfig()
				.setUpdateStrategy( UpdateStrategy.READ_WRITE )) );
				
		this.activitiesPanel.add(activityWidget, BorderLayout.NORTH);
		
		//Create table of added activities
		final JTable table = new JTable(mActTable);
		Utilities.setTableDefaults(table);
		JScrollPane scroll =  new JScrollPane( table ); 
		table.addMouseListener( new MouseAdapter() {
			@Override
			public void mouseClicked( MouseEvent event ) {
				//Ensure user double clicks table
				if ( event.getClickCount() != 2 )return; 
				@SuppressWarnings("unchecked")
				ListTableModel<Activity> model = (ListTableModel<Activity>) table.getModel();	
				if (table.getSelectedRow() >= 0){
					Activity t = model.getValueAt( table.getSelectedRow() );					
					System.out.println(t);
					JFrame subFrame = new JFrame();
					new OutputView(subFrame);
					subFrame.pack();
					subFrame.setVisible(true);
					
				}					
			}
		});	
		//Set Height of table
		scroll.setPreferredSize( new Dimension((int)  scroll.getPreferredSize().getWidth(), 200));
		this.activitiesPanel.add(scroll,BorderLayout.SOUTH);
		
		//Add Buttons
		JButton addBtn = new JButton("Add");
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Activity a =  activityWidget.getToInspect();
				if (a.getActivity() != null && !a.getActivity().equals("")) {
					try {
						if (cohort.getId() == 0)saveCohort();
						JComboBox<Objective> obj = activityWidget.getComponent("objective");
						int select = obj.getSelectedIndex();
						if (select > 0 ){
							System .out.printf("Selected %d choice and found %s \n", select, objectives.toArray()[select - 1]);
							a.setObjective((Objective)objectives.toArray()[select - 1]);
						}
						System.out.println(a);
						
						activities.add(saveActivity(a));
						
						Objective [] objs = new Objective[objectives.size()];
						objs = objectives.toArray(objs);
						a = new Activity();
						a.setObjectives(objs);
						
						activityWidget.setToInspect(a);
					} catch (Exception e) {
						JOptionPane.showMessageDialog(container, "Unable to add Activity: "+e.getMessage());
						e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
					}	
				}else
					JOptionPane.showMessageDialog(container, "Must specify the activity to be added");
				refreshActivities();
			}
		});
		
		JPanel btnPanel = new JPanel();
		btnPanel.add(addBtn);
		this.activitiesPanel.add(btnPanel, BorderLayout.CENTER);
	}

	protected void buildMNEPanel(){
		mnePanel = this.makeTextPanel("Monitoring and Evaulation");
		tabbedPane.addTab("M&E Questions", mnePanel);
		tabbedPane.setMnemonicAt(3, KeyEvent.VK_5);
		
		if (this.question == null)question = new Question();
		if (this.questions == null)questions = new ArrayList<Question>();
		if (this.mQuestTable == null)mQuestTable = new ListTableModel<Question>(Question.class, this.questions, "Question", "Type");
		this.questionWidget = new SwingMetawidget();
		questionWidget.setConfig( "org/csp/views/metawidget.xml" );
		questionWidget.setToInspect(question);
		questionWidget.setReadOnly(readOnly);
		questionWidget.addWidgetProcessor( new BeansBindingProcessor(
				new BeansBindingProcessorConfig()
				.setUpdateStrategy( UpdateStrategy.READ_WRITE )) );
		this.mnePanel.add(questionWidget, BorderLayout.NORTH);
		
		//Create table of added questions
		final JTable table = new JTable(mQuestTable);
		Utilities.setTableDefaults(table);
		
		table.addMouseListener( new MouseAdapter() {
			@Override
			public void mouseClicked( MouseEvent event ) {
				
				//Ensure user double clicks table
				if ( event.getClickCount() != 2 )return; 
				System.out.println("Clicked");
				@SuppressWarnings("unchecked")
				ListTableModel<Question> model = (ListTableModel<Question>) table.getModel();	
				Question t = model.getValueAt( table.getSelectedRow() );
				System.out.println(t);
				buildIndicatorAddView();
//				IndicatorsAddView iav = new IndicatorsAddView(t);
//				JFrame frame = new JFrame();
//				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//				frame.getContentPane().add(iav, BorderLayout.CENTER);
//				frame.pack();
//				frame.setVisible(true);
			}			
		});		
		JScrollPane scroll =  new JScrollPane( table ); 
		//Set Height of table
		scroll.setPreferredSize( new Dimension((int)  scroll.getPreferredSize().getWidth(), 200));
		this.mnePanel.add(scroll,BorderLayout.SOUTH);
		
		//Add Buttons
		JButton addBtn = new JButton("Add");
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Question q = questionWidget.getToInspect();
				try{
					if (cohort.getId() == 0)saveCohort();
					if (q.getQuestion() != null && !q.getQuestion().equals("")){
						q = saveQuestion(q);
						questions.add( q );
						questionWidget.setToInspect(new Question());
					}else
						JOptionPane.showMessageDialog(null, "Must Specify a question");
				}catch(Exception e){
					JOptionPane.showMessageDialog(null, "Unable to add Question: "+ e.getMessage());
					e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
				}
				//TODO  conversion for enum not working for questions
				refreshMNE();
			}
		});
		
		JPanel btnPanel = new JPanel();
		btnPanel.add(addBtn);
		this.mnePanel.add(btnPanel, BorderLayout.CENTER);
	}
		
	protected void buildIndicatorPanel(){
		this.indicatorPanel = this.makeTextPanel("Indicators");
		tabbedPane.addTab("Indicators", indicatorPanel);
		tabbedPane.setMnemonicAt(3, KeyEvent.VK_6);
		
		this.indicatorWidget = new SwingMetawidget();
		if (this.indicator == null){
			this.indicator = new Indicator();
			this.indicator.setQuestion(this.question);
		}
		if (this.indicators == null)this.indicators = new ArrayList<Indicator>();
		if (this.mIndTable == null)this.mIndTable = new ListTableModel<Indicator>(Indicator.class, this.indicators, "Indicator","Type","Direction", "Definition", "Frequency","Target");
		
		indicatorWidget.setConfig( "org/csp/views/metawidget.xml" );
		indicatorWidget.setToInspect(this.indicator);
		indicatorWidget.addWidgetProcessor( new BeansBindingProcessor(
				new BeansBindingProcessorConfig()
				.setUpdateStrategy( UpdateStrategy.READ_WRITE )) );
//		this.indicatorPanel.add(indicatorWidget, BorderLayout.NORTH);
		
		final JTable table = new JTable(mIndTable);
		table.addMouseListener( new MouseAdapter() {
			@Override
			public void mouseClicked( MouseEvent event ) {
				//Ensure user double clicks table
				if ( event.getClickCount() != 2 )return; 
				@SuppressWarnings("unchecked")
				ListTableModel<Indicator> model = (ListTableModel<Indicator>) table.getModel();	
				if (table.getSelectedRow() >= 0){
					Indicator t = model.getValueAt( table.getSelectedRow() );
					System.out.println(t);
				}					
			}
		});	
		Utilities.setTableDefaults(table);
		JScrollPane scroll =  new JScrollPane( table ); 
		//Set Height of table
		scroll.setPreferredSize( new Dimension((int)  scroll.getPreferredSize().getWidth(), 200));
		this.indicatorPanel.add(scroll,BorderLayout.SOUTH);
		
		JButton addBtn = new JButton("Add");
		JPanel btnPanel = new JPanel();
		btnPanel.add(addBtn);
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (questions.size() > 0) buildIndicatorAddView();
				else JOptionPane.showMessageDialog(null, "Must First Add Questions before creating indicators");		
			}
		});
		this.indicatorPanel.add(btnPanel, BorderLayout.CENTER);
	}
	
	protected void buildOutcomePanel(){
		this.outcomePanel = this.makeTextPanel("Outcomes");
		tabbedPane.addTab("Outcomes", outcomePanel);
		tabbedPane.setMnemonicAt(3, KeyEvent.VK_7);
		
		if (this.outcomeWidget == null)outcomeWidget = new SwingMetawidget();
		if (this.outcome == null)this.outcome = new Outcome();
		if (this.outcomes == null)this.outcomes = new ArrayList<Outcome>();
		if (this.mOutTable == null)this.mOutTable = new ListTableModel<Outcome>(Outcome.class, this.outcomes,"Outcome", "Timeframe");
		this.outcomeWidget.setConfig( "org/csp/views/metawidget.xml" );
		outcomeWidget.setToInspect(outcome);
		outcomeWidget.setReadOnly(readOnly);
		outcomeWidget.addWidgetProcessor( new BeansBindingProcessor(
				new BeansBindingProcessorConfig()
				.setUpdateStrategy( UpdateStrategy.READ_WRITE )) );
		this.outcomePanel.add(outcomeWidget, BorderLayout.NORTH);
		
		final JTable table = new JTable(mOutTable);
		Utilities.setTableDefaults(table);
		JScrollPane scroll =  new JScrollPane( table ); 
		//Set Height of table
		scroll.setPreferredSize( new Dimension((int)  scroll.getPreferredSize().getWidth(), 200));
		this.outcomePanel.add(scroll,BorderLayout.SOUTH);
		
		JButton addBtn = new JButton("Add");
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Outcome o = outcomeWidget.getToInspect();
				try {
					o=saveOutcome(o);
					outcomes.add(o);
					outcomeWidget.setToInspect(new Outcome());
					
					refreshOutcomes();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Unable to add Outcome");
					e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
				}				
			}
		});
		JPanel btnPanel = new JPanel();
		btnPanel.add(addBtn);
		this.outcomePanel.add(btnPanel, BorderLayout.CENTER);
	}
	
	private void indicatorCombo(){
		this.indicatorWidget.setToInspect(this.indicator);
		if (questions.size() > 0){
			Question [] quests = new Question[questions.size()];
			quests = questions.toArray(quests);								
			indicator.setQuestions(quests);
		}
	}
	
	protected void buildIndicatorAddView(){
		JFrame frame = new JFrame();
		JPanel panel = new JPanel(new BorderLayout());
		frame.add(panel);
		JPanel btnPanel = new JPanel();
		panel.add(btnPanel, BorderLayout.SOUTH);
		
		indicatorCombo();		
		panel.add(indicatorWidget, BorderLayout.NORTH);

		JButton addBtn = new JButton("Save");
		btnPanel.add(addBtn);
		addBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Indicator in = indicatorWidget.getToInspect();
				if (in.getQuestion() == null)JOptionPane.showMessageDialog(null, "Must select a question");
				else{
					try{
						in = iController.save(in);
						if (in.getId() > 0){
							indicators.add(in);
							refreshIndicators();
							indicator = new Indicator();
							indicatorCombo();
							JOptionPane.showMessageDialog(null, "Added Indicator");
						}else JOptionPane.showMessageDialog(null, "Unable to save Indicator");
					}catch(Exception e){
						JOptionPane.showMessageDialog(null, "Unable to save Indicator");
					}
				}
			}
		});
		
		JButton clearBtn = new JButton("Clear");
		btnPanel.add(clearBtn);
		clearBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				indicator = new Indicator();
				indicatorCombo();
			}
		});
		
		frame.pack();
		frame.setVisible(true);
	}
	
	protected void refreshOutcomes() {
		this.mOutTable.importCollection(this.outcomes);	
	}	

	protected void refreshBeneficiaries(){
		mBenTable.importCollection(beneficiaries);
	}
	
	protected void refreshObjectives() {
		if (this.activity != null){
			Objective [] objs = new Objective[this.objectives.size()];
			objs = objectives.toArray(objs);
			System.out.printf("Attempting to add %d objectives to activity \n", objs.length);
			this.activity.setObjectives(objs);
			this.activityWidget.setToInspect(activity);
		}
		this.mObjnTable.importCollection(objectives);		
	}
	
	protected void refreshActivities() {
		this.mActTable.importCollection(activities);		
	}
	
	protected void refreshMNE() {
		this.mQuestTable.importCollection(questions);		
	}
	
	protected void refreshIndicators(){
		this.mIndTable.importCollection(indicators);
	}	
}
