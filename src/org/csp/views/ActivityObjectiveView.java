package org.csp.views;

import java.awt.Container;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JOptionPane;

import org.csp.controller.ActivityObjectiveController;
import org.csp.controller.SettingController;
import org.csp.models.Activity;
import org.csp.models.ActivityObjective;
import org.csp.models.Objective;
import org.csp.utilities.ListTableModel;
import org.metawidget.inspector.InspectionResultConstants;
import org.metawidget.inspector.annotation.UiAction;
import org.metawidget.inspector.annotation.UiAttribute;
import org.metawidget.inspector.annotation.UiHidden;
import org.metawidget.swing.SwingMetawidget;

public class ActivityObjectiveView extends ModelView<ActivityObjective> {

	Collection<ActivityObjective> actobjs;
	
	public ActivityObjectiveView(Container container, boolean useLeftPanel,boolean readOnly, ActivityObjective model, boolean useButtons,boolean displayResults) {
		super(container, useLeftPanel, readOnly, model, useButtons, displayResults);
	}

	public ActivityObjectiveView(Container container, boolean useLeftPanel,boolean readOnly, ActivityObjective model, boolean useButtons) {
		super(container, useLeftPanel, readOnly, model, useButtons);
	}

	public ActivityObjectiveView(Container container, boolean useLeftPanel,boolean readOnly, ActivityObjective model) {
		super(container, useLeftPanel, readOnly, model);
	}

	public ActivityObjectiveView(Container container, boolean useLeftPanel,boolean readOnly) {
		super(container, useLeftPanel, readOnly);
	}

	public ActivityObjectiveView(Container container, boolean useLeftPanel) {
		super(container, useLeftPanel);
	}

	public ActivityObjectiveView(Container container) {
		super(container);
	}
	
	protected void initUI(){
		super.initUI();
		enhanceActivity();
	}
	
	protected void enhanceActivity(){
		if (!this.readOnly){
			SwingMetawidget constant = (SwingMetawidget)this.mMetawidget.getComponent("activity");
			constant.setReadOnly(true);
		}
	}

	@Override
	protected void configureModel() {
		if(this.model == null){
			this.model = new ActivityObjective();
			this.model.setActivity(new Activity());
			this.model.setObjective(new Objective());
		}
		if (this.controller == null)this.controller = ActivityObjectiveController.getInstance(SettingController.getInstance().getConnection());
		if (actobjs  == null)actobjs = new ArrayList<ActivityObjective>();
		this.mTModel = new ListTableModel<ActivityObjective>(ActivityObjective.class, actobjs, "activity", "objective" );
		if (model.getActivity().getId() > 0 || model.getObjective().getId() > 0){
			reloadCollection();
		}
	}
	
	public void setActivity(Activity activity){
		this.model.setActivity(activity);
		reloadCollection();
	}
	
	private void reloadCollection(){
		try{
			this.actobjs = this.controller.read(model);
			System.out.println(actobjs.size());
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
		refreshTable(actobjs);
	}
	
	public void setObjective(Objective o){
		this.model.setObjective(o);
	}
	
	@UiHidden
	public boolean isReadOnly() {
		return this.readOnly;
	}
	
	@UiAction
	@UiAttribute( name = InspectionResultConstants.HIDDEN, value = "${this.readOnly}" )
	public void add(){
		ActivityObjective ao = this.mMetawidget.getToInspect();
		if (ao.getObjective().getId() > 0){
			System.out.println("Objective selected: "+ao.getObjective());
			try {
				boolean isNew = (ao.getId() == 0);
				if (isNew)
					this.controller.save(ao);
				else
					this.controller.update(ao);
				
				if (isNew){
					this.actobjs.add(ao);
					refreshTable(actobjs);
				}
				clear();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(container, "Unable to add Objective");
				e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
			}
		}
	}
	
	@UiAction
	@UiAttribute( name = InspectionResultConstants.HIDDEN, value = "${this.readOnly}" )
	public void delete(){
		ActivityObjective ao = this.mMetawidget.getToInspect();
		if (ao.getId() > 0){
			try {
				if (this.controller.delete(ao))
					JOptionPane.showMessageDialog(container, "Record deleted successfully");
				
				clear();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(container, "Unable to delete record");
				e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
			}
		}else
			JOptionPane.showMessageDialog(container, "Select the record to be deleted first");
	}
	
	
	@UiAction
	public void clear(){
		ActivityObjective temp = new ActivityObjective();
		temp.setActivity(model.getActivity());
		temp.setObjectives(model.getObjectives());
		
		model = temp;
		this.mMetawidget.setToInspect(model);
		this.enhanceActivity();
	}
	
}
