package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import org.csp.controller.SettingController;

public abstract class View {
	
protected static final int COMPONENT_SPACING = 5;
	
	protected Container container;
	protected boolean useLeftPanel;
	protected boolean readOnly;
	protected boolean useButtons;
	
	protected long startTime;
	protected boolean shown;
	
	protected String className;
	protected SettingController sc;
	
	public View(Container container){
		this(container, true);
	}
	
	public View(Container container, boolean useLeftPanel){
		this(container, useLeftPanel, true, true);
		
	}
	public View(Container container, boolean useLeftPanel, boolean readOnly){
		this(container, useLeftPanel, readOnly, true);
	}
	public View(Container container, boolean useLeftPanel, boolean readOnly, boolean useButtons){
		
		startTime = System.nanoTime();
		this.className = this.getClass().toString();
		
		this.container = container;
		this.useLeftPanel = useLeftPanel;
		this.readOnly = readOnly;
		this.useButtons = useButtons;
		
		shown = false;		
		this.init();
	}

	protected void init(){		
		sc = SettingController.getInstance();
		configureListeners();
		setSystemUI();
		if (useLeftPanel)buildleftPanel();
	}
	
	/**
	 * Will attempt to use some version of theme different from the default Java SWING themes
	 */
	protected void setSystemUI() {
		try {
			for ( LookAndFeelInfo info : UIManager.getInstalledLookAndFeels() )
				if ( "Nimbus".equals(info.getName())) { UIManager.setLookAndFeel( info.getClassName() ); break;}			
		} catch ( Exception e ) { }		
	}
	
	/**
	 * Add the image components of the application
	 * 
	 */
	protected void buildleftPanel() {
		JLabel label = new JLabel();
		URL url = Thread.currentThread().getContextClassLoader().getResource( "res/simmeda_icon.png" );
		if (url != null){
			ImageIcon imageAddressBook = new ImageIcon( url );
			label = new JLabel( imageAddressBook );
		}
		label.setVerticalAlignment( SwingConstants.TOP );
		label.setBorder( BorderFactory.createEmptyBorder( COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING * 2 ) );
		container.add( label, BorderLayout.WEST );
	}
	
	//Add listeners to the open and close event of the various forms
	protected void configureListeners(){
		if (this.container != null){
			this.container.addComponentListener(new ComponentAdapter(){
				//code run when container is hidden
				public void componentHidden(ComponentEvent e) {
					shown = false;
					//Keep track of how long a view was kept open for
					if (startTime != 0){
						double seconds = (double)((System.nanoTime() - startTime) / 1000000000.0);
						sc.sendToServer(String.format("open,%s,%f", className, seconds), SettingController.SVR_PERF_MSG);
						startTime = 0;
					}
				}
				//code run when container is shown
				public void componentShown(ComponentEvent e) {
					shown = true;
					if (startTime != 0 ){
						double seconds = (double)((System.nanoTime() - startTime) / 1000000000.0);
						sc.sendToServer(String.format("load,%s,%f", className, seconds), SettingController.SVR_PERF_MSG);
					}
					//Else restart the counter to determine how long the system is used
					else startTime = System.nanoTime();
				}
			});
		}
	}
}
