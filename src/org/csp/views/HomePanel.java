package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import org.csp.controller.UserController;

public class HomePanel extends View {
	
	private static final int WIDTH = 460;
	private static final int HEIGHT = 350;
	private static final int buttonWidth = 110;
	private static final int buttonHeight = 30;
	private static final int iconWidth = 64;
	private static final int iconHeight = 64;
	
	private JFrame subFrame;
	private JPanel containerPanel;
	protected JPanel panelRight;	
	
	/**
	 * @wbp.parser.constructor
	 */

	public HomePanel(Container container, boolean useLeftPanel,
			boolean readOnly, boolean useButtons) {
		super(container, useLeftPanel, readOnly, useButtons);
	}
	
	public HomePanel(Container container, boolean useLeftPanel, boolean readOnly){
		super(container, useLeftPanel, readOnly, true);
	}


	public HomePanel(Container container, boolean useLeftPanel) {
		super(container, useLeftPanel);
	}


	public HomePanel(Container container) {
		super(container);
	}
	
	protected void createTopBtns(){
		JPanel topPanel = new JPanel(new BorderLayout());
		this.panelRight.add(topPanel, BorderLayout.NORTH);
		JPanel btnPanel = new JPanel();
		topPanel.add(btnPanel, BorderLayout.EAST);
		
		JButton feedbackBtn = new JButton(new ImageIcon(HomePanel.class.getResource("/res/feedback-icon.png")));
		feedbackBtn.setSize(buttonHeight, buttonHeight);
		feedbackBtn.setToolTipText("Feedback");
		btnPanel.add(feedbackBtn);
		feedbackBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				sendFeedback();
			}
		});
		
		JButton logoutBtn = new JButton(new ImageIcon(HomePanel.class.getResource("/res/logout-icon.png")));
		logoutBtn.setSize(buttonHeight, buttonHeight);
		logoutBtn.setToolTipText("Logout");
		btnPanel.add(logoutBtn);
		logoutBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				logout();
			}
		});
		
		JButton exitBtn = new JButton(new ImageIcon(HomePanel.class.getResource("/res/exit-icon.png")));
		exitBtn.setSize(buttonHeight, buttonHeight);
		exitBtn.setToolTipText("Exit");
		btnPanel.add(exitBtn);
		exitBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				exit();
			}
		});
	}
	
	/**
	 * @wbp.parser.entryPoint
	 */
	protected void init(){	
		super.init();
		containerPanel = new JPanel();
		Dimension d = new Dimension(WIDTH, HEIGHT);
		containerPanel.setPreferredSize(d);
		SpringLayout springLayout = new SpringLayout();
		containerPanel.setLayout(springLayout);
		
//		JMenuBar mainMenuBar = new JMenuBar();
//		container.add(mainMenuBar, BorderLayout.NORTH);
//		JMenu aboutMenu = new JMenu("About");
//		mainMenuBar.add(aboutMenu);
//		JMenuItem aboutOption = new JMenuItem("About Dammeri");
//		aboutMenu.add(aboutOption);
//		JMenuItem aboutMEOption = new JMenuItem("About M&E");
//		aboutMenu.add(aboutMEOption);
		
		
		panelRight = new JPanel(new BorderLayout());
		panelRight.setBorder( BorderFactory.createEmptyBorder( COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING ) );
		container.add( panelRight, BorderLayout.CENTER );
		panelRight.add(containerPanel, BorderLayout.CENTER);
		
		createTopBtns();
		
		JPanel rightpanel = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, rightpanel, 29, SpringLayout.NORTH, containerPanel);
		springLayout.putConstraint(SpringLayout.WEST, rightpanel, 10, SpringLayout.WEST, containerPanel);
		springLayout.putConstraint(SpringLayout.SOUTH, rightpanel, -10, SpringLayout.SOUTH, containerPanel);
		springLayout.putConstraint(SpringLayout.EAST, rightpanel, 209, SpringLayout.WEST, containerPanel);
		SpringLayout sl_rightpanel = new SpringLayout();
		rightpanel.setLayout(sl_rightpanel);
		containerPanel.add(rightpanel);
		
		JButton btnManageMembers = new JButton("Persons");
		btnManageMembers.setToolTipText("Manages all individuals created in the system.");		
		sl_rightpanel.putConstraint(SpringLayout.NORTH, btnManageMembers, 34, SpringLayout.NORTH, rightpanel);
		btnManageMembers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (subFrame != null)subFrame.dispose();
				subFrame = new JFrame();
				subFrame.setLocation(container.getLocationOnScreen());
				buildMemberUI(subFrame);
				subFrame.pack();
				subFrame.setVisible(true);	
			}
		});
		btnManageMembers.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
		rightpanel.add(btnManageMembers);
		
		JButton btnManageProject = new JButton("Projects");
		sl_rightpanel.putConstraint(SpringLayout.NORTH, btnManageProject, 34, SpringLayout.SOUTH, btnManageMembers);
		sl_rightpanel.putConstraint(SpringLayout.WEST, btnManageProject, 0, SpringLayout.WEST, btnManageMembers);
		btnManageProject.setToolTipText("Manages all project related information in the database. This includes all activies and indicators contained within Projects.");
		btnManageProject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (subFrame != null)subFrame.dispose();
				subFrame = new JFrame();
				subFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);				
				subFrame.setLocation(container.getLocationOnScreen());
				buildProjectUI(subFrame);
				subFrame.pack();
				subFrame.setVisible(true);	
			}
		});
		btnManageProject.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
		rightpanel.add(btnManageProject);
		
		JButton btnViewReports = new JButton("Queries");
		sl_rightpanel.putConstraint(SpringLayout.NORTH, btnViewReports, 39, SpringLayout.SOUTH, btnManageProject);
		sl_rightpanel.putConstraint(SpringLayout.SOUTH, btnViewReports, 69, SpringLayout.SOUTH, btnManageProject);
		btnViewReports.setToolTipText("Povides ability to search and download data from within the database");
		sl_rightpanel.putConstraint(SpringLayout.EAST, btnViewReports, 0, SpringLayout.EAST, btnManageMembers);
		btnViewReports.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (subFrame != null)subFrame.dispose();
				subFrame = new JFrame();
				subFrame.setLocation(container.getLocationOnScreen());
				buildQueryUI(subFrame);
				subFrame.pack();
				subFrame.setVisible(true);	
			}
		});
		btnViewReports.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
		rightpanel.add(btnViewReports);
		
		JLabel members_icon = new JLabel();
		sl_rightpanel.putConstraint(SpringLayout.EAST, members_icon, -125, SpringLayout.EAST, rightpanel);
		sl_rightpanel.putConstraint(SpringLayout.WEST, btnManageMembers, 6, SpringLayout.EAST, members_icon);
		members_icon.setIcon(new ImageIcon(HomePanel.class.getResource("/res/37-Group-icon.png")));
		members_icon.setPreferredSize(new Dimension(iconWidth, iconHeight));
		rightpanel.add(members_icon);
		
		JLabel projets_icon = new JLabel();
		sl_rightpanel.putConstraint(SpringLayout.EAST, projets_icon, -125, SpringLayout.EAST, rightpanel);
		sl_rightpanel.putConstraint(SpringLayout.NORTH, projets_icon, 80, SpringLayout.NORTH, rightpanel);
		sl_rightpanel.putConstraint(SpringLayout.SOUTH, members_icon, -6, SpringLayout.NORTH, projets_icon);
		projets_icon.setIcon(new ImageIcon(HomePanel.class.getResource("/res/project-icon.png")));
		projets_icon.setPreferredSize(new Dimension(iconWidth, iconHeight));
		rightpanel.add(projets_icon);
		
		JLabel reports_icon = new JLabel();
		sl_rightpanel.putConstraint(SpringLayout.WEST, reports_icon, 0, SpringLayout.WEST, members_icon);
		sl_rightpanel.putConstraint(SpringLayout.SOUTH, reports_icon, -10, SpringLayout.SOUTH, rightpanel);
		reports_icon.setIcon(new ImageIcon(HomePanel.class.getResource("/res/project-plan-icon.png")));
		reports_icon.setPreferredSize(new Dimension(iconWidth, iconHeight));
		rightpanel.add(reports_icon);
		
		//Left Panel Components
		JPanel leftpanel = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, leftpanel, 29, SpringLayout.NORTH, containerPanel);
		springLayout.putConstraint(SpringLayout.WEST, leftpanel, 6, SpringLayout.EAST, rightpanel);
		springLayout.putConstraint(SpringLayout.SOUTH, leftpanel, -10, SpringLayout.SOUTH, containerPanel);
		springLayout.putConstraint(SpringLayout.EAST, leftpanel, -10, SpringLayout.EAST, containerPanel);
		SpringLayout sl_leftpanel;
		sl_leftpanel = new SpringLayout();
		leftpanel.setLayout(sl_leftpanel);
		containerPanel.add(leftpanel);
		
		JButton btnManageOrganizations = new JButton("Organizations");
		btnManageOrganizations.setToolTipText("Manages all organizations refered to by the system. Organizations can refer to both funders and partner organizations");
		sl_leftpanel.putConstraint(SpringLayout.NORTH, btnManageOrganizations, 38, SpringLayout.NORTH, leftpanel);
		btnManageOrganizations.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (subFrame != null)subFrame.dispose();
				subFrame = new JFrame();	
				subFrame.setLocation(container.getLocationOnScreen());
				buildOrganizationUI(subFrame);
				subFrame.pack();
				subFrame.setVisible(true);
			}
		});
		btnManageOrganizations.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
		leftpanel.add(btnManageOrganizations);
		
		
		
		
		
		JLabel organization_icon = new JLabel();
		sl_leftpanel.putConstraint(SpringLayout.WEST, btnManageOrganizations, 6, SpringLayout.EAST, organization_icon);
		organization_icon.setIcon(new ImageIcon(HomePanel.class.getResource("/res/Business-Organization-icon.png")));
		organization_icon.setPreferredSize(new Dimension(iconWidth, iconHeight));
		leftpanel.add(organization_icon);
		
		
		
		if (UserController.getInstance().isAdmin()){
			
			JButton btnManageUsers = new JButton("Users");
			sl_leftpanel.putConstraint(SpringLayout.NORTH, btnManageUsers, 31, SpringLayout.SOUTH, btnManageOrganizations);
			sl_leftpanel.putConstraint(SpringLayout.WEST, btnManageUsers, 0, SpringLayout.WEST, btnManageOrganizations);
			btnManageUsers.setToolTipText("Manages who have access to the system");
			btnManageUsers.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (subFrame != null)subFrame.dispose();
					subFrame = new JFrame();	
					subFrame.setLocation(container.getLocationOnScreen());
					buildUserUI(subFrame);
					subFrame.pack();
					subFrame.setVisible(true);
				}
			});
			btnManageUsers.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
			leftpanel.add(btnManageUsers);
			
			JLabel uers_icon = new JLabel();
			sl_leftpanel.putConstraint(SpringLayout.NORTH, uers_icon, 87, SpringLayout.NORTH, leftpanel);
			sl_leftpanel.putConstraint(SpringLayout.EAST, uers_icon, -123, SpringLayout.EAST, leftpanel);
			sl_leftpanel.putConstraint(SpringLayout.WEST, organization_icon, 0, SpringLayout.WEST, uers_icon);
			sl_leftpanel.putConstraint(SpringLayout.SOUTH, organization_icon, -6, SpringLayout.NORTH, uers_icon);
			uers_icon.setIcon(new ImageIcon(HomePanel.class.getResource("/res/users-icon.png")));
			uers_icon.setPreferredSize(new Dimension(iconWidth, iconHeight));
			leftpanel.add(uers_icon);
		
			//Show Settings only for Administrative Users
		
		
		
			JButton btnSettings = new JButton("Settings");
			sl_leftpanel.putConstraint(SpringLayout.NORTH, btnSettings, 38, SpringLayout.SOUTH, btnManageUsers);
			sl_leftpanel.putConstraint(SpringLayout.SOUTH, btnSettings, 68, SpringLayout.SOUTH, btnManageUsers);
			btnSettings.setToolTipText("Controls the operations of the database");
			sl_leftpanel.putConstraint(SpringLayout.EAST, btnSettings, 0, SpringLayout.EAST, btnManageOrganizations);
			btnSettings.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (subFrame != null)subFrame.dispose();
					subFrame = new JFrame();
					subFrame.setLocation(container.getLocationOnScreen());
					buildSettingUI(subFrame);
					subFrame.pack();
					subFrame.setVisible(true);
				}
			});
			btnSettings.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
			leftpanel.add(btnSettings);
			
			JLabel settings_icon = new JLabel();
			sl_leftpanel.putConstraint(SpringLayout.NORTH, settings_icon, 6, SpringLayout.SOUTH, uers_icon);
			sl_leftpanel.putConstraint(SpringLayout.WEST, settings_icon, 0, SpringLayout.WEST, organization_icon);
			settings_icon.setIcon(new ImageIcon(HomePanel.class.getResource("/res/database-settings-icon.png")));
			settings_icon.setPreferredSize(new Dimension(iconWidth, iconHeight));
			leftpanel.add(settings_icon);
		}
		
		
		
	}
	
	
	public void buildPersonUI(Container container){
		
	}
	
	public void buildUserUI(JFrame container){
		new UserManagerView(container, true, this.readOnly);
		container.setName("Users");
		container.setTitle("Manage Users");
	}
	
	public void buildMemberUI(JFrame container){
		new MemberManageView(container, true, this.readOnly,null,true,true);
		container.setName("Members");
		container.setTitle("Manage Members");
	}
	
	public void buildOrganizationUI(JFrame container){
		new OrganizationManageView(container, true, this.readOnly);
		container.setName("Organizations");
		container.setTitle("Manage Organizations");
	}
	
	public void buildProjectUI(JFrame container){
		new CohortManageView(container, true, this.readOnly);
		container.setName("Projects");
		container.setTitle("Manage Projects");
	}
	
	public void buildSettingUI(JFrame container){
		new SettingManageView(container);
		container.setName("Settings");
		container.setTitle("Manage Settings");
	}
	
	public void buildQueryUI(JFrame container){
		new ReportsManageView2(container);
		container.setName("Queries");
		container.setTitle("Database Queries");
	}
	
	public Container getParent(){
		Container parent = this.container.getParent();
		while (parent.getParent() != null)parent = parent.getParent();
		return parent;
	}
	
	public void logout(){
		if (subFrame != null)subFrame.dispose();
		
		this.exit();
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFrame tempFrame = new JFrame();
					new UserLoginView(tempFrame, true, false);
					tempFrame.pack();
					tempFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void exit(){
		Container parent = this.getParent();
		if (parent instanceof JFrame){
			JFrame p = (JFrame)parent;
			WindowEvent windowClosing = new WindowEvent(p, WindowEvent.WINDOW_CLOSING);
			p.dispatchEvent(windowClosing);
		}
	}
	
	
	public void sendFeedback(){
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FeedbackView frame = new FeedbackView();
					frame.setPreferredSize(new Dimension(300,500));
					frame.pack();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
