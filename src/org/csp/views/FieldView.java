package org.csp.views;

import java.awt.Container;

import org.csp.controller.FieldController;
import org.csp.controller.SettingController;
import org.csp.models.Field;
import org.csp.models.Indicator;
import org.csp.models.Question;

public class FieldView extends ModelView<Field> {

	
	
	public FieldView(Container container, boolean useLeftPanel,
			boolean readOnly, Field model, boolean useButtons,
			boolean displayResults) {
		super(container, useLeftPanel, readOnly, model, useButtons, displayResults);
	}

	public FieldView(Container container, boolean useLeftPanel,
			boolean readOnly, Field model, boolean useButtons) {
		super(container, useLeftPanel, readOnly, model, useButtons);
	}

	public FieldView(Container container, boolean useLeftPanel,
			boolean readOnly, Field model) {
		super(container, useLeftPanel, readOnly, model);
	}

	public FieldView(Container container, boolean useLeftPanel, boolean readOnly) {
		super(container, useLeftPanel, readOnly);
	}

	public FieldView(Container container, boolean useLeftPanel) {
		super(container, useLeftPanel);
	}

	public FieldView(Container container) {
		super(container);
	}

	@Override
	protected void configureModel() {
		if(this.model == null){
			this.model = new Field();
			this.model.setIndicator(new Indicator(new Question()));
		}		
		this.controller = FieldController.getInstance(SettingController.getInstance().getConnection());
	}
	
	
	

}
