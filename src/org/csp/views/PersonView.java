package org.csp.views;

import java.awt.Container;

import org.csp.controller.PersonController;
import org.csp.models.Person;

public class PersonView extends ModelView<Person> {

	public PersonView(Container container) {
		super(container);
	}

	@Override
	protected void configureModel() {
		this.model = new Person();
		this.controller = PersonController.getInstance();
	}

}
