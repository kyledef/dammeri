package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.util.Collection;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.table.DefaultTableCellRenderer;

import org.csp.controller.Controller;
import org.csp.utilities.ListTableModel;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.metawidget.swing.Facet;
import org.metawidget.swing.SwingMetawidget;
import org.metawidget.swing.layout.FlowLayout;
import org.metawidget.swing.widgetprocessor.binding.beansbinding.BeansBindingProcessor;
import org.metawidget.swing.widgetprocessor.binding.beansbinding.BeansBindingProcessorConfig;

public abstract class BaseUIView <T extends Comparable<T>, C> {


	protected Container container;
	protected static final int COMPONENT_SPACING = 5;
	protected JPanel panelRight;

	protected SwingMetawidget mMetawidget;
	protected SwingMetawidget buttonsMetawidget;
	
	public String saveMsg;
	public String saveErrMsg;
	public String searchErrMsg;
	public String deleteMsg;
	public String deleteErrMsg;
	
	protected Controller<T> controller;
	protected T model;
	protected ListTableModel<T> mTModel;
	
	//Abstract methods
	protected abstract void initUI();
	protected abstract void configureModel();
	protected abstract void setMessageConstants();
	public abstract void createMDialog(T model);
	
	public BaseUIView(Container container){
		this.container = container;
		this.initBase();
	}
	
	protected void initBase() {
		this.setSystemUI();
		this.setMessageConstants();
		panelRight = new JPanel();
		panelRight.setLayout( new BorderLayout() );
		panelRight.setBorder( BorderFactory.createEmptyBorder( COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING ) );
		container.add( panelRight, BorderLayout.CENTER );
		
		configureModel();
		buildleftPanel();
		initUI();
	}
	
	public void buildleftPanel() {
		JLabel label = new JLabel();
		URL url = Thread.currentThread().getContextClassLoader().getResource( "img/simmeda_icon.png" );
		if (url != null){
			ImageIcon imageAddressBook = new ImageIcon( url );
			label = new JLabel( imageAddressBook );
		}
		
		label.setVerticalAlignment( SwingConstants.TOP );
		label.setBorder( BorderFactory.createEmptyBorder( COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING * 2 ) );
		container.add( label, BorderLayout.WEST );
		
	}
	
	protected void setSystemUI() {
		try {
			for ( LookAndFeelInfo info : UIManager.getInstalledLookAndFeels() ) {
				if ( "Nimbus".equals( info.getName() ) ) {
					UIManager.setLookAndFeel( info.getClassName() );
					System.out.println("Using system styles");
					break;
				}
			}
		} catch ( Exception e ) {
			System.out.println("Unable to use local style. Reverting to default Java");
		}
		
	}
	
	public Controller<T> getController(){
		return controller;
	}
	
	protected void setTableDefaults(JTable table){
		table.setAutoCreateColumnsFromModel( true );
		table.setRowHeight( 35 );
		table.setShowVerticalLines( false );
		table.setShowHorizontalLines(true);
		table.setCellSelectionEnabled( false );
		table.setRowSelectionAllowed( true );
		
		table.setDefaultRenderer(Class.class, new DefaultTableCellRenderer(){
			private static final long serialVersionUID = -2883561376070724744L;
			{ setHorizontalAlignment( SwingConstants.CENTER ); }
		});
	}
	
	public JComponent createResultsSection(){
		final JTable table = new JTable( mTModel );
		setTableDefaults(table);
		table.addMouseListener( new MouseAdapter() {
			@Override
			public void mouseClicked( MouseEvent event ) {
				//Ensure user double clicks table
				if ( event.getClickCount() != 2 )return; 
				@SuppressWarnings("unchecked")
				ListTableModel<T> model = (ListTableModel<T>) table.getModel();	
				T t = model.getValueAt( table.getSelectedRow() );
				createMDialog(t);
			}			
		});		
		return  new JScrollPane( table );
	}
	
	public JComponent createSearchSection(){
		System.out.println("Attempting to create search section");
		
		mMetawidget = new SwingMetawidget();
		mMetawidget.setConfig( "org/csp/views/metawidget.xml" );
		mMetawidget.setToInspect( model );
		mMetawidget.addWidgetProcessor( new BeansBindingProcessor(new BeansBindingProcessorConfig().setUpdateStrategy( UpdateStrategy.READ_WRITE )) );
		buildBtnReflection();
		return mMetawidget;
	}
	
	
	public void buildBtnReflection(){
		Facet facetButtons = new Facet();
		facetButtons.setName( "buttons" );
		facetButtons.setOpaque( false );
		mMetawidget.add( facetButtons );
		
		buttonsMetawidget = new SwingMetawidget();
		//buttonsMetawidget.setConfig( "org/csp/views/metawidget.xml" );
		buttonsMetawidget.setToInspect( this );
		buttonsMetawidget.setMetawidgetLayout( new FlowLayout() );
		facetButtons.add( buttonsMetawidget );
	}
	
	protected void refreshTable(){
		refreshTable(null);
	}
	
	protected void refreshTable(Collection<T> list) {
		if (list == null)list = controller.getAll();	
		this.mTModel.importCollection(list);		
	}
}
