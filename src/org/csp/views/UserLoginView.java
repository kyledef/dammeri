package org.csp.views;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.csp.controller.SettingController;
import org.csp.controller.UserController;
import org.csp.models.User;
import org.metawidget.inspector.annotation.UiAction;
import org.metawidget.inspector.annotation.UiComesAfter;
import org.metawidget.swing.Stub;
import org.metawidget.swing.SwingMetawidget;

public class UserLoginView extends UserView {

	SwingMetawidget buttonsMetawidget;
	UserController uController;
	private int WIDTH = 500;
	private int HEIGHT = 100;
	
	
	public UserLoginView(Container container) {		
		super(container);
	}
	
	public UserLoginView(Container container, boolean useLeftPanel,
			boolean readOnly, User model, boolean useButtons) {
		super(container, useLeftPanel, readOnly, model, useButtons);
	}

	public UserLoginView(Container container, boolean useLeftPanel,
			boolean readOnly, User model) {
		super(container, useLeftPanel, readOnly, model);
	}

	public UserLoginView(Container container, boolean useLeftPanel,
			boolean readOnly) {
		super(container, useLeftPanel, readOnly);
	}

	public UserLoginView(Container container, boolean readOnly) {
		super(container, readOnly);
	}


	protected void init(){	
		super.init();
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		container.setSize( WIDTH, HEIGHT );
		container.setName("Login");
		
		int x = (screen.width-WIDTH)/2;
        int y = (screen.height-HEIGHT)/2;
        container.setLocation(x,y);
	}

	
	
	protected void configureModel(){
		super.configureModel();
		if (this.controller == null)System.out.println("User Controller is Null");
		uController = (UserController)this.controller;
	}
	
	@Override
	protected void setAccessLevel(){
		if (this.mMetawidget != null)this.mMetawidget.setReadOnly(false);
	}
	
	protected void buildRightPanel(){
		super.buildRightPanel();
		//Set the access level stud to remove from UI
		mMetawidget.add(new Stub("accessLevel"));	
		if (password != null)password.addActionListener(new ActionListener(){ public void actionPerformed(ActionEvent e){ login(); }});
	}
	
	@UiAction
	public void login(){
		try{
			User u = this.mMetawidget.getToInspect();			
			if (this.uController.authenticate(u)){ 								//Authenticate will check user if valid user
				((JFrame)container).dispose();									//Close the current login window
				(new MainFrame()).setVisible(true);								//Launch the Main window
			}
			else
				JOptionPane.showMessageDialog(this.container, "Incorrect User Name and Password");
		}catch(Exception e){
			System.out.println("Error Occurred");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}
	
	@UiAction
	@UiComesAfter( "login" )
	public void close(){
		System.exit(0);
	}
	

}
