package org.csp.views;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.csp.models.Indicator;
import org.csp.models.Question;
import org.csp.utilities.ListTableModel;
import org.csp.utilities.Utilities;
import org.metawidget.inspector.annotation.UiAction;
import org.metawidget.swing.Facet;
import org.metawidget.swing.SwingMetawidget;
import org.metawidget.swing.layout.FlowLayout;

public class IndicatorsAddView extends JPanel {
	
	private static final long serialVersionUID = -4578037760388418213L;
	protected Question question;
	
	Indicator indicator;
	Collection<Indicator> indicators;
	ListTableModel<Indicator> mInTable;		
	SwingMetawidget indicatorWidget;
	
	
	public IndicatorsAddView(Question question){
		super(new BorderLayout());
		this.question = question;
		
		indicator = new Indicator();
		indicators = new ArrayList<Indicator>();
		
		indicatorWidget = new SwingMetawidget();
		indicatorWidget.setConfig( "org/csp/views/metawidget.xml" );
		indicatorWidget.setToInspect(indicator);
		this.add(indicatorWidget, BorderLayout.CENTER);
		
		mInTable = new ListTableModel<Indicator>(Indicator.class, this.indicators, "type", "definition", "frequency", "display", "range");
		JTable table = new JTable(mInTable);
		Utilities.setTableDefaults(table);
		this.add( new JScrollPane( table ),BorderLayout.SOUTH);
		
		this.buildBtnReflection();
	}

	@UiAction
	public void save(){
		Indicator tmpI = this.indicatorWidget.getToInspect();
		if (tmpI.getIndicator() != null)
			this.indicators.add(tmpI);
		
		this.refreshTable(this.indicators);
	
		Indicator i = new Indicator();
		i.setQuestion(question);
		this.indicatorWidget.setToInspect(i);
	}
	
	@UiAction
	public void remove(){		
		JOptionPane.showMessageDialog(this, "Remove not implemented as yet");
	}
	
	@UiAction
	public void close(){
		if (this.getParent() instanceof JFrame)
			this.getParent().setVisible(false);
		else System.exit(0);
	}
	
	public void buildBtnReflection(){
		Facet facetButtons = new Facet();
		facetButtons.setName( "buttons" );
		facetButtons.setOpaque( false );
		indicatorWidget.add( facetButtons );
		
		SwingMetawidget buttonsMetawidget = new SwingMetawidget();
		buttonsMetawidget.setToInspect( this );
		buttonsMetawidget.setMetawidgetLayout( new FlowLayout() );
		facetButtons.add( buttonsMetawidget );
	}
	
	protected void refreshTable(Collection<Indicator> list) {
		if (list == null)list = this.indicators;		
		this.mInTable.importCollection(list);		
	}
	
	public static void main(String [] args){
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Question quest = new Question();
		
		IndicatorsAddView ai = new IndicatorsAddView(quest);
		frame.getContentPane().add(ai, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
	}

}
