package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.csp.controller.SettingController;
import org.csp.controller.UserController;
import org.csp.models.User;
import org.csp.models.UserLog;
import org.csp.utilities.ListTableModel;
import org.csp.utilities.Utilities;
import org.metawidget.inspector.InspectionResultConstants;
import org.metawidget.inspector.annotation.UiAction;
import org.metawidget.inspector.annotation.UiAttribute;

public class UserManagerView extends UserView{
	
	private JPanel logPanel;
	
	public UserManagerView(Container container, boolean useLeftPanel,
			boolean readOnly, User model, boolean useButtons) {
		super(container, useLeftPanel, readOnly, model, useButtons);
	}

	public UserManagerView(Container container, boolean useLeftPanel,
			boolean readOnly, User model) {
		super(container, useLeftPanel, readOnly, model);
	}

	public UserManagerView(Container container, boolean useLeftPanel,
			boolean readOnly) {
		super(container, useLeftPanel, readOnly);
	}

	public UserManagerView(Container container, boolean readOnly) {
		super(container, readOnly);
	}

	public UserManagerView(Container container) {
		super(container);
	}

	protected void configureModel(){
		useButtons = true;
		displayResults = true;
		super.configureModel();
		this.mTModel = new ListTableModel<User>(User.class, controller.getAll(), "UserName", "Password", "AccessLevel" );
	}
	
	protected void buildRightPanel(){		
		super.buildRightPanel();
		this.logPanel = new JPanel(new BorderLayout());
		container.add(logPanel, BorderLayout.EAST);		
		logPanel.setPreferredSize(new Dimension(200,100));
	}

	@UiAction
	public void find(){		
		User u = this.mMetawidget.getToInspect();		
		try {
			Collection<User> us = this.controller.find(u);
			if (us.size() > 0){
				clear(us);
			}else JOptionPane.showMessageDialog(this.container, "No user found");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this.container, "Unable to find user");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}		
	}
	
	@UiAction
	@UiAttribute( name = InspectionResultConstants.HIDDEN, value = "${this.readOnly}" )
	public void save(){
		boolean encryptPassword = true;
		User u = this.mMetawidget.getToInspect();		
		int response;			
		try {		
			if (u.getId() != 0){//Confirm user wants to update an existing user
				response = JOptionPane.showConfirmDialog(null, "Update the current User?", "Update", JOptionPane.OK_CANCEL_OPTION);
				if (response != JOptionPane.OK_OPTION)return;
				//Only check if password saved for already created users
				response = JOptionPane.showConfirmDialog(null, "Did you change the password?","Password Change", JOptionPane.YES_NO_OPTION );
				if (response == JOptionPane.YES_OPTION)	encryptPassword = true;
				else encryptPassword = false;
			}
			
			if (u.getUserName() == null || u.getUserName().equals("")){
				JOptionPane.showMessageDialog(null, "Must specify a username");
				return;
			}else if (u.getPassword() == null || u.getPassword().equals("")){
				JOptionPane.showMessageDialog(null, "Must specify password");
				return;
			}else if (u.getAccessLevel() == null){
				u.setAccessLevel("VIEWER");
			}
			
			if (encryptPassword){
				u.setPassword(Utilities.encrypt(u.getPassword()));
			}
			
			u = this.controller.save(u);
			//User saved
			if (u.getId() != 0){
				JOptionPane.showMessageDialog(this.container, "Saved user: "+u.getUserName());
				clear();
				this.refreshTable();
				return;
			}
		} catch (Exception e) { e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }		
		JOptionPane.showMessageDialog(this.container, "Unable to Save user");
	}
	
	@UiAction
	@UiAttribute( name = InspectionResultConstants.HIDDEN, value = "${this.readOnly}" )
	public void delete(){
		super.delete(null);
		clear();
	}
	
	@UiAction
	public void clear(){
		clear(null);
	}
	
	@UiAction
	public void viewUserLogs(){
		User u = this.mMetawidget.getToInspect();
		if (u.getId() > 0){
			try{
				this.logPanel.removeAll();
				this.logPanel.add(new JLabel("User Logs for: "+u.getUserName()),BorderLayout.NORTH);
				Collection<UserLog> logs = UserController.getInstance(SettingController.getInstance().getConnection()).listUserEvents(u);
				if (logs.size() > 0){
					Iterator<UserLog> i = logs.iterator();
					StringBuilder stb;
					DefaultListModel<String> ulModel = new DefaultListModel<String>();
	
					while(i.hasNext()){
						UserLog ul = i.next();
						stb = new StringBuilder();
						Calendar cal = Calendar.getInstance();
						cal.setTime(ul.getDate());
						
						stb.append(cal.get(Calendar.DAY_OF_MONTH))
							.append("-")
							.append(cal.get(Calendar.MONTH))
							.append("-")
							.append(cal.get(Calendar.YEAR))
							.append(":")
							.append(ul.getAction());
						
						ulModel.addElement(stb.toString());	
					}
					JList<String>ulList = new JList<String>(ulModel);
					this.logPanel.add(ulList, BorderLayout.CENTER);
					this.logPanel.updateUI();
				}else
					JOptionPane.showMessageDialog(this.container, "No logs available for selected user");
			} catch (Exception e) { e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
		}else
			JOptionPane.showMessageDialog(this.container, "Select User First to View Logs");
	}
	
	public void clear(Collection<User> uc){
		User u = new User();
		this.mMetawidget.setToInspect(u);
		if (uc == null)this.refreshTable();
		else this.refreshTable(uc);
	}
	
	
}
