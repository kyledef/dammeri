package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.csp.controller.IndicatorController;
import org.csp.controller.SettingController;
import org.csp.models.Cohort;
import org.csp.models.Indicator;
import org.csp.models.Question;
import org.csp.utilities.ListTableModel;
import org.metawidget.inspector.annotation.UiAction;
import org.metawidget.inspector.annotation.UiComesAfter;
import org.metawidget.inspector.annotation.UiHidden;

public class IndicatorView extends ModelView<Indicator> {

	
	
	@SuppressWarnings("unused")
	private ListTableModel<Indicator> tModel;
	private Collection<Indicator> indicators;
	private ActionListener actionListener;

	public IndicatorView(Container container, boolean useLeftPanel, boolean readOnly, Indicator model, boolean useButtons) {			
		super(container, useLeftPanel, readOnly, model, useButtons);		
	}

	public IndicatorView(Container container, boolean useLeftPanel, boolean readOnly, Indicator model) {			
		super(container, useLeftPanel, readOnly, model);		
	}

	public IndicatorView(Container container, boolean useLeftPanel,boolean readOnly) {			
		super(container, useLeftPanel, readOnly);		
	}

	public IndicatorView(Container container, boolean useLeftPanel) {
		super(container, useLeftPanel);		
	}

	public IndicatorView(Container container) {
		super(container);
	}

	@Override
	protected void configureModel() {
		if(this.model == null)this.model = new Indicator();
		if(this.model.getQuestion() == null){
			Question q = new Question();
			q.setCohort(new Cohort());
			this.model.setQuestion(q);	
		}
		
		this.controller = IndicatorController.getInstance(SettingController.getInstance().getConnection());
	}
	
	@Override
	protected void buildRightPanel(){
		super.buildRightPanel();
		if (this.useButtons){
			JPanel bottomBar = new JPanel();			
			this.panelRight.add(bottomBar, BorderLayout.SOUTH);			
			bottomBar.add(buttonsMetawidget);
		}
	}

	@UiAction
	@UiComesAfter( "clear" )
	public void save(){
		Indicator i = this.mMetawidget.getToInspect();
		if (i.getQuestion() != null && i.getQuestion().getId() > 0){
			try{				
				i = this.controller.save(i);
				if (i.getId() > 0){
					JOptionPane.showMessageDialog(container, "Saved Indicator Successfully");					
					if (this.indicators != null) this.indicators.add(i);
					if (this.actionListener != null)this.actionListener.actionPerformed(null);
					
					i = new Indicator(i.getQuestions());					
					this.mMetawidget.setToInspect(i);
					return;
				}
			}catch(Exception e){ JOptionPane.showMessageDialog(container, "Unable to save Indicator"); e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
		}else
			JOptionPane.showMessageDialog(container, "Unable to save Indicator without sepcifying the question first");
	}
	
	@UiAction
	@UiComesAfter( "save" ) 
	public void delete(){
		Indicator i = this.mMetawidget.getToInspect();
		if (i.getId() > 0){
			try {
				if (controller.delete(i)){
					JOptionPane.showMessageDialog(container, "Indicator Deleted");					
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(container, "Unable to delete Indicator");
				return;
			}			
		}
		clear();
	}
	
	@UiAction
	public void clear(){
		Indicator i = this.mMetawidget.getToInspect();
		i = new Indicator(i.getQuestions());					
		this.mMetawidget.setToInspect(i);
	}
	
	public void setQuestions(Question [] questions){
		this.model.setQuestions(questions);
		this.mMetawidget.setToInspect(this.model);
	}
	
	
	public void load(Indicator indicator){
		if (indicator.getId() > 0){
			this.model = indicator;
			this.mMetawidget.setToInspect(this.model);
		}
	}

	@UiHidden
	public void setQuestion(Question q) {
		this.model.setQuestion(q);
		this.mMetawidget.setToInspect(this.model);		
	}
	
	@UiHidden
	public void setTableModel(ListTableModel<Indicator> tModel){
		if (tModel == null)System.out.println("Table Model was null");
		this.tModel = tModel;
	}
	
	@UiHidden
	public void setIndicators(Collection <Indicator> indicators){
		if(indicators == null)System.out.println("Indicator was null");
		this.indicators = indicators;
	}
	
	@UiHidden
	public ActionListener getActionListener() {
		return actionListener;
	}

	@UiHidden
	public void setActionListener(ActionListener actionListener) {
		this.actionListener = actionListener;
	}
	
//	@UiAction
//	public void find(){
//		
//	}
}
