package org.csp.views;

import java.awt.Container;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.JOptionPane;

import org.csp.controller.OutputController;
import org.csp.controller.SettingController;
import org.csp.models.Activity;
import org.csp.models.Output;
import org.csp.utilities.ListTableModel;
import org.metawidget.inspector.annotation.UiAction;
import org.metawidget.inspector.annotation.UiComesAfter;
import org.metawidget.inspector.annotation.UiHidden;

public class OutputView extends ModelView<Output> {

	
	
	@SuppressWarnings("unused")
	private ListTableModel<Output> tModel;
	private Collection<Output> outputs;
	private ActionListener actionListener;
	
	public OutputView(Container container, boolean useLeftPanel, boolean readOnly, Output model, boolean useButtons) {			
		super(container, useLeftPanel, readOnly, model, useButtons);
	}

	public OutputView(Container container, boolean useLeftPanel, boolean readOnly, Output model) {			
		super(container, useLeftPanel, readOnly, model);
	}

	public OutputView(Container container, boolean useLeftPanel, boolean readOnly) {			
		super(container, useLeftPanel, readOnly);
	}

	public OutputView(Container container, boolean useLeftPanel) {
		super(container, useLeftPanel);
	}

	public OutputView(Container container) {
		super(container);
	}

	@Override
	protected void configureModel() {
		this.model = new Output();
		this.controller = OutputController.getInstance();

	}

	@UiAction
	@UiComesAfter( "clear" )
	public void save(){
		Output o = this.mMetawidget.getToInspect();
		if (o.getOutput() != null ){
			try{
				o = controller.save(o);
				if (o.getId() > 0){
					JOptionPane.showMessageDialog(null, "Successfully Saved Input");
					if (this.outputs != null)outputs.add(o);
					if (this.actionListener != null)this.actionListener.actionPerformed(null);
					
//					if (this.tModel != null && this.outputs != null){
//						outputs.add(o);
//						this.tModel.importCollection(outputs);
//					}
				}
			}catch(Exception e){
				
			}
		}
	}
	
	@UiAction
	@UiComesAfter( "save" ) 
	public void delete(){
		Output o = this.mMetawidget.getToInspect();
		if (o.getId() > 0)
			try {
				this.controller.delete(o);
			} catch (Exception e) {
				e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
			}
	}
	
	
	@UiAction
	public void clear(){
		Output out = new Output();
		out.setActivities(this.model.getActivities());
		out.setActivity(this.model.getActivity());
		this.model = out;
		this.mMetawidget.setToInspect(this.model);
	}
	
	@UiAction
	public void close(){
		this.container.setVisible(false);
	}
	@UiHidden
	public void setActivities(Collection<Activity> activities) {
		Activity [] acts = new Activity[0];
		acts = activities.toArray(acts);
		this.model.setActivities(acts);
		//TODO Display Dropdown based on activities specified [TEST]
//		JComboBox<Activity> actBox = new JComboBox<Activity>(acts);
//		actBox.setName("activity");
//		this.mMetawidget.add("Activity", actBox);
	}
	@UiHidden
	public void setTableModel(ListTableModel<Output> tModel){
		this.tModel = tModel;
	}
	@UiHidden
	public void setOutputs(Collection<Output> output){
		this.outputs = output;
	}
	@UiHidden
	public void addActionListener(ActionListener actionListener) {
		this.actionListener = actionListener;		
	}
}
