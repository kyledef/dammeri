package org.csp.views;

import java.awt.Container;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.JOptionPane;

import org.csp.controller.InputController;
import org.csp.controller.SettingController;
import org.csp.models.Activity;
import org.csp.models.Input;
import org.csp.utilities.ListTableModel;
import org.metawidget.inspector.annotation.UiAction;
import org.metawidget.inspector.annotation.UiComesAfter;
import org.metawidget.inspector.annotation.UiHidden;

public class InputView extends ModelView<Input> {

	@SuppressWarnings("unused")
	private ListTableModel<Input> tModel;
	private Collection<Input> inputs;
	private ActionListener actionListener;

	public InputView(Container container, boolean useLeftPanel,boolean readOnly, Input model, boolean useButtons) {			
		super(container, useLeftPanel, readOnly, model, useButtons);
	}

	public InputView(Container container, boolean useLeftPanel,	boolean readOnly, Input model) {
		super(container, useLeftPanel, readOnly, model);
	}

	public InputView(Container container, boolean useLeftPanel, boolean readOnly) {
		super(container, useLeftPanel, readOnly);
	}

	public InputView(Container container, boolean useLeftPanel) {
		super(container, useLeftPanel);
	}

	public InputView(Container container) {
		super(container);
	}

	@Override
	protected void configureModel() {
		if (this.model == null)this.model = new Input();
		if (this.model.getActivity() == null)this.model.setActivity(new Activity());
		this.controller = InputController.getInstance(SettingController.getInstance().getConnection());		
	}

	@UiAction
	@UiComesAfter( "clear" )
	public void save(){
		Input i = this.mMetawidget.getToInspect();
		if (i.getInput() != null && !i.getInput().equals("")){
			try{
				i = controller.save(i);
				System.out.printf("Activity %s of %s saved \n",i, i.getActivity());
				if (i.getId() > 0){
					JOptionPane.showMessageDialog(null, "Successfully Saved Input");
					if (this.inputs != null)inputs.add(i);
					if (this.actionListener != null)this.actionListener.actionPerformed(null);
				}
			}catch(Exception e){
				JOptionPane.showMessageDialog(null, "Unable to save Input");
			}
		}else{
			JOptionPane.showMessageDialog(null,"Must specify the Input to be saved");
		}
	}
	
	@UiAction
	@UiComesAfter( "save" ) 
	public void delete(){
		Input i = this.mMetawidget.getToInspect();
		try{
			if (controller.delete(i))JOptionPane.showMessageDialog(null, "Successfully Deleted Input");
		}catch(Exception e){JOptionPane.showMessageDialog(null, "Unable to delete Input"); }
	}
	
	@UiAction
	public void clear(){
		Input i = new Input();
		this.mMetawidget.setToInspect(i);
		
	}

	@UiHidden
	public void setActivities(Collection<Activity> activities) {
		Activity [] acts = new Activity[0];
		acts = activities.toArray(acts);
		//TODO Display Dropdown based on activities specified [TEST]
//		JComboBox<Activity> actBox = new JComboBox<Activity>(acts);
		this.model.setActivities(acts);
//		actBox.setName("activity");
//		this.mMetawidget.add("Activity", actBox);
	}
	@UiHidden
	public void setTableModel(ListTableModel<Input> tModel){
		this.tModel = tModel;
	}
	@UiHidden
	public void setInputs(Collection<Input> inputs){
		this.inputs = inputs;
	}
	@UiHidden
	public void addActionListener(ActionListener actionListener){
		this.actionListener = actionListener;
	}
}
