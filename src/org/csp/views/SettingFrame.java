package org.csp.views;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.border.EmptyBorder;

import org.csp.controller.SettingController;

public class SettingFrame extends JFrame {

	private static final long serialVersionUID = -6594849155370799512L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SettingFrame frame = new SettingFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SettingFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		JButton btnChangeIcon = new JButton("Change Icon");
		btnChangeIcon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(contentPane, "Changing the icon not implemented as yet");
			}
		});
		sl_contentPane.putConstraint(SpringLayout.NORTH, btnChangeIcon, 47, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, btnChangeIcon, 32, SpringLayout.WEST, contentPane);
		contentPane.add(btnChangeIcon);
		
		JCheckBox chckbxEnableSecureLogin = new JCheckBox("Enable Secure Login");
		chckbxEnableSecureLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(contentPane, "Security features disabled for demonstration");
			}
		});
		sl_contentPane.putConstraint(SpringLayout.NORTH, chckbxEnableSecureLogin, 19, SpringLayout.SOUTH, btnChangeIcon);
		sl_contentPane.putConstraint(SpringLayout.WEST, chckbxEnableSecureLogin, 0, SpringLayout.WEST, btnChangeIcon);
		contentPane.add(chckbxEnableSecureLogin);
		
		JButton btnBackupDatabase = new JButton("Backup Database");
		btnBackupDatabase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(contentPane, "Database backup not yet implemented");
			}
		});
		sl_contentPane.putConstraint(SpringLayout.NORTH, btnBackupDatabase, 0, SpringLayout.NORTH, btnChangeIcon);
		sl_contentPane.putConstraint(SpringLayout.EAST, btnBackupDatabase, -50, SpringLayout.EAST, contentPane);
		contentPane.add(btnBackupDatabase);
		
		JButton btnClearDatabase = new JButton("Clear Database");
		btnClearDatabase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(contentPane, "Database clearing not yet implemented");
			}
		});
		sl_contentPane.putConstraint(SpringLayout.NORTH, btnClearDatabase, 0, SpringLayout.NORTH, chckbxEnableSecureLogin);
		sl_contentPane.putConstraint(SpringLayout.WEST, btnClearDatabase, 5, SpringLayout.WEST, btnBackupDatabase);
		sl_contentPane.putConstraint(SpringLayout.EAST, btnClearDatabase, 0, SpringLayout.EAST, btnBackupDatabase);
		contentPane.add(btnClearDatabase);
		
		JButton btnResetUserAccess = new JButton("Reset User Access");
		btnResetUserAccess.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(contentPane, "Database user reset not yet implemented");
			}
		});
		sl_contentPane.putConstraint(SpringLayout.NORTH, btnResetUserAccess, 21, SpringLayout.SOUTH, btnClearDatabase);
		sl_contentPane.putConstraint(SpringLayout.WEST, btnResetUserAccess, 0, SpringLayout.WEST, btnClearDatabase);
		contentPane.add(btnResetUserAccess);
	}
}
