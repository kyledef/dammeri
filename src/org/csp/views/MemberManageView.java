
package org.csp.views;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collection;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.csp.controller.OrganizationController;
import org.csp.controller.PersonController;
import org.csp.controller.SettingController;
import org.csp.models.Member;
import org.csp.models.Organization;
import org.csp.models.Person;
import org.csp.utilities.ListTableModel;
import org.metawidget.inspector.InspectionResultConstants;
import org.metawidget.inspector.annotation.UiAction;
import org.metawidget.inspector.annotation.UiAttribute;

public class MemberManageView extends MemberView{

	//TODO Apply enhance UI when user selects a person
	//TODO Other Functionality not working on first launch
	//TODO work still needed to improve the find functionality
	//		The following	 needs work (finding by sex, role, organization)
	
	OrganizationController oc;
	PersonController pc;
	
	Organization organization;
	
	public MemberManageView(Container container) {
		super(container);
		if (container instanceof JFrame){
			((JFrame)container).setTitle(SettingController.DEFAULT_NAME +" : Manage Persons");
		}
	}
	
	public MemberManageView(Container container, boolean useLeftPanel,
			boolean readOnly, Member model, boolean useButtons,
			boolean displayResults) {
		super(container, useLeftPanel, readOnly, model, useButtons, displayResults);
	}

	public MemberManageView(Container container, boolean useLeftPanel,
			boolean readOnly, Member model, boolean useButtons) {
		super(container, useLeftPanel, readOnly, model, useButtons);
	}

	public MemberManageView(Container container, boolean useLeftPanel,
			boolean readOnly, Member model) {
		super(container, useLeftPanel, readOnly, model);
	}

	public MemberManageView(Container container, boolean useLeftPanel,
			boolean readOnly) {
		super(container, useLeftPanel, readOnly);
	}

	public MemberManageView(Container container, boolean useLeftPanel) {
		super(container, useLeftPanel);
	}

	protected void configureModel(){
		displayResults = true;
		
		super.configureModel();		
		oc = OrganizationController.getInstance(SettingController.getInstance().getConnection());
		pc = PersonController.getInstance(SettingController.getInstance().getConnection());
		this.mTModel = new ListTableModel<Member>(Member.class, controller.getAll(), "Person", "Organization", "Role" );
	}
	
	protected void buildRightPanel(){
		super.buildRightPanel();
		enhanceUI();		
	}	
	
	protected void enhanceUI(){
		if (!this.readOnly){
			relateOrganization();
				
			@SuppressWarnings("unchecked")		
			final JComboBox <String> roleBox =  (JComboBox<String>)mMetawidget.getComponent("role");
			roleBox.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					if (roleBox.getSelectedItem().equals("OTHER")){
						roleBox.setEditable(true);
						roleBox.setSelectedIndex(0);
					}				
				}
			});
		}
	}
	
	protected void relateOrganization(){
		
		@SuppressWarnings("unchecked")
		JComboBox <Organization>orgBox = (JComboBox<Organization>)mMetawidget.getComponent("organization");
		if (model.getOrganization() != null &&  model.getOrganization().getId() > 0)
			for (int i = 0; i < orgBox.getItemCount(); i++)		
				if (orgBox.getItemAt(i) != null)			
					if (orgBox.getItemAt(i).equals(model.getOrganization()))
						orgBox.setSelectedIndex(i);	
	}
	
	public void resetMember(Member u){
		Organization o = u.getOrganization();
		if (o == null)o = new Organization();
		
		Member mN = new Member();
		mN.setOrganization(o);
		mN.setPerson(new Person());
		this.mMetawidget.setToInspect(mN);
		this.enhanceUI();
	}
	
	@UiAction
	@UiAttribute( name = InspectionResultConstants.HIDDEN, value = "${!this.displayResults}" )
	public void find(){		
		//TODO find not working. Not expected giving results
		
		Member u = this.mMetawidget.getToInspect();
		Person p = u.getPerson();
		
		//Place error messages as place holders for now
		if (p.getAddress() != null){
			JOptionPane.showMessageDialog(null, "Unable to search on the address field");
			resetMember(u);
			return;
		}if (p.getPhone() != null){
			JOptionPane.showMessageDialog(null, "Unable to search on the phone contact field");
			resetMember(u);
			return;
		}if (p.getEmail() != null){
			JOptionPane.showMessageDialog(null, "Unable to search on the email field");
			resetMember(u);
			return;
		}
		
		
		resetMember(u);															//Rebuild Member for the UI
		
		try {
			Collection<Member> us = this.controller.find(u);
			if (us.size() > 0){
				this.refreshTable(us);
			}else JOptionPane.showMessageDialog(this.container, "No user found");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this.container, "No user found");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}
	
	@UiAction
	@UiAttribute( name = InspectionResultConstants.HIDDEN, value = "${this.readOnly}" )
	public void save(){
		try {
			Member m = this.mMetawidget.getToInspect();
			Organization o = m.getOrganization();
			Person p = m.getPerson();
			System.out.println(p);
			
			if (o == null){														//Ensure that an organization is selected from the list
				JOptionPane.showMessageDialog(this.container, "No Organization Selected");
				return;
			}
			
			String valid = pc.validate(p);
			if (!valid.equals("ok")){											//Ensure that the Person data is entered correctly
				JOptionPane.showMessageDialog(this.container, valid);
				return;
			}
			
			valid = controller.validate(m);										//Ensure the Member data is valid
			if (!valid.equals("ok")){
				JOptionPane.showMessageDialog(this.container, valid);
				return;
			}

			if (m.getId() != 0){												//Confirm user wants to update an existing member
				if( JOptionPane.showConfirmDialog(this.container, "Update the current Member?" ) != JOptionPane.OK_OPTION)return;
				pc.update(p);													//Update the Person references
				controller.update(m);											//Update the Member record
			}else{
				if (this.pc.isPersonExist(p)){									//Check if person exists cannot have duplicates for searching purposes
					JOptionPane.showMessageDialog(this.container, "Person By that name already exists");
					return;
				}
				p = pc.save(p);													//Person is unique we can now save
				try{
					if (p.getId() != 0){										//check if we saved the person
						m.setPerson(p);
						if (o != null && o.getName() != null){					//Check if the organization need to be saved (TODO reassess if needed)
							if (o.getId() == 0){
								Collection<Organization> oCol = oc.get(o);		//Check if Organization exists
								if (oCol.size() > 0)o = oCol.iterator().next(); //Assign the organization with id back to o
								else o = oc.save(o);							//Else attempt to save the organization entered
							}
							if (o.getId() == 0)
								throw new Exception("unable to save organization");//A serious error occurred if up to this point 
							else m.setOrganization(o);							//Set the organization with the id to the member
						}
						this.controller.save(m);								//Save the Member
						if (m.getId() > 0){
							JOptionPane.showMessageDialog(this.container, "Saved Member");
							
							if (this.organization != null)
								this.refreshTable(this.controller.get(new Member(organization)));//Set the table to display only the members for the specified organization
							else this.refreshTable();							//Display all the members within the system
							if (this.refreshable != null)
								this.refreshable.refresh();
							clear();											//Clear the screen for adding a new members
							return;
						}
					}
					return;
				}catch(Exception e){
					pc.delete(p);
					((Member)this.mMetawidget.getToInspect()).setPerson(p);
					throw new Exception(e);
				}
			}
			JOptionPane.showMessageDialog(this.container, "Unable to Save Member");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this.container, "Unable to Save Member ");
			
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}			
	}

	@UiAction
	public void clear(){
		Member m = new Member();
		//Get local Organization
		m.setPerson(new Person());
		Organization o = new Organization();
		if (this.model.getOrganization().getId() > 0)o = model.getOrganization();			
		m.setOrganization(o);
		
		this.model = m;
		this.mMetawidget.setToInspect(model);	
		enhanceUI();
		
		try{if (this.organization != null)this.refreshTable(this.controller.get(new Member(organization)));
		else this.refreshTable();}catch(Exception e){}
	}
	
//	@UiAction
	@UiAttribute( name = InspectionResultConstants.HIDDEN, value = "${this.readOnly}" )
	public void export(){
		JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		 if (fc.showOpenDialog(this.container) == JFileChooser.APPROVE_OPTION){
			 File file = fc.getSelectedFile();
			 if (file.exists() && file.isDirectory()){	
				 this.controller.exportToCSV(file.getAbsolutePath(), "/members.csv", this.mTModel.exportList());
				 JOptionPane.showMessageDialog(null, "Contacts Exported");
			 }
		 }
	}

	@UiAction
	@UiAttribute( name = InspectionResultConstants.HIDDEN, value = "${this.readOnly}" )
	public void delete(){
		super.delete(null);
		clear();
	}
	
	@UiAction
	public void close(){
		if (container instanceof JFrame){
			((JFrame)container).dispose();
		}else{
			System.exit(0);
		}
	}

	protected void handleModelSelect(Member model){
		this.mMetawidget.setToInspect(model);
		this.model = model;
		if (!this.readOnly)relateOrganization();
		this.enhanceUI();
	}
	
	public void setModel(Member model) {
		this.model = model;
		this.mMetawidget.setToInspect(this.model);
		if (!this.readOnly)this.enhanceUI();
		if (model.getOrganization().getId() != 0){
			organization = model.getOrganization();
			filterOrganization();
		}
	}

	protected void filterOrganization() {
		if (organization != null){
			try { this.mTModel.importCollection(controller.get(new Member(organization)));				
			} catch (Exception e) { e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
		}
	}	
}
