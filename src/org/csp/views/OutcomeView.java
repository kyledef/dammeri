package org.csp.views;

import java.awt.Container;

import org.csp.controller.OutcomeController;
import org.csp.models.Outcome;

public class OutcomeView extends ModelView<Outcome> {

	
	public OutcomeView(Container container) {
		super(container);
	}

	@Override
	protected void configureModel() {
		this.model = new Outcome();
		this.controller = OutcomeController.getInstance();

	}

}
