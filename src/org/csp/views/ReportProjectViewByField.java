package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.csp.controller.BeneficiaryController;
import org.csp.controller.CohortController;
import org.csp.controller.SettingController;
import org.csp.models.Beneficiary;
import org.csp.models.Cohort;
import org.csp.utilities.ListTableModel;
import org.csp.utilities.Utilities;

import com.toedter.calendar.JDateChooser;

public class ReportProjectViewByField extends View{

	public static final int DATE_VIEW = 0;
	public static final int KEYWORD_VIEW = 1;
	public static final int BENEFICIARY_VIEW = 2;
	public static final int LOCATION_VIEW = 3;
	
	int field;
	JPanel containerPanel;
	JPanel panelRight;
	
	protected ListTableModel<Cohort> mTModel;
	protected JTable table;
	protected CohortController controller;
	protected ArrayList<Cohort> cohorts;
	
	public ReportProjectViewByField(Container container, int field) {
		super(container);
		this.field = field;
		initUI();
	}
	
	

	public ReportProjectViewByField(Container container, int field, boolean useLeftPanel,
			boolean readOnly, boolean useButtons) {
		super(container, useLeftPanel, readOnly, useButtons);
		this.field = field;
		initUI();
	}



	public ReportProjectViewByField(Container container,int field, boolean useLeftPanel) {
		super(container, useLeftPanel);
		this.field = field;
		initUI();
	}



	public ReportProjectViewByField(Container container) {
		super(container);
		this.field = KEYWORD_VIEW;
		initUI();
	}



	protected void initUI(){
		//Initialize panels and add to container
		containerPanel = new JPanel(new BorderLayout());
		panelRight = new JPanel(new BorderLayout());
		panelRight.setBorder( BorderFactory.createEmptyBorder( COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING ) );
		container.add( panelRight, BorderLayout.CENTER );		
		panelRight.add(containerPanel, BorderLayout.CENTER);
		
		//Create table for results
		containerPanel.add(this.createResultsSection(), BorderLayout.SOUTH);
		
		
		switch (field){
			case DATE_VIEW:
				containerPanel.add(buildDateView(), BorderLayout.CENTER);
				break;
			case KEYWORD_VIEW:
				containerPanel.add(buildKeywordView(), BorderLayout.CENTER);
				break;
			case BENEFICIARY_VIEW:
				containerPanel.add(buildBeneficiaryView(), BorderLayout.CENTER);
				break;
			case LOCATION_VIEW:
				containerPanel.add(buildLocationView(), BorderLayout.CENTER);
				break;
			default:
				containerPanel.add(buildKeywordView(), BorderLayout.CENTER);
				break;
		}
		
		containerPanel.repaint();
		
	}
	
	public JComponent createResultsSection(){
		controller = CohortController.getInstance(SettingController.getInstance().getConnection());
		cohorts = new ArrayList<Cohort>();
		
		mTModel = new ListTableModel<Cohort>(Cohort.class, cohorts, "Project", "EndDate" ,"StartDate", "TeamLead", "Funder");
		table = new JTable( mTModel );
		Utilities.setTableDefaults(table);		
	
		table.addMouseListener( new MouseAdapter() {
			@Override
			public void mouseClicked( MouseEvent event ) {
				//Ensure user double clicks table
				if ( event.getClickCount() != 2 )return; 
				@SuppressWarnings("unchecked")
				ListTableModel<Cohort> model = (ListTableModel<Cohort>) table.getModel();	
				if (table.getSelectedRow() >= 0){
					Cohort t = model.getValueAt( table.getSelectedRow() );
					System.out.println(t + "CLICKED");
				}					
			}
		});	
		
		JScrollPane scroll =  new JScrollPane( table ); 
		//Set Height of table
		scroll.setPreferredSize( new Dimension((int)  scroll.getPreferredSize().getWidth(), 200));
		return scroll;
	}
	
	protected JComponent buildDateView(){
		System.out.println("Running build Date view");
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		
		//Create date Fields
		JPanel datePanel = new JPanel();
		panel.add(datePanel, BorderLayout.NORTH);
		
		JLabel startDate_lbl = new JLabel("Start Date");
		final JDateChooser startDate = new JDateChooser();
		datePanel.add(startDate_lbl);
		datePanel.add(startDate);
		
		JLabel endDate_lbl = new JLabel("End Date");
		final JDateChooser endDate = new JDateChooser();
		datePanel.add(endDate_lbl);
		datePanel.add(endDate);
		
		//Add Buttons
		JPanel btnPanel = new JPanel();
		panel.add(btnPanel, BorderLayout.CENTER);
		JButton find = new JButton("Find");
		find.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (startDate.getDate() == null)JOptionPane.showMessageDialog(null, "Must Specify a starting date");
				else{
					Date start = startDate.getDate();
					Date end = null;
					if (endDate.getDate() == null)end = new Date();
					else end = endDate.getDate();

					if (start.after(end))JOptionPane.showMessageDialog(null, "Start date cannot be after ending date");
					else{
						Collection <Cohort> results = null;
						try{
//							System.out.printf("Searching betweeen %s and %s\n", start, end);
							results = controller.getByDate(start, end);	
							mTModel.importCollection(results);
						}catch (Exception e){ e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
						if (results == null || results.size() < 1)JOptionPane.showMessageDialog(null, "No results found");
						
					}
				}
			}
		});
		btnPanel.add(find);
		JButton clear = new JButton("Clear");
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Clicked Clear");
				startDate.setDate(null);
				endDate.setDate(null);
			}
		});
		btnPanel.add(clear);
		return panel;
	}
	
	protected JComponent buildKeywordView(){
		System.out.println("Building the Keyword view");
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		
		//Create keyword Fields
		JPanel keywordPanel = new JPanel();
		panel.add(keywordPanel, BorderLayout.NORTH);
		
		//Build ComboBox
		final String [] fields = {"Select Field","Name","Funder", "Budget","Team Lead"};
		final String [] dbNames = {"", "name", "funder","totalBudget","teamLead" };
		JLabel keyword_lbl = new JLabel("Keyword: ");
		keyword_lbl.setHorizontalAlignment(SwingConstants.RIGHT);
		final JTextField keyword = new JTextField();
		keywordPanel.setLayout(new GridLayout(2, 2));
		keywordPanel.add(keyword_lbl);
		keywordPanel.add(keyword);
		
		
		JLabel field_lbl = new JLabel("Field: ");
		field_lbl.setHorizontalAlignment(SwingConstants.RIGHT);
		final JComboBox<String> field_box = new JComboBox<String>(fields);
		keywordPanel.add(field_lbl);
		keywordPanel.add(field_box);
		
		//Add Buttons
		JPanel btnPanel = new JPanel();
		panel.add(btnPanel, BorderLayout.CENTER);
		JButton find = new JButton("Find");
		btnPanel.add(find);
		find.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String value = keyword.getText();
				if (value == null || value.equals(""))JOptionPane.showMessageDialog(null, "Must specify terms to search for");
				else{
					int selected = field_box.getSelectedIndex();
					if (selected < 1)JOptionPane.showMessageDialog(null, "Must select dimension to search for");
					else{
						String field = dbNames[selected];
						Collection<Cohort> results = null;
						try{
							System.out.printf("Attempting to find %s with a value %s\n", field,value);
							results = controller.findByKeyword(field,value);
							mTModel.importCollection(results);
						}catch(Exception e){ e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
						if (results == null || results.size() < 1)JOptionPane.showMessageDialog(null, "No results found");
						
						
					}
				}
			}
		});
		
		
		JButton clear = new JButton("Clear");
		btnPanel.add(clear);
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				keyword.setText("");
			}
		});
		
		return panel;
	}
	/**
	 * @wbp.parser.entryPoint
	 */	
	protected JComponent buildBeneficiaryView(){
		JPanel panel = new JPanel(new BorderLayout());
		//Create beneficiaries Fields
		JPanel beneficiaryPanel = new JPanel();
		beneficiaryPanel.setLayout(new GridLayout(0, 1));
		panel.add(beneficiaryPanel, BorderLayout.NORTH);
		
		JCheckBox isGroup = new JCheckBox("Use Individuals");
		beneficiaryPanel.add(isGroup);
		isGroup.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED){
                	//TODO load individuals
                }else{
                	//TODO load groups
                }
            }
        });
		
		final String [] fields = BeneficiaryController.getInstance(this.controller.getConnection()).getAgeGroup();
		final JComboBox<String> field_box = new JComboBox<String>(fields);
		beneficiaryPanel.add(field_box);
		
		//Add Buttons
		JPanel btnPanel = new JPanel();
		panel.add(btnPanel, BorderLayout.CENTER);
		JButton find = new JButton("Find");
		find.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int select = field_box.getSelectedIndex();
				if (select < 1)JOptionPane.showMessageDialog(null, "Must select a beneficiary");
				else{
					try {
						ArrayList<Cohort> cList = new ArrayList<Cohort>();
						Collection<Beneficiary> bList = BeneficiaryController.getInstance(controller.getConnection()).read("age", fields[select]);
						Iterator<Beneficiary> ib = bList.iterator();
						while(ib.hasNext()){
							Beneficiary b = ib.next();
							if (b.getCohort() != null)cList.add(b.getCohort());
						}
						mTModel.importCollection(cList);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		btnPanel.add(find);
		JButton clear = new JButton("Clear");
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
//		btnPanel.add(clear);
		return panel;
	}
	
	protected JComponent buildLocationView(){
		JPanel panel = new JPanel(new BorderLayout());
		//Create location Fields
		JPanel locationPanel = new JPanel();
		panel.add(locationPanel, BorderLayout.NORTH);
		
		final String [] fields = {"Select Location"};
		//TODO implement select distinct location
		final JComboBox<String> field_box = new JComboBox<String>(fields);
		locationPanel.add(field_box);
		
		//Add Buttons
		JPanel btnPanel = new JPanel();
		panel.add(btnPanel, BorderLayout.CENTER);
		JButton find = new JButton("Find");
		find.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnPanel.add(find);
		JButton clear = new JButton("Clear");
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
//		btnPanel.add(clear);
		return panel;
	}
}
