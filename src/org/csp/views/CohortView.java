package org.csp.views;

import java.awt.Container;
import java.util.Collection;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import org.csp.controller.CohortController;
import org.csp.controller.SettingController;
import org.csp.models.Cohort;
import org.csp.models.Member;
import org.csp.models.Organization;
import org.csp.models.Person;
import org.csp.models.Project;
import org.metawidget.inspector.annotation.UiAction;
import org.metawidget.inspector.annotation.UiHidden;

import com.toedter.calendar.JDateChooser;

public class CohortView extends ModelView<Cohort> {

	protected boolean isExisting = true;
	
	public CohortView(Container container) {
		super(container);
	}

	public CohortView(Container container, boolean useLeftPanel) {
		super(container, useLeftPanel);
	}

	public CohortView(Container container, boolean useLeftPanel,boolean readOnly, Cohort model, boolean useButtons) {
		super(container, useLeftPanel, readOnly, model, useButtons);
	}

	public CohortView(Container container, boolean useLeftPanel,boolean readOnly, Cohort model) {
		super(container, useLeftPanel, readOnly, model);
	}

	public CohortView(Container container, boolean useLeftPanel,boolean readOnly) {
		super(container, useLeftPanel, readOnly);
	}

	@Override
	protected void configureModel() {
		this.model =   new Cohort();
		model.setProject(new Project());
		model.setTeamLead(new Member(new Person()));
		model.setFunder(new Organization());
		this.controller = CohortController.getInstance(SettingController.getInstance().getConnection());
	}
	
	public void find(){
		Cohort c = this.mMetawidget.getToInspect();
		System.out.println("Searching for :"+c);
		if (c != null){
			try{
				JDateChooser eDate = (JDateChooser)this.mMetawidget.getComponent("startDate");
				if (eDate.getDate() != null)c.setStartDate(eDate.getDate());
				eDate = (JDateChooser)this.mMetawidget.getComponent("endDate");
				if (eDate.getDate() != null)c.setEndDate(eDate.getDate());
				//TODO develop a specific set of functionality for the date using a range
				
				if (c.getFunder() != null && c.getFunder().getId() == 0)c.setFunder(null);
				if (c.getTeamLead() != null && c.getTeamLead().getId() == 0)c.setTeamLead(null);
				if (c.getTotalBudget() == 0)c.setTotalBudget(-1);
				
				Collection<Cohort> cCols = this.controller.find(c);
				
				if (cCols.size() > 0)this.refreshTable(cCols);
				else JOptionPane.showMessageDialog(this.container, "No Project found");
			}catch (Exception e){
				JOptionPane.showMessageDialog(this.container, "No Project found");
				e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
			}
		}
	}
	
	public void save(){
		Cohort m = this.mMetawidget.getToInspect();
		if (m.getId() != 0){
			int response;
			response = JOptionPane.showConfirmDialog(null, "Update the current Project?", "Update", JOptionPane.YES_NO_OPTION );
			if (response != JOptionPane.YES_OPTION)return;
		
			try {
				this.controller.save(m);
				JOptionPane.showMessageDialog(container, "Project Successfully Update");
			} catch (Exception e) {
				JOptionPane.showMessageDialog(container, "Unable to save project");
				e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
			}
		}else{
			JOptionPane.showMessageDialog(this.container, "Unable to save new project with this option.");
		}
	}
	
	public void load(Cohort c) throws Exception{
		if (this.mMetawidget != null){
			this.model = this.controller.refreshModel(c);
			this.mMetawidget.setToInspect(c);
			
			if (!this.readOnly){			
				//Reinitialize the dates
				JDateChooser sDate = (JDateChooser)this.mMetawidget.getComponent("startDate");
				sDate.setDate(c.getStartDate());
				
				JDateChooser eDate = (JDateChooser)this.mMetawidget.getComponent("endDate");
				eDate.setDate(c.getEndDate());
				
				//Attempt to relate the team leader
				if (c.getTeamLead() != null){
					@SuppressWarnings("unchecked")
					JComboBox<Member> team = (JComboBox<Member>) mMetawidget.getComponent("teamLead");
					for (int i = 0; i < team.getItemCount(); i++)
						if (team.getItemAt(i) != null)
							if (team.getItemAt(i).equals(c.getTeamLead()))
								team.setSelectedIndex(i);
				}
				
				//Attempt to relate the funder to the list of organization
				if (c.getFunder() != null){
					@SuppressWarnings("unchecked")
					JComboBox<Organization> orgs = (JComboBox<Organization>) mMetawidget.getComponent("funder");
					for (int i = 0; i < orgs.getItemCount(); i++)
					if (orgs.getItemAt(i) != null)
						if (orgs.getItemAt(i).equals(c.getFunder()))
							orgs.setSelectedIndex(i);
				}
			}
		}else{
			System.out.println("Metawidget was not initialized");
		}
	}
	
	public void clear(){
		model = new Cohort(new Project(), new Organization());
		this.mMetawidget.setToInspect(model);
	}

	
	@UiHidden
	public boolean isExisting() { return isExisting;}	
	public void setExisting(boolean isExisting) { this.isExisting = isExisting; }
		
	@UiHidden
	public boolean isReadOnly() {
		return this.readOnly;
	}
	
	
	@UiAction
	@UiHidden
//	@UiAttribute( name = HIDDEN, value = "${this.readOnly}" )
	public void viewIndicators(){
		System.out.println("Viewining Indicators");
	}

	
}
