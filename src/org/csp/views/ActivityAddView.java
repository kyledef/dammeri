package org.csp.views;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import org.csp.models.Activity;
import org.csp.models.Input;
import org.csp.models.Output;
import org.csp.utilities.ListTableModel;
import org.metawidget.swing.SwingMetawidget;

public class ActivityAddView extends JPanel {

	private static final long serialVersionUID = -5682653866946566680L;
	
	Activity activity;
	Input input;
	Output output;
	
	Collection<Input>inputs;
	Collection<Output>outputs;
	
	SwingMetawidget activityMetawidget;
	SwingMetawidget inputMetawidget;
	SwingMetawidget outputMetawidget;
	
	ListTableModel<Input> mInpTable;
	ListTableModel<Output>moutTable;
	
	JTabbedPane tabbedPane;
	JComponent detailsPanel;
	JComponent outputPanel;
	JComponent inputPanel;
	
	IRefreshable dataset;
	
	
	public ActivityAddView(){
		super(new BorderLayout());
		init();
	}
	
	public ActivityAddView(Activity activity){
		super(new BorderLayout());
		this.init();
		this.activity = activity;
	}
	
	protected void init(){	
		this.buildTabs();		
		this.buildUIFields();		
	}
	
	public void buildUIFields() {
		this.buildDetailsPanel();
		this.buildInputsPanel();
		this.buildOutputsPanel();
	}

	public void buildTabs(){
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		add(tabbedPane, BorderLayout.CENTER);
		
		detailsPanel = this.makeTextPanel("Details");
		tabbedPane.addTab("Details", detailsPanel);
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
		
		outputPanel = this.makeTextPanel("Details");
		tabbedPane.addTab("Outputs", outputPanel);
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_2);
		
		inputPanel = this.makeTextPanel("Details");
		tabbedPane.addTab("Inputs", inputPanel);
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_3);
	}
	
	public void buildDetailsPanel(){
		if (this.activity == null)this.activity = new Activity();
		this.activityMetawidget = new SwingMetawidget();
		activityMetawidget.setConfig( "org/csp/views/metawidget.xml" );
		activityMetawidget.setToInspect(this.activity);
		this.detailsPanel.add(this.activityMetawidget, BorderLayout.CENTER);
	}
	
	public void buildOutputsPanel(){
		if (this.output == null)this.output = new Output();		
		if (this.outputs == null)outputs = new ArrayList<Output>();
		if (this.moutTable == null)this.moutTable = new ListTableModel<Output>(Output.class, this.outputs, "description" ,"feedback", "followUp","type");
		this.outputMetawidget = new SwingMetawidget();
		outputMetawidget.setConfig( "org/csp/views/metawidget.xml" );
		outputMetawidget.setToInspect(this.output);
		this.outputPanel.add(this.outputMetawidget, BorderLayout.CENTER);
		
		//Create table of added objectives
		JTable table = new JTable(moutTable);
		setTableDefaults(table);
		this.outputPanel.add(new JScrollPane( table ),BorderLayout.SOUTH);
	}
	
	public void buildInputsPanel(){
		if (this.input == null)this.input = new Input();
		if (this.inputs == null)this.inputs = new ArrayList<Input>();
		if (this.mInpTable == null)this.mInpTable = new ListTableModel<Input>(Input.class, this.inputs, "description" ,"duration");
		this.inputMetawidget = new SwingMetawidget();
		inputMetawidget.setConfig( "org/csp/views/metawidget.xml" );
		inputMetawidget.setToInspect(this.input);
		this.inputPanel.add(this.inputMetawidget, BorderLayout.CENTER);
		
		//Create table of added objectives
		JTable table = new JTable(mInpTable);
		setTableDefaults(table);
		this.inputPanel.add(new JScrollPane( table ),BorderLayout.SOUTH);
	}
	
	protected JComponent makeTextPanel(String text) {
        JPanel panel = new JPanel(false);
        JLabel filler = new JLabel(text);
        filler.setHorizontalAlignment(JLabel.CENTER);
        panel.setLayout(new BorderLayout());
        panel.add(filler, BorderLayout.NORTH);
        return panel;
    }
	
	protected void setTableDefaults(JTable table){
		table.setAutoCreateColumnsFromModel( true );
		table.setRowHeight( 35 );
		table.setShowVerticalLines( false );
		table.setShowHorizontalLines(true);
		table.setCellSelectionEnabled( false );
		table.setRowSelectionAllowed( true );
		
		table.setDefaultRenderer(Class.class, new DefaultTableCellRenderer(){
			private static final long serialVersionUID = -2883561376070724744L;
			{ setHorizontalAlignment( SwingConstants.CENTER ); }
		});
	}
	
	public IRefreshable getDataset() {
		return dataset;
	}

	public void setDataset(IRefreshable dataset) {
		this.dataset = dataset;
	}

	public static void main(String [] args){
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ActivityAddView act = new ActivityAddView();
		frame.getContentPane().add(act, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);		
	}
}
