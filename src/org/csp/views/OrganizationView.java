package org.csp.views;

import java.awt.Container;

import org.csp.controller.OrganizationController;
import org.csp.models.Organization;

public class OrganizationView extends ModelView<Organization> {

	public OrganizationView(Container container) {
		super(container);
	}
	
	public OrganizationView(Container container, boolean useLeftPanel,
			boolean readOnly, Organization model, boolean useButtons) {
		super(container, useLeftPanel, readOnly, model, useButtons);
	}

	public OrganizationView(Container container, boolean useLeftPanel,
			boolean readOnly, Organization model) {
		super(container, useLeftPanel, readOnly, model);
	}

	public OrganizationView(Container container, boolean useLeftPanel,
			boolean readOnly) {
		super(container, useLeftPanel, readOnly);
	}

	public OrganizationView(Container container, boolean useLeftPanel) {
		super(container, useLeftPanel);
	}

	@Override
	protected void configureModel() {
		this.model = new Organization();
		this.controller = OrganizationController.getInstance();
	}
}
