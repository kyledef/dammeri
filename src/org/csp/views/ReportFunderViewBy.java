package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.csp.controller.CohortController;
import org.csp.controller.OrganizationController;
import org.csp.controller.SettingController;
import org.csp.models.Cohort;
import org.csp.models.Organization;
import org.csp.utilities.ListTableModel;
import org.csp.utilities.Utilities;

public class ReportFunderViewBy extends View {

	protected ListTableModel<Organization> mTModel;
	protected JTable table;
	


	protected OrganizationController controller;
	protected ArrayList<Organization> organizations;
	
	int field;
	JPanel containerPanel;
	JPanel panelRight;
	boolean useInternal = true;
	
	public ReportFunderViewBy(Container container) {
		super(container);
		initUI();
	}
	public ReportFunderViewBy(Container container, boolean b) {
		super(container, b);
		initUI();
	}
	public ReportFunderViewBy(Container container, boolean useLeftPanel,boolean readOnly, boolean useButtons) {
		super(container, useLeftPanel, readOnly, useButtons);
		initUI();
	}
	public ReportFunderViewBy(Container container, boolean useLeftPanel,boolean readOnly) {
		super(container, useLeftPanel, readOnly);
		initUI();
	}
	
	public ReportFunderViewBy(Container container, boolean useLeftPanel, JTable table, boolean useInteral){
		super(container, useLeftPanel);
		this.table = table;
		this.useInternal = useInteral;
	}
	
	protected void initUI(){
		containerPanel = new JPanel(new BorderLayout());
		panelRight = new JPanel(new BorderLayout());
		panelRight.setBorder( BorderFactory.createEmptyBorder( COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING ) );
		container.add( panelRight, BorderLayout.CENTER );		
		panelRight.add(containerPanel, BorderLayout.CENTER);
		//Create table for results
		
		if (this.useInternal)containerPanel.add(this.createResultsSection(), BorderLayout.SOUTH);
		containerPanel.add(buildSearchField(), BorderLayout.NORTH);
	}

	protected Component buildSearchField() {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		
		
		JPanel keywordPanel = new JPanel();
		keywordPanel.setLayout(new GridLayout(1, 2));
		panel.add(keywordPanel, BorderLayout.NORTH);
		JLabel project_lbl = new JLabel("Project: ");
		project_lbl.setHorizontalAlignment(SwingConstants.RIGHT);
		final JTextField project = new JTextField();
		keywordPanel.add(project_lbl);
		keywordPanel.add(project);
		
		//Add Buttons
		JPanel btnPanel = new JPanel();
		panel.add(btnPanel, BorderLayout.CENTER);
		JButton find = new JButton("Find");
		find.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String text = project.getText();
				if (text == null || text.equals(""))
					JOptionPane.showMessageDialog(null, "No project specified");
				else{
					try{
						ArrayList<Organization> orgs = new ArrayList<Organization>();
						CohortController cc = CohortController.getInstance(SettingController.getInstance().getConnection());
						
						Collection<Cohort> results = cc.findByKeyword("name", text);
						
						if (results.size() < 1)JOptionPane.showMessageDialog(null, "Unable to find any projects by that name");
						else{
							Iterator<Cohort> ic = results.iterator();
							while(ic.hasNext()){
								Cohort c = ic.next();
								if (c.getFunder() != null)orgs.add(c.getFunder());
							}
							if (orgs.size() < 1)JOptionPane.showMessageDialog(null, "No Funders found");
							else mTModel.importCollection(orgs);
						}
					}catch(Exception ex){
						JOptionPane.showMessageDialog(null, "Unable to find any projects by that name");
						ex.printStackTrace();
					}
				}
			}
		});
		btnPanel.add(find);
		JButton clear = new JButton("Clear");
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnPanel.add(clear);
		return panel;
	}
	
	
	protected void find(){
		
	}
	
	
	public Component createResultsSection() {
		controller = OrganizationController.getInstance();
		organizations = new ArrayList<Organization>();
		
		mTModel = new ListTableModel<Organization>(Organization.class, organizations,"name","address","type" );
		table = new JTable( mTModel );
		Utilities.setTableDefaults(table);		
	
		table.addMouseListener( new MouseAdapter() {
			@Override
			public void mouseClicked( MouseEvent event ) {
				//Ensure user double clicks table
				if ( event.getClickCount() != 2 )return; 
				@SuppressWarnings("unchecked")
				ListTableModel<Organization> model = (ListTableModel<Organization>) table.getModel();	
				if (table.getSelectedRow() >= 0){
					Organization t = model.getValueAt( table.getSelectedRow() );
					System.out.println(t + "CLICKED");
				}					
			}
		});	
		
		JScrollPane scroll =  new JScrollPane( table ); 
		//Set Height of table
		scroll.setPreferredSize( new Dimension((int)  scroll.getPreferredSize().getWidth(), 200));
		return scroll;
	}
	
	public JTable getTable() {
		return table;
	}
	public void setTable(JTable table) {
		this.table = table;
	}

}
