package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import org.csp.controller.Controller;
import org.csp.controller.SettingController;
import org.csp.models.Model;
import org.csp.utilities.ListTableModel;
import org.csp.utilities.Utilities;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.metawidget.inspector.annotation.UiHidden;
import org.metawidget.swing.Facet;
import org.metawidget.swing.SwingMetawidget;
import org.metawidget.swing.layout.FlowLayout;
import org.metawidget.swing.widgetprocessor.binding.beansbinding.BeansBindingProcessor;
import org.metawidget.swing.widgetprocessor.binding.beansbinding.BeansBindingProcessorConfig;

public abstract class ModelView <T extends Comparable<T>> extends View{

	protected SwingMetawidget mMetawidget;
	protected Controller<T> controller;
	protected T model;
	protected ListTableModel<T> mTModel;
	protected JTable table;
	protected JPanel panelRight;
	
	public String saveMsg;
	public String saveErrMsg;
	public String searchErrMsg;
	public String deleteMsg;
	public String deleteErrMsg;
	
	protected IRefreshable refreshable;
	protected boolean useButtons = false;
	protected boolean displayResults = false;
	protected boolean readOnly = false;
	
	//Items required to build the right click menu
	JPopupMenu popupMenu;
	ArrayList<JMenuItem>  popupMenuItems;
	SwingMetawidget buttonsMetawidget;
	protected JScrollPane scrollResults;
	
	/**
	 * 
	 * @param container
	 */
	public ModelView(Container container){
		this(container, true);
	}
	
	/**
	 * 
	 * @param container Specifies the component that will hold the elements to be displayed
	 * @param useLeftPanel use to determine if to load the information side panel
	 */
	public ModelView(Container container, boolean useLeftPanel){
		this(container, useLeftPanel, true);
	}
	
	/**
	 * 
	 * @param container Specifies the component that will hold the elements to be displayed
	 * @param useLeftPanel use to determine if to load the information side panel
	 * @param readOnly  true will cause the fields to be non editable, false is the default value
	 */
	public ModelView(Container container, boolean useLeftPanel, boolean readOnly){
		this(container, useLeftPanel, readOnly, null);
	}
	
	/**
	 * 
	 * @param container Specifies the component that will hold the elements to be displayed
	 * @param useLeftPanel use to determine if to load the information side panel
	 * @param readOnly  true will cause the fields to be non editable, false is the default value
	 * @param model will specify the object and its value to be loaded into the UI elements
	 */
	public ModelView(Container container, boolean useLeftPanel, boolean readOnly, T model){
		this(container, useLeftPanel, readOnly, model, true);
	}
	
	/**
	 * 
	 * @param container Specifies the component that will hold the elements to be displayed
	 * @param useLeftPanel use to determine if to load the information side panel
	 * @param readOnly  true will cause the fields to be non editable, false is the default value
	 * @param model will specify the object and its value to be loaded into the UI elements
	 * @param useButtons false will prevent button generation based on reflection
	 */
	public ModelView(Container container, boolean useLeftPanel, boolean readOnly, T model, boolean useButtons){
		this(container, useLeftPanel, readOnly, model, true, false);
	}
	
	/**
	 * 
	 * @param container
	 * @param useLeftPanel
	 * @param readOnly
	 * @param model
	 * @param useButtons
	 * @param displayResults
	 */
	public ModelView(Container container, boolean useLeftPanel, boolean readOnly, T model, boolean useButtons, boolean displayResults){
		super(container, useLeftPanel, readOnly,useButtons);	
		this.readOnly = readOnly;
		this.useButtons = useButtons;
		this.displayResults = displayResults;
		if (model != null)this.model = model;
		initUI();
	}
	
	/**
	 * 
	 */
	protected abstract void configureModel();
	
	/**
	 * 
	 */
	protected void setAccessLevel(){
		if (this.mMetawidget != null) this.mMetawidget.setReadOnly(this.readOnly);
		else System.out.println("Metawidget was not initialized");
	}
	
	/**
	 * 
	 */
	protected void initUI(){
		this.configureModel();
		this.buildRightPanel();
		this.setAccessLevel();
	}
	
	protected SwingMetawidget startInspection(){ return startInspection(null); }
	
	/**
	 * 
	 * @param metaWidget
	 * @return
	 */
	protected SwingMetawidget startInspection(SwingMetawidget metaWidget){	
		boolean useInternal = (metaWidget == null);
		if (metaWidget == null)metaWidget = new SwingMetawidget();
		metaWidget.setConfig( "/org/csp/views/metawidget.xml" );
		metaWidget.setToInspect( model );			
		metaWidget.addWidgetProcessor( new BeansBindingProcessor(new BeansBindingProcessorConfig().setUpdateStrategy( UpdateStrategy.READ_WRITE )) );
		metaWidget.setReadOnly(this.readOnly);
		if (useInternal)this.mMetawidget = metaWidget;
		return metaWidget;
	}
	
	protected void buildRightPanel(){
		panelRight = new JPanel(new BorderLayout());
		panelRight.setBorder( BorderFactory.createEmptyBorder( COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING ) );
		container.add( panelRight, BorderLayout.CENTER );
		
		JPanel panel = new JPanel(new BorderLayout());
		panelRight.add(panel ,BorderLayout.NORTH);
		
		startInspection();
		
		if (useButtons)this.buildBtnReflection();
		if (displayResults)panel.add(this.createResultsSection() , BorderLayout.SOUTH);		
		panel.add(mMetawidget, BorderLayout.CENTER);	
		
	}
	
	public JComponent createResultsSection(Container panel, ListTableModel<?> tModel, int height, String pos, final ActionListener mouseClick,final ActionListener rightClick){
		table = new JTable(tModel);
		Utilities.setTableDefaults(table);
		JScrollPane scroll =  new JScrollPane( table ); 
		//Set Height of table
		scroll.setPreferredSize( new Dimension((int)  scroll.getPreferredSize().getWidth(), height));
		if (panel != null)panel.add( scroll, pos);	
		
		table.addMouseListener( new MouseAdapter() {
			@Override
			public void mouseClicked( MouseEvent event ) {
				System.out.println("Clicked");
				//Ensure user double clicks table
//				if ( event.getClickCount() != 2 )return; 
				if (SwingUtilities.isLeftMouseButton(event)){
					@SuppressWarnings("rawtypes")
					ListTableModel model = (ListTableModel) table.getModel();
					if (table.getSelectedRow() >= 0){
						Model m = (Model) model.getValueAt( table.getSelectedRow() );	
						System.out.println("ID of the selected is: "+m.getId());				
						if (mouseClick != null)mouseClick.actionPerformed(new ActionEvent(table, m.getId(), "select"));
					}	
				}
			}
			@Override
			public void mousePressed(MouseEvent event){
				System.out.println("Pressed");
				if (SwingUtilities.isRightMouseButton(event)){
					@SuppressWarnings("rawtypes")
					ListTableModel model = (ListTableModel) table.getModel();
					if (table.getSelectedRow() >= 0){
						Model m = (Model) model.getValueAt( table.getSelectedRow() );	
						if (rightClick != null)rightClick.actionPerformed(new ActionEvent(table, m.getId(), "menu"));
					}
				}
			}
		});	
		
		scrollResults =  new JScrollPane( table ); 		
		//Set Height of table
		scrollResults.setPreferredSize( new Dimension((int)  scrollResults.getPreferredSize().getWidth(), height));
		return scrollResults;
	}
	
	public JComponent createResultsSection(Container panel, ListTableModel<?> tModel, final ActionListener mouseClick,final ActionListener rightClick){
		return createResultsSection(panel, tModel, 200, BorderLayout.PAGE_END,  mouseClick,rightClick);
	}
	public JComponent createResultsSection( int height, String pos, final ActionListener mouseClick,final ActionListener rightClick){
		return this.createResultsSection(null, this.mTModel, height, pos, mouseClick, rightClick);
	}
	public JComponent createResultsSection( final ActionListener mouseClick,final ActionListener rightClick){
		return this.createResultsSection(null, this.mTModel, mouseClick, rightClick);
	}
	
	
	//The following two createResultsSection was implemented before.
	//TODO eventual synchornize the code base between them
	
	public JComponent createResultsSection(int height){
		table = new JTable( mTModel );
		Utilities.setTableDefaults(table);		
	
		table.addMouseListener( new MouseAdapter() {
			@Override
			public void mouseClicked( MouseEvent event ) {
				//Ensure user double clicks table
				if ( event.getClickCount() != 2 )return; 
				@SuppressWarnings("unchecked")
				ListTableModel<T> model = (ListTableModel<T>) table.getModel();	
				if (table.getSelectedRow() >= 0){
					T t = model.getValueAt( table.getSelectedRow() );					
					handleModelSelect(t);
				}					
			}
		});	
		this.buildModelTableMenu();
		scrollResults =  new JScrollPane( table ); 
		
		//Set Height of table
		scrollResults.setPreferredSize( new Dimension((int)  scrollResults.getPreferredSize().getWidth(), height));
		return scrollResults;
		
	}
	
	public JComponent createResultsSection(){
		return createResultsSection(200);
	}

	public void buildBtnReflection(){
		buildBtnReflection(null);
	}
	
	public void buildBtnReflection(SwingMetawidget widget){
		
		if (widget == null)widget = this.mMetawidget;
		
		Facet facetButtons = new Facet();
		facetButtons.setName( "buttons" );
		facetButtons.setOpaque( false );
		widget.add( facetButtons );
		
		buttonsMetawidget = new SwingMetawidget();
		buttonsMetawidget.setConfig("/org/csp/views/metawidget.xml");
		buttonsMetawidget.setMetawidgetLayout( new FlowLayout() );
		buttonsMetawidget.setToInspect( this );		
		facetButtons.add( buttonsMetawidget );
	}
	
	protected void refreshTable(){
		refreshTable(null);
	}
	
	protected void refreshTable(Collection<T> list) {
		if (list == null)list = controller.getAll();	
		this.mTModel.importCollection(list);		
	}

	@UiHidden
	public IRefreshable getRefreshable() {
		return refreshable;
	}

	public void setRefreshable(IRefreshable refreshable) {
		this.refreshable = refreshable;
	}

	@UiHidden
	public JPanel getPanelRight() {
		return panelRight;
	}

	public void setPanelRight(JPanel panelRight) {
		this.panelRight = panelRight;
	}

	@UiHidden
	public T getModel() {
		return model;
	}

	public void setModel(T model) {
		this.model = model;
		this.mMetawidget.setToInspect(this.model);
	}
	
	@UiHidden
	public SwingMetawidget getmMetawidget() {
		return mMetawidget;
	}

	public void setmMetawidget(SwingMetawidget mMetawidget) {
		this.mMetawidget = mMetawidget;
	}

	@UiHidden
	public ListTableModel<T> getmTModel() {
		return mTModel;
	}

	public void setmTModel(ListTableModel<T> mTModel) {
		this.mTModel = mTModel;
	}

	@UiHidden
	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	@UiHidden
	public boolean isUseButtons() {
		return useButtons;
	}

	public void setUseButtons(boolean useButtons) {
		this.useButtons = useButtons;
	}

	@UiHidden
	public boolean isDisplayResults() {
		return displayResults;
	}
	
	public void setDisplayResults(boolean displayResults) {
		this.displayResults = displayResults;
	}
	
	@UiHidden
	public boolean isReadOnly() {
		return readOnly;
	}
	

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
		if (this.mMetawidget != null)this.mMetawidget.setReadOnly(readOnly);
	}

	public JScrollPane getScrollResults() {
		return scrollResults;
	}

	public void setScrollResults(JScrollPane scrollResults) {
		this.scrollResults = scrollResults;
	}

	protected void handleModelSelect(T model){
		this.mMetawidget.setToInspect(model);
	}
	
	protected void buildModelTableMenu(){
		if (this.popupMenu == null){
			this.popupMenu = new JPopupMenu();
			this.popupMenuItems = new ArrayList<JMenuItem>();
			
			JMenuItem item;
			//View Options
			item = new JMenuItem("View");
			item.addActionListener(new ActionListener() {
	            @SuppressWarnings("unchecked")
				@Override
	            public void actionPerformed(ActionEvent e) {
	            	ListTableModel<T> model = (ListTableModel<T>) table.getModel();	
					if (table.getSelectedRow() >= 0){
						T t = model.getValueAt( table.getSelectedRow() );
//						System.out.println(t);
						handleModelSelect(t);
					}
	            }
	        });
			popupMenuItems.add(item);
			popupMenu.add(item);
			
			//Delete Options
			item = new JMenuItem("Delete");
			item.addActionListener(new ActionListener() {
	            @SuppressWarnings("unchecked")
	            public void actionPerformed(ActionEvent e) {
	            	ListTableModel<T> model = (ListTableModel<T>) table.getModel();	
					if (table.getSelectedRow() >= 0)delete(model.getValueAt( table.getSelectedRow() ));						
					else JOptionPane.showMessageDialog(null, "Must select record first before deleting");
	            }
	        });
			popupMenuItems.add(item);
			popupMenu.add(item);
			
			if (table != null)table.setComponentPopupMenu(popupMenu);
		}
	}
	
	public void delete(T model){
		if (model == null)model = this.mMetawidget.getToInspect();
		try {
			if (model != null){
				Model m = (Model)model;
				int response;
				
				if (m.getId() != 0){
					response = JOptionPane.showConfirmDialog(null, "Are you sure you would like to delete the selected record", "Delete", JOptionPane.OK_CANCEL_OPTION);
					if (response == JOptionPane.OK_OPTION){
						if (this.controller.delete(model)){
							JOptionPane.showMessageDialog(null, "Record successfully deleted");
							refreshTable();
						}
					}
					return;
				}
						
			}
				
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Unable to delete record");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}
}
