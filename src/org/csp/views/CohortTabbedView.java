package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.csp.controller.ActivityController;
import org.csp.controller.BeneficiaryController;
import org.csp.controller.CohortController;
import org.csp.controller.IndicatorController;
import org.csp.controller.InputController;
import org.csp.controller.ObjectiveController;
import org.csp.controller.OutcomeController;
import org.csp.controller.OutputController;
import org.csp.controller.ProjectController;
import org.csp.controller.QuestionActivityController;
import org.csp.controller.QuestionController;
import org.csp.controller.QuestionOutcomeController;
import org.csp.controller.QuestionOutputController;
import org.csp.controller.SettingController;
import org.csp.models.Activity;
import org.csp.models.ActivityObjective;
import org.csp.models.Beneficiary;
import org.csp.models.Cohort;
import org.csp.models.Indicator;
import org.csp.models.Input;
import org.csp.models.Model;
import org.csp.models.Objective;
import org.csp.models.Outcome;
import org.csp.models.Output;
import org.csp.models.Project;
import org.csp.models.Question;
import org.csp.models.QuestionActivity;
import org.csp.models.QuestionOutcome;
import org.csp.models.QuestionOutput;
import org.csp.utilities.ListTableModel;
import org.csp.utilities.Utilities;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.metawidget.swing.SwingMetawidget;
import org.metawidget.swing.widgetprocessor.binding.beansbinding.BeansBindingProcessor;
import org.metawidget.swing.widgetprocessor.binding.beansbinding.BeansBindingProcessorConfig;

import com.j256.ormlite.support.ConnectionSource;
import com.toedter.calendar.JDateChooser;

public class CohortTabbedView extends CohortView{	
	
	//TODO clear after deleting questions
	//TODO Change next to finish when get to output (aka no more next)
	//TODO (High) When reselecting Beneficiary Type created with other not showing up
	//TODO (High) When reselecting Beneficiary Age Group created with other not showing up
	//TODO (Medium) Increase the width of the form
	//TODO (Low) Right align the fields of the form
	
	//General UI Variables
	JTabbedPane tabbedPane;
	ActionListener actionListener;
	
	//General Variables
	ConnectionSource conn;
	JLabel projectName;
	JFrame subFrame;

	SettingController sController;
	
	//Question Variables
	JComponent mnePanel;
	Question question;
	SwingMetawidget questionWidget;
	Collection<Question>questions;
	ListTableModel<Question> mQuestTable;
	QuestionController qController;
	SwingWorker<Boolean, Integer> questionWorker;
	JComboBox<?> additionalQbox;
	QuestionActivityController qaController;
	QuestionOutcomeController qoController;
	QuestionOutputController qoutController;
	
	//Activity Variables
	ActivityController aController;
	JComponent activitiesPanel;
	Activity activity;
	SwingMetawidget activityWidget;
	Collection<	Activity>activities;
	ListTableModel<Activity> mActTable;
	SwingWorker<Boolean, Integer> activityWorker;
	
	//Objective Variables
	ObjectiveController oController;
	JComponent objectivesPanel;
	Objective objective;
	SwingMetawidget objectiveWidget;	
	Collection<Objective>objectives;
	ListTableModel<Objective> mObjnTable;
	SwingWorker<Boolean, Integer> objectiveWorker;
	
	//Cohort Variables
	Cohort cohort;
	JComponent detailsPanel;
	SwingMetawidget cohortWidget;
	ProjectController pController;
	CohortController cController;
	SwingWorker<Boolean, Integer> cohortWorker;
	
	//Beneficiary Variables
	BeneficiaryController bController;
	JComponent beneficiariesPanel;
	Beneficiary beneficiary;
	SwingMetawidget beneficiaryWidget;
	Collection<Beneficiary> beneficiaries;
	ListTableModel<Beneficiary> mBenTable;
	SwingWorker<Boolean, Integer> beneficiaryWorker;
	
	//Outcome Variables
	JComponent outcomePanel;	
	Outcome outcome;
	SwingMetawidget outcomeWidget;
	Collection<Outcome>outcomes;
	ListTableModel<Outcome> mOutTable;
	OutcomeController outController;
	SwingWorker<Boolean, Integer> outcomeWorker;
	
	//Indicator Variables
	JComponent indicatorPanel;
	Indicator indicator;
	SwingMetawidget indicatorWidget;
	Collection<Indicator> indicators;
	ListTableModel<Indicator> mIndTable;
	IndicatorController iController;
	SwingWorker<Boolean, Integer> indicatorWorker;
	
	JComponent inPanel;
	Input input;
	SwingMetawidget inputWidget;
	InputController inController;
	SwingWorker<Boolean, Integer> inputOutputWorker;
	ListTableModel<Input> mInputTable;
	Collection<Input> inputs;
	JPanel secondaryInOutPanel;
	
	JComponent outPanel;
	Output output;
	Collection<Output> outputs;
	SwingMetawidget outputWidget;
	OutputController outputController;
	ListTableModel<Output> mOutputTable;
	
	protected int currentTab = 0;
	protected int objectiveNumber = 0;

	boolean isCohortSaved = false;
	boolean isCohortLoaded = false;
	boolean isBeneficiarySaved = false;
	boolean isObjectiveSaved = false;
	boolean isOutcomeSaved = false;
	boolean isActivitySaved = false;
	boolean isQuestionSaved = false;
	
	public CohortTabbedView(Container container) {
		super(container, false);
	}
	public CohortTabbedView(Container container, boolean useLeftPanel, boolean readOnly, Cohort model){
		super(container, useLeftPanel, readOnly, model);
		if (model != null)this.loadCohort(model);
	}
	public CohortTabbedView(Container container, boolean useLeftPanel,boolean readOnly, Cohort model, boolean useButtons) {
		super(container, useLeftPanel, readOnly, model, useButtons);
		if (model != null)this.loadCohort(model);
	}
	public CohortTabbedView(Container container, boolean useLeftPanel,boolean readOnly) {
		super(container, useLeftPanel, readOnly);
	}
	public CohortTabbedView(Container container, boolean useLeftPanel) {
		super(container, useLeftPanel);
	}
	
	protected void initUI(){
		initControllers();
		super.initUI();			
		addBottomBarButtons();	
	}
	
	protected void addBottomBarButtons(){
		final JPanel btnPanel = new JPanel();		
		JButton btnPrevious = new JButton("< Previous");
		btnPrevious.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				currentTab = tabbedPane.getSelectedIndex();
				if (currentTab > 0)
					tabbedPane.setSelectedIndex(--currentTab);
			}
		});
		btnPanel.add(btnPrevious);
		
		JButton btnNext = new JButton("Next >");
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				currentTab = tabbedPane.getSelectedIndex();
				if (currentTab < tabbedPane.getTabCount()-1)
					tabbedPane.setSelectedIndex(++currentTab);				
				if (model.getId() == 0)
					try { saveCohort(); }						
					catch (Exception e) { e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }					
			}
		});
		btnPanel.add(btnNext);
		
		if (!this.readOnly){
			
			JButton btnSave = new JButton("Save");
			btnSave.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					try { 
						saveAll(); 
						JOptionPane.showMessageDialog(container, "Record Saved");
					}catch (Exception e) { JOptionPane.showMessageDialog(container, "Unable to save Project"); e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
				}
			});
			btnPanel.add(btnSave);
			
			JButton btnCancel = new JButton("Cancel");
			btnCancel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					int response = JOptionPane.showConfirmDialog(null, "Canceling will result in loss of all changes. Do you want to continue?", "Confirm Cancel", JOptionPane.OK_CANCEL_OPTION);
					if (response == JOptionPane.OK_OPTION){
						container.setVisible(false);
					}					
				}
			});
			btnPanel.add(btnCancel);
		}
		
		container.add(btnPanel, BorderLayout.SOUTH);
	}
	
	protected void extraUISetup(){
		projectName = new JLabel();
		projectName.setHorizontalAlignment(JTextField.CENTER);
		container.add(this.projectName, BorderLayout.NORTH);
		if (!readOnly){			
			//Set up Listener for changes in project description
			final JTextField nameF = ((SwingMetawidget)mMetawidget.getComponent("project")).getComponent("name");
			if (nameF != null){
				nameF.getDocument().addDocumentListener(new DocumentListener() {
					public void changedUpdate(DocumentEvent e) { updateLabel(e); }
					public void insertUpdate(DocumentEvent e)  { updateLabel(e); }
					public void removeUpdate(DocumentEvent e)  { updateLabel(e); }
					private void updateLabel(DocumentEvent e)  {
						EventQueue.invokeLater(new Runnable() {
		                    public void run() { projectName.setText(nameF.getText()); }	                    
		                });
					}
				});
			}
			//Set Date change listener
			JDateChooser eDate = (JDateChooser)mMetawidget.getComponent("endDate");
			eDate.addPropertyChangeListener(new PropertyChangeListener (){
				@Override
				public void propertyChange(PropertyChangeEvent e) {					
					JDateChooser sDate = (JDateChooser)mMetawidget.getComponent("startDate");
					Date end = null;
					if (e.getNewValue() instanceof Date)end = (Date)e.getNewValue();															
					if (end != null){	
						if (sDate != null && sDate.getDate() != null){
							if (sDate.getDate().after(end)){
								JOptionPane.showMessageDialog(container, "Start of project cannot be after project ends.");
							}
						}else{
							Date today = new Date();
							if (today.after(end)){
								JOptionPane.showMessageDialog(container, "Project end date cannot be in the past");
							}
						}
					}
				}
			});
		}
	}
	
	public void loadCohort(Cohort cohort){
		if (cohort != null && cohort.getId() != 0){
			try {
				//#1 Refresh model to ensure we have all the required project, objectives, questions models in the object
				this.load(cohort);
				//#2 Load Beneficiaries
				if (this.beneficiaries == null)this.beneficiaries = new ArrayList<Beneficiary>();
				this.beneficiaries.addAll(this.model.getBeneficiaries());
				this.refreshBeneficiaries();
				System.out.printf("Found %d beneficiaries in %s \n", beneficiaries.size(), model.getProject());
				
				//#3 Load Objectives
				Project p = this.model.getProject();
				if (p.getId() > 0){
					this.objectives.addAll(p.getObjectives());	
					System.out.printf("Found %d objectives in %s \n", objectives.size(), model.getProject());
					this.refreshObjectives(); 
				}
				
				//#4 Load Activities
				this.activities.addAll(this.model.getActivities());
				if (activities.size() > 0){
					this.refreshActivities();
				}
				this.reloadActivity(true);
				System.out.printf("Found %d activities in %s \n", activities.size(), model.getProject());
				
				//#5 Load Outcomes
				this.outcomes.addAll(this.model.getOutcomes());
				if (outcomes.size() > 0)this.refreshOutcomes();
				System.out.printf("Found %d outcomes in %s\n",this.outcomes.size(), model.getProject() );
				
				//#6 Load Questions
				this.questions.addAll(this.model.getQuestions());
				System.out.printf("Found %d questions in %s \n", questions.size(), model.getProject());
				if (this.questions.size() > 0){
					this.refreshMNE();
					
					//#7 Load Indicators
					Iterator<Question>qi = questions.iterator();
					if (this.indicators == null)indicators = new ArrayList<Indicator>();
					while (qi.hasNext()){
						Question q = qi.next();					
						this.indicators.addAll(q.getIndicators());
						System.out.printf("Questions %s has %d indicators: \n",q, q.getIndicators().size() );
					}
					if (indicators.size() > 0){
						this.refreshIndicators();
					}
					this.reloadIndicators(true);
					System.out.printf("Found %d indicators in %s \n", indicators.size(), model.getProject());
				}
				
				boolean useActivity = false;
				if (this.inputs == null)inputs = new ArrayList<Input>();
				try{inputs.addAll(this.model.getInputs());}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
				if (inputs.size() == 0)useActivity = true;
				
				//#8 - 9 Load Inputs and Outputs
				Iterator<Activity> ia = activities.iterator();
				if (this.outputs == null)outputs = new ArrayList<Output>();
//				if (useActivity)if (this.inputs == null)inputs = new ArrayList<Input>();
				
				while (ia.hasNext()){
					Activity a = ia.next();
					if (useActivity)if (a.getInputs().size() > 0)inputs.addAll(a.getInputs());
					if (a.getOutputs().size() > 0)outputs.addAll(a.getOutputs());
				}
				
				this.refreshInputs();
				this.reloadInput(true);
				this.refreshOutputs();
				this.reloadOutput(true);
				System.out.println("Refreshing items");
			} catch (Exception e) {
				e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
			}
		}else{
			System.out.println("Entered a cohort without an id");
		}
	}
	
	protected void buildTabs(){
		buildBeneficiariesPanel();
		addBeneficiariesPanel();
//		configureResultSection(beneficiariesPanel, mBenTable);
		
		buildObjectiviesPanel();
		addObjectiviesPanel();
//		configureResultSection(objectivesPanel, mObjnTable);
		
		buildOutcomePanel();
		addOutcomePanel();
		
		buildActivitiesPanel();
		addActivitiesPanel();
//		configureResultSection(activitiesPanel, mActTable);
		
		buildMNEPanel();
		addMNEPanel();
		
		buildIndicatorPanel();
		addIndicatorPanel();		
		
		buildInputPanel();
		addInputPanel();
		
		buildOutputPanel();
		addOutputPanel();
		
	}
	
	protected void initControllers(){		
		sController = SettingController.getInstance();
		conn = sController.getConnection();
		qController = QuestionController.getInstance(conn);	
		aController = ActivityController.getInstance(conn);
		oController = ObjectiveController.getInstance(conn);
		cController = CohortController.getInstance(conn);
		pController = ProjectController.getInstance(conn);
		bController = BeneficiaryController.getInstance(conn);
		outController= OutcomeController.getInstance(conn);
		iController = IndicatorController.getInstance(conn);
		inController = InputController.getInstance(conn);
		outputController = OutputController.getInstance(conn);
		
		qaController = QuestionActivityController.getInstance(conn);
		qoController = QuestionOutcomeController.getInstance(conn);
		qoutController=QuestionOutputController.getInstance(conn);
	}
	
	/**
	 * 
	 */
	protected void buildRightPanel(){
		panelRight = new JPanel(new BorderLayout());
		panelRight.setBorder( BorderFactory.createEmptyBorder( COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING ) );
		container.add( panelRight, BorderLayout.CENTER );
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		panelRight.add(tabbedPane, BorderLayout.NORTH);			
		if (displayResults)panelRight.add(this.createResultsSection() , BorderLayout.SOUTH);		
				
		buildDetailsPanel();
		addBeneficiaryTab();
		addObjectiveTab();
		addActivitiesTab();
		addOutcomeTab();		
		addMNETab();
		addIndicatorTab();
		addInputTab();
		addOutputTab();
		buildTabs();
	}
	/**
	 * @param widget
	 * @param m
	 */
	protected void configureWidget(SwingMetawidget widget, Model m){
		widget.setConfig( "org/csp/views/metawidget.xml" );
		widget.setToInspect(m);
		widget.setReadOnly(readOnly);
		widget.addWidgetProcessor( new BeansBindingProcessor(new BeansBindingProcessorConfig().setUpdateStrategy( UpdateStrategy.READ_WRITE )) );
	}
	
	protected JComponent configureResultsSection(Container panel, ListTableModel<?> tModel, int height, String pos, final ActionListener mouseClick,final ActionListener rightClick){
		final JTable table = new JTable(tModel);
		Utilities.setTableDefaults(table);
		JScrollPane scroll =  new JScrollPane( table ); 
		//Set Height of table
		scroll.setPreferredSize( new Dimension((int)  scroll.getPreferredSize().getWidth(), height));
		panel.add( scroll, pos);	
		
		table.addMouseListener( new MouseAdapter() {
			@Override
			public void mouseClicked( MouseEvent event ) {
				System.out.println("Clicked");
				//Ensure user double clicks table
				if ( event.getClickCount() != 2 )return; 
				if (SwingUtilities.isLeftMouseButton(event)){
					@SuppressWarnings("rawtypes")
					ListTableModel model = (ListTableModel) table.getModel();
					if (table.getSelectedRow() >= 0){
						Model m = (Model) model.getValueAt( table.getSelectedRow() );	
						System.out.println("ID of the selected is: "+m.getId());				
						if (mouseClick != null)mouseClick.actionPerformed(new ActionEvent(table, m.getId(), "select"));
					}	
				}
			}
			@Override
			public void mousePressed(MouseEvent event){
				System.out.println("Pressed");
				if (SwingUtilities.isRightMouseButton(event)){
					@SuppressWarnings("rawtypes")
					ListTableModel model = (ListTableModel) table.getModel();
					if (table.getSelectedRow() >= 0){
						Model m = (Model) model.getValueAt( table.getSelectedRow() );	
						if (rightClick != null)rightClick.actionPerformed(new ActionEvent(table, m.getId(), "menu"));
					}
				}
			}
		});	
		
		return scroll;
	}
	
	protected JComponent configureResultsSection(Container panel, ListTableModel<?> tModel, final ActionListener mouseClick,final ActionListener rightClick){
		return configureResultsSection(panel, tModel, 200, BorderLayout.PAGE_END,  mouseClick,rightClick);
	}
	/**
	 * 
	 * @param panel
	 * @param tModel
	 * @param height
	 * @param pos
	 */
	protected void configureResultSection(Container panel, ListTableModel<?> tModel, int height, String pos){		
		this.configureResultsSection(panel, tModel, height, pos, null, null);
	}
	/**
	 * 
	 * @param panel
	 * @param tModel
	 */
	protected void configureResultSection(Container panel, ListTableModel<?> tModel){
		configureResultSection(panel,tModel, 200,BorderLayout.PAGE_END );
	}
	
	protected void buildDetailsPanel() {
		detailsPanel = new JPanel(new BorderLayout());
		tabbedPane.addTab("1.Details", detailsPanel);
		this.startInspection();
		this.cohort = this.model;		
		
		detailsPanel.add(this.mMetawidget, BorderLayout.CENTER);	
		extraUISetup();
	}
	protected boolean saveCohort() throws Exception{
		Cohort tCohort = this.mMetawidget.getToInspect();	
		if (tCohort.getId() == 0 || tCohort.getProject().getId() == 0){
			isCohortSaved = false;
			Project tProject = tCohort.getProject();
			
			//Save Project First
			if (pController == null)pController = ProjectController.getInstance();
			tProject = pController.save(tProject);
						
			if (tProject.getId() != 0){ //Project Successfully saved
				System.out.println("Saved Project with Id: " + tProject.getId());
				tCohort.setProject(tProject);			
				
				JDateChooser sDate = (JDateChooser)this.mMetawidget.getComponent("startDate");
				tCohort.setStartDate(sDate.getDate());
				
				JDateChooser eDate = (JDateChooser)this.mMetawidget.getComponent("endDate");
				tCohort.setEndDate(eDate.getDate());
				
				//Validate Dates
								
				if (eDate.getDate() != null){
					if (sDate.getDate() != null){
						if (sDate.getDate().after(eDate.getDate())){
							JOptionPane.showMessageDialog(container, "Start of project cannot be after project ends.");
							isCohortSaved = false;
							return false;
						}
					}else{
						Date today = new Date();
						sDate.setDate(today);
						if (today.after(eDate.getDate())){
							JOptionPane.showMessageDialog(container, "Project end date cannot be in the past");
							isCohortSaved = false;
							return false;
						}
					}
				}else{
					JOptionPane.showMessageDialog(container, "The project should have a designated end date");
					isCohortSaved = false;
					return false;
				}
				
				//Save the Cohort Object
				tCohort = cController.save(tCohort);
				if (tCohort.getId() != 0) //Cohort Successfully saved
				{					
					isCohortSaved = true;
					this.cohort = this.model =tCohort;
					// System.out.printf("Cohort Created with id: %d and project id %d \n",this.cohort.getId(), this.cohort.getProject().getId() );;
					load(this.model);
					if (this.refreshable != null)this.refreshable.refresh();
					if (this.actionListener != null)this.actionListener.actionPerformed(null);
				}
			}
		}else {
			isCohortSaved = true;
		}
		
		return isCohortSaved;
	}
	
	/*
	 * Input
	 */
	protected void buildInputPanel() {
		if (input == null)input = new Input();
		if (inputs == null)inputs = new ArrayList<Input>();
		if (this.inputWidget == null){
			inputWidget = new SwingMetawidget();
			configureWidget(inputWidget, input);
		}
		mInputTable = new ListTableModel<Input>(Input.class, inputs,"Input","Type","Cost");
		JPanel panel = new JPanel(new BorderLayout());		
		
		configureResultsSection(panel, mInputTable , new ActionListener(){
			public void actionPerformed(ActionEvent e) {//Handle Model Select
				System.out.println("Inside the Input Table");
				try{
					System.out.println(e.getID());
					loadInput(inController.read(e.getID()));
				}catch(Exception ex){ ex.printStackTrace(); }
			}
		}, new ActionListener(){
			//Handle Right Click
			@Override
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		
		inPanel.add(panel, BorderLayout.SOUTH);
		JPanel btnPanel = new JPanel();
		inPanel.add(btnPanel, BorderLayout.CENTER);
		
		JButton clearBtn = new JButton("Clear");
		btnPanel.add(clearBtn);
		clearBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				reloadInput(true);
			}
		});
		
		if (!this.readOnly){
			
			JButton addInput = new JButton("Add");
			addInput.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Input i = inputWidget.getToInspect();
					if (i.getInput() != null && !i.getInput().equals("")){
						try{
							if (model.getId() == 0){
								if (model.getProject().getName() == null || model.getProject().getName().equals("")){
									JOptionPane.showMessageDialog(container, "Unable to add Outcome. First add the details of the project in the details tab before attempting to add activities to the project");
									return;
								}
								saveCohort();
							}
						}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
						try{
							if (i.getType() == null)
								JOptionPane.showMessageDialog(container, "Must relate the input to a particular type");
							
							else{
								boolean isNew = (i.getId() == 0);
								i = saveInput(i);
								if (i.getId() > 0){
									if (!isNew)removeInput(i);
									
									inputs.add(i);
									
									refreshInputs();
									reloadInput(true);
								}
								else{
									System.out.println("No id was created");
								}
							}
						}catch(Exception e){
							JOptionPane.showMessageDialog(container, "Unable to add Input. Ensure input for the specified activity has not been previously added");
							e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
						}
							
					}else
						JOptionPane.showMessageDialog(container, "First specify the input");
				}
			});
			
			JButton deleteInput = new JButton("Delete");
			deleteInput.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					deleteInput((Input)inputWidget.getToInspect());
				}
			});
			btnPanel.add(addInput);
			btnPanel.add(deleteInput);
			
		}	
		
	}
	protected void removeInput(Input i) {
		Iterator <Input> ib = this.inputs.iterator();
		while (ib.hasNext()){
			Input in = ib.next();
			if (i.getId() == in.getId())//inputs.remove(in);
				ib.remove();
		}		
	}
	protected void addInputPanel() {
		this.inPanel.add(this.inputWidget, BorderLayout.NORTH);
	}
	protected void addInputTab(){
		inPanel = new JPanel(new BorderLayout());
		tabbedPane.addTab("8.Inputs", inPanel);
	}
	protected void refreshInputs(){
		this.mInputTable.importCollection(inputs);
	}
	protected Input saveInput(Input i) throws Exception{
		if (!this.isCohortSaved)this.saveCohort();
		if (i.getId() != 0)return this.inController.update(i);
		i.setCohort(this.model);
		i = this.inController.save(i);
		return i;
	}
	protected void reloadInput(boolean useNew){
		if (useNew)input = new Input();
		if (this.activities.size() > 0){
			this.input.setActivities(this.activities.toArray(new Activity[this.activities.size()]));
		}
		this.inputWidget.setToInspect(input);
	}
	protected void deleteInput(Input i){
		try{
			if (i.getId() > 0){
				if (JOptionPane.showConfirmDialog(null, "Delete this Input?", "Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
					if (this.inController.delete(i)){
						this.removeInput(i);
						refreshInputs();
						JOptionPane.showMessageDialog(container, "Input Deleted");
						reloadInput(true);
					}
				}
			}else if (i.getInput() != null && !i.getInput().equals("")){
				reloadInput(true);
			}else
				JOptionPane.showMessageDialog(container, "No Input Selected to be deleted");
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
	}
	/**
	 * @param i
	 */
	protected void loadInput(Input i){
		System.out.println(i);
		Activity [] activities = new Activity[this.activities.size()];
		i.setActivities(this.activities.toArray(activities));
		this.inputWidget.setToInspect(i);
		if (!this.readOnly){
			if (i.getActivity()!= null){
				@SuppressWarnings("unchecked")
				JComboBox<Activity> acts = (JComboBox<Activity>)this.inputWidget.getComponent("activity");
				for(int index = 0; index < acts.getItemCount(); index++)
					if (acts.getItemAt(index) != null && acts.getItemAt(index).equals(i.getActivity()))
						acts.setSelectedIndex(index);
			}
		}
	}
	
	/*
	 * Output
	 */
	protected void buildOutputPanel(){
		if (output == null)output = new Output();
		if (outputs == null)outputs = new ArrayList<Output>();
		if (this.outputWidget == null){
			outputWidget = new SwingMetawidget();
			configureWidget(outputWidget, output);
		}
		JPanel panel = new JPanel(new BorderLayout());	
		mOutputTable = new ListTableModel<Output>(Output.class, outputs, "Output","Type");
		
		configureResultsSection(panel, mOutputTable , new ActionListener(){//Handle Model Select
			public void actionPerformed(ActionEvent e) {
				System.out.println("Inside the Question Table");
				try{
					loadOutput(outputController.read(e.getID()));
				}catch(Exception ex){ ex.printStackTrace(); }
			}
		}, new ActionListener(){
			//Handle Right Click
			@Override
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		outPanel.add(panel, BorderLayout.SOUTH);	
		JPanel btnPanel = new JPanel();
		outPanel.add(btnPanel, BorderLayout.CENTER);
		
		JButton clearBtn = new JButton("Clear");
		btnPanel.add(clearBtn);
		clearBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				reloadOutput(true);
			}
		});
		
		if (!this.readOnly){
			
			JButton addOutput = new JButton("Add");
			btnPanel.add(addOutput);
			addOutput.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Output out = outputWidget.getToInspect();
					if (out.getOutput() != null && !out.getOutput().equals("")){
						//Validate the cohort
						try{ if (model.getId() == 0)saveCohort(); }
						catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
						try{
							if (out.getType() == null)
								JOptionPane.showMessageDialog(container, "Must relate the output to a particular type");
							else if (out.getActivity() == null)
								JOptionPane.showMessageDialog(container, "Select the appropriate activity for this output");
							
							else{
								boolean isNew = (out.getId() == 0);
								out = saveOutput(out);
								if (out.getId() != 0){
									outputWidget.setToInspect(new Output());
									if (!isNew)removeOutput(out);
									outputs.add(out);
									refreshOutputs();
									reloadOutput(true);
								}else JOptionPane.showMessageDialog(container, "Unable to save Output. Check to ensure output not already added");
							}
						}catch(Exception e){
							JOptionPane.showMessageDialog(container, "Unable to add Output");
						}
					}else
						JOptionPane.showMessageDialog(container, "Must specify the output to be saved");
				}
			});	

			JButton deleteBtn = new JButton("Delete");
			btnPanel.add(deleteBtn);
			deleteBtn.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent arg0) {
					deleteOutput((Output)outputWidget.getToInspect());
				}
			});
		}
	}
	protected void removeOutput(Output out) {
		Iterator <Output> ib = this.outputs.iterator();
		while (ib.hasNext()){
			Output in = ib.next();
			if (out.getId() == in.getId())//outcomes.remove(in);
				ib.remove();
		}		
	}
	protected void addOutputPanel(){
		this.outPanel.add(this.outputWidget, BorderLayout.NORTH);
	}
	protected void addOutputTab(){
		outPanel = new JPanel(new BorderLayout());
		tabbedPane.addTab("9.Outputs", outPanel);
	}
	protected void refreshOutputs(){
		this.mOutputTable.importCollection(outputs);
	}
	protected Output saveOutput(Output o) throws Exception{
		if (!this.isCohortSaved)this.saveCohort();
		if (o.getId() != 0)return this.outputController.update(o);
		return this.outputController.save(o);
	}
	protected void reloadOutput(boolean useNew){
		if (useNew)output = new Output();
		if (this.activities.size() > 0){
			Activity [] acts = new Activity[this.activities.size()];
			this.activities.toArray(acts);
			this.output.setActivities(acts);
		}
		this.outputWidget.setToInspect(output);
		
	}
	protected void deleteOutput(Output out){
		try{
			if (out.getId() > 0){
				if (JOptionPane.showConfirmDialog(null, "Delete this Output?", "Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
					if (this.outputController.delete(out)){
						this.removeOutput(out);
						refreshOutputs();
						JOptionPane.showMessageDialog(container, "Output Deleted");
						reloadOutput(true);
					}
				}
			}else if (out.getOutput() != null && !out.getOutput().equals(""))
				reloadOutput(true);
			else
				JOptionPane.showMessageDialog(container, "No Output selected");
				
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
	}
	
	protected void loadOutput(Output o){
		Activity [] activities = new Activity[this.activities.size()];
		o.setActivities(this.activities.toArray(activities));
		this.outputWidget.setToInspect(o);
		if (!this.readOnly){
			if (o.getActivity() != null){
				@SuppressWarnings("unchecked")
				JComboBox<Activity> acts = (JComboBox<Activity>)this.outputWidget.getComponent("activity");
				for(int index = 0; index < acts.getItemCount(); index++)
					if (acts.getItemAt(index) != null && acts.getItemAt(index).equals(o.getActivity()))
						acts.setSelectedIndex(index);
			}
		}
	}
	
	protected void enhanceIndicator(){
		if (!this.readOnly){
			//Add event to convert Other to a text Input
			@SuppressWarnings("unchecked")
			final JComboBox <String> frequencyBox = (JComboBox<String>)this.indicatorWidget.getComponent("frequency");
			frequencyBox.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					if (frequencyBox.getSelectedItem().equals("OTHER")){
						frequencyBox.setEditable(true);
						frequencyBox.setSelectedIndex(0);
					}				
				}
			});
		}
	}
	
	/*
	 * Indicators
	 */
	protected void buildIndicatorPanel() {
		//TODO when a custom frequency not shown on reselect
		if (this.indicator == null)indicator = new Indicator(new Question());
		if (this.indicators == null)indicators = new ArrayList<Indicator>();
		if (this.mIndTable == null)mIndTable = new ListTableModel<Indicator>(Indicator.class, indicators, "Indicator","Question","Type");
		configureResultsSection(indicatorPanel, mIndTable , new ActionListener(){
			//Handle Model Select
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Inside the Indicator Table");
				int id = e.getID();
				try{					
					loadIndicator(iController.read(id));
				}catch(Exception ex){ ex.printStackTrace(); }
			}
		}, new ActionListener(){
			public void actionPerformed(ActionEvent e) {//Handle Right Click
			}
		});
		
		if (indicatorWidget == null)indicatorWidget = new SwingMetawidget();
		configureWidget(indicatorWidget, indicator);
		
		enhanceIndicator();
		
		JPanel btnPanel = new JPanel();
		btnPanel.setPreferredSize(new Dimension(100,100));
		indicatorPanel.add(btnPanel, BorderLayout.CENTER);
		
		JButton clearBtn = new JButton("Clear");
		btnPanel.add(clearBtn);
		clearBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				reloadIndicators(true);
			}
		});
		
		if (!this.readOnly){
			JButton addBtn = new JButton("Add");
			btnPanel.add(addBtn);
			addBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Indicator indicator = indicatorWidget.getToInspect();
					if (indicator.getIndicator() != null && !indicator.getIndicator().equals("")){
						//Validate the Cohort
						try{
							if (model.getId() == 0){
								if (model.getProject().getName() == null || model.getProject().getName().equals("")){
									JOptionPane.showMessageDialog(container, "Unable to add Outcome. First add the details of the project in the details tab before attempting to add activities to the project");
									return;
								}
								saveCohort();
							}
						}catch(Exception ex){ex.printStackTrace();}
						try{
							if (indicator.getQuestion() == null)
								JOptionPane.showMessageDialog(container, "First select a question before saving the indicator");
							else if (indicator.getType() == null)
								JOptionPane.showMessageDialog(container, "Select the type of Indicators");
							else if (indicator.getFrequency() == null)
								JOptionPane.showMessageDialog(container, "Select the frequency for measuring the indicator");
							else{
								//TODO perform validation on the target if type is quantitative
								
								boolean isNew = (indicator.getId() == 0);
								indicator = saveIndicator(indicator);
								
								if (indicator.getId() != 0){
									if (!isNew)removeIndicators(indicator);										
									indicators.add(indicator);									
									refreshIndicators();
									reloadIndicators(true);
								}else JOptionPane.showMessageDialog(container, "Unable to save Indicator");
							}
						}catch(Exception ex){
							JOptionPane.showMessageDialog(container, "Unable to add Indicator");
							ex.printStackTrace();
						}
						
					}
				}
			});
			
			JButton deleteBtn = new JButton("Delete");
			btnPanel.add(deleteBtn);
			deleteBtn.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent arg0) {
					deleteIndicator((Indicator)indicatorWidget.getToInspect());
				}
			});
			
			JButton addResult = new JButton("Add Result");
			btnPanel.add(addResult);
			addResult.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent arg0) {					
					Indicator i = indicatorWidget.getToInspect();
					if (i.getIndicator() != null && !i.getIndicator().equals(""))
						addFieldToIndicator(i);
					else
						JOptionPane.showMessageDialog(container, "First Select or Create an Indicator before adding a result");
				}
			});
		}
	}
	protected void removeIndicators(Indicator indicator) {
		Iterator <Indicator> ib = this.indicators.iterator();
		while (ib.hasNext()){
			Indicator in = ib.next();
			if (indicator.getId() == in.getId())//indicators.remove(in);
				ib.remove();
		}
	}
	protected void addIndicatorPanel() {		
		this.indicatorPanel.add(indicatorWidget, BorderLayout.NORTH);
	}
	protected void addIndicatorTab(){
		indicatorPanel = new JPanel(new BorderLayout());
		tabbedPane.addTab("7.Indicators", indicatorPanel);
	}

	protected void refreshIndicators(){
		this.mIndTable.importCollection(indicators);
	}	
	protected Indicator saveIndicator(Indicator i) throws Exception{
		if (!this.isCohortSaved)this.saveCohort();
		if (i.getQuestion() == null || i.getQuestion().getId() == 0 )throw new Exception("Indicator must be associated with a question");
		if (i.getId() != 0)return this.iController.update(i);
		iController.save(i);
		if (i.getId() > 0)System.out.println("Saved Indicator: "+ i);
		else System.out.println("Unable to save indicator");
		return i;
	}
	protected void reloadIndicators(boolean useNew){
		Question [] quest = new Question[this.questions.size()];
		if (useNew)this.indicator = new Indicator(new Question());	
		this.indicator.setQuestions(this.questions.toArray(quest));
		quest = this.questions.toArray(quest);
		
		for(int i = 0; i < quest.length;i++)System.out.println(quest[i]);
		
		this.configureWidget(this.indicatorWidget, this.indicator);
		enhanceIndicator();
	}
	protected void deleteIndicator(Indicator i){
		try{
			if (i.getId() > 0){
				if (JOptionPane.showConfirmDialog(null, "Delete this indicator?", "Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
					if (iController.delete(i)){
						this.removeIndicators(i);
						refreshIndicators();
						JOptionPane.showMessageDialog(container, "Indicator Deleted");
						reloadIndicators(true);
					}
				}
				return;
			}else if (i.getIndicator() != null || !i.getIndicator().equals("")){
				reloadIndicators(true);
			}else{
				JOptionPane.showMessageDialog(container, "No Indicator Selected to be deleted");
			}
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
	}
	protected void loadIndicator(Indicator i){
		Question [] q = new Question[questions.size()];
		i.setQuestions(	questions.toArray(q) );
		indicatorWidget.setToInspect(i);
		if (!this.readOnly){
			if (i.getQuestion() != null){
				@SuppressWarnings("unchecked")
				JComboBox<Question> quests = (JComboBox<Question>)indicatorWidget.getComponent("question");
				for(int index =0; index < quests.getItemCount(); index++){
					if (quests.getItemAt(index) != null && quests.getItemAt(index).equals(i.getQuestion()))
						quests.setSelectedIndex(index);
				}
			}
		}
	}
	protected void addFieldToIndicator(Indicator i){
		try{ 
			if (i.getId() == 0 && !i.getIndicator().equals(""))saveIndicator(i);
			if (subFrame != null)subFrame.dispose();
			subFrame = new JFrame();
			FieldManageView fmv = new FieldManageView(subFrame, false, false, null, true, true);
			fmv.setIndicator(i);
			subFrame.pack();
			subFrame.setVisible(true);
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
	}
	
	/*
	 * M&E Questions 
	 */
	
	protected void enhanceQuestion(){
		if (!this.readOnly){
			additionalQbox = new JComboBox<Object>();
			additionalQbox.setName("additionalOption");
			JLabel label = new JLabel("Choose relation First before additional options");
			label.setName("addtionalLabel");
			questionWidget.add(label);
			questionWidget.add(additionalQbox);
		
			try{
				setQuestionActionListener(null);
			}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
		}
	}
	
	protected void setQuestionActionListener(String selected){
		@SuppressWarnings("unchecked")
		final JComboBox<String> relationCmb = (JComboBox<String>)questionWidget.getComponent("relation");
		if (selected != null)relationCmb.setSelectedItem(selected);
		
		if (relationCmb != null){
			relationCmb.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent ae) {
					System.out.println(relationCmb.getSelectedItem());
					if (relationCmb.getSelectedItem() != null){
						String selected = (String)relationCmb.getSelectedItem();
						if (selected.equalsIgnoreCase(Question.OUTCOME)){
							questionWidget.remove(additionalQbox);
//							((JLabel)questionWidget.getComponent("addtionalLabel")).setText("Select Outcome:");
							
							Outcome [] outs = new Outcome[outcomes.size()];
							additionalQbox = new JComboBox<Outcome>(outcomes.toArray(outs));
							
							questionWidget.add(additionalQbox);
							setQuestionActionListener(selected);
							relationCmb.setSelectedItem(selected);
						}else if (selected.equalsIgnoreCase(Question.OUTPUT)){
							questionWidget.remove(additionalQbox);
//							((JLabel)questionWidget.getComponent("addtionalLabel")).setText("Select Output:");
							
							Output [] outs = new Output[outputs.size()];
							additionalQbox = new JComboBox<Output>(outputs.toArray(outs));
							
							questionWidget.add(additionalQbox);
							setQuestionActionListener(selected);
							relationCmb.setSelectedItem(selected);
						}else if (selected.equalsIgnoreCase(Question.ACTIVITY)){
							questionWidget.remove(additionalQbox);
//							((JLabel)questionWidget.getComponent("addtionalLabel")).setText("Select Activity:");
							
							Activity [] acts = new Activity[activities.size()];
							additionalQbox = new JComboBox<Activity>(activities.toArray(acts));
							
							questionWidget.add(additionalQbox);
							setQuestionActionListener(selected);
							relationCmb.setSelectedItem(selected);
						}else{
							JOptionPane.showMessageDialog(container, "Not a valid Option");
						}
					}
					
				}
			});
		}
	}
	protected void saveRelation(String relation, Question q){
		try{
			if (relation.equalsIgnoreCase(Question.ACTIVITY)){
				Activity activity = (Activity)additionalQbox.getSelectedItem();
				System.out.println(activity);
				QuestionActivity qa = new QuestionActivity(q,activity);
				this.qaController.save(qa);
			}else if (relation.equalsIgnoreCase(Question.OUTCOME)){
				Outcome outcome = (Outcome)additionalQbox.getSelectedItem();
				System.out.println(outcome);
				QuestionOutcome qo = new QuestionOutcome(q, outcome);
				this.qoController.save(qo);
			}else if (relation.equalsIgnoreCase(Question.OUTPUT)){
				Output output = (Output)additionalQbox.getSelectedItem();
				System.out.println(output);
				QuestionOutput qoout = new QuestionOutput(q,output);
				this.qoutController.save(qoout);
			}
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
	}
	
	protected void loadQuestion(Question q){
		questionWidget.setToInspect(q);
	}
	protected void addMNETab(){
		mnePanel = new JPanel(new BorderLayout());
		tabbedPane.addTab("6.M&E Questions", mnePanel);
	}
	protected void buildMNEPanel() {		
		if (question == null)question = new Question();
		if (questionWidget == null)questionWidget = new SwingMetawidget();
		configureWidget(questionWidget, question);
		
		enhanceQuestion();
		
		if (questions == null)questions = new ArrayList<Question>();
		if (mQuestTable == null)mQuestTable = new ListTableModel<Question>(Question.class, questions, "Question", "Type","Relation");
		configureResultsSection(this.mnePanel, mQuestTable , new ActionListener(){
			//Handle Model Select
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Inside the Question Table");
				int id = e.getID();
				try{
					loadQuestion(qController.read(id));
					setQuestionActionListener(null);
				}catch(Exception ex){ ex.printStackTrace(); }
			}
		}, new ActionListener(){
			//Handle Right Click
			@Override
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		JPanel btnPanel = new JPanel();
		mnePanel.add(btnPanel, BorderLayout.CENTER);
		
		JButton clearBtn = new JButton("Clear");
		btnPanel.add(clearBtn);
		clearBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				question = new Question();
				questionWidget.setToInspect(question);
			}
		});
		
		if (!this.readOnly){
			//Add Buttons
			JButton addBtn = new JButton("Add");
			btnPanel.add(addBtn);
			addBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Question q = questionWidget.getToInspect();
					try{
						if (model.getId() == 0){
							if (model.getProject().getName() == null || model.getProject().getName().equals("")){
								JOptionPane.showMessageDialog(container, "Unable to add Question. First add the details of the project in the details tab before attempting to add activities to the project");
								return;
							}
							saveCohort();
						}
						if (q.getQuestion() != null && !q.getQuestion().equals("")){
							if (q.getType() == null){
								JOptionPane.showMessageDialog(container, "Select the type of question");
								return;
							}
							boolean isNew = (q.getId() == 0);
							q = saveQuestion(q);
							
							if (q.getId() > 0){		
								if (isNew){
									if (q.getRelation()!= null){
										String relation = q.getRelation();
										saveRelation(relation, q);
									}
								}
								else if (!isNew)removeQuestion(q);
								
								questions.add(q);
								question = new Question();
								question.setType(q.getType());
								questionWidget.setToInspect(question);
								setQuestionActionListener(null);
								refreshMNE();
								reloadIndicators(false);
							}else
								JOptionPane.showMessageDialog(container, "Unable to add question. Ensure that Question not already created");
						}else
							JOptionPane.showMessageDialog(container, "Must Specify a question");
					}catch(Exception e){
						e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
						JOptionPane.showMessageDialog(container, "Unable to add Question.  Ensure that Question not already created");
					}
				}
			});
			
			JButton deleteBtn = new JButton("Delete");
			btnPanel.add(deleteBtn);
			deleteBtn.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent arg0) {
					deleteQuestion((Question)questionWidget.getToInspect());
					question = new Question();
					questionWidget.setToInspect(question);
					setQuestionActionListener(null);
				}
			});
		}
	}
	protected void removeQuestion(Question q) {
		Iterator <Question> ib = this.questions.iterator();
		while (ib.hasNext()){
			Question in = ib.next();
			if (q.getId() == in.getId())//outcomes.remove(in);
				ib.remove();
		}	
	}
	protected void addMNEPanel() {
		this.mnePanel.add(questionWidget, BorderLayout.NORTH);
	}
	protected void refreshMNE() {
		this.mQuestTable.importCollection(questions);		
	}
	protected void saveQuestions() throws Exception{
		if (!this.isCohortSaved)this.saveCohort();		
		Iterator<Question> qs = this.questions.iterator();
		while(qs.hasNext())saveQuestion(qs.next());
	}
	protected Question saveQuestion(Question q) throws Exception{
		if (!this.isCohortSaved)this.saveCohort();
		q.setCohort(this.model);
		if (q.getId() > 0)
			this.qController.update(q);
		else
			this.qController.save(q);
		if (q.getId() > 0)System.out.println("Saved Question "+q);
		else System.out.println("Unable to save Question: "+q);
		return q;
	}
	protected void deleteQuestion(Question q){
		try{
			if (q.getId() > 0){
				if (JOptionPane.showConfirmDialog(null, "Delete this Question?", "Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
					if (this.qController.delete(q)){
						this.removeQuestion(q);
						refreshMNE();
						this.reloadIndicators(false);								
					}
				}
			}else if (q.getQuestion() != null && !q.getQuestion().equals("")){
				question = new Question();
				this.questionWidget.setToInspect(question);
			}else
				JOptionPane.showMessageDialog(container, "No question selected");
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
	}
	
	/*
	 * Activities
	 */
	protected void addActivitiesTab(){
		activitiesPanel = new JPanel(new BorderLayout());
		tabbedPane.addTab("4.Activities", activitiesPanel);
	}
	protected void buildActivitiesPanel() {				
		if (activity == null)activity = new Activity();
		if (activityWidget == null)activityWidget = new SwingMetawidget();
		configureWidget(activityWidget, activity);
		if (activities == null)activities = new ArrayList<Activity>();
		if (mActTable == null)mActTable = new ListTableModel<Activity>(Activity.class, activities, "Activity");		
		
		configureResultsSection(activitiesPanel, mActTable , new ActionListener(){
			public void actionPerformed(ActionEvent e) {//Handle Model Select
				try{
					loadActivity(aController.read(e.getID()));
				}catch(Exception ex){ ex.printStackTrace(); }
			}
		}, new ActionListener(){
			public void actionPerformed(ActionEvent e) {//Handle Right Click
				
			}
		});
		
		JPanel btnPanel = new JPanel();
		activitiesPanel.add(btnPanel, BorderLayout.CENTER);
		
		JButton clearBtn = new JButton("Clear");
		btnPanel.add(clearBtn);
		clearBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				reloadActivity(true);
			}
		});
		
		if (!this.readOnly){
			//Add Buttons
			JButton addBtn = new JButton("Add");
			btnPanel.add(addBtn);
			addBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Activity a =  activityWidget.getToInspect();	
					if (a.getActivity() != null && !a.getActivity().equals("")) {
						//Attempt to save project first before saving activity
						try{
							if (model.getId() == 0)
								if (model.getProject().getName() == null || model.getProject().getName().equals("")){
									JOptionPane.showMessageDialog(container, "Unable to add Activity. First add the details of the project in the details tab before attempting to add activities to the project");
									return;
								}
								saveCohort();
						}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}	
						try {
//							JComboBox<Objective> obj = activityWidget.getComponent("objective");
//							int select = obj.getSelectedIndex();
//							if (select > 0 )
//								a.setObjective((Objective)objectives.toArray()[select - 1]);
//							else 
//								if (JOptionPane.showConfirmDialog(null, "No objective selected for this activity. Do you want to continue?", "Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION)return;
//							
							boolean isNew = (a.getId() == 0);
							saveActivity(a);
							
							if (a.getId() != 0){
								if (!isNew)removeActivity(a);
								
								activities.add(a);
								refreshActivities();
								reloadActivity(true);
							}else
								JOptionPane.showMessageDialog(container, "Unable to save the activity. Check to ensure the activity is not already created");
						} catch (Exception e) { e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
					}else
						JOptionPane.showMessageDialog(container, "Must specify the activity to be added");
				}
			});
	
			JButton deleteBtn = new JButton("Delete");
			btnPanel.add(deleteBtn);
			deleteBtn.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent arg0) {
					Activity act = activityWidget.getToInspect();
					deleteActivity(act);
				}
			});
		}
		
		JButton objecBtn = new JButton("Objectives");
		btnPanel.add(objecBtn);
		objecBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				Activity act = activityWidget.getToInspect();
				if (act != null && act.getActivity()!= null && !act.getActivity().equals(""))
					loadActivityObjecties(act);
				else
					JOptionPane.showMessageDialog(container, "First Select or Create an Activity before adding objectives");
			}
		});
	}
	protected void removeActivity(Activity a) {
		Iterator <Activity> ib = this.activities.iterator();
		while (ib.hasNext()){
			Activity ben = ib.next();
			if (a.getId() == ben.getId())//activities.remove(ben);
				ib.remove();
		}
	}
	protected void addActivitiesPanel() {
		this.activitiesPanel.add(activityWidget, BorderLayout.NORTH);		
	}
	protected void refreshActivities() {
		this.reloadInput(false);
		this.reloadOutput(false);
		this.mActTable.importCollection(activities);		
	}
	protected void saveActivities() throws Exception{
		if (!this.isCohortSaved)this.saveCohort();
		if (this.model.getId() > 0){
			Iterator<Activity> ia = this.activities.iterator();
			while(ia.hasNext())saveActivity(ia.next());
		}
	}
	protected Activity saveActivity(Activity a)throws Exception{
		if (!this.isCohortSaved)this.saveCohort();
		if (this.model.getId() > 0){
			if (a.getId() != 0)return this.aController.update(a);
			a.setCohort(this.model);			
			this.aController.save(a);
			if (a.getId() > 0)System.out.println("Saved: "+ a);
			else System.out.println("Unable to save: "+a);			
		}
		return a;
	}
	protected void reloadActivity(boolean useNew){
//		Objective [] objs = new Objective[objectives.size()];
		if (useNew)activity = new Activity();
//		activity.setObjectives(objectives.toArray(objs));						
		activityWidget.setToInspect(activity);
	}
	protected void deleteActivity(Activity act){
		try{
			if (act.getId() > 0){
				if (JOptionPane.showConfirmDialog(null, "Delete this Activity?", "Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
					if (aController.delete(act)){
						this.removeActivity(act);
						refreshActivities();
						JOptionPane.showMessageDialog(container, "Activities Deleted");
						reloadActivity(true);
						reloadInput(false);
						reloadOutput(false);
					}
				}
			}else if (act.getActivity() != null && !act.getActivity().equals(""))
				reloadActivity(true);
			else
				JOptionPane.showMessageDialog(container, "No activity selected");
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
	}
	protected void loadActivity(Activity act){
		Objective [] objArr = new Objective[objectives.size()];
		act.setObjectives(objectives.toArray(objArr));
		activityWidget.setToInspect(act);
		if (!this.readOnly){
			if (act.getObjective() != null){				
				@SuppressWarnings("unchecked")
				JComboBox<Objective> objs = (JComboBox<Objective>) activityWidget.getComponent("objective");
				for(int i =0; i < objs.getItemCount(); i++)
					if(objs.getItemAt(i) != null)
						if (objs.getItemAt(i).equals(act.getObjective()))
							objs.setSelectedIndex(i);
				}
		}	
	}
	
	protected void loadActivityObjecties(Activity act){
		try{
			if (act.getId() == 0)this.saveActivity(act);
			if (act.getId() > 0){
				if (subFrame != null)subFrame.dispose();
				subFrame = new JFrame();
				ActivityObjective ao = new ActivityObjective();
				ao.setActivity(act);
				Objective [] obs = new Objective[this.objectives.size()];
				ao.setObjectives(this.objectives.toArray(obs));
				
				new ActivityObjectiveView(subFrame, false, this.readOnly, ao, true, true);
				
				subFrame.pack();
				subFrame.setVisible(true);
			}
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
	}
	
	/*
	 * Outcomes
	 */
	protected void addOutcomeTab(){
		outcomePanel = new JPanel(new BorderLayout());
		tabbedPane.addTab("5.Outcomes", outcomePanel);
	}
	protected void buildOutcomePanel() {		
		if (outcomeWidget == null)outcomeWidget = new SwingMetawidget();
		if (outcome == null)this.outcome = new Outcome();
		configureWidget(outcomeWidget, outcome);
		if (outcomes == null)outcomes = new ArrayList<Outcome>();
		if (mOutTable == null)mOutTable = new ListTableModel<Outcome>(Outcome.class, outcomes,"Outcome", "Timeframe", "type");
		configureResultsSection(outcomePanel, mOutTable , new ActionListener(){
			public void actionPerformed(ActionEvent e) {//Handle Model Select
				System.out.println("Inside the Outcome Table");
				int id = e.getID();
				try{
					System.out.println(id);
					Outcome q = outController.read(id);
					if (q != null)outcomeWidget.setToInspect(q);
				}catch(Exception ex){ ex.printStackTrace(); }
			}
		}, new ActionListener(){
			public void actionPerformed(ActionEvent e) {//Handle Right Click
			}
		});
		JPanel btnPanel = new JPanel();
		outcomePanel.add(btnPanel, BorderLayout.CENTER);
		
		JButton clearBtn = new JButton("Clear");
		btnPanel.add(clearBtn);
		clearBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				outcome = new Outcome();
				outcomeWidget.setToInspect(outcome);
			}
		});
		
		if (!this.readOnly){
			JButton addBtn = new JButton("Add");
			btnPanel.add(addBtn);
			addBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Outcome out = outcomeWidget.getToInspect();
					if (out.getOutcome() == null || out.getOutcome().equals("")){
						JOptionPane.showMessageDialog(container, "Must specify the outcome");
						return;
					}
					try{
						if (model.getId() == 0){
							if (model.getProject().getName() == null || model.getProject().getName().equals("")){
								JOptionPane.showMessageDialog(container, "Unable to add Outcome. First add the details of the project in the details tab before attempting to add activities to the project");
								return;
							}
							saveCohort();
						}
					}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
					try {
						
						if (out.getTimeframe() == null)
							JOptionPane.showMessageDialog(container, "Must select a timeframe for the outcome");
						else if(out.getType() == null)
							JOptionPane.showMessageDialog(container, "Must select the type of outcome");
						else{
							boolean isNew = (out.getId() == 0);
							out = saveOutcome(out);
							if (out.getId() > 0){
								if (!isNew)removeOutcome(out);
								outcomes.add(out);
								outcome = new Outcome();
								outcomeWidget.setToInspect(outcome);
								refreshOutcomes();
							}else
								JOptionPane.showMessageDialog(container, "Unable to save Outcome. Ensure outcome is not already created for this project.");
						}
					} catch (Exception e) {
						JOptionPane.showMessageDialog(container, "Unable to add Outcome. Check if the outcome has been previously added. Note that all outcomes but be unique.");
						e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
					}				
				}
			});
			
			JButton deleteBtn = new JButton("Delete");
			btnPanel.add(deleteBtn);
			deleteBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					deleteOutcome((Outcome)outcomeWidget.getToInspect());
				}
			});
		}
	}
	protected void removeOutcome(Outcome out) {
		Iterator <Outcome> ib = this.outcomes.iterator();
		while (ib.hasNext()){
			Outcome in = ib.next();
			if (out.getId() == in.getId())//outcomes.remove(in);
				ib.remove();
		}	
	}
	protected void addOutcomePanel() {
		outcomePanel.add(outcomeWidget, BorderLayout.NORTH);
	}
	protected void refreshOutcomes() {
		this.mOutTable.importCollection(this.outcomes);	
	}	
	protected void reloadOutcome(boolean useNew){
		if (useNew)outcome = new Outcome();
		outcomeWidget.setToInspect(outcome);
	}
	protected Outcome saveOutcome(Outcome o) throws Exception {
		if (this.model.getId() == 0)this.saveCohort();
		if (o.getId() != 0)return this.outController.update(o);
		o.setCohort(this.model);
		return this.outController.save(o);		
	}
	protected void saveOutcomes() throws Exception{
		if (!this.isCohortSaved)this.saveCohort();		
		Iterator<Outcome> os = this.outcomes.iterator();
		while(os.hasNext()) saveOutcome(os.next());		
	}
	protected void deleteOutcome(Outcome out){
		try{
			if (out.getId() > 0){
				if (JOptionPane.showConfirmDialog(null, "Delete this Outcome?", "Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
					if (this.outController.delete(out)){
						this.removeOutcome(out);
						this.refreshOutcomes();						
						JOptionPane.showMessageDialog(container, "Outcome Deleted");
						reloadOutcome(true);
					}
				}
			}else if (out.getOutcome() != null && !out.getOutcome().equals(""))
				reloadOutcome(true);
			else
				JOptionPane.showMessageDialog(container, "No Outcome selected");
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
	}
	
	/*
	 * Objectives
	 */
	protected void addObjectiveTab(){
		objectivesPanel = new JPanel(new BorderLayout());
		tabbedPane.addTab("3.Objectives", objectivesPanel);
	}
	protected void buildObjectiviesPanel() {				
		if (objective == null)objective = new Objective();
		if (objectiveWidget == null)objectiveWidget = new SwingMetawidget();
		configureWidget(objectiveWidget,objective);
		if (objectives == null)objectives = new ArrayList<Objective>();
		if (mObjnTable == null)mObjnTable = new ListTableModel<Objective>(Objective.class, objectives, "Objective");	
		configureResultsSection(objectivesPanel, mObjnTable , new ActionListener(){
			//Handle Model Select
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Inside the Objective Table");
				int id = e.getID();
				try{
					Objective q = oController.read(id);
					objectiveWidget.setToInspect(q);
				}catch(Exception ex){ ex.printStackTrace(); }
			}
		}, new ActionListener(){
			//Handle Right Click
			@Override
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		JPanel btnPanel = new JPanel();
		objectivesPanel.add(btnPanel, BorderLayout.CENTER);
		
		JButton clearBtn = new JButton("Clear");
		btnPanel.add(clearBtn);
		clearBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				objective = new Objective();
				objectiveWidget.setToInspect(objective);
			}
		});
		
		if (!this.readOnly){
			//Add Buttons
			JButton addBtn = new JButton("Add");
			btnPanel.add(addBtn);
			addBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Objective o = objectiveWidget.getToInspect();
					if (o.getObjective() == null || o.getObjective() .equals("")){
						JOptionPane.showMessageDialog(container, "First specify the objective before saving");
						return;
					}
					try{
						if (model.getId() == 0){
							if (model.getProject().getName() == null || model.getProject().getName().equals("")){
								JOptionPane.showMessageDialog(container, "Unable to add Objective. First add the details of the project in the details tab before attempting to add activities to the project");
								return;
							}
							saveCohort();
						}
					}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
					try {
						boolean isNew = (o.getId() == 0);
						o = saveObjective(o);
						if (isNew){
							if (objectiveNumber ==0 && objectives.size() > 0)
								fixObjectiveNumber();
							o.setNumber(++objectiveNumber);
						}else removeObjective(o);
						objectives.add(o);
						objective = new Objective();
						objectiveWidget.setToInspect(objective);
						refreshObjectives();
					} catch (Exception e) {
						JOptionPane.showMessageDialog(container, "Unable to add Objective");
						e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
					}	
				}
			});
			
			JButton deleteBtn = new JButton("Delete");
			btnPanel.add(deleteBtn);
			deleteBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					deleteObjective((Objective)objectiveWidget.getToInspect());
				}
			});
		}
	}
	protected void removeObjective(Objective o) {
		Iterator <Objective> ib = this.objectives.iterator();
		while (ib.hasNext()){
			Objective in = ib.next();
			if (o.getId() == in.getId())//objectives.remove(in);
				ib.remove();
		}
	}
	protected void fixObjectiveNumber() {
		Iterator <Objective> i = this.objectives.iterator();
		while(i.hasNext()){
			Objective o = i.next();
			if (this.objectiveNumber < o.getNumber())
				this.objectiveNumber = o.getNumber();
		}
	}
	protected void addObjectiviesPanel() {
		objectivesPanel.add(objectiveWidget, BorderLayout.NORTH);
	}
	protected void refreshObjectives() {				
		this.reloadActivity(false);	
		this.mObjnTable.importCollection(objectives);		
	}
	protected Objective saveObjective(Objective o) throws Exception{
		if (model.getId() == 0)this.saveCohort();
		if (o.getId() != 0)return this.oController.update(o);
		o.setProject(this.model.getProject());
		o = this.oController.save(o);
		if (o.getId() > 0)System.out.println("Saved : "+ o);
		else System.out.println("Unable to save objective: "+o);
		return o;
	}
	protected void saveObjectives() throws Exception{
		if (!this.isCohortSaved)this.saveCohort();
		Iterator<Objective> oi = this.objectives.iterator();
		if (this.model.getProject().getId() > 0)
			while(oi.hasNext())saveObjective(oi.next());
	}
	protected void deleteObjective(Objective o){
		try{
			if (o.getId() > 0){
				if (JOptionPane.showConfirmDialog(null, "Delete this Objective?", "Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
					if (oController.delete(o)){
						this.removeObjective(o);
						refreshObjectives();
						JOptionPane.showMessageDialog(container, "Objective Deleted");
						objective = new Objective();
						objectiveWidget.setToInspect(objective);
						reloadActivity(false);
					}
				}
			}else if (o.getObjective() != null && !o.getObjective().equals("")){
				objective = new Objective();
				objectiveWidget.setToInspect(objective);
			}else
				JOptionPane.showMessageDialog(container, "Unable to add Beneficiary ");
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
	}

	/*
	 * Beneficiaries
	 */
	protected void addBeneficiaryTab(){
		beneficiariesPanel = new JPanel(new BorderLayout());
		tabbedPane.addTab("2. Beneficiaries", beneficiariesPanel);
	}
	
	protected void enhanceBeneficiaries(){
		//TODO Implement functionality to allow users to enter values for when other is selected
		if (!this.readOnly){
			@SuppressWarnings("unchecked")
			final JComboBox <String> roleBox =  (JComboBox<String>)beneficiaryWidget.getComponent("beneficiaryType");
			if (roleBox != null){
				roleBox.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e) {
						if (roleBox.getSelectedItem().equals("Other")){
							roleBox.setEditable(true);
							roleBox.setSelectedIndex(0);
						}				
					}
				});
			}
			//TODO enhance when selected the values will show in dropdown box
			@SuppressWarnings("unchecked")
			final JComboBox <String> ageBox =  (JComboBox<String>)beneficiaryWidget.getComponent("ageGroup");
			if (ageBox != null){
				ageBox.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e) {
						if (ageBox.getSelectedItem().equals("Other")){
							ageBox.setEditable(true);
							ageBox.setSelectedIndex(0);
						}				
					}
				});
			}
		}
	}
	protected void removeBeneficiary(Beneficiary b) {
		Iterator <Beneficiary> ib = this.beneficiaries.iterator();
		while (ib.hasNext()){
			Beneficiary ben = ib.next();
			if (b.getId() == ben.getId())//beneficiaries.remove(ben);
				ib.remove();
		}
	}	
	protected void buildBeneficiariesPanel() {
				
		if (beneficiary == null)beneficiary = new Beneficiary();
		beneficiaryWidget = new SwingMetawidget();
		configureWidget(beneficiaryWidget, beneficiary);	

		enhanceBeneficiaries();
		
		if (beneficiaries == null)beneficiaries = new ArrayList<Beneficiary>();
		if (mBenTable == null)mBenTable = new ListTableModel<Beneficiary>(Beneficiary.class, beneficiaries, "BeneficiaryType", "Sex", "AgeGroup");
		configureResultsSection(this.beneficiariesPanel, mBenTable , new ActionListener(){
			//Handle Model Select
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Inside the Beneficiaries Table");
				int id = e.getID();
				try{
					Beneficiary b = bController.read(id);
					beneficiaryWidget.setToInspect(b);
				}catch(Exception ex){ ex.printStackTrace(); }
			}
		}, new ActionListener(){
			//Handle Right Click
			@Override
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		
		JCheckBox check = new JCheckBox("Individual Beneficiary");
		check.setToolTipText("Enables the ability to specify specific persons as beneficiaries of the project");
//		beneficiaryWidget.add(check);
		
		JPanel btnPanel = new JPanel();
		beneficiariesPanel.add(btnPanel, BorderLayout.CENTER);
		
		JButton clearBtn = new JButton("Clear");
		btnPanel.add(clearBtn);
		clearBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				beneficiary = new Beneficiary();
				beneficiaryWidget.setToInspect(beneficiary);
				enhanceBeneficiaries();
			}
		});
		
		if (!this.readOnly){
			//Add Buttons
			JButton addBtn = new JButton("Add");
			btnPanel.add(addBtn);
			addBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Beneficiary b = beneficiaryWidget.getToInspect();
					if (b.getBeneficiaryType() != null){
						try{
							if (model.getId() == 0)
								if (model.getProject().getName() == null || model.getProject().getName().equals("")){
									JOptionPane.showMessageDialog(container, "Unable to add Beneficiary. First add the details of the project in the details tab before attempting to add activities to the project");
									return;
								}
								saveCohort();
						}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
						try{
							
							boolean isNew = (b.getId() == 0);							
							
							
							b.setCohort(model);
							b = saveBeneficiary(b);
							
							if (b.getId() > 0){
								if (!isNew)removeBeneficiary(b);								
								beneficiaries.add(b);								
								beneficiary = new Beneficiary();
								beneficiaryWidget.setToInspect(beneficiary);
								enhanceBeneficiaries();
								refreshBeneficiaries();
							}
						}catch(Exception e){
							JOptionPane.showMessageDialog(container, "Unable to add Beneficiary ");
							e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
						}
					}else
						JOptionPane.showMessageDialog(container, "Must specify beneficiary before saving ");
						
				}

				
			});
			
			JButton deleteBtn = new JButton("Delete");
			btnPanel.add(deleteBtn);
			deleteBtn.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					deleteBeneficiary((Beneficiary)beneficiaryWidget.getToInspect());
				}
			});
		}
	}
	protected void addBeneficiariesPanel(){
		beneficiariesPanel.add(beneficiaryWidget, BorderLayout.NORTH);
	}
	protected void refreshBeneficiaries(){
		mBenTable.importCollection(beneficiaries);
		mBenTable.fireTableDataChanged();
	}
	protected Beneficiary saveBeneficiary(Beneficiary b) throws Exception{
		if (!this.isCohortSaved)this.saveCohort();
		if (b.getId() != 0)return this.bController.update(b);
		b.setCohort(this.model);
		b = this.bController.save(b);
		if (b.getId() != 0)System.out.println("Saved : " + b);
		else System.out.println("Unable to save "+b);			
		return b;
	}
	protected void saveBeneficiaries() throws Exception{
		if (!this.isCohortSaved)this.saveCohort();
		if (this.model.getId() > 0){
			Iterator<Beneficiary> bs = this.beneficiaries.iterator();
			while(bs.hasNext())saveBeneficiary(bs.next());
		}		
	}
	protected void deleteBeneficiary(Beneficiary b){
		try{
			if (b.getId() > 0){
				if (JOptionPane.showConfirmDialog(null, "Delete this Beneficiary?", "Confirm", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
					if(bController.delete(b)){
						this.removeBeneficiary(b);
						refreshBeneficiaries();
						JOptionPane.showMessageDialog(container, "Beneficiary Deleted");
						beneficiary = new Beneficiary();
						beneficiaryWidget.setToInspect(beneficiary);
					}
				}
			}else if (b.getDescription() != null && b.getDescription().equals("")){
				beneficiary = new Beneficiary();
				beneficiaryWidget.setToInspect(beneficiary);
			}else
				JOptionPane.showMessageDialog(container, "No Beneficiary selected to be deleted");
		}catch(Exception ex){ex.printStackTrace();}
	}
	

	/**
	 * will save all the changes made via the form to the database
	 * Will save new record or update if record previously existed
	 */
	protected void saveCloseAction() {	
		try{
			saveAll();
			JOptionPane.showMessageDialog(container, "Project Saved");			
			this.container.setVisible(false);
		}catch(Exception e){
			JOptionPane.showMessageDialog(container, "Could not save changes to the project: " + e.getMessage());
			System.out.println(e.getMessage());	
		}
	}
	
	public void saveAll() throws Exception{
		if (this.saveCohort()){
			this.saveObjectives();
			this.saveBeneficiaries();
			this.saveActivities();
			this.saveQuestions();
			this.saveOutcomes();
		}
	}
	
	public void setRefreshable(IRefreshable refreshable){
		this.refreshable = refreshable;
	}

	public void addActionListener(ActionListener actionListener) {
		this.actionListener = actionListener;		
	}
}
