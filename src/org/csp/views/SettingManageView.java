package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SpringLayout;
import javax.swing.SwingWorker;
import javax.swing.border.EtchedBorder;

import org.csp.controller.OrganizationController;
import org.csp.controller.PersonController;
import org.csp.controller.SettingController;
import org.csp.models.Organization;
import org.csp.models.Person;
import org.kyledef.version.SettingManager;
import org.kyledef.version.Updater;

public class SettingManageView extends View {

	private int WIDTH = 440;
	private int HEIGHT = 290;
	
	JPanel containerPanel;
	protected JPanel panelRight;
	private int iconWidth = 64;
	private int iconHeight = 64;
	private JTextArea taskOutput;
	private JProgressBar progressBar;
	private JFrame updateframe;
	private JButton closeBtn;
	private OrganizationController oc;
	private PersonController pc;
	
	private String organizationExportName = "organization.csv";
	private String personExportName = "person.csv";
	public Updater updater;
	
	
	public SettingManageView(Container container) {
		super(container);
		initControllers();
		initUI();
	}
	
	protected void initControllers(){
		oc = OrganizationController.getInstance(SettingController.getInstance().getConnection());
		pc = PersonController.getInstance(SettingController.getInstance().getConnection());
	}
	
	/**
	 * @wbp.parser.entryPoint
	 */
	protected void initUI(){
		containerPanel = new JPanel();
		Dimension d = new Dimension(WIDTH, HEIGHT);
		containerPanel.setPreferredSize(d);
		SpringLayout springLayout = new SpringLayout();
		containerPanel.setLayout(springLayout);
		
		SpringLayout sl_rightPanel = new SpringLayout();
		JPanel rightPanel = new JPanel(sl_rightPanel);
		springLayout.putConstraint(SpringLayout.WEST, rightPanel, 10, SpringLayout.WEST, containerPanel);
		rightPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		containerPanel.add(rightPanel);
		
		JLabel backup_icon = new JLabel();
		sl_rightPanel.putConstraint(SpringLayout.NORTH, backup_icon, 0, SpringLayout.NORTH, rightPanel);
		sl_rightPanel.putConstraint(SpringLayout.EAST, backup_icon, -150, SpringLayout.EAST, rightPanel);
		backup_icon.setIcon(new ImageIcon(SettingManageView.class.getResource("/res/backup-icon.png")));
		backup_icon.setPreferredSize(new Dimension(iconWidth, iconHeight));
		rightPanel.add(backup_icon);
		
		JButton btnBackupDatabase = new JButton("Backup Database");
		sl_rightPanel.putConstraint(SpringLayout.WEST, btnBackupDatabase, 6, SpringLayout.EAST, backup_icon);
		btnBackupDatabase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				startBackupProcess();
			}
		});
		rightPanel.add(btnBackupDatabase);
		
		JButton btnRestoreBackup = new JButton("Restore Backup");
		sl_rightPanel.putConstraint(SpringLayout.WEST, btnRestoreBackup, 6, SpringLayout.EAST, backup_icon);
		sl_rightPanel.putConstraint(SpringLayout.EAST, btnRestoreBackup, -15, SpringLayout.EAST, rightPanel);
		sl_rightPanel.putConstraint(SpringLayout.SOUTH, btnBackupDatabase, -6, SpringLayout.NORTH, btnRestoreBackup);
		sl_rightPanel.putConstraint(SpringLayout.EAST, btnBackupDatabase, 0, SpringLayout.EAST, btnRestoreBackup);
		sl_rightPanel.putConstraint(SpringLayout.SOUTH, btnRestoreBackup, 0, SpringLayout.SOUTH, backup_icon);
		btnRestoreBackup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				startRestoreProcess();
			}
		});
		rightPanel.add(btnRestoreBackup);
		
		JLabel update_icon = new JLabel();
		sl_rightPanel.putConstraint(SpringLayout.NORTH, update_icon, 17, SpringLayout.SOUTH, backup_icon);
		sl_rightPanel.putConstraint(SpringLayout.WEST, update_icon, 0, SpringLayout.WEST, backup_icon);
		update_icon.setIcon(new ImageIcon(SettingManageView.class.getResource("/res/update-icon.png")));
		update_icon.setPreferredSize(new Dimension(64, 64));
		rightPanel.add(update_icon);
		
		JButton btnUpdate = new JButton("Update");
		sl_rightPanel.putConstraint(SpringLayout.NORTH, btnUpdate, 33, SpringLayout.SOUTH, btnRestoreBackup);
		sl_rightPanel.putConstraint(SpringLayout.WEST, btnUpdate, 6, SpringLayout.EAST, update_icon);
		sl_rightPanel.putConstraint(SpringLayout.EAST, btnUpdate, -15, SpringLayout.EAST, rightPanel);
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				startUpdate();				
			}
		});
		rightPanel.add(btnUpdate);
		
		JButton btnResetDatabase = new JButton("Reset Database");
		sl_rightPanel.putConstraint(SpringLayout.WEST, btnResetDatabase, 0, SpringLayout.WEST, btnBackupDatabase);
		sl_rightPanel.putConstraint(SpringLayout.SOUTH, btnResetDatabase, -10, SpringLayout.SOUTH, rightPanel);
		btnResetDatabase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (JOptionPane.showConfirmDialog(null, "This operation will reset the database. All data will be lost. Do you want to continue","Reset", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
					if (SettingController.getInstance().recreateDatabase())
						JOptionPane.showMessageDialog(null, "Reset Operation Successful");
					else
						JOptionPane.showMessageDialog(null, "Reset Operation Failed");
				}
			}
		});
		rightPanel.add(btnResetDatabase);
		
		JLabel restore_label = new JLabel();
		sl_rightPanel.putConstraint(SpringLayout.EAST, btnResetDatabase, 135, SpringLayout.EAST, restore_label);
		sl_rightPanel.putConstraint(SpringLayout.NORTH, restore_label, 18, SpringLayout.SOUTH, update_icon);
		sl_rightPanel.putConstraint(SpringLayout.WEST, restore_label, 0, SpringLayout.WEST, backup_icon);
		restore_label.setIcon(new ImageIcon(SettingManageView.class.getResource("/res/reset.png")));
		restore_label.setPreferredSize(new Dimension(64, 64));
		rightPanel.add(restore_label);
		
		
		panelRight = new JPanel(new BorderLayout());
		panelRight.setBorder( BorderFactory.createEmptyBorder( COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING ) );
		container.add( panelRight, BorderLayout.CENTER );
		
		panelRight.add(containerPanel, BorderLayout.NORTH);
		
		SpringLayout sl_leftPanel = new SpringLayout();
		JPanel leftPanel = new JPanel(sl_leftPanel);

		springLayout.putConstraint(SpringLayout.NORTH, rightPanel, 0, SpringLayout.NORTH, leftPanel);
		springLayout.putConstraint(SpringLayout.SOUTH, rightPanel, 0, SpringLayout.SOUTH, leftPanel);
		springLayout.putConstraint(SpringLayout.EAST, rightPanel, -6, SpringLayout.WEST, leftPanel);
		springLayout.putConstraint(SpringLayout.WEST, leftPanel, 234, SpringLayout.WEST, containerPanel);
		springLayout.putConstraint(SpringLayout.EAST, leftPanel, -6, SpringLayout.EAST, containerPanel);
		springLayout.putConstraint(SpringLayout.NORTH, leftPanel, 10, SpringLayout.NORTH, containerPanel);
		springLayout.putConstraint(SpringLayout.SOUTH, leftPanel, -51, SpringLayout.SOUTH, containerPanel);
		sl_rightPanel.putConstraint(SpringLayout.NORTH, leftPanel, 0, SpringLayout.NORTH, backup_icon);
		sl_rightPanel.putConstraint(SpringLayout.WEST, leftPanel, 10, SpringLayout.EAST, btnBackupDatabase);
		sl_rightPanel.putConstraint(SpringLayout.SOUTH, leftPanel, 0, SpringLayout.SOUTH, restore_label);
		sl_rightPanel.putConstraint(SpringLayout.EAST, leftPanel, 0, SpringLayout.EAST, rightPanel);
		containerPanel.add(leftPanel);
		leftPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		
		JLabel personIcon = new JLabel();
		sl_leftPanel.putConstraint(SpringLayout.NORTH, personIcon, 0, SpringLayout.NORTH, leftPanel);
		sl_leftPanel.putConstraint(SpringLayout.WEST, personIcon, 0, SpringLayout.WEST, leftPanel);
		personIcon.setIcon(new ImageIcon(SettingManageView.class.getResource("/res/37-Group-icon.png")));
		personIcon.setPreferredSize(new Dimension(64, 64));
		leftPanel.add(personIcon);
		
		JButton btnExportPersons = new JButton("Export Persons");
		btnExportPersons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				exportPersons();
			}
		});
		sl_leftPanel.putConstraint(SpringLayout.WEST, btnExportPersons, 6, SpringLayout.EAST, personIcon);
		sl_leftPanel.putConstraint(SpringLayout.SOUTH, btnExportPersons, -192, SpringLayout.SOUTH, leftPanel);
		leftPanel.add(btnExportPersons);
		
		JButton btnImportPersons = new JButton("Import Persons");
		btnImportPersons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				importPersons();
			}
		});
		sl_leftPanel.putConstraint(SpringLayout.NORTH, btnImportPersons, 6, SpringLayout.SOUTH, btnExportPersons);
		sl_leftPanel.putConstraint(SpringLayout.WEST, btnImportPersons, 6, SpringLayout.EAST, personIcon);
		leftPanel.add(btnImportPersons);
		
		JLabel organixationIcon = new JLabel();
		sl_leftPanel.putConstraint(SpringLayout.NORTH, organixationIcon, 6, SpringLayout.SOUTH, personIcon);
		sl_leftPanel.putConstraint(SpringLayout.WEST, organixationIcon, 0, SpringLayout.WEST, personIcon);
		organixationIcon.setIcon(new ImageIcon(SettingManageView.class.getResource("/res/Business-Organization-icon.png")));
		organixationIcon.setPreferredSize(new Dimension(64, 64));
		leftPanel.add(organixationIcon);
		
		JButton btnImportOrganization = new JButton("Import Organization");
		btnImportOrganization.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				importOrganizations();
			}
		});
		sl_leftPanel.putConstraint(SpringLayout.WEST, btnImportOrganization, 0, SpringLayout.WEST, btnExportPersons);
		sl_leftPanel.putConstraint(SpringLayout.SOUTH, btnImportOrganization, 0, SpringLayout.SOUTH, organixationIcon);
		sl_leftPanel.putConstraint(SpringLayout.EAST, btnImportOrganization, -2, SpringLayout.EAST, leftPanel);
		leftPanel.add(btnImportOrganization);
		
		JButton btnExportOrganization = new JButton("Export Organization");
		sl_leftPanel.putConstraint(SpringLayout.WEST, btnExportOrganization, 0, SpringLayout.WEST, btnExportPersons);
		sl_leftPanel.putConstraint(SpringLayout.SOUTH, btnExportOrganization, 0, SpringLayout.NORTH, btnImportOrganization);
		sl_leftPanel.putConstraint(SpringLayout.EAST, btnExportOrganization, 0, SpringLayout.EAST, btnImportOrganization);
		btnExportOrganization.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				exportOrganizations();
			}
		});
		leftPanel.add(btnExportOrganization);
	}
	
	protected void exportOrganizations() {
		try{
			Collection<Organization> models = oc.getAll();
			JFileChooser fc = new JFileChooser();
			fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			 if (fc.showOpenDialog(this.container) == JFileChooser.APPROVE_OPTION){
				 File file = fc.getSelectedFile();
				 if (file.exists() && file.isDirectory()){
					 if (this.oc.exportToCSV(file.getAbsolutePath()+"/", this.organizationExportName, models)){
						 JOptionPane.showMessageDialog(null, "Successfully Exported Organizations to "+this.organizationExportName);
					 }
				 }
			 }			
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
		// TODO Test
	}

	protected void importOrganizations() {
		JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		if (fc.showOpenDialog(this.container) == JFileChooser.APPROVE_OPTION){
			File file = fc.getSelectedFile();
			if (file.exists()){
				int i = file.getName().lastIndexOf(".");
				if (i > 0){
					String extension = file.getName().substring(i+1);
					if (extension.equals("csv")){
						if (JOptionPane.showConfirmDialog(this.container, "This operation may change data currently stored in the database. Continue") == JOptionPane.YES_OPTION){
							File f = file.getParentFile();
							if (this.oc.importToCSV(f.getAbsolutePath()+"/", file.getName(), new Organization("","","")))
								JOptionPane.showMessageDialog(this.container, "Successfully Imported Organizations");
							else
								JOptionPane.showMessageDialog(this.container, "Unable to complete Importing Organizations");
						}
					}else{
						JOptionPane.showMessageDialog(this.container, "Unsupported file");
					}
				}
			}
		}
		// TODO Auto-generated method stub
		
	}

	protected void importPersons() {
		JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		if (fc.showOpenDialog(this.container) == JFileChooser.APPROVE_OPTION){
			File file = fc.getSelectedFile();
			if (file.exists()){
				int i = file.getName().lastIndexOf(".");
				if (i > 0){
					String extension = file.getName().substring(i+1);
					if (extension.equals("csv")){
						if (JOptionPane.showConfirmDialog(this.container, "This operation may change data currently stored in the database. Continue") == JOptionPane.YES_OPTION){
							File f = file.getParentFile();
							System.out.println(f.getAbsolutePath());
							if (this.pc.importToCSV(f.getAbsolutePath()+"/", file.getName(), new Person("","","","","","","","") ))
								JOptionPane.showMessageDialog(this.container, "Successfully Imported Persons");
							else
								JOptionPane.showMessageDialog(this.container, "Unable to complete Importing Persons");
						}
					}else{
						JOptionPane.showMessageDialog(this.container, "Unsupported file");
					}
				}
			}
		}
		// TODO Auto-generated method stub
	}

	protected void exportPersons() {
		
		try{
			Collection<Person> models = pc.getAll();
			JFileChooser fc = new JFileChooser();
			fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			 if (fc.showOpenDialog(this.container) == JFileChooser.APPROVE_OPTION){
				 File file = fc.getSelectedFile();
				 if (file.exists() && file.isDirectory()){
					 if (this.pc.exportToCSV(file.getAbsolutePath()+"/", this.personExportName, models)){
						 File newFile = new File(file.getAbsoluteFile()+"/"+this.personExportName);
						 if (newFile.exists())
							 JOptionPane.showMessageDialog(null, "Successfully Exported Persons to "+this.personExportName);
						 else
							 JOptionPane.showMessageDialog(null, "Unable to complete export");
					 }
				 }
			 }			
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
		
		// TODO Test
	}

	public void startBackupProcess(){
		JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		 if (fc.showOpenDialog(this.container) == JFileChooser.APPROVE_OPTION){
			 File file = fc.getSelectedFile();
			 if (file.exists() && file.isDirectory()){
				 String location = this.getBackup();
				 if (location != null){
					 File destFile = new File(file.getAbsolutePath()+"\\"+this.getBackUpName());
					 File sourceFile = new File(location);
					 if (sourceFile.exists())sourceFile.renameTo(destFile);
				 }else
					 JOptionPane.showMessageDialog(null, "Error occurred while creating backup");
			 }
		 }
		 
	}
	
	public String getBackUpName(){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		
		return  new StringBuilder().
			append("dammeri").
			append("-").
			append(cal.get(Calendar.DAY_OF_MONTH)).
			append("-").
			append(cal.get(Calendar.MONTH)).
			append("-").
			append(cal.get(Calendar.YEAR)).
			append(".zip").toString();
	}
	
	public String getBackup(){
		try {
			String location = new StringBuilder().
					append(System.getProperty("java.io.tmpdir")).
					append(getBackUpName()).toString();
			
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(location));
			out.putNextEntry(new ZipEntry("dimmeri.db"));
			
			FileInputStream inFile = new FileInputStream(SettingController.getInstance().getDBLocation()+"dimmeri.db");
			
			byte[] b = new byte[1024];
	        int count;
	        
			while ((count = inFile.read(b)) > 0)
				out.write(b,0,count);
			
			out.close();
			inFile.close();
			return location;
		} catch (Exception e) { e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
		return null;
	}
	
	public void startRestoreProcess(){
		JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		if (fc.showOpenDialog(this.container) == JFileChooser.APPROVE_OPTION){
			File file = fc.getSelectedFile();
			if (file.exists()){
				int i = file.getName().lastIndexOf(".");
				if (i > 0){
					String extension = file.getName().substring(i+1);
					if (extension.equals("zip")){
						if (restore(file))
							JOptionPane.showMessageDialog(null, "Successfully Restored the database to a previous version");
						else
							JOptionPane.showMessageDialog(null, "Unable to restore the database");
						return;
					}
				}
			}
			JOptionPane.showMessageDialog(null, "Error occurred while restoring backup. Check to ensure the appropriate file was selected");
		}
	}
	
	public boolean restore(File selected){
		try {
			ZipInputStream zis = new ZipInputStream(new FileInputStream(selected));
			ZipEntry ze = zis.getNextEntry();
			
			while(ze!=null){
				File newFile = new File(SettingController.getInstance().getDBLocation() + "dimmeri.db");
				FileOutputStream fos = new FileOutputStream(newFile);
				
				int count;
				byte[] b = new byte[1024];
				while ((count = zis.read(b)) > 0)
					fos.write(b,0,count);
				
				fos.close();
				ze = zis.getNextEntry();
			}
			
			zis.closeEntry();
			zis.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
		
		return false;
	}

	public boolean export2CSV(){
		return false;
	}

	public void startUpdate(){
		updateframe = new JFrame("Update Dammeri");
		updateframe.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JPanel panel = new JPanel(new BorderLayout());
		updateframe.setContentPane(panel);
		
		progressBar = new JProgressBar(0,100);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		panel.add(progressBar, BorderLayout.NORTH);
		
		taskOutput = new JTextArea(5, 20);
		taskOutput.setEditable(false);
		panel.add(new JScrollPane(taskOutput), BorderLayout.SOUTH);
		
		closeBtn = new JButton("Cancel");
		updateframe.getContentPane().add(closeBtn, BorderLayout.CENTER);
		closeBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				updateframe.dispose();
			}
		});
		closeBtn.setEnabled(false);
		
		updateframe.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		
		
		UpdateTask tTask = new UpdateTask();
		tTask.addPropertyChangeListener(new PropertyChangeListener(){
			@Override
			public void propertyChange(PropertyChangeEvent pce) {
				if ("progress".equalsIgnoreCase(pce.getPropertyName())){
					int progress = (Integer)pce.getNewValue();
					progressBar.setValue(progress);
					taskOutput.append("");
				}
			}
		});
		tTask.execute();
		
		updateframe.pack();
		updateframe.setVisible(true);
	}
	
	class UpdateTask extends SwingWorker<Boolean, Void>{
		@Override
        public Boolean doInBackground() {
			setProgress(10);
			taskOutput.append("Initializing Loaded\n");
			
			SettingManager sm = new SettingManager();
			sm.loadSettings();
			setProgress(20);
			taskOutput.append("Settings Loaded\n");
			
			SettingController sc = SettingController.getInstance();
			setProgress(30);
			taskOutput.append("Searching for updates\n");
			
			if (!sc.isNeedUpdate()){
				taskOutput.append("Application Up to date. No update Required\n");
				closeBtn.setText("Close");
				return false;
			}else{
				setProgress(40);
				taskOutput.append("Updating to version: "+ new Double(sc.getLatestVersion()).toString() +"\n");
				
				try{
					updater = new Updater(sc.getUpdateLocation(), "dammeri", new Double(sc.getLatestVersion()).toString());
					setProgress(50);
					taskOutput.append("Downloading ...\n");
					updater.retrieveUpdate();
					setProgress(60);
					taskOutput.append("Download Complete\n");
					
					return true;
				}catch(Exception e){
					setProgress(100);
					taskOutput.append("Downloading update failed");
					e.printStackTrace();
					
				}
				System.out.println(sc.getUpdateLocation()+sc.getLatestVersion());
			}
			return false;
		}
		@Override
        public void done() {
			System.out.println("Done");
			try{
				Boolean result = get();
				if (result){
					setProgress(80);
					taskOutput.append("Checking update");
					
					
					if (updater.isUpdateExist()){
						setProgress(80);
						taskOutput.append("File downloaded successfully");
						
						closeBtn.setText("Restart");
						closeBtn.removeAll();
						if (JOptionPane.showConfirmDialog(container, "Do you want to restart to complete the update?","Update",JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
							replaceCommand();
						}
					}
				}
				closeBtn.setEnabled(true);
				
			}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
			
			
			updateframe.repaint();
			updateframe.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}
	
//	public boolean replaceCommand(){
//		try{
//			Path destinPath = Paths.get(updater.getDirectory(),updater.getAppString());
//			Path sourcePath = Paths.get(updater.getFstream().getAbsolutePath());
//			
//			System.out.printf("Destination %s Source %s\n", destinPath, sourcePath);
//			
//			while(true){
//				try{
//					if (sourcePath.toFile().length() > 800000){ //A heuristic to try and ensure full download before replacing					
//						Files.move(sourcePath, destinPath, StandardCopyOption.REPLACE_EXISTING);	
//						return true;
//					}
//				}catch(Exception ein){ein.printStackTrace();}
//				Thread.sleep(1000);
//			}
//		}catch(Exception ein){ein.printStackTrace(); return false;}
//	}
	
	public void replaceCommand(){
		
		final String SETTING_LOC = "./settings/";
		final String SETTING_FNAME = "update";
		try{
			Properties prop = new Properties();	
			prop.load(new FileInputStream(SETTING_LOC + SETTING_FNAME));
			prop.setProperty("uptodateVersion", new Double(sc.getLatestVersion()).toString());
		
//			Updater u = new Updater();
			
			
			String exec = new StringBuilder().
					append(java.lang.System.getProperty( "java.home" ) ).
					append(java.io.File.separator ).
					append("bin").
					append(java.io.File.separator).
					append("java").append(" ").
					append("-jar").append(" ").
					append( new java.io.File( "." ).getAbsolutePath() ).
		    	    append( java.io.File.separator ).
		    	    append( "updater.jar" ).
		    	    append(" -i").
					toString();
		
			Runtime.getRuntime().exec(exec);			
		} catch (Exception e) {e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);JOptionPane.showMessageDialog(null, e.getMessage());}
		finally{System.exit(0);}
	}
}
