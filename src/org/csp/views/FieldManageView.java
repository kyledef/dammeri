package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.csp.controller.FieldController;
import org.csp.controller.SettingController;
import org.csp.models.Field;
import org.csp.models.Indicator;
import org.csp.utilities.ListTableModel;
import org.metawidget.inspector.InspectionResultConstants;
import org.metawidget.inspector.annotation.UiAction;
import org.metawidget.inspector.annotation.UiAttribute;
import org.metawidget.inspector.annotation.UiHidden;

import com.toedter.calendar.JDateChooser;

public class FieldManageView extends FieldView{
	
	Collection<Field>fields;
	Indicator indicator;
	FieldController fController;
	
	JPanel topPanel;

	public FieldManageView(Container container, boolean useLeftPanel,boolean readOnly, Field model, boolean useButtons,boolean displayResults, Indicator indicator) {
		super(container, useLeftPanel, readOnly, model, useButtons, displayResults);
		this.indicator = indicator;
	}
	
	public FieldManageView(Container container, boolean useLeftPanel,
			boolean readOnly, Field model, boolean useButtons,
			boolean displayResults) {
		super(container, useLeftPanel, readOnly, model, useButtons, displayResults);
	}

	public FieldManageView(Container container, boolean useLeftPanel,
			boolean readOnly, Field model, boolean useButtons) {
		super(container, useLeftPanel, readOnly, model, useButtons);
	}

	public FieldManageView(Container container, boolean useLeftPanel,
			boolean readOnly, Field model) {
		super(container, useLeftPanel, readOnly, model);
	}

	public FieldManageView(Container container, boolean useLeftPanel,
			boolean readOnly) {
		super(container, useLeftPanel, readOnly);
	}

	public FieldManageView(Container container, boolean useLeftPanel) {
		super(container, useLeftPanel);
	}

	public FieldManageView(Container container) {
		super(container);
	}
	@Override
	protected void configureModel() {
		super.configureModel();
		fController = (FieldController)this.controller;
		fields = new ArrayList<Field>();
		this.mTModel = new ListTableModel<Field>(Field.class, fields, "Indicator","Value","CaptureDate");
	}
	
	@UiAction
	@UiAttribute( name = InspectionResultConstants.HIDDEN, value = "${this.readOnly}" )
	public void add(){
		try{
			Field f = this.mMetawidget.getToInspect();
			Indicator i = f.getIndicator();
			if(f.getValue() == null || f.getValue().equals(""))JOptionPane.showMessageDialog(this.container, "Unable to save field. First specify the value to be saved");
			else if (i == null)JOptionPane.showMessageDialog(this.container, "Unable to save field. Ensure that it is associated with an indicator");
			else {
				
				if (!this.readOnly){
					JDateChooser sDate = (JDateChooser)this.mMetawidget.getComponent("captureDate");
					f.setCaptureDate(sDate.getDate());
				}
				
				if (f.getCaptureDate() == null)f.setCaptureDate(new Date());
				this.controller.save(f);
				if (f.getId() > 0){
					fields.add(f);
					refreshFields();
					model = new Field();
					model.setIndicator(f.getIndicator());
					this.mMetawidget.setToInspect(model);
				}
			}
		}catch(Exception e){
			JOptionPane.showMessageDialog(this.container, "Unable to save field, Ensure that it was not prevoius added for this indicator");
			e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
		}
	}
	@UiAction
	public void clear(){
		Field tmp = this.model;
		this.model = new Field();
		this.model.setIndicator(tmp.getIndicator());
		this.mMetawidget.setToInspect(this.model);
		enhanceUI();
	}

	@UiHidden
	public Indicator getIndicator() {
		return indicator;
	}

	public void setIndicator(Indicator indicator) {
		this.indicator = indicator;
		this.model.setIndicator(indicator);
		this.mMetawidget.setToInspect(this.model);
		if (this.indicator.getId() > 0){
			enhanceUI();
			refreshFields();
		}
	}
	
	public void refreshFields(){		
		this.mTModel.importCollection(fController.get(this.indicator));
	}
	
	protected void enhanceUI(){
		if (this.indicator.getId() > 0){
			JLabel label = new JLabel("Indicator: "+this.indicator.getIndicator());
			label.setAlignmentX(JLabel.CENTER_ALIGNMENT);
			label.setAlignmentY(JLabel.CENTER_ALIGNMENT);
			this.container.add(label, BorderLayout.NORTH);
		}
	}
	
}
