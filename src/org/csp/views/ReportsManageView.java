package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.csp.models.Cohort;
import org.csp.utilities.ListTableModel;

public class ReportsManageView extends CohortView implements ActionListener,ItemListener {
	JPanel containerPanel;
//	JPanel panelRight;
	int WIDTH = 650;
	int PWIDTH = 130;
	int HEIGHT = 510;
	JPanel subPanel;
	boolean addResize;
	boolean customized = false;
	JLabel info;
	int currScreen;
	JPanel centerPanel;
//	private static final int buttonWidth = 110;
//	private static final int buttonHeight = 30;
	
	String standardString = "Standard";
	String customString = "Custom";
	String viewString = "View";
	String exportString = "Export";
	boolean export;
	
	JCheckBox [] boxes;
	JCheckBox objChk;
	JCheckBox actChk;
	JCheckBox outChk;
	JCheckBox questChk;
	JCheckBox indicChk;
	JCheckBox inputChk;
	JCheckBox outputChk;
	JButton btnPrevious;
	JButton btnNext;
	JButton btnExit;

	public ReportsManageView(Container container, boolean useLeftPanel,boolean readOnly, Cohort model, boolean useButtons) {
		super(container, useLeftPanel, readOnly, model, useButtons);
	}

	public ReportsManageView(Container container, boolean useLeftPanel,boolean readOnly, Cohort model) {
		super(container, useLeftPanel, readOnly, model);
	}

	public ReportsManageView(Container container, boolean useLeftPanel,boolean readOnly) {
		super(container, useLeftPanel, readOnly);
	}

	public ReportsManageView(Container container, boolean useLeftPanel) {
		super(container, useLeftPanel);
	}

	public ReportsManageView(Container container) {
		super(container);
	}

	@Override
	protected void configureModel() {
		super.configureModel();
		this.mTModel = new ListTableModel<Cohort>(Cohort.class, controller.getAll(), "Project", "EndDate", "StartDate" );
		this.model = new Cohort();
	}
	
	protected void init(){	
		super.init();
		addResize = true;
		panelRight = new JPanel(new BorderLayout());
		panelRight.setBorder( BorderFactory.createEmptyBorder( COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING ) );
		container.add( panelRight, BorderLayout.CENTER );
		centerPanel = new JPanel(new BorderLayout());
		panelRight.add(centerPanel, BorderLayout.CENTER);
		boxes = new JCheckBox[8];
	}
	
	@Override
	protected void buildRightPanel(){
		buildNavPanel(this.panelRight);		
		setUpInfoPane(this.panelRight);
		buildSearchPanel(this.panelRight);
	}
	
	protected void buildSearchPanel(JPanel panel){
		JPanel searchPanel = new JPanel();
		panel.add(searchPanel, BorderLayout.WEST);
	}
	
	protected void setUpInfoPane(JPanel panel){
		JPanel infoPanel = new JPanel();
		panel.add(infoPanel, BorderLayout.NORTH);
		info = new JLabel();
		infoPanel.add(info);
		info.setHorizontalAlignment(JLabel.CENTER);
		currScreen = 1;
		//setup result section -> placed here to ensure only done once
		this.createResultsSection(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				try{model = controller.read(e.getID());}							//TODO Improve Here (does not consistently work)
				catch(Exception ex){ ex.printStackTrace();}
			}
		},new ActionListener(){public void actionPerformed(ActionEvent e) { }});
		
		this.buildProjectList();
	}
	
	/**
	 * The buildNavPanel will develop the navigation panel and the bottom of the screen
	 * that allows users to traverse between the different options of the report
	 * @param The container which the panel will be stored. It expects the panel to user a border layout
	 */
	protected void buildNavPanel(JPanel panel){
		JPanel navBtnPanel = new JPanel();
		panel.add(navBtnPanel, BorderLayout.PAGE_END);
		
		btnPrevious = new JButton("< Previous");
		btnPrevious.setEnabled(false);
		navBtnPanel.add(btnPrevious);
		
		btnNext = new JButton("Next >");
		navBtnPanel.add(btnNext);
		btnExit = new JButton("Exit");
		navBtnPanel.add(btnExit);
		
		btnPrevious.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				currScreen--;
				if (currScreen < 2)btnPrevious.setEnabled(false);
				else btnPrevious.setEnabled(true);
				if (currScreen < 4)btnNext.setEnabled(true);
				if (currScreen == 3){ 
					buildOptionsFormat(customized);
				}else if (currScreen == 2){
					btnNext.setText(" Next >");
					buildQueryChoice();
				}else if (currScreen == 1){
					buildProjectList();
				}
			}
		});
		
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (model.getId() > 0){
					currScreen++;
					if (currScreen > 1) btnPrevious.setEnabled(true);
					if (currScreen == 3){
						btnNext.setText("View Project");
						buildOptionsFormat(customized);
					}else if (currScreen == 2){
						btnNext.setText(" Next >");
						buildQueryChoice();
					}else if (currScreen == 4){
						showResults();
						btnNext.setEnabled(false);
					}
				}else
					JOptionPane.showMessageDialog(container, "First Select Project before proceeding");
			}
		});
		
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				container.setVisible(false);
			}
		});
	}
	
	protected void showResults() {
		info.setText("Project Details");
		centerPanel.removeAll();
		if (this.model != null && this.model.getId() > 0){
			
			if (!this.customized){
				if (this.export)
					exportStandard(centerPanel);
				else
					displayStandard(centerPanel);
			}else{
				if (this.export)
					exportCustom(centerPanel);
				else
					displayCustom(centerPanel);
			}
		}else
			JOptionPane.showMessageDialog(null, "No model selected");
		
		centerPanel.repaint();
		
	}

	private void displayCustom(JPanel centerPanel2) {
		// TODO Auto-generated method stub
		
	}

	private void exportCustom(JPanel centerPanel2) {
		// TODO Auto-generated method stub
		
	}

	private void displayStandard(JPanel centerPanel2) {
		// TODO Auto-generated method stub
		
	}

	private void exportStandard(JPanel centerPanel2) {
		// TODO Auto-generated method stub
		
	}

	protected void buildProjectList(){
		info.setText("Step 1 of 4: Select the project");
		centerPanel.removeAll();
		centerPanel.add(this.scrollResults, BorderLayout.CENTER);	
		centerPanel.repaint();
	}
	
	protected void buildQueryChoice(){
		info.setText("Step 2 of 4: Select the type of Query");
		
		centerPanel.removeAll();
		JPanel btnPanel = new JPanel(new GridLayout(0, 1));
		centerPanel.add(btnPanel, BorderLayout.CENTER);
		
		JRadioButton stndBtn = new JRadioButton(standardString);
		btnPanel.add(stndBtn);
		stndBtn.setMnemonic(KeyEvent.VK_S);
		stndBtn.setActionCommand(standardString);
		stndBtn.addActionListener(this);
		
		JRadioButton custBtn = new JRadioButton(customString);
		btnPanel.add(custBtn);
		custBtn.setMnemonic(KeyEvent.VK_C);
		custBtn.setActionCommand(customString);
		custBtn.addActionListener(this);
		
		ButtonGroup group = new ButtonGroup();
		group.add(stndBtn);
		group.add(custBtn);
		
		if (this.customized)custBtn.setSelected(true);
		else stndBtn.setSelected(true);
		
		centerPanel.repaint();
	}

	protected void buildOptionsFormat(boolean choices){		
		StringBuilder stb = new StringBuilder();
		stb.append("Step 3 of 4: Select Options for ");
		if (choices) stb.append("Customized ");
		else stb.append("Standard ");			
		stb.append("Query");
		
		info.setText(stb.toString());
		centerPanel.removeAll();
		
		if (choices){
			JPanel checkPanel = new JPanel(new GridLayout(0,1));
			centerPanel.add(checkPanel, BorderLayout.CENTER);
			
			if (this.boxes == null)this.boxes = new JCheckBox[8];//Array to facilitate functionality for check all
			int i = 0;
			
			JCheckBox all = new JCheckBox("Select All");
			all.addItemListener(this);
			checkPanel.add(all);
			
			if (objChk == null){
				objChk = new JCheckBox("Objectives");
				this.boxes[i++] = objChk; //Add to an array of check-boxes to facilitate functionality for check all
				objChk.setSelected(true);
			}
			checkPanel.add(objChk);
			objChk.addItemListener(this);
			
			if (actChk == null){
				actChk = new JCheckBox("Activities");
				this.boxes[i++] = actChk; 
				actChk.setSelected(true);
			}
			checkPanel.add(actChk);
			actChk.addItemListener(this);
			
			if (outChk == null){
				outChk = new JCheckBox("Outcomes");
				this.boxes[i++] = outChk; 
				 outChk.setSelected(true);
			}
			checkPanel.add(outChk);
			outChk.addItemListener(this);
			
			if (questChk == null){
				questChk = new JCheckBox("Questions");
				this.boxes[i++] = questChk; 
				 questChk.setSelected(true);
			}
			checkPanel.add(questChk);
			questChk.addItemListener(this);
			
			if (indicChk == null){
				indicChk = new JCheckBox("Indicators");
				this.boxes[i++] = indicChk; 
				 indicChk.setSelected(true);
			}
			checkPanel.add(indicChk);
			indicChk.addItemListener(this);
			
			if (inputChk == null){
				inputChk = new JCheckBox("Inputs");
				this.boxes[i++] = inputChk;
				 inputChk.setSelected(true);
			}
			checkPanel.add(inputChk);
			inputChk.addItemListener(this);
			
			if (outputChk == null){
				outputChk = new JCheckBox("Outputs");
				this.boxes[i++] = outputChk; 
				 outputChk.setSelected(true);
			}
			checkPanel.add(outputChk);
			outputChk.addItemListener(this);
		}
		
		JPanel radioPanel = new JPanel(new GridLayout(0,1));
		radioPanel.setPreferredSize(new Dimension(250,200));
		centerPanel.add(radioPanel, BorderLayout.WEST);
		JRadioButton viewBtn = new JRadioButton(viewString);
		radioPanel.add(viewBtn);
		viewBtn.setActionCommand(viewString);
		viewBtn.addActionListener(this);
		
		JRadioButton exportBtn = new JRadioButton(exportString);
		radioPanel.add(exportBtn);
		exportBtn.setActionCommand(exportString);
		exportBtn.addActionListener(this);
		
		ButtonGroup group = new ButtonGroup();
		group.add(viewBtn);
		group.add(exportBtn);
		
		if (this.export)
			exportBtn.setSelected(true);
		else
			viewBtn.setSelected(true);
		
		centerPanel.repaint();
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		JCheckBox cb = (JCheckBox) e.getItem();

		if (e.getStateChange() == ItemEvent.SELECTED){
			if (cb.getText().equalsIgnoreCase("Select All"))
				for(int i =0;i < boxes.length; i++)
					if (boxes[i] != null)
						boxes[i].setSelected(true);
		}else{
			if (cb.getText().equalsIgnoreCase("Select All"))
				for(int i =0;i < boxes.length; i++)
					if (boxes[i] != null)
						boxes[i].setSelected(false);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		if (command.equals(this.customString))
			customized = true;
		else if (command.equals(this.standardString))
			customized = false;
		else if (command.equals(this.viewString))
			export = false;
		else if (command.equals(this.exportString))
			export = true;
	}
	
}
