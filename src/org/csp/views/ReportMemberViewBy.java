package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.csp.controller.MemberController;
import org.csp.models.Member;
import org.csp.utilities.ListTableModel;
import org.csp.utilities.Utilities;

public class ReportMemberViewBy extends View {

	public static final int ACTIVITY_VIEW = 0;
	public static final int PROJECT_VIEW = 1;
	
	protected ListTableModel<Member> mTModel;
	protected JTable table;
	protected MemberController controller;
	protected ArrayList<Member> members;
	
	JPanel containerPanel;
	JPanel panelRight;
	private int field;
	
	public ReportMemberViewBy(Container container, int field) {
		super(container);
		this.field = field;
		initUI();
	}

	public ReportMemberViewBy(Container subPanel, int beneficiaryView, boolean b) {
		
		super(subPanel, b);
		this.field = beneficiaryView;
		initUI();
	}

	protected void initUI(){
		containerPanel = new JPanel(new BorderLayout());
		panelRight = new JPanel(new BorderLayout());
		panelRight.setBorder( BorderFactory.createEmptyBorder( COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING ) );
		container.add( panelRight, BorderLayout.CENTER );		
		panelRight.add(containerPanel, BorderLayout.CENTER);
		//Create table for results
		containerPanel.add(this.createResultsSection(), BorderLayout.SOUTH);
		switch(field){
			case ACTIVITY_VIEW:
				containerPanel.add(buildActivityField(), BorderLayout.NORTH);
				break;
			case PROJECT_VIEW:
				containerPanel.add(buildProjectView(), BorderLayout.NORTH);
				break;
			default:
				break;
		}
		
	}
	
	public Component createResultsSection() {
		controller = MemberController.getInstance();
		members = new ArrayList<Member>();		
		mTModel = new ListTableModel<Member>(Member.class, members,"person","organization","role");
		table = new JTable( mTModel );
		Utilities.setTableDefaults(table);	
		table.addMouseListener( new MouseAdapter() {
			@Override
			public void mouseClicked( MouseEvent event ) {
				//Ensure user double clicks table
				if ( event.getClickCount() != 2 )return; 
				@SuppressWarnings("unchecked")
				ListTableModel<Member> model = (ListTableModel<Member>) table.getModel();	
				if (table.getSelectedRow() >= 0){
					Member t = model.getValueAt( table.getSelectedRow() );
					System.out.println(t + "CLICKED");
				}					
			}
		});		
		JScrollPane scroll =  new JScrollPane( table ); 
		//Set Height of table
		scroll.setPreferredSize( new Dimension((int)  scroll.getPreferredSize().getWidth(), 200));
		return scroll;
	}
	
	protected Component buildActivityField() {
		JPanel panel = new JPanel(new BorderLayout());
		JPanel keywordPanel = new JPanel();
		keywordPanel.setLayout(new GridLayout(1, 2));
		panel.add(keywordPanel, BorderLayout.NORTH);
		JLabel project_lbl = new JLabel("Activity: ");
		project_lbl.setHorizontalAlignment(SwingConstants.RIGHT);
		final JTextField project = new JTextField();
		keywordPanel.add(project_lbl);
		keywordPanel.add(project);
		
		//Add Buttons
		JPanel btnPanel = new JPanel();
		panel.add(btnPanel, BorderLayout.CENTER);
		JButton find = new JButton("Find");
		find.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnPanel.add(find);
		JButton clear = new JButton("Clear");
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnPanel.add(clear);
		return panel;
	}
	
	protected Component buildProjectView() {
		JPanel panel = new JPanel(new BorderLayout());
		JPanel keywordPanel = new JPanel();
		keywordPanel.setLayout(new GridLayout(1, 2));
		panel.add(keywordPanel, BorderLayout.NORTH);
		JLabel project_lbl = new JLabel("Project: ");
		project_lbl.setHorizontalAlignment(SwingConstants.RIGHT);
		final JTextField project = new JTextField();
		keywordPanel.add(project_lbl);
		keywordPanel.add(project);
		
		//Add Buttons
		JPanel btnPanel = new JPanel();
		panel.add(btnPanel, BorderLayout.CENTER);
		JButton find = new JButton("Find");
		find.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnPanel.add(find);
		JButton clear = new JButton("Clear");
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnPanel.add(clear);
		return panel;
	}
	
}
