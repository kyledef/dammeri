package org.csp.views;

import java.awt.Container;

import org.csp.controller.FollowUpController;
import org.csp.models.FollowUpTask;

public class FollowUpTaskView extends ModelView <FollowUpTask>{

	public FollowUpTaskView(Container container) {
		super(container);
	}

	@Override
	protected void configureModel() {
		FollowUpTask ft = new FollowUpTask();
		System.out.println(ft);
		this.model = ft;
		this.controller = FollowUpController.getInstance();
	}

}
