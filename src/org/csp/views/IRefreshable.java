package org.csp.views;

public interface IRefreshable {
	public boolean refresh();
}
