package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SpringLayout;

import org.csp.controller.CohortController;
import org.csp.models.Cohort;
import org.csp.models.Project;
import org.csp.utilities.ListTableModel;
import org.csp.utilities.Utilities;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.metawidget.swing.SwingMetawidget;
import org.metawidget.swing.widgetprocessor.binding.beansbinding.BeansBindingProcessor;
import org.metawidget.swing.widgetprocessor.binding.beansbinding.BeansBindingProcessorConfig;

public class ProjectManageView extends CohortView implements IRefreshable{

	Collection<Cohort> cohorts;
	ListTableModel<Cohort> proTable;
	CohortController cc;
	JPanel findPanel;
	SwingMetawidget findCohort;
	
	private JFrame subFrame;
	JPanel centerPanel;
	
	public ProjectManageView(Container container) {
		super(container);
	}
	
	protected void init(){
		super.init();
		//Placed after the initialization because instantiated in  the parent nit method
		cc = (CohortController)this.controller;
	}
	
	/**
	 * @wbp.parser.entryPoint
	 */
	protected void buildRightPanel(){
		centerPanel = new JPanel();
		centerPanel.setLayout(new BorderLayout());
		
		
		//Buttons for configuration options in the center
		JPanel buttonPanel = new JPanel();
		Dimension panelD = new Dimension(580,180); 
		buttonPanel.setPreferredSize(panelD);
		
		
//		this.container.add(buttonPanel, BorderLayout.CENTER);
		
		
		SpringLayout sl_buttonPanel = new SpringLayout();
		buttonPanel.setLayout(sl_buttonPanel);
		
		JLabel lblManagingProjects = new JLabel("Managing Projects");
		buttonPanel.add(lblManagingProjects);
		
		JButton btnAddProject = new JButton("Add Project");
		btnAddProject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Open Dialog For adding new Project");
				buildAddProjectUI();
			}
		});
		sl_buttonPanel.putConstraint(SpringLayout.NORTH, btnAddProject, 46, SpringLayout.NORTH, buttonPanel);
		sl_buttonPanel.putConstraint(SpringLayout.WEST, btnAddProject, 42, SpringLayout.WEST, buttonPanel);
		sl_buttonPanel.putConstraint(SpringLayout.WEST, lblManagingProjects, 0, SpringLayout.WEST, btnAddProject);
		sl_buttonPanel.putConstraint(SpringLayout.SOUTH, lblManagingProjects, -6, SpringLayout.NORTH, btnAddProject);
		buttonPanel.add(btnAddProject);
		
		JButton btnFindProject = new JButton("Find Project");
		btnFindProject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Open Dialog to find new User");
				buildFindDialog();
			}
		});
		sl_buttonPanel.putConstraint(SpringLayout.NORTH, btnFindProject, 6, SpringLayout.SOUTH, btnAddProject);
		sl_buttonPanel.putConstraint(SpringLayout.WEST, btnFindProject, 42, SpringLayout.WEST, buttonPanel);
		sl_buttonPanel.putConstraint(SpringLayout.EAST, btnFindProject, 0, SpringLayout.EAST, lblManagingProjects);
		buttonPanel.add(btnFindProject);
		
		JButton btnUpcomingActivity = new JButton("Upcoming Activities");
		sl_buttonPanel.putConstraint(SpringLayout.NORTH, btnUpcomingActivity, 6, SpringLayout.SOUTH, btnFindProject);
		sl_buttonPanel.putConstraint(SpringLayout.WEST, btnUpcomingActivity, 42, SpringLayout.WEST, buttonPanel);
		sl_buttonPanel.putConstraint(SpringLayout.WEST, btnUpcomingActivity, 0, SpringLayout.WEST, lblManagingProjects);
		btnUpcomingActivity.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Upcoming activities selected");
				buildActivitiesPanel();
			}
		});
		buttonPanel.add(btnUpcomingActivity);
		
		//Table for current projects ordered by time in south
		cohorts = this.controller.getAll();
		proTable = new ListTableModel<Cohort>(Cohort.class, cohorts, "Project", "EndDate" ,"StartDate");
		JTable table = new JTable(proTable);
		Utilities.setTableDefaults(table);
//		this.container.add(new JScrollPane( table ),BorderLayout.SOUTH);
		
		centerPanel.add(buttonPanel, BorderLayout.CENTER);
		centerPanel.add(new JScrollPane( table ),BorderLayout.SOUTH);
		this.container.add(centerPanel, BorderLayout.CENTER);
	}
	
	public boolean refresh(){
		cohorts = this.controller.getAll();
		if (cohorts.size() > 0){
			proTable.importCollection(cohorts);
			return true;
		}
		return false;
	}

	protected void buildAddProjectUI() {
		this.subFrame = new JFrame();
		subFrame.setLayout(new BorderLayout());
		CohortAddView ap = new CohortAddView(this.subFrame);
		ap.setDataset(this);
		subFrame.getContentPane().add(ap,BorderLayout.CENTER );
		subFrame.pack();
		subFrame.setVisible(true);	
	}
	
	protected void buildActivitiesPanel() {
		// TODO Create a class that will list the activities in order
	}

	/**
	 * Builds the section and defines the logic responsible for building the find dialog
	 */
	protected void buildFindDialog() {
		// Create a panel on the side to present dialog to add values
		findPanel = new JPanel(new BorderLayout());
		Dimension panelD = new Dimension(300,400); 
		findPanel.setPreferredSize(panelD);
		
		//Build Button to start find operations
		JButton btnFind = new JButton("Find");
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Cohort c = findCohort.getToInspect();
				System.out.println(c);
			}
		});
		JButton btnClose = new JButton("close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				findPanel.setVisible(false);
				subFrame.dispose();
			}
		});
		JPanel fBtnPanel = new JPanel();
		
		fBtnPanel.add(btnFind);
		fBtnPanel.add(btnClose);
		
		findPanel.add(fBtnPanel, BorderLayout.SOUTH);
		
		findCohort = new SwingMetawidget();
		findCohort.setConfig( "org/csp/views/metawidget.xml" );
		
		//Create Cohort with initialized project for inspection
		Cohort cFind = new Cohort();
		cFind.setProject(new Project());
		
		findCohort.setToInspect( cFind );		
		findCohort.addWidgetProcessor( new BeansBindingProcessor(
				new BeansBindingProcessorConfig()
				.setUpdateStrategy( UpdateStrategy.READ_WRITE )) );
		findPanel.add(findCohort ,BorderLayout.CENTER);
		
		this.subFrame = new JFrame();
		subFrame.setLayout(new BorderLayout());
		this.subFrame.add(findPanel, BorderLayout.CENTER);
		subFrame.pack();
		subFrame.setVisible(true);
		
	}
}
