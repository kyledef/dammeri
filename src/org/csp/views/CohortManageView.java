package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.csp.controller.CohortController;
import org.csp.controller.SettingController;
import org.csp.models.Cohort;
import org.csp.utilities.ListTableModel;
import org.metawidget.inspector.InspectionResultConstants;
import org.metawidget.inspector.annotation.UiAction;
import org.metawidget.inspector.annotation.UiAttribute;
import org.metawidget.inspector.annotation.UiHidden;
import org.metawidget.swing.SwingMetawidget;

import com.toedter.calendar.JDateChooser;

public class CohortManageView extends CohortView implements IRefreshable {
	//TODO For future release consider 
	
	JFrame subFrame;
	private JPanel panelSec;
	private SwingMetawidget currCohortWidget;
	private JButton saveBtn;
	private JButton deleteBtn;
	private boolean addResize = false;
	
	final int DETAIL_WIDTH = 550;
	final int DETAIL_HEIGHT = 620;
	final int RESULT_HEIGHT = 620;
	private JTextField nameField;
	private JDateChooser startField;
	private JDateChooser endField;
	
	public CohortManageView(Container container) {
		super(container);
	}
	
	public CohortManageView(Container container, boolean useLeftPanel,
			boolean readOnly, Cohort model, boolean useButtons) {
		super(container, useLeftPanel, readOnly, model, useButtons);
	}

	public CohortManageView(Container container, boolean useLeftPanel,
			boolean readOnly, Cohort model) {
		super(container, useLeftPanel, readOnly, model);
	}

	public CohortManageView(Container container, boolean useLeftPanel,boolean readOnly) {
		super(container, useLeftPanel, readOnly);
	}

	public CohortManageView(Container container, boolean useLeftPanel) {
		super(container, useLeftPanel);
	}

	@Override
	protected void configureModel() {
		super.configureModel();
		this.mTModel = new ListTableModel<Cohort>(Cohort.class, controller.getAll(), "Project", "EndDate", "StartDate" );
	}
	
	/**
	 * Overrides the buildRightPanel of the parent class
	 * Creates the additional section needed to display the details of project
	 * Though the name is right it actually refers to the center of the panel in this form
	 */
	protected void buildRightPanel(){
		panelRight = new JPanel(new BorderLayout());
		panelRight.setBorder( BorderFactory.createEmptyBorder( COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING ) );
		container.add( panelRight, BorderLayout.CENTER );
		
		JPanel panel = new JPanel(new BorderLayout());
		panelRight.add(panel ,BorderLayout.NORTH);
		if (mMetawidget == null)mMetawidget = new SwingMetawidget();		
		SwingMetawidget btnsWidget = new SwingMetawidget();		
		this.buildBtnReflection(btnsWidget);
		panel.add(btnsWidget, BorderLayout.NORTH);
		
		JPanel filters = new JPanel();
		panel.add(filters, BorderLayout.CENTER);
//		setUpFilters(filters);
		
		panel.add(this.createResultsSection(RESULT_HEIGHT) , BorderLayout.PAGE_END);		
		setUpSection();
	}
	
	protected void setUpSection(){
		if (panelSec != null)if (panelSec.getComponentCount() > 0)panelSec.removeAll();	
		panelSec = new JPanel(new BorderLayout());
		panelSec.setBorder( BorderFactory.createEmptyBorder( COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING ) );
		this.panelRight.getParent().add(panelSec,BorderLayout.EAST);
		panelSec.setPreferredSize(new Dimension(DETAIL_WIDTH,DETAIL_HEIGHT));		
	}
	
	protected void setUpFilters(JPanel panel){
		panel.setLayout(new GridLayout(1,3));
		
		nameField = new JTextField();
		panel.add(nameField);
		nameField.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				filter();
			}
		});
		
		startField = new JDateChooser();
		panel.add(startField);
		startField.getDateEditor().addPropertyChangeListener(
			    new PropertyChangeListener() {
			        public void propertyChange(PropertyChangeEvent e) {
			        	filter();
			        }
			    });
		
		endField = new JDateChooser();
		panel.add(endField);
		endField.getDateEditor().addPropertyChangeListener(
	    new PropertyChangeListener() {
	        public void propertyChange(PropertyChangeEvent e) {
	        	filter();
	        }
	    });
	}
	
	public void filter(){
		if (nameField != null && startField !=  null && endField != null){
			String  name = nameField.getText();
			Date end = endField.getDate();
			Date start = startField.getDate();
			
			ArrayList<Cohort> list = new ArrayList<Cohort>();
			try{
				if (end != null && start != null){
					if (start.after(end))JOptionPane.showMessageDialog(container, "Start date cannot be after ending date");
					else{
						ArrayList<Cohort> results = (ArrayList<Cohort>)((CohortController) this.controller).getByDate(start, end);
						if (results.size() > 0){
							if (name != null && !name.equals("")){
								Iterator<Cohort> iC = results.iterator();
								while(iC.hasNext()){
									Cohort c = iC.next();
									if (c.getProject().getName().contains(name))
										list.add(c);
								}
							}
						} 
					}
				}
			}catch(Exception e){
				e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
			}
			System.out.println(list.size());
		}
	}
	
	@UiAction
	public void clearFilter(){
		this.refresh();
	}
	
	/**
	 * Creates a new Frame with the fields to search for a Cohort
	 */
	@UiAction
	public void findProject(){	
		if (!panelSec.isVisible())setUpSection();
		if (panelSec.getComponentCount() > 0)panelSec.removeAll();
		final CohortView cView = new CohortView(panelSec,false,false);
		
		//Set to use the Model of the ManageView
		cView.setmTModel(mTModel);
		JPanel btnPanel = new JPanel();
		panelSec.add(btnPanel, BorderLayout.SOUTH);
		
		//Configure Find
		JButton btn = new JButton("Search");
		btnPanel.add(btn);
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { cView.find(); }
		});
		
		btn = new JButton("Clear");
		btnPanel.add(btn);
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { cView.clear(); }
		});		
		
	}
	
	@UiAction
	@UiAttribute( name = InspectionResultConstants.HIDDEN, value = "${this.readOnly}" )
	public void addNewProject(){
		if (!panelSec.isVisible())setUpSection();
		buildCohortView(null);	
	}
	
	@Override
	public boolean refresh() {
		this.mTModel.importCollection(this.controller.getAll());
		return true;
	}
	
	protected void handleModelSelect(final Cohort c){
		this.buildCohortView(c);
	}
	
	protected void addDetailsButtons(JPanel btnPanel){
		saveBtn = new JButton("Update");
		saveBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (currCohortWidget != null && currCohortWidget.getToInspect() != null){
					CohortController cc = CohortController.getInstance();
					try{
						//TODO not updating as expected
						cc.save((Cohort) currCohortWidget.getToInspect() );
						JOptionPane.showMessageDialog(container, "Project Updated" );
					}catch(Exception e){  JOptionPane.showMessageDialog(container, "Unable to update Project" ); e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);}
				}else{
					JOptionPane.showMessageDialog(container, "No Project Selected" );
				}
			}
		});
		btnPanel.add(saveBtn);
			deleteBtn = new JButton("Delete");
			deleteBtn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (currCohortWidget != null && currCohortWidget.getToInspect() != null){
						CohortController cc = CohortController.getInstance();
						try{
							if (cc.delete((Cohort) currCohortWidget.getToInspect() ))
								JOptionPane.showMessageDialog(container, "Project Deleted" );
							else JOptionPane.showMessageDialog(container, "Unable to delete Project" ); 
						}catch(Exception e){ JOptionPane.showMessageDialog(container, "Unable to delete Project" );  }
					}else{
						JOptionPane.showMessageDialog(container, "No Project Selected" );
					}
				}
			});
		btnPanel.add(deleteBtn);
		
	}
	
	protected void buildCohortView(Cohort c){
		
		//Attempt to detect if full screen. will change size if not
		if (container instanceof JFrame)			
			if (((JFrame)container).getExtendedState() == JFrame.MAXIMIZED_BOTH)
				addResize = false;
		
		//Resize window to accommodate the panel for editing or adding Cohorts
		if (addResize){
			Dimension d = container.getSize();
			d.setSize(d.getWidth() + 400, d.getHeight() + 300);
			container.setSize(d);
			addResize = false; //Set to false to prevent from resizing on next load
		}
		
		if (panelSec.getComponentCount() > 0)panelSec.removeAll();	
		
		panelSec.setPreferredSize(new Dimension(DETAIL_WIDTH,DETAIL_HEIGHT));		
		CohortTabbedView ctv = new CohortTabbedView(panelSec, false, this.readOnly,c);
		ctv.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				refresh();
			}
		});
//		if (c != null)ctv.loadCohort(c);		
		panelSec.repaint();
	}
	
	@UiHidden
	public boolean isReadOnly() {
		return this.readOnly;
	}
}
