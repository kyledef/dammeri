package org.csp.views;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.ListSelectionModel;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.csp.controller.BeneficiaryController;
import org.csp.controller.CohortController;
import org.csp.controller.ObjectiveController;
import org.csp.controller.QuestionActivityController;
import org.csp.controller.QuestionOutcomeController;
import org.csp.controller.QuestionOutputController;
import org.csp.controller.SettingController;
import org.csp.models.Activity;
import org.csp.models.Beneficiary;
import org.csp.models.Cohort;
import org.csp.models.Indicator;
import org.csp.models.Input;
import org.csp.models.Member;
import org.csp.models.Objective;
import org.csp.models.Organization;
import org.csp.models.Outcome;
import org.csp.models.Output;
import org.csp.models.Project;
import org.csp.models.Question;
import org.csp.models.QuestionActivity;
import org.csp.models.QuestionOutcome;
import org.csp.models.QuestionOutput;
import org.csp.utilities.ListTableModel;
import org.metawidget.swing.SwingMetawidget;

public class ReportsManageView2 extends CohortView implements ActionListener,ItemListener {
	JPanel containerPanel;
	protected JPanel panelRight;
	private int WIDTH = 650;
	private int PWIDTH = 130;
	private int HEIGHT = 510;
	private JPanel subPanel;
	private boolean addResize;
	private boolean customized = false;
	private JLabel info;
	private int currScreen;
	protected JPanel centerPanel;
	private static final int buttonWidth = 110;
	private static final int buttonHeight = 30;
	
//	protected Cohort cohort;
	
//	protected ListTableModel<Cohort> mTModel;
//	protected JTable table;
	
	String standardString = "Standard";
	String customString = "Custom";
	String viewString = "View";
	String exportString = "Export";
	boolean export;
	
	private JCheckBox [] boxes;
	private JCheckBox objChk;
	private JCheckBox actChk;
	private JCheckBox outChk;
	private JCheckBox questChk;
	private JCheckBox indicChk;
	private JCheckBox inputChk;
	private JCheckBox outputChk;
	private JButton btnPrevious;
	private JButton btnNext;
	private JButton btnExit;
	
	public ReportsManageView2(Container container, boolean useLeftPanel,boolean readOnly) {
		super(container, useLeftPanel, readOnly);
	}
	public ReportsManageView2(Container container, boolean useLeftPanel,boolean readOnly, Cohort model, boolean useButtons) {
		super(container, useLeftPanel, readOnly, model, useButtons);
	}
	public ReportsManageView2(Container container, boolean useLeftPanel,boolean readOnly, Cohort model) {
		super(container, useLeftPanel, readOnly, model);
	}
	public ReportsManageView2(Container container, boolean useLeftPanel) {
		super(container, useLeftPanel);
	}
	public ReportsManageView2(Container container) {
		super(container);
	}

	@Override
	protected void configureModel() {
		super.configureModel();
		this.mTModel = new ListTableModel<Cohort>(Cohort.class, controller.getAll(), "Project", "EndDate", "StartDate" );
		this.model = new Cohort();
	}

	protected void init(){	
		super.init();
		addResize = true;
		panelRight = new JPanel(new BorderLayout());
		panelRight.setBorder( BorderFactory.createEmptyBorder( COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING, COMPONENT_SPACING ) );
		container.add( panelRight, BorderLayout.CENTER );
		
		centerPanel = new JPanel(new BorderLayout());
		panelRight.add(centerPanel, BorderLayout.CENTER);
		
		boxes = new JCheckBox[8];
	}
	
	protected void buildRightPanel(){
		buildNavPanel(this.panelRight);		
		setUpInfoPane(this.panelRight);
		buildSearchPanel(this.panelRight);
	}
	
	protected void buildSearchPanel(JPanel panel){
		JPanel searchPanel = new JPanel();
		panel.add(searchPanel, BorderLayout.WEST);
//		ReportFunderViewBy rfv = new ReportFunderViewBy(searchPanel, false, this.table, false);
		
	}
	
	protected void setUpInfoPane(JPanel panel){
		JPanel infoPanel = new JPanel();
		panel.add(infoPanel, BorderLayout.NORTH);
		info = new JLabel();
		infoPanel.add(info);
		info.setHorizontalAlignment(JLabel.CENTER);
		currScreen = 1;
		//setup result section -> placed here to ensure only done once
		this.createResultsSection(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				//TODO Improve Here (does not consistently work)
				System.out.println("Detected");
				try{model = controller.read(e.getID());}
				catch(Exception ex){ ex.printStackTrace();}
			}
		},new ActionListener(){public void actionPerformed(ActionEvent e) { }});
		
		this.buildProjectList();
	}
	
	protected void buildNavPanel(JPanel panel){
		JPanel navBtnPanel = new JPanel();
		panel.add(navBtnPanel, BorderLayout.PAGE_END);
		
		btnPrevious = new JButton("< Previous");
		btnPrevious.setEnabled(false);
		navBtnPanel.add(btnPrevious);
		
		btnNext = new JButton("Next >");
		navBtnPanel.add(btnNext);
		btnExit = new JButton("Exit");
		navBtnPanel.add(btnExit);
		
		btnPrevious.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				currScreen--;
				if (currScreen < 2)btnPrevious.setEnabled(false);
				else btnPrevious.setEnabled(true);
				if (currScreen < 4)btnNext.setEnabled(true);
				if (currScreen == 3){ 
					buildOptionsFormat(customized);
				}else if (currScreen == 2){
					btnNext.setText(" Next >");
					buildQueryChoice();
				}else if (currScreen == 1){
					buildProjectList();
				}
			}
		});
		
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (model.getId() > 0){
					currScreen++;
					if (currScreen > 1) btnPrevious.setEnabled(true);
					if (currScreen == 3){
						btnNext.setText("View Project");
						buildOptionsFormat(customized);
					}else if (currScreen == 2){
						btnNext.setText(" Next >");
						buildQueryChoice();
					}else if (currScreen == 4){
						showResults();
						btnNext.setEnabled(false);
					}
				}else
					JOptionPane.showMessageDialog(null, "First Select Project before proceeding");
			}
		});
		
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				container.setVisible(false);
			}
		});
	}
	
	protected void buildProjectList(){
		info.setText("Step 1 of 4: Select the project");
		centerPanel.removeAll();
		centerPanel.add(this.scrollResults, BorderLayout.CENTER);	
		centerPanel.repaint();
	}
	
	protected void buildQueryChoice(){
		info.setText("Step 2 of 4: Select the type of Query");
		
		centerPanel.removeAll();
		JPanel btnPanel = new JPanel(new GridLayout(0, 1));
		centerPanel.add(btnPanel, BorderLayout.CENTER);
		
		JRadioButton stndBtn = new JRadioButton(standardString);
		btnPanel.add(stndBtn);
		stndBtn.setMnemonic(KeyEvent.VK_S);
		stndBtn.setActionCommand(standardString);
		stndBtn.addActionListener(this);
		
		JRadioButton custBtn = new JRadioButton(customString);
		btnPanel.add(custBtn);
		custBtn.setMnemonic(KeyEvent.VK_C);
		custBtn.setActionCommand(customString);
		custBtn.addActionListener(this);
		
		ButtonGroup group = new ButtonGroup();
		group.add(stndBtn);
		group.add(custBtn);
		
		if (this.customized)custBtn.setSelected(true);
		else stndBtn.setSelected(true);
		
		centerPanel.repaint();
	}
	
	protected void buildOptionsFormat(boolean choices){		
		StringBuilder stb = new StringBuilder();
		stb.append("Step 3 of 4: Select Options for ");
		if (choices) stb.append("Customized ");
		else stb.append("Standard ");			
		stb.append("Query");
		
		info.setText(stb.toString());
		centerPanel.removeAll();
		
		if (choices){
			JPanel checkPanel = new JPanel(new GridLayout(0,1));
			centerPanel.add(checkPanel, BorderLayout.CENTER);
			
			if (this.boxes == null)this.boxes = new JCheckBox[8];//Array to facilitate functionality for check all
			int i = 0;
			
			JCheckBox all = new JCheckBox("Select All");
			all.addItemListener(this);
			checkPanel.add(all);
			
			if (objChk == null){
				objChk = new JCheckBox("Objectives");
				this.boxes[i++] = objChk; //Add to an array of check-boxes to facilitate functionality for check all
				objChk.setSelected(true);
			}
			checkPanel.add(objChk);
			objChk.addItemListener(this);
			
			if (actChk == null){
				actChk = new JCheckBox("Activities");
				this.boxes[i++] = actChk; 
				actChk.setSelected(true);
			}
			checkPanel.add(actChk);
			actChk.addItemListener(this);
			
			if (outChk == null){
				outChk = new JCheckBox("Outcomes");
				this.boxes[i++] = outChk; 
				 outChk.setSelected(true);
			}
			checkPanel.add(outChk);
			outChk.addItemListener(this);
			
			if (questChk == null){
				questChk = new JCheckBox("Questions");
				this.boxes[i++] = questChk; 
				 questChk.setSelected(true);
			}
			checkPanel.add(questChk);
			questChk.addItemListener(this);
			
			if (indicChk == null){
				indicChk = new JCheckBox("Indicators");
				this.boxes[i++] = indicChk; 
				 indicChk.setSelected(true);
			}
			checkPanel.add(indicChk);
			indicChk.addItemListener(this);
			
			if (inputChk == null){
				inputChk = new JCheckBox("Inputs");
				this.boxes[i++] = inputChk;
				 inputChk.setSelected(true);
			}
			checkPanel.add(inputChk);
			inputChk.addItemListener(this);
			
			if (outputChk == null){
				outputChk = new JCheckBox("Outputs");
				this.boxes[i++] = outputChk; 
				 outputChk.setSelected(true);
			}
			checkPanel.add(outputChk);
			outputChk.addItemListener(this);
		}
		
		JPanel radioPanel = new JPanel(new GridLayout(0,1));
		radioPanel.setPreferredSize(new Dimension(250,200));
		centerPanel.add(radioPanel, BorderLayout.WEST);
		JRadioButton viewBtn = new JRadioButton(viewString);
		radioPanel.add(viewBtn);
		viewBtn.setActionCommand(viewString);
		viewBtn.addActionListener(this);
		
		JRadioButton exportBtn = new JRadioButton(exportString);
		radioPanel.add(exportBtn);
		exportBtn.setActionCommand(exportString);
		exportBtn.addActionListener(this);
		
		ButtonGroup group = new ButtonGroup();
		group.add(viewBtn);
		group.add(exportBtn);
		
		if (this.export)
			exportBtn.setSelected(true);
		else
			viewBtn.setSelected(true);
		
		centerPanel.repaint();
	}
	
	protected void showResults(){
		info.setText("Project Details");
		centerPanel.removeAll();
		if (this.model != null && this.model.getId() > 0){
			
			if (!this.customized){
				if (this.export)
					exportStandard(centerPanel);
				else
					displayStandard(centerPanel);
			}else{
				if (this.export)
					exportCustom(centerPanel);
				else
					displayCustom(centerPanel);
			}
		}else
			JOptionPane.showMessageDialog(null, "No model selected");
		
		centerPanel.repaint();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		if (command.equals(this.customString))
			customized = true;
		else if (command.equals(this.standardString))
			customized = false;
		else if (command.equals(this.viewString))
			export = false;
		else if (command.equals(this.exportString))
			export = true;
	}
	
	@Override
	public void itemStateChanged(ItemEvent e) {
		JCheckBox cb = (JCheckBox) e.getItem();

		if (e.getStateChange() == ItemEvent.SELECTED){
			if (cb.getText().equalsIgnoreCase("Select All"))
				for(int i =0;i < boxes.length; i++)
					if (boxes[i] != null)
						boxes[i].setSelected(true);
		}else{
			if (cb.getText().equalsIgnoreCase("Select All"))
				for(int i =0;i < boxes.length; i++)
					if (boxes[i] != null)
						boxes[i].setSelected(false);
		}
	}
	
	protected void displayCustom(JPanel panel) {
		JPanel infoPane = new JPanel();
		infoPane.setLayout(new BoxLayout(infoPane, BoxLayout.Y_AXIS));
		panel.add(new JScrollPane(infoPane));
		
		SwingMetawidget cohortWidget = new SwingMetawidget();
		cohortWidget.setConfig( "/org/csp/views/metawidget.xml" );
		cohortWidget.setToInspect( model );	
		cohortWidget.setReadOnly(true);		
		infoPane.add(cohortWidget);
		infoPane.add(new JSeparator(SwingConstants.HORIZONTAL));
		
		if (this.objChk != null && this.objChk.isSelected()){
			infoPane.add(new JLabel("Objectives"));
			infoPane.add(displayObjectives());
			infoPane.add(new JSeparator(SwingConstants.HORIZONTAL));
		}
		if (this.outChk != null && this.outChk.isSelected()){
			infoPane.add(new JLabel("Outcomes"));
			infoPane.add(displayOutcomes());
			infoPane.add(new JSeparator(SwingConstants.HORIZONTAL));
		}
		
		if (this.questChk != null && this.questChk.isSelected()){
			infoPane.add(new JLabel("Questions"));
			infoPane.add(displayQuestions());
			infoPane.add(new JSeparator(SwingConstants.HORIZONTAL));
		}
		
		if (this.indicChk != null && this.indicChk.isSelected()){
			infoPane.add(new JLabel("Indicators"));
			infoPane.add(displayIndicators());
			infoPane.add(new JSeparator(SwingConstants.HORIZONTAL));
		}
		
		if (this.actChk != null && this.actChk.isSelected()){
			infoPane.add(new JLabel("Activities"));
			infoPane.add(displayActivities());
			infoPane.add(new JSeparator(SwingConstants.HORIZONTAL));
		}
		
		if(this.inputChk != null && this.inputChk.isSelected()){
			infoPane.add(new JLabel("Input"));
			infoPane.add(displayInputs());
			infoPane.add(new JSeparator(SwingConstants.HORIZONTAL));
		}
		
		if (this.outChk != null && this.outChk.isSelected()){
			infoPane.add(new JLabel("Output"));
			infoPane.add(displayOutput());
			infoPane.add(new JSeparator(SwingConstants.HORIZONTAL));
		}
	}
	
	protected void exportCustom(JPanel centerPanel2) {
		try{
			XSSFWorkbook wb = new XSSFWorkbook();
			
			Cohort c = model;
			
			wb = exportDetailsIndivid(wb, c);
			
			wb = exportBeneficiariesIndivid(wb,c);
			
			if (this.objChk != null && this.objChk.isSelected())
				wb = exportObjectivesIndivid(wb,c);
			
			if (this.actChk != null && this.actChk.isSelected())
				wb = exportActivitiesIndivid(wb,c);
			
			if (this.outChk != null && this.outChk.isSelected())
				wb = exportOutcomesIndivid(wb,c);
			
			if (this.questChk != null && this.questChk.isSelected())
				wb = exportQuestionsIndivid(wb,c);
			
			if (this.indicChk != null && this.indicChk.isSelected())
				wb = exportIndicatorsIndivid(wb, c);
			
			if(this.inputChk != null && this.inputChk.isSelected())
				wb = exportInputs(wb, c);
			
			
			String fileName = "cohorts-"+c.getId()+"-custom.xlsx";
			
			JFileChooser fc = new JFileChooser();
			fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			 if (fc.showOpenDialog(this.container) == JFileChooser.APPROVE_OPTION){
				 File file = fc.getSelectedFile();
				 if (file.exists() && file.isDirectory()){
					 FileOutputStream out = new FileOutputStream(file.getAbsolutePath() +"/"+fileName);						
					 wb.write(out);
					 out.close();
				 }
			 }
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
	}
	
	//TODO Improve individual sheet Exports
		
	protected void exportStandard(JComponent panel){
		try{
			XSSFWorkbook wb = new XSSFWorkbook();
			
			CohortController cc = CohortController.getInstance(SettingController.getInstance().getConnection());
			Collection<Cohort> cols = cc.getAll();
			Iterator<Cohort> iCol = cols.iterator();
			while (iCol.hasNext()){
				Cohort c = iCol.next();
				wb = exportDetailsIndivid(wb, c);
				wb = exportBeneficiariesIndivid(wb,c);
				wb = exportObjectivesIndivid(wb,c);
				wb = exportActivitiesIndivid(wb,c);
				wb = exportOutcomesIndivid(wb,c);
				
				String fileName = "cohorts-"+c.getId()+"-standard.xlsx";
				
				JFileChooser fc = new JFileChooser();
				fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				 if (fc.showOpenDialog(this.container) == JFileChooser.APPROVE_OPTION){
					 File file = fc.getSelectedFile();
					 if (file.exists() && file.isDirectory()){
						 FileOutputStream out = new FileOutputStream(file.getAbsolutePath() +"/"+fileName);						
						 wb.write(out);
						 out.close();
					 }
				 }
			}
		}catch(Exception e){e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
	}
	
	public XSSFWorkbook exportDetailsIndivid(XSSFWorkbook wb, Cohort c) throws Exception{
		return this.exportDetailsIndivid(wb, c, 0);
	}

	public XSSFWorkbook exportDetailsIndivid(XSSFWorkbook wb, Cohort c, int rowNum) throws Exception{
		Sheet sheet;
		Row row;
		Cell cell;

		String [] cohortFields= {"id","project","startDate","endDate","funder", "totalBudget", "teamLead"};
		String [] projectFields = {"name","description","location"};

		sheet = wb.createSheet("Details");
		
		row = sheet.createRow(rowNum);
		
		row.createCell(0).setCellValue("FIELD");
		row.createCell(1).setCellValue("VALUE");
		
		rowNum++;
		
		for(int i = 0; i < cohortFields.length; i++, rowNum++){
			row = sheet.createRow(rowNum);
			String fStr = cohortFields[i];				
			if (fStr.equals("project")){
				Project p = c.getProject();
				
				for(int j = 0; j < projectFields.length; j++){
					cell = row.createCell(0);
					cell.setCellValue(projectFields[j].toUpperCase());
					
					cell = row.createCell(1);
					Field f = p.getClass().getDeclaredField(projectFields[j]);
					f.setAccessible(true);
					if (f.get(p) != null)
						cell.setCellValue(f.get(p).toString());
					else 
						cell.setCellValue("");
					
					rowNum++;
					row = sheet.createRow(rowNum);
				}
			}else if (fStr.equals("funder")){
				Organization funder = c.getFunder();
				if (funder != null){
					cell = row.createCell(0);
					cell.setCellValue("FUNDER");
					cell = row.createCell(1);
					cell.setCellValue(funder.getName());
				}
			}else if (fStr.equals("teamLeader")){
				Member teamL = c.getTeamLead();
				if (teamL != null){
				
					cell = row.createCell(0);
					cell.setCellValue("teamLeader".toUpperCase());
					cell = row.createCell(1);
					cell.setCellValue(teamL.toString());
				}
			}else{
				cell = row.createCell(0);
				cell.setCellValue(fStr.toUpperCase());
				
				cell = row.createCell(1);
				Field f = c.getClass().getDeclaredField(fStr);
				f.setAccessible(true);
				if (f.get(c) != null)
					cell.setCellValue(f.get(c).toString());
				else 
					cell.setCellValue("");
			}
		}
			
		return wb;
	}
	
	public XSSFWorkbook exportBeneficiariesIndivid(XSSFWorkbook wb, Cohort c) throws Exception{
		Sheet sheet = wb.createSheet("Beneficiaries");
		Row row;
		Cell cell;
		int rowNum;

		Iterator<Beneficiary> iBen =  c.getBeneficiaries().iterator();
		
		row = sheet.createRow(0);
		int col = 0;

		Collection<String> beHead = BeneficiaryController.getInstance().extractField(new Beneficiary("","","")).keySet();
		Iterator<String> ib = beHead.iterator();
		
		while (ib.hasNext()){
			row.createCell(col++).setCellValue(ib.next().toUpperCase());
		}
		
		rowNum = 1;
		while (iBen.hasNext()){
			Beneficiary b = iBen.next();
			
			row = sheet.createRow(rowNum++);
			col = 0;
			ib = beHead.iterator();
			while (ib.hasNext()){
				String fStr = ib.next();
				cell = row.createCell(col++);
				Field f = b.getClass().getDeclaredField(fStr);
				f.setAccessible(true);
				if (f.get(b) != null)
					cell.setCellValue(f.get(b).toString());
				else 
					cell.setCellValue("");
			}
		}

		return wb;
	}

	public XSSFWorkbook exportObjectivesIndivid(XSSFWorkbook wb, Cohort c) throws Exception{
		Sheet sheet;
		Row row;
		Cell cell;
		int rowNum;

		sheet = wb.createSheet("Objectives");
			
		Iterator<Objective> iObj = c.getProject().getObjectives().iterator();
		row = sheet.createRow(0);
		int col = 0;

		Collection<String> ohead = ObjectiveController.getInstance().extractField(new Objective("")).keySet();
		Iterator<String> io = ohead.iterator();
		while (io.hasNext()){
			row.createCell(col++).setCellValue(io.next().toUpperCase());
		}
		rowNum = 1;
		while(iObj.hasNext()){
			Objective o = iObj.next();
			row = sheet.createRow(rowNum++);
			col = 0;
			io = ohead.iterator();
			while (io.hasNext()){
				String fStr = io.next();
				cell = row.createCell(col++);
				Field f = o.getClass().getDeclaredField(fStr);
				f.setAccessible(true);
				if (f.get(o) != null)
					cell.setCellValue(f.get(o).toString());
				else 
					cell.setCellValue("");
			}
		}
		return wb;
	}

	public XSSFWorkbook exportActivitiesIndivid(XSSFWorkbook wb, Cohort c) throws Exception{
		Sheet inActSheet, outActSheet;
		
		Row inRow, outRow;
		int inRowNum, outRowNum;

		inActSheet = wb.createSheet("Activities (with Inputs)");
		outActSheet =wb.createSheet("Activities (with Outputs)");

		inRow = inActSheet.createRow(0);
		inRow.createCell(0).setCellValue("ACTIVITY");
		inRow.createCell(1).setCellValue("INPUT");
		inRow.createCell(2).setCellValue("COST");
		inRow.createCell(3).setCellValue("INPUT TYPE");

		outRow = outActSheet.createRow(0);
		outRow.createCell(0).setCellValue("ACTIVITY");
		outRow.createCell(1).setCellValue("OUTPUT");
		outRow.createCell(2).setCellValue("OUTPUT TYPE");
		
		
		inRowNum = 1;
		outRowNum = 1;
		
		int	maxInActRow = 1,
			maxOutActRow= 1;
		
		
		Iterator <Activity> iAct = c.getActivities().iterator();

		while (iAct.hasNext()){
			inRow = inActSheet.createRow(inRowNum);
			outRow = outActSheet.createRow(outRowNum);
			
			Activity a = iAct.next();
			inRow.createCell(0).setCellValue(a.getActivity());
			outRow.createCell(0).setCellValue(a.getActivity());
			

			Iterator<Input> inputs = a.getInputs().iterator();
			while (inputs.hasNext()){
				Input input = inputs.next();
				
				inRow.createCell(1).setCellValue(input.getInput());
				inRow.createCell(2).setCellValue(input.getCost());
				inRow.createCell(3).setCellValue(input.getType());
				++inRowNum;
				
				inRow = inActSheet.getRow(inRowNum);
				if (inRow == null)inRow = inActSheet.createRow(inRowNum);
				System.out.println(inRowNum);
				
				if (inRowNum > maxInActRow)maxInActRow = inRowNum;
			}

			
			
			//Output delayed because it overwrites the existing data
			
			Iterator<Output> outputs = a.getOutputs().iterator();
			while (outputs.hasNext()){
				Output output = outputs.next();
				
				outRow.createCell(1).setCellValue(output.getOutput());
				outRow.createCell(2).setCellValue(output.getType());
				++outRowNum;
				outRow = outActSheet.getRow(outRowNum);
				if (outRow == null)
					outRow = outActSheet.createRow(outRowNum);
				
				if (outRowNum > maxOutActRow)maxOutActRow = outRowNum;
			}
			
			inRowNum = maxInActRow;
			inRowNum++;
			
			outRowNum = maxOutActRow;
			outRowNum++;
		}
		
		return wb;
	}

	public XSSFWorkbook exportOutcomesIndivid(XSSFWorkbook wb, Cohort c) throws Exception{
		Sheet sheet = wb.createSheet("Outcomes");
		Row row;
		
		row = sheet.createRow(0);
		row.createCell(0).setCellValue("OUTCOME");
		row.createCell(1).setCellValue("TIMEFRAME");
		row.createCell(2).setCellValue("TYPE");
		row.createCell(3).setCellValue("M&E Question");
		
		int rowNum = 1;
		
		Iterator<Outcome>outcomes = c.getOutcomes().iterator();
		while (outcomes.hasNext()){
			Outcome o = outcomes.next();
			row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(o.getOutcome());
			row.createCell(1).setCellValue(o.getTimeframe());
			row.createCell(2).setCellValue(o.getType());
			
			if (o.getQuestionOutcomes().size() > 0){
				Iterator<QuestionOutcome> qos = o.getQuestionOutcomes().iterator();
				while (qos.hasNext()){
					QuestionOutcome qo = qos.next();
					row.createCell(3).setCellValue(qo.getQuestion().getQuestion());
					row = sheet.createRow(rowNum++);
				}
			}
		}
		return wb;
	}
	
	public XSSFWorkbook exportQuestionsIndivid(XSSFWorkbook wb, Cohort c){
		Sheet sheet = wb.createSheet("Questions");
		Row row;
		int rowNum = 1;
		
		row = sheet.createRow(0);
		row.createCell(0).setCellValue("Question");
		row.createCell(1).setCellValue("Type");
		row.createCell(2).setCellValue("Indicators");
		row.createCell(3).setCellValue("No. of Recordings");
		row.createCell(4).setCellValue("Relation");
		
		Iterator<Question> questions = c.getQuestions().iterator();
		while(questions.hasNext()){
			Question q = questions.next();
			row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(q.getQuestion());
			row.createCell(1).setCellValue(q.getType());
			try{
				if (q.getRelation() != null){
					row.createCell(4).setCellValue(q.getRelation());
					if (q.getRelation().equals(Question.ACTIVITY)){
						QuestionActivity qa = new QuestionActivity();
						qa.setQuestion(q);
						Iterator<QuestionActivity> qas = QuestionActivityController.getInstance(SettingController.getInstance().getConnection()).find(qa).iterator();
						if (qas.hasNext())
							row.createCell(5).setCellValue(qas.next().getActivity().getActivity());
					}else if (q.getRelation().equals(Question.OUTCOME)){
						QuestionOutcome qout = new QuestionOutcome();
						qout.setQuestion(q);
						Iterator<QuestionOutcome> qouts = QuestionOutcomeController.getInstance(SettingController.getInstance().getConnection()).find(qout).iterator();
						if (qouts.hasNext())
							row.createCell(5).setCellValue(qouts.next().getOutcome().getOutcome());
						
					}else if (q.getRelation().equals(Question.OUTPUT)){
						QuestionOutput qo = new QuestionOutput();
						qo.setQuestion(q);
						Iterator<QuestionOutput> qos = QuestionOutputController.getInstance(SettingController.getInstance().getConnection()).find(qo).iterator();
						if (qos.hasNext())
							row.createCell(5).setCellValue(qos.next().getOutput().getOutput());
					}
				}
			}catch (Exception e) {
				e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
			}
			
			if (q.getIndicators().size() > 0){
				Iterator<Indicator> is = q.getIndicators().iterator();
				while(is.hasNext()){
					Indicator i = is.next();
					row.createCell(2).setCellValue(i.getIndicator());
					row.createCell(3).setCellValue(i.getFields().size());
					row = sheet.createRow(rowNum++);					
				}
			}
		}
		return wb;
	}
	
	public XSSFWorkbook exportIndicatorsIndivid(XSSFWorkbook wb, Cohort c){
		Sheet sheet = wb.createSheet("Indicators");
		int rowNum = 1;
//		int lastRow =1;
		int maxRow = 1;
		Row row;
		//build heading
		row = sheet.createRow(0);
		row.createCell(0).setCellValue("Question");
		row.createCell(1).setCellValue("Indicator");
		row.createCell(2).setCellValue("Type");
		row.createCell(3).setCellValue("Frequency");
		row.createCell(4).setCellValue("Source");
		row.createCell(5).setCellValue("Target");
		row.createCell(6).setCellValue("Recording Value");
		row.createCell(7).setCellValue("Date");
		
		Iterator<Question> questions = c.getQuestions().iterator();
		while (questions.hasNext()){
			Question q = questions.next();
			if (q.getIndicators().size() > 0){
				Iterator<Indicator> indicators = q.getIndicators().iterator();
				
				while (indicators.hasNext()){
					Indicator i = indicators.next();
					row = sheet.createRow(rowNum++);
					
					row.createCell(0).setCellValue(i.getQuestion().getQuestion());
					row.createCell(1).setCellValue(i.getIndicator());
					row.createCell(2).setCellValue(i.getType());
					row.createCell(3).setCellValue(i.getFrequency());
					row.createCell(4).setCellValue(i.getMeansOfVerification());
					row.createCell(5).setCellValue(i.getTarget());
					
					//Add Field Details
					if (i.getFields().size() > 0){
						Iterator<org.csp.models.Field> fields = i.getFields().iterator();
						
						while (fields.hasNext()){
							
							org.csp.models.Field f = fields.next(); //To Avoid conflict with reflection Field
							row.createCell(6).setCellValue(f.getValue());
							row.createCell(7).setCellValue(f.getCaptureDate());
							row = sheet.createRow(rowNum++);
							if (rowNum > maxRow)maxRow = rowNum;
						}
						
						rowNum = maxRow;
					}
				}
			}
		}
		
		return wb;
	}
	
	public XSSFWorkbook exportInputs(XSSFWorkbook wb, Cohort c){
		Sheet sheet = wb.createSheet("Inputs");
		int rowNum = 1;
		Row row;
		Iterator<Input> inputs = c.getInputs().iterator();
		
		row = sheet.createRow(0);
		row.createCell(0).setCellValue("Input");
		row.createCell(1).setCellValue("Type");
		row.createCell(2).setCellValue("Cost");
		row.createCell(3).setCellValue("Level"); //Activity or project
		row.createCell(4).setCellValue("Activity");
		
		while(inputs.hasNext()){
			Input i = inputs.next();
			
			row = sheet.createRow(rowNum++);
			
			row.createCell(0).setCellValue(i.getInput());
			row.createCell(1).setCellValue(i.getType());
			row.createCell(2).setCellValue(i.getCost());
			if (i.getActivity() != null && i.getActivity().getId() > 0){
				row.createCell(3).setCellValue("Activity"); //Activity or project
				row.createCell(4).setCellValue(i.getActivity().getActivity());
			}else row.createCell(3).setCellValue("Project");
		}
		return wb;
}
	
	public XSSFWorkbook exportSummary(XSSFWorkbook wb, Cohort c){
		
		return wb;
	}
	
	protected JList<String> displayBeneficiaries(){
		StringBuilder stb;
		DefaultListModel <String> benModel = new DefaultListModel<String>();
		Iterator <Beneficiary> bIter = this.model.getBeneficiaries().iterator();
		while (bIter.hasNext()){
			Beneficiary b = bIter.next();
			stb = new StringBuilder();
			stb.append("Type: ").append(b.getBeneficiaryType());
			if (b.getSex() != null)stb.append(" Sex: ").append(b.getSex());
			if (b.getAgeGroup() != null)stb.append(" Age Group: ").append(b.getAgeGroup());
			benModel.addElement(stb.toString());
		}
		JList <String>benList = new JList<String>(benModel);
		benList.setLayoutOrientation(JList.VERTICAL);
		return benList;
	}
	
	protected JList<String> displayObjectives(){
		StringBuilder stb;
		DefaultListModel <String> objModel = new DefaultListModel<String>();
		Iterator <Objective> oIter = this.model.getProject().getObjectives().iterator();
		while (oIter.hasNext()){
			Objective o = oIter.next();
			stb = new StringBuilder();
			stb.append("Objective: ").append(o.getObjective());			
			objModel.addElement(stb.toString());
		}
		JList <String>objList = new JList<String>(objModel);
		objList.setLayoutOrientation(JList.VERTICAL);
		return objList;
	}
	
	protected JList<String> displayOutcomes(){
		StringBuilder stb;
		DefaultListModel <String> outModel = new DefaultListModel<String>();
		Iterator <Outcome> outIter = this.model.getOutcomes().iterator();
		while (outIter.hasNext()){
			Outcome o = outIter.next();
			stb = new StringBuilder();
			stb.append("Outcome: ").append(o.getOutcome());
			outModel.addElement(stb.toString());
		}
		JList <String>outList = new JList<String>(outModel);
		outList.setLayoutOrientation(JList.VERTICAL);
		return outList;
	}
	
	protected JList<String> displayActivities(){
		StringBuilder stb;
		DefaultListModel <String> actModel = new DefaultListModel<String>();
		Iterator <Activity> actIter = this.model.getActivities().iterator();
		while (actIter.hasNext()){
			Activity a = actIter.next();
			stb = new StringBuilder();
			stb.append("Activities: ").append(a.getActivity());
			actModel.addElement(stb.toString());
		}
		JList <String>actList = new JList<String>(actModel);
		actList.setLayoutOrientation(JList.VERTICAL);
		return actList;
	}
	
	protected JList<String> displayQuestions(){
		StringBuilder stb;
		DefaultListModel <String> outModel = new DefaultListModel<String>();
		Iterator <Question> iter = this.model.getQuestions().iterator();
		while (iter.hasNext()){
			Question o = iter.next();
			stb = new StringBuilder();
			stb.append("Type ").append(o.getType()).append(" Question: ").append(o.getQuestion());
			outModel.addElement(stb.toString());
		}
		JList <String>outList = new JList<String>(outModel);
		outList.setLayoutOrientation(JList.VERTICAL);
		return outList;
	}
	
	protected JList<String> displayIndicators(){
		StringBuilder stb;
		DefaultListModel <String> outModel = new DefaultListModel<String>();
		Iterator <Question> ia = this.model.getQuestions().iterator();
		ArrayList<Indicator> indicators = new ArrayList<Indicator>();
		while (ia.hasNext()){
			indicators.addAll(ia.next().getIndicators());
		}
		
		Iterator <Indicator> iter = indicators.iterator();
		while (iter.hasNext()){
			Indicator o = iter.next();
			stb = new StringBuilder();
			stb.append(o.getIndicator());
			outModel.addElement(stb.toString());
		}
		JList <String>outList = new JList<String>(outModel);
		outList.setLayoutOrientation(JList.VERTICAL);
		return outList;
	}
	
	protected JList<String> displayInputs(){
		StringBuilder stb;
		DefaultListModel <String> outModel = new DefaultListModel<String>();
		Iterator <Input> iter = this.model.getInputs().iterator();
		while (iter.hasNext()){
			Input o = iter.next();
			stb = new StringBuilder();
			stb.append(o.getInput());
			if (o.getActivity() != null)stb.append("Activity: ").append(o.getActivity().getActivity());
			outModel.addElement(stb.toString());
		}
		JList <String>outList = new JList<String>(outModel);
		outList.setLayoutOrientation(JList.VERTICAL);
		return outList;
	}
	
	protected JList<String> displayOutput(){
		StringBuilder stb;
		DefaultListModel <String> outModel = new DefaultListModel<String>();
		Iterator <Activity> ia = this.model.getActivities().iterator();
		ArrayList<Output> outputs = new ArrayList<Output>();
		while (ia.hasNext()){
			outputs.addAll(ia.next().getOutputs());
		}
		
		Iterator <Output> iter = outputs.iterator();
		while (iter.hasNext()){
			Output o = iter.next();
			stb = new StringBuilder();
			stb.append(o.getOutput()).append(" Activity: ").append(o.getActivity());
			outModel.addElement(stb.toString());
		}
		JList <String>outList = new JList<String>(outModel);
		outList.setLayoutOrientation(JList.VERTICAL);
		return outList;
	}
	
	protected void displayStandard(JComponent panel){
		JPanel infoPane = new JPanel();
		infoPane.setLayout(new BoxLayout(infoPane, BoxLayout.Y_AXIS));
		panel.add(new JScrollPane(infoPane));
		
		SwingMetawidget cohortWidget = new SwingMetawidget();
		cohortWidget.setConfig( "/org/csp/views/metawidget.xml" );
		cohortWidget.setToInspect( model );	
		cohortWidget.setReadOnly(true);		
		infoPane.add(cohortWidget);
		infoPane.add(new JSeparator(SwingConstants.HORIZONTAL));
		
		//Adding Beneficiaries
		infoPane.add(new JLabel("Beneficiaries"));		
		infoPane.add(displayBeneficiaries());
		infoPane.add(new JSeparator(SwingConstants.HORIZONTAL));
		
		//Adding Objectives
		infoPane.add(new JLabel("Objectives"));
		infoPane.add(displayObjectives());
		infoPane.add(new JSeparator(SwingConstants.HORIZONTAL));
		
		//Adding Outcomes
		infoPane.add(new JLabel("Outcomes"));
		infoPane.add(displayOutcomes());
		infoPane.add(new JSeparator(SwingConstants.HORIZONTAL));
		
		//Adding Activities
		infoPane.add(new JLabel("Activities"));
		infoPane.add(displayActivities());
		infoPane.add(new JSeparator(SwingConstants.HORIZONTAL));
	}
	
	protected void buildCustomizedQuerySection(JTabbedPane tabbedPane){
		SpringLayout sl_containerPanel = new SpringLayout();
		containerPanel = new JPanel( sl_containerPanel);
		Dimension d = new Dimension(WIDTH, HEIGHT);
		containerPanel.setPreferredSize(d);
		tabbedPane.addTab("Customize Query", containerPanel);
		
		SpringLayout sl_memberPanel = new SpringLayout();
		JPanel memberPanel = new JPanel( sl_memberPanel);
		sl_containerPanel.putConstraint(SpringLayout.NORTH, memberPanel, 0, SpringLayout.NORTH, containerPanel);
		sl_containerPanel.putConstraint(SpringLayout.EAST, memberPanel, -10, SpringLayout.EAST, containerPanel);
		memberPanel.setPreferredSize(new Dimension(this.PWIDTH, this.HEIGHT));
		containerPanel.add(memberPanel, BorderLayout.CENTER);
		
		JButton btnMemberByProject = new JButton("By Project");
		btnMemberByProject.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
		btnMemberByProject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				if (subPanel == null)createSubPanel();
				else subPanel.removeAll();
				new ReportMemberViewBy(subPanel, ReportMemberViewBy.PROJECT_VIEW, false);					
				subPanel.revalidate();
			}
		});
		memberPanel.add(btnMemberByProject);
		
		JButton btnMemberByActivity = new JButton("By Activity");
		btnMemberByActivity.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				if (subPanel == null)createSubPanel();
				else subPanel.removeAll();
				
				new ReportMemberViewBy(subPanel, ReportMemberViewBy.ACTIVITY_VIEW, false);					
				subPanel.revalidate();
			}
		});
		sl_memberPanel.putConstraint(SpringLayout.NORTH, btnMemberByProject, 6, SpringLayout.SOUTH, btnMemberByActivity);
		sl_memberPanel.putConstraint(SpringLayout.EAST, btnMemberByProject, 0, SpringLayout.EAST, btnMemberByActivity);
		btnMemberByActivity.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
		memberPanel.add(btnMemberByActivity);
		
		
		SpringLayout sl_projectPanel = new SpringLayout();
		JPanel projectPanel = new JPanel( sl_projectPanel);
		sl_containerPanel.putConstraint(SpringLayout.SOUTH, projectPanel, -53, SpringLayout.SOUTH, containerPanel);
		sl_containerPanel.putConstraint(SpringLayout.NORTH, projectPanel, 0, SpringLayout.NORTH, containerPanel);
		sl_containerPanel.putConstraint(SpringLayout.WEST, projectPanel, 0, SpringLayout.WEST, containerPanel);
		projectPanel.setPreferredSize(new Dimension(this.PWIDTH, this.HEIGHT));
		containerPanel.add(projectPanel, BorderLayout.WEST);
		
		JButton btnProjectByBeneficiary = new JButton("By Beneficiary");
		btnProjectByBeneficiary.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
		btnProjectByBeneficiary.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				if (subPanel == null)createSubPanel();
				else subPanel.removeAll();
				
				new ReportProjectViewByField(subPanel, ReportProjectViewByField.BENEFICIARY_VIEW, false);				
				subPanel.revalidate();
			}
		});
		projectPanel.add(btnProjectByBeneficiary);
		
		JButton btnProjectByTime = new JButton("By Date");
		btnProjectByTime.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
		sl_projectPanel.putConstraint(SpringLayout.WEST, btnProjectByTime, 0, SpringLayout.WEST, projectPanel);
		sl_projectPanel.putConstraint(SpringLayout.WEST, btnProjectByBeneficiary, 0, SpringLayout.WEST, btnProjectByTime);
		btnProjectByTime.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (subPanel == null)createSubPanel();
				else subPanel.removeAll();
				
				new ReportProjectViewByField(subPanel, ReportProjectViewByField.DATE_VIEW, false);				
				subPanel.revalidate();
			}
		});
		projectPanel.add(btnProjectByTime);
		
		JButton btnProjectByCustom = new JButton("By Keywords");
		btnProjectByCustom.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
		btnProjectByCustom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				if (subFrame != null)subFrame.dispose();				
//				subFrame = new JFrame();
//				subFrame.setLocation(container.getLocationOnScreen());
//				new ReportProjectViewByField(subFrame, ReportProjectViewByField.KEYWORD_VIEW);
//				subFrame.pack();
//				subFrame.setVisible(true);
				
				if (subPanel == null)createSubPanel();
				else subPanel.removeAll();
				
				new ReportProjectViewByField(subPanel, ReportProjectViewByField.KEYWORD_VIEW, false);				
				subPanel.revalidate();
			}
		});
		sl_projectPanel.putConstraint(SpringLayout.WEST, btnProjectByCustom, 0, SpringLayout.WEST, projectPanel);
		sl_projectPanel.putConstraint(SpringLayout.NORTH, btnProjectByBeneficiary, 6, SpringLayout.SOUTH, btnProjectByCustom);
		sl_projectPanel.putConstraint(SpringLayout.NORTH, btnProjectByCustom, 6, SpringLayout.SOUTH, btnProjectByTime);
		projectPanel.add(btnProjectByCustom);
		
		
		
		
		SpringLayout sl_organizationPanel = new SpringLayout();
		JPanel organizationPanel = new JPanel( sl_organizationPanel);
		sl_containerPanel.putConstraint(SpringLayout.WEST, organizationPanel, 19, SpringLayout.EAST, projectPanel);
		sl_containerPanel.putConstraint(SpringLayout.WEST, memberPanel, 21, SpringLayout.EAST, organizationPanel);
		sl_containerPanel.putConstraint(SpringLayout.SOUTH, memberPanel, 0, SpringLayout.SOUTH, organizationPanel);
		sl_containerPanel.putConstraint(SpringLayout.SOUTH, organizationPanel, 157, SpringLayout.NORTH, containerPanel);
		sl_containerPanel.putConstraint(SpringLayout.NORTH, organizationPanel, 0, SpringLayout.NORTH, containerPanel);
		
		JLabel lblSearchForMembers = new JLabel("Search For Members:");
		sl_memberPanel.putConstraint(SpringLayout.NORTH, btnMemberByActivity, 6, SpringLayout.SOUTH, lblSearchForMembers);
		sl_memberPanel.putConstraint(SpringLayout.EAST, btnMemberByActivity, -10, SpringLayout.EAST, lblSearchForMembers);
		sl_memberPanel.putConstraint(SpringLayout.WEST, lblSearchForMembers, 0, SpringLayout.WEST, memberPanel);
		sl_memberPanel.putConstraint(SpringLayout.EAST, lblSearchForMembers, 130, SpringLayout.WEST, memberPanel);
		sl_memberPanel.putConstraint(SpringLayout.NORTH, lblSearchForMembers, 10, SpringLayout.NORTH, memberPanel);
		lblSearchForMembers.setHorizontalAlignment(SwingConstants.CENTER);
		memberPanel.add(lblSearchForMembers);
		
		JLabel lblSearchForProjects = new JLabel("Search For Projects:");
		sl_projectPanel.putConstraint(SpringLayout.NORTH, btnProjectByTime, 6, SpringLayout.SOUTH, lblSearchForProjects);
		lblSearchForProjects.setHorizontalAlignment(SwingConstants.CENTER);
		sl_projectPanel.putConstraint(SpringLayout.NORTH, lblSearchForProjects, 10, SpringLayout.NORTH, projectPanel);
		sl_projectPanel.putConstraint(SpringLayout.WEST, lblSearchForProjects, 0, SpringLayout.WEST, projectPanel);
		sl_projectPanel.putConstraint(SpringLayout.EAST, lblSearchForProjects, 130, SpringLayout.WEST, projectPanel);
		projectPanel.add(lblSearchForProjects);
		containerPanel.add(organizationPanel);
		organizationPanel.setPreferredSize(new Dimension(this.PWIDTH, this.HEIGHT));
		
		
		JButton btnOrganizationByProject = new JButton("By Project");
		btnOrganizationByProject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
				if (subPanel == null)createSubPanel();
				else subPanel.removeAll();
				
				new ReportFunderViewBy(subPanel, false);				
				subPanel.revalidate();								
			}
		});
		btnOrganizationByProject.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
		organizationPanel.add(btnOrganizationByProject);
		
		JLabel lblSearchForFunders = new JLabel("Search For Organizations:");
		sl_organizationPanel.putConstraint(SpringLayout.NORTH, btnOrganizationByProject, 6, SpringLayout.SOUTH, lblSearchForFunders);
		sl_organizationPanel.putConstraint(SpringLayout.WEST, btnOrganizationByProject, 10, SpringLayout.WEST, lblSearchForFunders);
		sl_organizationPanel.putConstraint(SpringLayout.NORTH, lblSearchForFunders, 10, SpringLayout.NORTH, organizationPanel);
		sl_organizationPanel.putConstraint(SpringLayout.WEST, lblSearchForFunders, 0, SpringLayout.WEST, organizationPanel);
		sl_organizationPanel.putConstraint(SpringLayout.EAST, lblSearchForFunders, 0, SpringLayout.EAST, organizationPanel);
		lblSearchForFunders.setHorizontalAlignment(SwingConstants.CENTER);
		organizationPanel.add(lblSearchForFunders);
		
		JPanel panel = new JPanel();
		sl_containerPanel.putConstraint(SpringLayout.NORTH, panel, 10, SpringLayout.SOUTH, organizationPanel);
		sl_containerPanel.putConstraint(SpringLayout.WEST, panel, 0, SpringLayout.WEST, containerPanel);
		sl_containerPanel.putConstraint(SpringLayout.EAST, panel, -10, SpringLayout.EAST, containerPanel);
		containerPanel.add(panel);
		
		JButton btnExportAllProjects = new JButton("Export All Projects");
		panel.add(btnExportAllProjects);
	}
	
	protected void buildStandardQuerySection(JTabbedPane tabbedPane){
		JPanel containerPanel = new JPanel(new BorderLayout());
		tabbedPane.addTab("Standard Query", containerPanel);
		
		CohortController cc = CohortController.getInstance(SettingController.getInstance().getConnection());
		Collection<Cohort> cs = cc.getAll();
		Cohort [] cohorts = new Cohort[cs.size()];
		cs.toArray(cohorts);
		JList <Cohort> list = new JList<Cohort>(cohorts);
		list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		
		JScrollPane listScroller = new JScrollPane(list);
		listScroller.setPreferredSize(new Dimension(250, 80));
		containerPanel.add(listScroller, BorderLayout.WEST);
		
		JPanel btnPanel = new JPanel();
		containerPanel.add(btnPanel, BorderLayout.EAST);
		
		JButton viewQuery = new JButton("View Query");
		btnPanel.add(viewQuery);
		viewQuery.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				// TODO Add functionality for standard report
			}
		});
	}
	
	protected void createSubPanel(){
		if (container instanceof JFrame)			
			if (((JFrame)container).getExtendedState() == JFrame.MAXIMIZED_BOTH)
				addResize = false;
		//Resize window to accommodate the panel for editing or adding Cohorts
		if (addResize){			
			Dimension d = container.getPreferredSize();
			d.setSize(d.getWidth(), d.getHeight() + 300);
			container.setSize(d);
			addResize = false; //Set to false to prevent from resizing on next load
		}
				
		this.subPanel = new JPanel(new BorderLayout());		
		this.panelRight.setSize(new Dimension());
		this.panelRight.add(subPanel, BorderLayout.SOUTH);
	}
	
	
}
