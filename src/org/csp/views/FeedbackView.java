package org.csp.views;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;

import org.csp.controller.SettingController;
import org.csp.main.ServerCommunication;

public class FeedbackView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2034409340666254103L;
	private JPanel contentPane;
	private JTextField txtTitle;
	private JLabel lblTitle;
	private JLabel lblType;
	private JComboBox<String> cmbType;
	private JLabel lblComments;
	private JTextArea txtComments;
	private JLabel lblEmail;
	private JTextField txtEmail;
	private JPanel panel;
	private JButton btnSend;
	private JButton btnCancel;

	/**
	 * Create the frame.
	 */
	public FeedbackView() {
//		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout());
		
		JPanel contentPanel = new JPanel();
		contentPane.add(contentPanel);
		contentPanel.setLayout(new GridLayout(9,2));
		
		lblTitle = new JLabel("Title");
		contentPanel.add(lblTitle);
		
		txtTitle = new JTextField();
		contentPanel.add(txtTitle);
		txtTitle.setColumns(10);
		
		lblType = new JLabel("Feedback Type");
		contentPanel.add(lblType);
		
		
		String [] option = {"Error","General", "Improvement", "Suggestion"};
		cmbType = new JComboBox<String>(option);
		contentPanel.add(cmbType);
		
		lblComments = new JLabel("Comments");
		contentPanel.add(lblComments);
		
		txtComments = new JTextArea();
		txtComments.setRows(100);
		contentPanel.add(txtComments);
		
		lblEmail = new JLabel("Email For Response(Optional)");
		contentPanel.add(lblEmail);
		
		txtEmail = new JTextField();
		contentPanel.add(txtEmail);
		txtEmail.setColumns(10);
		
		panel = new JPanel();
		contentPanel.add(panel);
		
		btnSend = new JButton("Send");
		btnSend.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				sendFeedback(txtTitle.getText(), (String)cmbType.getSelectedItem(), txtComments.getText() , txtEmail.getText());
			}
		});
		panel.add(btnSend);
		
		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		panel.add(btnCancel);
	}
	
	public void sendFeedback(final String title, final String type, final String comments,final String email){
		if (title == null || title.length() < 3)
			JOptionPane.showMessageDialog(this, "Must Enter a title");
		else if (comments == null || comments.length() < 2)
			JOptionPane.showMessageDialog(this, "Must Enter a Comment");
		else {
			SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
				protected Void doInBackground() throws Exception {
					StringBuilder stb = new StringBuilder();
						stb.append(title).append(",")
							.append(type).append(",")
							.append(comments);
					if (email == null)
						stb.append(",").append(email);
													
					(new ServerCommunication()).sendToServer(stb.toString(), SettingController.SVR_FEED_MSG);
					
					return null;
				}
				public void done() {
					JOptionPane.showMessageDialog(null, "Feedback Successfully Sent");
				}
			};
			worker.execute();
		}
	}
}
