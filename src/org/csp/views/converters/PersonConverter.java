package org.csp.views.converters;

import java.util.Collection;

import org.csp.controller.PersonController;
import org.csp.controller.SettingController;
import org.csp.models.Person;
import org.jdesktop.beansbinding.Converter;

public class PersonConverter extends Converter<Person, String> {


	@Override
	public String convertForward(Person p) {
		if (p != null)return p.getFirstName() + " " + p.getLastName();
		return null;
	}

	@Override
	public Person convertReverse(String s) {
		PersonController pc = PersonController.getInstance(SettingController.getInstance().getConnection());
		Collection <Person> c = pc.get(s);
		if (c != null && !c.isEmpty()){
			//Assuming that we will only get one first name last name combination
			return c.iterator().next();
		}
		return null;
	}

}
