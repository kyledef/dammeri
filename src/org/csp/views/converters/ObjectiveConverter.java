package org.csp.views.converters;

import org.csp.controller.ObjectiveController;
import org.csp.controller.SettingController;
import org.csp.models.Objective;
import org.jdesktop.beansbinding.Converter;

public class ObjectiveConverter extends  Converter<Objective, String> {

	@Override
	public String convertForward(Objective o) {
		return o.toString();
	}

	@Override
	public Objective convertReverse(String o) {
		return ObjectiveController.getInstance(SettingController.getInstance().getConnection()).get(o);
	}

}
