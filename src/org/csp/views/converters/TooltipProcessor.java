package org.csp.views.converters;

import java.util.Map;

import javax.swing.JComponent;

import org.metawidget.swing.SwingMetawidget;
import org.metawidget.widgetprocessor.iface.WidgetProcessor;


public class TooltipProcessor implements WidgetProcessor<JComponent, SwingMetawidget> {
	
	public JComponent processWidget( JComponent widget, String elementName ,Map<String, String> attributes, SwingMetawidget metawidget ) {		
		
		widget.setToolTipText( attributes.get( "tooltip" ) );
		return widget;
	}
	
}
