package org.csp.views.converters;

import org.csp.controller.ActivityController;
import org.csp.controller.SettingController;
import org.csp.models.Activity;
import org.jdesktop.beansbinding.Converter;

public class ActivityConverter extends Converter<Activity, String>  {

	@Override
	public String convertForward(Activity a) {
		return a.toString();
	}

	@Override
	public Activity convertReverse(String s) {
		return ActivityController.getInstance(SettingController.getInstance().getConnection()).get(s);
	}

}
