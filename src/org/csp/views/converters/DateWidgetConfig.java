package org.csp.views.converters;

import java.util.Date;

public class DateWidgetConfig {
	private Date mDate;
	
	public DateWidgetConfig setDate( Date date ) {
		mDate = date;
		return this;
	}

	public Date getDate() {
		return mDate;
	}
	
	
	
	@Override
	public boolean equals(Object obj){
		DateWidgetConfig config = (DateWidgetConfig)obj;
		if (this.mDate != null && config.mDate != null)return this.mDate.equals(config.mDate);		
		return false;
	}
	
	@Override
	public int hashCode(){
		if (this.mDate != null)
			return this.mDate.hashCode();
		else
			return 11;
	}
	
}
