package org.csp.views.converters;

import org.csp.controller.MemberController;
import org.csp.controller.SettingController;
import org.csp.models.Member;
import org.jdesktop.beansbinding.Converter;

public class MemberConverter extends Converter<Member, String> {

	@Override
	public String convertForward(Member p) {;
		return p.toString();
	}

	@Override
	public Member convertReverse(String s) {
		MemberController pc = MemberController.getInstance(SettingController.getInstance().getConnection());
		return pc.get(s);
	}
}
