package org.csp.views.converters;

import java.util.Date;
import java.util.Map;

import javax.swing.JComponent;

import org.metawidget.swing.SwingMetawidget;
import org.metawidget.widgetbuilder.iface.WidgetBuilder;

import com.toedter.calendar.JDateChooser;

public class DateWidgetBuilder implements  WidgetBuilder<JComponent, SwingMetawidget>  {

	private Date date;
	
	public DateWidgetBuilder(DateWidgetConfig config){
		this.date = config.getDate();
	}
	
	public DateWidgetBuilder(){
		
	}
	
	
	@Override
	public JComponent buildWidget(String elementName, Map<String, String> attributes,
			SwingMetawidget metawidget ) {
		
		if (attributes != null){
			String type = attributes.get("type");
			if (type != null){
				if (type.compareToIgnoreCase("java.util.Date") != 0)return null;
				JComponent calendar;
				if (date != null)
					calendar = new JDateChooser(this.date);
				else 
					calendar = new JDateChooser();
				
				//TODO determine how the value is saved and retrieved to the calendar object
				return calendar;
			}
		}
		return null;
	}


}
