package org.csp.views.converters;

import org.csp.controller.QuestionController;
import org.csp.controller.SettingController;
import org.csp.models.Question;
import org.jdesktop.beansbinding.Converter;


public class QuestionConverter extends Converter<Question, String> {

	@Override
	public String convertForward(Question q) { return q.toString(); }

	@Override
	public Question convertReverse(String s) {
		return QuestionController.getInstance(SettingController.getInstance().getConnection()).get(s);
	}

}
