package org.csp.views.converters;

import org.csp.controller.IndicatorController;
import org.csp.controller.SettingController;
import org.csp.models.Indicator;
import org.jdesktop.beansbinding.Converter;

public class IndicatorConverter extends Converter<Indicator, String>{

	@Override
	public String convertForward(Indicator i) {
		return i.toString();
	}

	@Override
	public Indicator convertReverse(String s) {		
		return IndicatorController.getInstance(SettingController.getInstance().getConnection()).get(s);
	}

}
