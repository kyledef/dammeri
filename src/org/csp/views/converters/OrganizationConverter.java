package org.csp.views.converters;

import org.csp.controller.OrganizationController;
import org.csp.controller.SettingController;
import org.csp.models.Organization;
import org.jdesktop.beansbinding.Converter;

public class OrganizationConverter extends Converter<Organization, String>{

	public String convertForward(Organization o) { return o.toString(); }
	public Organization convertReverse(String name) {
		OrganizationController oc = OrganizationController.getInstance(SettingController.getInstance().getConnection());
		try {	return oc.get(name); }
		catch (Exception e) { e.printStackTrace(); SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG); }
		return null;
	}
}
