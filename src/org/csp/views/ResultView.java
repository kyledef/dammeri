package org.csp.views;

import java.awt.Container;

import org.csp.models.Field;
import org.csp.models.Indicator;
import org.csp.models.Question;

public class ResultView extends ModelView<Field> {

	
	public ResultView(Container container, boolean useLeftPanel,
			boolean readOnly, Field model, boolean useButtons,
			boolean displayResults) {
		super(container, useLeftPanel, readOnly, model, useButtons, displayResults);
	}

	public ResultView(Container container, boolean useLeftPanel,
			boolean readOnly, Field model, boolean useButtons) {
		super(container, useLeftPanel, readOnly, model, useButtons);
	}

	public ResultView(Container container, boolean useLeftPanel,
			boolean readOnly, Field model) {
		super(container, useLeftPanel, readOnly, model);
	}

	public ResultView(Container container, boolean useLeftPanel,
			boolean readOnly) {
		super(container, useLeftPanel, readOnly);
	}

	public ResultView(Container container, boolean useLeftPanel) {
		super(container, useLeftPanel);
	}

	public ResultView(Container container) {
		super(container);
	}

	@Override
	protected void configureModel() {
		this.model = new Field();
		this.model.setIndicator(new Indicator(new Question()));
		
	}

}
