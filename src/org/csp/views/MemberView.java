package org.csp.views;

import java.awt.Container;

import org.csp.controller.MemberController;
import org.csp.controller.SettingController;
import org.csp.models.Member;
import org.csp.models.Organization;
import org.csp.models.Person;

public class MemberView extends ModelView<Member> {

	public MemberView(Container container) {
		super(container);
	}

	

	public MemberView(Container container, boolean useLeftPanel,
			boolean readOnly, Member model, boolean useButtons) {
		super(container, useLeftPanel, readOnly, model, useButtons);
	}



	public MemberView(Container container, boolean useLeftPanel,
			boolean readOnly, Member model) {
		super(container, useLeftPanel, readOnly, model);
	}



	public MemberView(Container container, boolean useLeftPanel,
			boolean readOnly) {
		super(container, useLeftPanel, readOnly);
	}



	public MemberView(Container container, boolean useLeftPanel) {
		super(container, useLeftPanel);
	}

	

	public MemberView(Container container, boolean useLeftPanel,
			boolean readOnly, Member model, boolean useButtons,
			boolean displayResults) {
		super(container, useLeftPanel, readOnly, model, useButtons, displayResults);
	}



	@Override
	protected void configureModel() {
		Member m;
		if (this.model == null) m = new Member();
		else m = this.model;
		m.setPerson(new Person());
		if (!this.readOnly){
			m.setOrganization(new Organization());//TODO Set the default organization to the local organization
			m.setRole("MEMBER");
		}
		this.model = m;
		this.controller = MemberController.getInstance(SettingController.getInstance().getConnection());
	}
	
	public void addMember(Member m){
		this.model = m;
		this.mMetawidget.setToInspect(this.model);
	}
}
