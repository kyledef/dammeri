package org.csp.views;

import java.awt.Container;

import javax.swing.JPasswordField;

import org.csp.controller.SettingController;
import org.csp.controller.UserController;
import org.csp.models.User;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.metawidget.inspector.annotation.MetawidgetAnnotationInspector;
import org.metawidget.inspector.composite.CompositeInspector;
import org.metawidget.inspector.composite.CompositeInspectorConfig;
import org.metawidget.inspector.propertytype.PropertyTypeInspector;
import org.metawidget.inspector.xml.XmlInspector;
import org.metawidget.inspector.xml.XmlInspectorConfig;
import org.metawidget.swing.SwingMetawidget;
import org.metawidget.swing.widgetprocessor.binding.beansbinding.BeansBindingProcessor;
import org.metawidget.swing.widgetprocessor.binding.beansbinding.BeansBindingProcessorConfig;

public class UserView extends ModelView<User> {

	protected JPasswordField password;
	
	public UserView(Container container) {
		super(container);
	}

	public UserView(Container container, boolean readOnly) {
		super(container, true, readOnly);
	}
	

	public UserView(Container container, boolean useLeftPanel,
			boolean readOnly, User model, boolean useButtons) {
		super(container, useLeftPanel, readOnly, model, useButtons);
	}

	public UserView(Container container, boolean useLeftPanel,
			boolean readOnly, User model) {
		super(container, useLeftPanel, readOnly, model);
	}

	public UserView(Container container, boolean useLeftPanel, boolean readOnly) {
		super(container, useLeftPanel, readOnly);
	}

	@Override
	protected void configureModel() {
		this.model = new User();
		this.controller = UserController.getInstance(SettingController.getInstance().getConnection());
	}
	
	@Override
	protected SwingMetawidget startInspection(){
		if (this.mMetawidget == null)mMetawidget = new SwingMetawidget();
		
		
		XmlInspectorConfig config = new XmlInspectorConfig();
		config.setInputStream( UserView.class.getResourceAsStream( "/org/csp/views/metawidget-metadata.xml" ));
		
		
		//Use Annotation inspector instead of xml inspector
		CompositeInspectorConfig inspectorConfig = new CompositeInspectorConfig().setInspectors(
				new XmlInspector( config ),
				new PropertyTypeInspector(),
				new MetawidgetAnnotationInspector() 
		);
		mMetawidget.setInspector( new CompositeInspector( inspectorConfig ) );
		
		mMetawidget.setToInspect( model );
		mMetawidget.addWidgetProcessor( new BeansBindingProcessor(new BeansBindingProcessorConfig().setUpdateStrategy( UpdateStrategy.READ_WRITE )) );
		mMetawidget.setReadOnly(this.readOnly);
		return mMetawidget;
	}
	
	protected void buildRightPanel(){		
		super.buildRightPanel();
		
		if (!this.readOnly){
			//Replace existing textField with Password field
			password = new JPasswordField(10);
			password.setName("password");
			mMetawidget.add(password);
		}
	}
}
