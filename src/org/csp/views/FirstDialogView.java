package org.csp.views;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.border.EmptyBorder;

import org.csp.controller.OrganizationController;
import org.csp.controller.SettingController;
import org.csp.controller.UserController;
import org.csp.models.Organization;
import org.csp.models.User;
import org.csp.utilities.Utilities;

public class FirstDialogView extends JFrame {

	private static final long serialVersionUID = 6697792535617687979L;
	private JPanel contentPane;
	private JTextField txtOrganization;
	private JTextField txtPassword;
	private JPanel cPanel;

	private int btnCount;
	private JButton btnPrevious;
	private JButton btnNext;
	

	/**
	 * Create the frame.
	 */
	public FirstDialogView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		this.btnCount = 0;
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel btnPanel = new JPanel();
		contentPane.add(btnPanel, BorderLayout.SOUTH);
		
		btnPrevious = new JButton("< Previous");
		btnPrevious.setEnabled(false);
		btnPrevious.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnCount--;
				if (btnCount == 0)btnPrevious.setEnabled(false);
				if (btnCount < 1)btnNext.setText("Next >");
				
				((CardLayout) cPanel.getLayout()).show(cPanel, Integer.toString(btnCount));
			}
		});
		btnPanel.add(btnPrevious);
		
		btnNext = new JButton("Next >");
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnCount++;
				if (btnCount > 0)btnPrevious.setEnabled(true);
				if (btnCount == 1)btnNext.setText("Finish");
				if (btnCount > 1){
					SettingController.getInstance().setFirstDialog(false);
					if (save())
						close();
				}else
					((CardLayout) cPanel.getLayout()).show(cPanel, Integer.toString(btnCount));
			}
		});
		btnPanel.add(btnNext);
		
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(contentPane, "Changes made will not be save. Cancel?","Cancel Operation", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					close();
			}
		});
		btnPanel.add(btnCancel);
		
		JPanel insidePanel = new JPanel();
		contentPane.add(insidePanel, BorderLayout.CENTER);
		insidePanel.setLayout(new BorderLayout(0, 0));
		
		JPanel topPanel = new JPanel();
		insidePanel.add(topPanel, BorderLayout.NORTH);
		topPanel.setLayout(new BorderLayout(0, 0));
		
		JLabel lblIcon = new JLabel("Icon");
		URL url = Thread.currentThread().getContextClassLoader().getResource( "res/simmeda_icon_small.png" );
		if (url != null){
			ImageIcon iconImg = new ImageIcon( url );
			lblIcon = new JLabel( iconImg );
		}
		topPanel.add(lblIcon, BorderLayout.WEST);
		
		JLabel lblDammeriInitialConfiguration = new JLabel("Dammeri Initial Configuration");
		topPanel.add(lblDammeriInitialConfiguration, BorderLayout.CENTER);
		
		cPanel = new JPanel(new CardLayout());
		insidePanel.add(cPanel, BorderLayout.CENTER);
		
		
		JPanel passwordChangePanel = new JPanel();
		cPanel.add(passwordChangePanel, "0");
		SpringLayout sl_passwordChangePanel = new SpringLayout();
		passwordChangePanel.setLayout(sl_passwordChangePanel);
		
		JLabel lblChangeAdministratorPassword = new JLabel("Change Administrator Password");
		sl_passwordChangePanel.putConstraint(SpringLayout.NORTH, lblChangeAdministratorPassword, 46, SpringLayout.NORTH, passwordChangePanel);
		sl_passwordChangePanel.putConstraint(SpringLayout.WEST, lblChangeAdministratorPassword, 10, SpringLayout.WEST, passwordChangePanel);
		passwordChangePanel.add(lblChangeAdministratorPassword);
		
		txtPassword = new JPasswordField();
		sl_passwordChangePanel.putConstraint(SpringLayout.NORTH, txtPassword, 46, SpringLayout.NORTH, passwordChangePanel);
		sl_passwordChangePanel.putConstraint(SpringLayout.WEST, txtPassword, 210, SpringLayout.WEST, passwordChangePanel);
		sl_passwordChangePanel.putConstraint(SpringLayout.EAST, txtPassword, -10, SpringLayout.EAST, passwordChangePanel);
		passwordChangePanel.add(txtPassword);
		txtPassword.setColumns(10);
		
		
		JPanel firstContentPanel = new JPanel();
		cPanel.add(firstContentPanel,"1");
		SpringLayout sl_contentPanel = new SpringLayout();
		firstContentPanel.setLayout(sl_contentPanel);
		
		txtOrganization = new JTextField();
		sl_contentPanel.putConstraint(SpringLayout.NORTH, txtOrganization, 32, SpringLayout.NORTH, firstContentPanel);
		sl_contentPanel.putConstraint(SpringLayout.WEST, txtOrganization, 189, SpringLayout.WEST, firstContentPanel);
		sl_contentPanel.putConstraint(SpringLayout.EAST, txtOrganization, -10, SpringLayout.EAST, firstContentPanel);
		firstContentPanel.add(txtOrganization);
		txtOrganization.setColumns(10);
		
		JLabel lblEnterOrganizationName = new JLabel("Enter Organization Name:");
		sl_contentPanel.putConstraint(SpringLayout.NORTH, lblEnterOrganizationName, 35, SpringLayout.NORTH, firstContentPanel);
		sl_contentPanel.putConstraint(SpringLayout.WEST, lblEnterOrganizationName, 10, SpringLayout.WEST, firstContentPanel);
		sl_contentPanel.putConstraint(SpringLayout.EAST, lblEnterOrganizationName, -23, SpringLayout.WEST, txtOrganization);
		firstContentPanel.add(lblEnterOrganizationName);
		
		
		
		
		
		

	}
	
	public boolean save(){
		boolean orgGood = true;
		boolean passGood= true;
		if (this.txtOrganization != null && !this.txtOrganization.getText().equals("")){
			String orgText = this.txtOrganization.getText();
			if (orgText.length() < 3){
				JOptionPane.showMessageDialog(this, "Organization Name should be longer");
				orgGood = false;
			}else{
				//TODO Save organization
				try{
					OrganizationController oc =OrganizationController.getInstance(SettingController.getInstance().getConnection());
					Organization o = oc.read(1);
					if (o!=null){
						o.setName(orgText);
						oc.update(o);
						orgGood = true;
					}
				}catch(Exception e){
					orgGood = false;
					JOptionPane.showMessageDialog(this, "Unable to update organization");
					e.printStackTrace(); 
					SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
				}
			}
		}else{
			JOptionPane.showMessageDialog(this, "Must enter a name for your organization");
			orgGood = false;
		}
		
		if (!orgGood){
			((CardLayout) cPanel.getLayout()).show(cPanel, "1");
		}else{
			//Check password
			if (this.txtPassword != null && this.txtPassword.equals("") == false){
				String password = this.txtPassword.getText();
				if (password.length() < 6){
					JOptionPane.showMessageDialog(this, "Password must be at least 6 characters long");
					passGood = false;
				}else{
					try{
						//TODO Save password
						UserController uc =UserController.getInstance(SettingController.getInstance().getConnection());
						User u =uc.read(1);
						u.setPassword(Utilities.encrypt(password));
						uc.update(u);
						passGood = true;
					}catch(Exception e){
						passGood = false;
						JOptionPane.showMessageDialog(this, "Unable to update administrator password");
						e.printStackTrace(); 
						SettingController.getInstance().sendToServer(e.getLocalizedMessage() +","+this.getClass().toString() , SettingController.SVR_DBG_MSG);
					}
				}
			}else{
				JOptionPane.showMessageDialog(this, "Must specify a new password for administrator");
				passGood = false;
			}
			if (!passGood){
				((CardLayout) cPanel.getLayout()).show(cPanel, "0");
			}
		}
		
		return orgGood && passGood;
	}
	
	public void close(){
		this.setVisible(false);
		this.dispose();
	}
}
